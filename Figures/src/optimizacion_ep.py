op = dbo.get('op')
db = dbo.get('ep')
OP = op.loc[op.IM.idxmin()]
años = int(len(db.index) / Ndl)
ymax = db.max().max()
x = db.index
g = Figura(**{
    'destino': BASE,
    'parametros': {
        'legend style': {
            'at': '{(0.50,0.96)}',
            'anchor': 'north',
            'legend columns': '3',
        },
    },
    'conv': (
        '{}.0f'.format(len(str(x.max()))),
        '{}.0f'.format(len(str(ymax))),
    ),
})
g.eje(**{
    'tipo': 'axis',
    'parametros': {
        'xlabel': '{Año}',
        'xmin': 1,
        'xmax': 365 * años,
        'xtick': tick(i * 365 for i in range(años + 1)),
        'xticklabels': tick(range(1, años + 1)),
        'x tick label as interval': None,
        'ylabel': '{Energía, \si{\MJ}}',
        'ymax': round(ymax * 1.1, -2),
        'ymin': 0,
        'reverse legend': None,
    },
})
g.serie(**{
    'x': x,
    'y': db.ER,
    'leyenda': 'Residual (Venta)',
    'parametros': {'lightgray': None},
})
g.serie(**{
    'x': x,
    'y': db.ES,
    'leyenda': 'Stock ({} días)'.format(OP.D),
    'parametros': {'gray': None, 'line width': '0.8pt'},
})
g.serie(**{
    'x': x,
    'y': db.EE,
    'leyenda': 'Producción',
    'parametros': {'line width': '1.2pt'},
})
node = r'\node[gray, right, font=\tiny] at (axis cs:{0},{1:5.0f}) {{{2}}};'
draw = r'\draw[gray, dashed] (axis cs:0,{1:5.0f}) -- (axis cs:{0},{1:5.0f}) node[right, font=\tiny] {{{2}}};'
xx = x.max()
yy = db.EE.median()
objs = [node.format(xx, yy * i, i) for i in range(2)]
objs.extend([draw.format(xx, yy * i, i) for i in range(2, OP.D+1)])
for obj in objs:
    g.objeto(obj, inicio=True)
g.tex()
