g = Figura(**{
    'destino': BASE,
    'parametros': {
        'legend style': {
            'at': '{(0.03,0.96)}',
            'anchor': 'north west',
        },
    },
    'conv': ('.0f', '.0f'),
})
g.eje(**{
    'tipo': 'axis',
    'parametros': {
        'xlabel': '{Fecha, año}',
        'ylabel': '{Consumo aparente nacional, \si{\kilogram\per\year}}',
    },
})
años = Mercado.extraer("Resumen.A20:A34")
consumo = Mercado.extraer("Resumen.J20:J34")
func, ajustes = recta(años, consumo)
printpns(ajustes['p_value'], ajustes['r_squared'])
g.serie(**{
    'x': años,
    'y': consumo,
    'leyenda': 'Consumo de \ch{O2}',
    'parametros': {'mark': '*'},
})
line = r'\draw[densely dotted] (axis cs:{},{}) -- (axis cs:{},{});'
g.objeto(line.format(años[0], func(años[0]), años[-1], func(años[-1])), inicio=True)
g.tex()
