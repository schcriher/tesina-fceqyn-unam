from math import ceil
from numpy import mean, std
xmin = 1984  # \  31 años
xmax = 2014  # / completos
rad = dbd.get('nasa/R')[str(xmin):str(xmax)]    # Datos reales (31 años completos)
hsp = dbd.get('hottel')                         # horas solares pico (kWh/(m²·día))
años = list(range(xmin, xmax + 1))
rads = list(rad[str(año)].sum() for año in años)
ymax = 2000
ymin = 0
unit = r'\kWh\per\m\squared'
m1 = mean(rads)
m2 = hsp.sum()
m3 = mean([m1, m2])
m4 = std(rads)
g = Figura(**{
    'destino': BASE,
    'conv': ('4.0f', '>5.2f'),
})
g.eje(**{
    'tipo': 'axis',
    'parametros': {
        'xlabel': '{Fecha, año}',
        'ylabel': '{{Energía, \si{{{}}}}}'.format(unit),
        'xmin': xmin,
        'xmax': xmax,
        'xtick': tick(range(xmin, xmax+1, 5)),
        'ymin': ymin,
        'ymax': ymax,
    },
})
g.serie(**{
    'x': años,
    'y': rads,
    'parametros': {'mark': '*', 'mark options': '{scale=0.5}'},
})
info = r'\draw[gray, densely dotted] (axis cs:{:4.1f},{:5.1f}) -- (axis cs:{:4.1f},{:5.1f}) node[right, font=\tiny]{{{}}};'.format
g.objeto(info(xmin, m3,    xmax, m3,    '$µ\\vphantom{+-σ}$'))
g.objeto(info(xmin, m3-m4, xmax, m3-m4, '$µ-σ$'))
g.objeto(info(xmin, m3+m4, xmax, m3+m4, '$µ+σ$'))
info = r"\draw (axis cs:{},{}) node[right, font=\scriptsize] {{{}}};".format
g.objeto(info(1986.915, 900, 'Energía total anual promedio (NASA): ~ \SI{{{:.0f}}}{{{}}}'.format(m1, unit)))
g.objeto(info(1986.915, 600, 'Energía total anual del modelo (Hottel): \SI{{{:.0f}}}{{{}}}'.format(m2, unit)))
g.tex()
