SAVE(FTEX, HEADER + r"""{
\newif\ifLotText\LotTexttrue

\newcommand*{\ListA}{131.35,163.02,194.68,226.34,258.01}
\newcommand*{\ArrayA}{{115.515,147.185,178.850,210.510,242.175,273.845}}

\newcommand*{\calc}[2]{\pgfmathparse{#2}\let#1\pgfmathresult}

\newcommand*{\tottext}[2]{\scalebox{0.90}{#1$\:\!$#2}}

\newcommand*{\BlockD}{% Manzana D - 0081
    \foreach \i in {5,...,10} {
        \calc \y {99.68 + 40 * \i}
        \draw[lot] (0,\y) -- (63.59,\y);
    }
    \draw[block] (0,0) -- (0,604.23) -- (63.59,604.23) -- (63.59,79.68);
    \ifLotText
        \foreach \lot in {26,...,30} {
            \calc \y {119.68 + 40 * (\lot - 21)}
            \node[tex] at (31.795,\y) {\tottext{D}{\lot}};
        }
        \node[tex] at (31.795,551.955) {\tottext{D}{31}};
    \fi
}

\newcommand*{\BlockL}{% Manzana L - 0089
    \foreach \i in {0,...,6} {
        \calc \x {112.59 + 38 * \i}
        \draw[lot] (\x,519.68) -- (\x,604.23);
    }
    \draw[block] (398.59,519.68) -- (73.59,519.68) -- (73.59,604.23) -- (459.42,604.23);
    \ifLotText
        \node[tex] at (93.09,561.955) {\tottext{L}{1}};
        \foreach \lot in {2,...,7} {
            \calc \x {131.59 + 38 * (\lot - 2)}
            \node[tex] at (\x,561.955) {\tottext{L}{\lot}};
        }
    \fi
}

\newcommand*{\BlockM}{% Manzana M - 0090
    \foreach \i in {0,...,5} {
        \calc \x {123.59 + 43 * \i}
        \draw[lot] (\x,309.68) -- (\x,499.68);
    }
    \draw[lot] (123.59,404.68) -- (338.59,404.68);
    \foreach \x in {83.59,338.59} {
        \foreach \y in \ListA {
            \draw[lot] (\x,\y+210) -- (\x+40,\y+210);
        }
    }
    \draw[block] (83.59,309.68) -- (83.59,499.68) -- (378.59,499.68) -- (378.59,309.68) -- cycle;
    \ifLotText
        \node[tex] at (103.59,483.845) {\tottext{M}{1}};
        \foreach \lot in {2,...,6} {
            \calc \x {145.09 + 43 * (\lot - 2)}
            \node[tex] at (\x,452.18) {\tottext{M}{\lot}};
        }
        \foreach \lot in {13,...,17} {
            \calc \x {145.09 + 43 * (17 - \lot)}
            \node[tex] at (\x,357.18) {\tottext{M}{\lot}};
        }
        \foreach \lot in {18,...,22} {
            \node[tex] at (103.59,\ArrayA[\lot-18]+210) {\tottext{M}{\lot}};
        }
    \fi
}

\tikzset{%
    diagram/.style = {gray, line width=1.5},
    block/.style = {black, line width=0.5},
    lot/.style = {gray, line width=0.25},
    tex/.style = {black},
    %
    zone/.style = {fill=gray!50},
}

\newcommand*{\RefX}{380}
\newcommand*{\RefY}{380}
\newcommand*{\Ref}[5]{%(1)blockOffset, (2)zoneYOffset, (3)x, (4)y, (5)text
    \path[zone] (\RefX,\RefY+#1+#2) rectangle (\RefX+#3,\RefY+#1+#2+#4);
    \draw[block] (\RefX,\RefY+#1) rectangle (\RefX+30,\RefY+#1+30);
    \node at (\RefX+15,\RefY+#1-7) {\scriptsize #5};
}

\begin{tikzpicture}[x=1mm,y=1mm,scale=0.30]

\node at (\RefX+15,\RefY+180) {Referencias:};
\node at (\RefX+15,\RefY+158) {\scriptsize Edificación};
\Ref{110}{20}{30}{10}{Arriba}
\Ref{55}{0}{10}{30}{Izquierda}
\Ref{0}{0}{30}{10}{Abajo}

\clip (-11,295) rectangle (345,615);

\newcommand*{\wide}{15}
% ┌────────
% │2      3
% │   ┌────
% │   │   5
% │1  │4   
% │   │   6
\path[zone] (  0,   299.68)       rectangle (      +\wide,499.68);       % 1
\path[zone] (  0,   604.23-\wide) rectangle ( 63.59,      604.23);       % 2
\path[zone] ( 73.59,604.23-\wide) rectangle (340.59,      604.23);       % 3
\path[zone] ( 83.59,309.68)       rectangle ( 83.59+\wide,499.68);       % 4
\path[zone] (123.59,499.68-\wide) rectangle (338.59,      499.68);       % 5
\path[zone] (123.59,309.68)       rectangle (338.59,      309.68+\wide); % 6

\BlockD
\BlockL
\BlockM

\coordinate (A) at (  -9.800, 614.230);
\coordinate (B) at ( 469.670, 614.320);
\coordinate (C) at ( 469.420, 431.500);
\coordinate (D) at ( 812.580, 431.470);
\coordinate (P) at ( -10,     -10    );

\draw[diagram] (P) -- (A) -- (B);

\end{tikzpicture}
}""")
