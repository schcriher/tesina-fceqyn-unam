hsp = dbd.get('hottel')
rad = dbd.get('nasa/Rdm')
hsp_mean = hsp.mean()
rad_mean = rad.mean()
m = (hsp_mean + rad_mean) / 2
g = Figura(**{
    'destino': BASE,
    'parametros': {
        'legend style': {
            'at': '{(0.50,0.04)}',
            'anchor': 'south',
            'legend columns': '2',
        },
    },
    'conv': ('3.0f', '5.3f'),
})
ymin = 1.6
ymax = 8.3
g.eje(**{
    'tipo': 'axis',
    'parametros': {
        'xlabel': r'{Mes del año}',
        'xmin': 1,
        'xmax': 365,
        'xtick': tick_dayofyear_12,
        'xticklabels': ticklabels_month,
        'x tick label as interval': True,
        'ylabel': '{Energía, \si{\kWh\per\m\squared\per\day}}',
        'ymin': ymin,
        'ymax': ymax,
        'ytick': tick(range(2, 10)),
    },
})
g.serie(**{
    'x': hsp.index,
    'y': hsp,
    'leyenda': 'Modelo de Hottel ($\mu\!=\!{:.4f}$)'.format(hsp_mean),
    'parametros': {'line width': '1.5pt', 'smooth': True},
})
g.serie(**{
    'x': hsp.index,
    'y': rad,
    'leyenda': 'Datos promedios ($\mu\!=\!{:.4f}$)'.format(rad_mean),
})
s = Sol()
refs = (
    (s.neo+1, r'Equinoccio\\de otoño'),
    (s.nsi+1, r'Solsticio\\de invierno'),
    (s.nep+1, r'Equinoccio\\de primavera'),
    (s.nsv+1, r'Solsticio\\de verano'),
)
info = r'\draw[gray, densely dashed] (axis cs:{x},2) -- (axis cs:{x},{y}) node[above, fill=white, font=\tiny] {t};'
for n, t in refs:
    g.objeto(info.format(
        t = '{{\TABULAR{{{}}}}}'.format(t),
        x = '{:3.0f}'.format(n),
        y = ymax - 0.5,
    ), inicio=True)
mean = r'\draw[lightgray, densely dotted] (axis cs:365,{0:.3f}) -- (axis cs:1,{0:.3f}) node[left, fill=white, font=\tiny] {{$\mu$}};'
g.objeto(mean.format(m), inicio=True)
print('   Promedios:\n    Hottel: {:.6f}\n    NASA:   {:.6f}'.format(hsp_mean, rad_mean))
g.tex()
