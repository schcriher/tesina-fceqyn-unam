from numpy import arange
rad = dbd.get('nasa/R')['1984':'2014']  # Datos reales (31 años completos)
pre = dbd.get('inta/P')                 # Precipitación, mm
data = (
    ('rad', rad, 0.69, 28, 80, '\HSP'),
    ('pre', pre, 0.22, 55, 17, '\mm'),
)
frac = arange(0, 1.0, 0.01)
x = [i * 100 for i in frac]
for name, db, xx, mx, my, unit in data:
    y = [max_number_of_days_in_low(db, f) for f in frac]
    x_opt = xx * 100
    y_opt = max_number_of_days_in_low(db, xx)
    g = Figura(**{
        'destino': '{}_{}'.format(BASE, name),
        'conv': ('0.2f', '3.0f'),
    })
    g.eje(**{
        'tipo': 'axis',
        'parametros': {
            'xlabel': '{Porcentaje para límite inferior, \si{\percent}}',
            'ylabel': '{Número de días consecutivos}',
        },
    })
    g.serie(**{
        'x': x,
        'y': y,
    })
    info = r'\draw[gray, densely dotted] (axis cs:{x1:4.1f},{y1:5.1f}) -- (axis cs:{x2:4.1f},{y2:5.1f}) {t};'
    g.objeto(info.format(**{
        'x1': x_opt,
        'y1': max(y),
        'x2': x_opt,
        'y2': 0,
        't': r'node[below, yshift=2, font=\tiny] {{{:.0f}}}'.format(x_opt).replace('.', ','),
    }), inicio=True)
    g.objeto(info.format(**{
        'x1': 100,
        'y1': y_opt,
        'x2': 0,
        'y2': y_opt,
        't': r'node[left, xshift=2, font=\tiny, fill=gray!20!white, rounded corners=4pt] {{{:.0f}}}'.format(y_opt).replace('.', ','),
    }), inicio=True)
    mean = r'\draw (axis cs:{},{}) node[font=\tiny] {{Valor medio \SI{{{:.2f}}}{{{}}}}};'
    g.objeto(mean.format(mx, my, db.mean(), unit))
    g.tex()
