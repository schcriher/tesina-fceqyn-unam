stock_obj = dbf.get_storer('agua').attrs.stock_obj  # 19334869 g ≈ 19.335 m³
pmax_obj = dbf.get_storer('agua').attrs.pmax_obj    # 7.368 mm
area_obj = dbf.get_storer('agua').attrs.area_obj    # 712 m²
db = dbf.get('agua')
Alote = 6648.33                                     # Manzana D, lote 31 del PIP, m²
x = pmax_obj                                        #  7.37 mm
y = area_obj * 100 / Alote                          # 10.71 %
g = Figura(**{
    'destino': BASE,
    'conv': ('5.1f', '5.1f'),
})
g.eje(**{
    'tipo': 'axis',
    'parametros': {
        'xlabel': '{Precipitación máxima, \si{\milli\meter\per\day}}',
        'xmin': 0,
        'xmax': 30,
        'ylabel': '{Uso del terreno para captación, \si{\percent}}',
        'ymin': 0,
        'ymax': 60,
    },
})
g.serie(**{
    'x': db.index,
    'y': db.values * (100 / Alote),
})
info = r'\draw[gray, densely dotted] (axis cs:{x1:4.1f},{y1:5.1f}) -- (axis cs:{x2:4.1f},{y2:5.1f}) {t};'
g.objeto(info.format(**{
    'x1': x,
    'y1': 60,
    'x2': x,
    'y2': 0,
    't': r'node[below, yshift=2, font=\tiny] {{{:.1f}}}'.format(x).replace('.', ','),
}), inicio=True)
g.objeto(info.format(**{
    'x1': x,
    'y1': y,
    'x2': 2,
    'y2': y,
    't': r'node[left,  xshift=2, font=\tiny] {{{:.2f}}}'.format(y).replace('.', ','),
}), inicio=True)
stock = r"\draw (axis cs:{x:4.1f},{y:5.1f}) node[font=\tiny] {{$Stock=\SI{{{s:.2f}}}{{\meter\cubed}}$}};"
g.objeto(stock.format(**{
    'x': 20,
    'y': 40,
    's': stock_obj / da,
}), inicio=True)
g.tex()
