from math import sin, cos
from numpy import mean

from Library.TikZ import draw, node, getrectangle
from Library.Panel import π2, ε, sinε, cosε, Paneles

sinΕ = sin(π2 - ε)
cosΕ = cos(π2 - ε)

Id = 5
*_, Xlote, Ylote, Pinst, Ihsep, Ypsep = Lotes[Id]
Alote = Xlote * Ylote               # m²
Ainst = 0.137 * Alote               # m² (ylim=15.88)
Ysep = Ypsep / cosε
P = Paneles(Xlote, Ylote, Pinst, Ainst, Hinst, Xpanel, Ypanel, Dpanel, β)
z1, z2 = P.distribución(Ihsep, Ysep)
Ainst = z1.xlim * z1.ylim

print("   Ainst/Alote={:.4%}".format(Ainst/Alote))
print("   z1.xlim={}, z1.ylim={:.6f}".format(z1.xlim, z1.ylim))
print("   z2.xlim={}, z2.ylim={:.6f}".format(z2.xlim, z2.ylim))

xl = 0, Xlote
yl = 0, Ylote
XL, YL = getrectangle(xl, yl)       # Limite del lote

xc = Xlc, Xlote - Xlc
yc = Ylc, Ylote - Ylc
XC, YC = getrectangle(xc, yc)       # Limite construible

XFC = (Xlote - 2 * Xlc - z1.x1 - z1.x2 * z1.nx) / 2  # correción de centrado en X

def xypos(x, y, n):
    #          /\l2
    #         / /\ x,y
    #        /  \/
    # x2,y2 /   /
    #       \  /l1
    #        \/
    #      x1,y1
    l1 = P.Xp * n
    l2 = P.Yp * 1
    x1 =  x - l1 * sinΕ
    y1 =  y - l1 * cosΕ
    x2 = x1 - l2 * sinε
    y2 = y1 + l2 * cosε
    return (x1, x2), (y1, y2)

def paneles(x, y, ni, nf):
    #      2__
    #      /\ε|
    #     / /\|1
    #    /  \/|
    # 3 /   /Ε|
    #   \  /  |
    #    \/___|
    #    4
    # Margen
    codes = []
    (x1, x2), (y1, y2) = xypos(x, y, ni)
    (x4, x3), (y4, y3) = xypos(x, y, nf)
    codes.append(draw((x1, x2, x3, x4), (y1, y2, y3, y4), 'fill=lightgray', cycle=True))
    # Separadores
    for n in range(ni + 1, nf):
        codes.append(draw(*xypos(x, y, n)))
    return codes

def zona(z, yoffset):
    codes = []

    x = xc[1] - XFC
    y = yc[1] - yoffset - z.y1 + Ysep * len(z.nxa) #  - z.nx * P.Xp * sinΕ

    for r in z.nxa:
        codes.extend(paneles(x, y, z.nx-r, z.nx))
        y -= Ysep

    for r in range(z.ny):
        codes.extend(paneles(x, y, 0, z.nx))
        y -= Ysep

    for r in reversed(z.nxb):
        codes.extend(paneles(x, y, 0, r))
        y -= Ysep

    return codes

# --- #

CODES = ['\n\\begin{tikzpicture}[x=5pt,y=5pt]']
CODES.append(draw(XL, YL, 'line width=1', cycle=True))
CODES.append(draw(XC, YC, 'densely dashed', cycle=True))

conf = r'anchor=south west, font=\scriptsize, xshift=-3, yshift=-2'
CODES.append(node(xl[0], yl[1], 'Límite del lote',    conf))
CODES.append(node(xc[0], yc[1], 'Límite construible', conf))

CODES.extend(zona(z1, 0))
CODES.extend(zona(z2, z1.ylim + Ihsep))

x0 = xc[0]
y0 = yc[1]
y1 = y0 - z1.ylim
y2 = y1 - Ihsep
CODES.append(draw(xc, [y1, y1], 'gray, densely dotted'))
CODES.append(draw(xc, [y2, y2], 'gray, densely dotted'))
CODES.append(draw(mean(xc), [y1, y2], '<->'))
CODES.append(node(mean(xc), mean([y1, y2]), '$Ihsep=\SI{{{:.1f}}}{{\meter}}$'.format(Ihsep), r'right, font=\scriptsize'))
conf = r'anchor={}, font=\tiny, yshift={}'.format
CODES.append(node(xc[1], y1, '{} paneles'.format(z1.n), conf('north east',  1.6)))
CODES.append(node(xc[1], y2, '{} paneles'.format(z2.n), conf('south east', -1.9)))
info = 'Superficie \SI{{{:.1f}}}{{\meter\squared}} (\SI{{{:.1f}}}{{\percent}})'
CODES.append(node(xc[0], y1, info.format(Ainst, Ainst / Alote * 100), conf('north west',  1.6)))

CODES.append(r'\end{tikzpicture}')

SAVE(FTEX, HEADER + '\n'.join(CODES))
