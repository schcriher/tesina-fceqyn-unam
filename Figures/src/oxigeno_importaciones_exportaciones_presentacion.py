g = Figura(**{
    'destino': BASE,
    'parametros': {
        'legend style': {
            'at': '{(0.03,0.96)}',
            'anchor': 'north west',
        },
    },
})
g.eje(**{
    'tipo': 'axis',
    'parametros': {
        'xlabel': '{Fecha, año}',
        'ylabel': '{Masa, \si{\kilogram}}',
    },
})
años = Mercado.extraer("Resumen.A20:A34")
g.serie(**{
    'x': años,
    'y': Mercado.extraer("Resumen.B20:B34"),
    'leyenda': 'Importaciones',
    'parametros': {'mark': '*'},
})
g.serie(**{
    'x': años,
    'y': Mercado.extraer("Resumen.E20:E34"),
    'leyenda': 'Exportaciones',
    'parametros': {'mark': 'o'},
})
g.tex()
