g = Figura(**{
    'destino': BASE,
    'parametros': {
        'legend style': {
            'at': '{(0.50,0.04)}',
            'anchor': 'south',
            'legend columns': '4',
        },
    },
    'conv': ('>3.0f', '.3f'),
})
g.eje(**{
    'tipo': 'axis',
    'parametros': {
        'xlabel': r'{Mes del año}',
        'xmin': 1,
        'xmax': 365,
        'xtick': tick_dayofyear_12,
        'xticklabels': ticklabels_month,
        'x tick label as interval': True,
        'ylabel': r'{Energía, \si{\mega\joule\per\m\squared\per\day}}',
        'ymin': 1.2,
        'ymax': 2.8,
    },
})
g.set_default('serie', {'smooth':True})

E0a  = dba.get_storer('E0').attrs.B     #  0
E1a  = dba.get_storer('E1').attrs.B     # 27
E1sa = dba.get_storer('E1s').attrs.B    # 30
E2a  = dba.get_storer('E2').attrs.B     # 11-44

f = ffv * fin / 1e6  # eficiencias; J→MJ
E0   = dba.get('E0')  * f
E1   = dba.get('E1')  * f
E1s  = dba.get('E1s') * f
E2   = dba.get('E2')  * f

E0t  = sum(E0)
E1t  = sum(E1)
E1st = sum(E1s)
E2t  = sum(E2)

p10  = (E1t  - E0t) / E0t  * 100
p1s0 = (E1st - E0t) / E0t  * 100
p20  = (E2t  - E0t) / E0t  * 100
p21s = (E2t - E1st) / E1st * 100

cfg = (
    (E0,  '{}°'.format(E0a),      {'densely dashed': True},     ''),
    (E1,  '{}°'.format(E1a),      {'line width': '0.3pt'},      r' (+\SI{{{:.0f}}}{{\percent}})'.format(p10)),
    (E1s, '{}°'.format(E1sa),     {'line width': '1.5pt'},      r' (+\SI{{{:.0f}}}{{\percent}})'.format(p1s0)),
    (E2,  '{}--{}°'.format(*E2a), {'densely dashdotted': True}, r' (+\num{{{:.0f}}}/+\SI{{{:.0f}}}{{\percent}})'.format(p20, p21s)),
)
for y, l, p, percent in cfg:
    g.serie(**{
        'x': range(1, 366),
        'y': y,
        'leyenda': '$\\beta$={}{}'.format(l, percent),
        'parametros': p,
    })
g.tex()
