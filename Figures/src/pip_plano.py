g2r = lambda g,m,s: (s/3600 + m/60 + g) * math.pi/180

T = r'\coordinate ({}) at ({:8.3f},{:8.3f});'
K = 'FGHIJKLMN'

G = [(185,38,39), (158,38,50), (159,44,7), (257,29,45), (125,53,30), (204,56,52), (137,57,11), (178,56,45), None]
L = [88.07, 123.59, 117.99, 28.94, 45.87, 76.68, 32.22, 120.10, 30.21]

limite = []
franja = []

x = 812.883                                     # \
y = 1390.910                                    #  > E
r = g2r(66,1,19) + g2r(89,58,54) - math.pi / 2  # /

franja.append(T.format('EE', x, y - 15 / math.sin(r)))

for k, g, l in zip(K, G, L):
    x = x + l * math.sin(r)
    y = y - l * math.cos(r)
    limite.append(T.format(k, x, y))
    if g:
        _r = g2r(*g)
        r = r + _r - math.pi     # referido al eje Y
    else:
        _r = 2 * (math.pi - r)   # espejo sobre el eje Y
        r = _r / 2
    ll = 15 / math.cos((_r - math.pi) / 2)
    rr = _r / 2 - r
    xx = x - ll * math.sin(rr)
    yy = y - ll * math.cos(rr)
    franja.append(T.format(2*k, xx, yy))

limite = '\n'.join(limite)
franja = '\n'.join(franja)

SAVE(FTEX, HEADER + r"""{
\newif\ifLotText\LotTexttrue

\newcommand*{\tottext}[2]{\scalebox{0.34}{#1#2}}

\newcommand*{\ListA}{131.35,163.02,194.68,226.34,258.01}
\newcommand*{\ArrayA}{{115.515,147.185,178.850,210.510,242.175,273.845}}
\newcommand*{\ArrayB}{{113.55,112.50,90.23,72.46}}

\newcommand*{\calc}[2]{\pgfmathparse{#2}\let#1\pgfmathresult}

\newcommand*{\BlockA}{% Manzana A - 0078
    \foreach \i in {0,...,13} {
        \calc \y {639.68 + 30 * \i}
        \draw[lot] (1279.59,\y) -- (1331.35,\y);
    }
    \draw[block] (1279.59,1140.89) -- (1279.59,609.68) -- (1331.35,609.68) -- (1331.35,1070.07);
    \ifLotText
        \foreach \lot in {1,...,15} {
            \calc \y {1044.68 - 30 * (\lot - 1)}
            \node[tex] at (1305.47,\y) {\tottext{A}{\lot}};
        }
    \fi
}

\newcommand*{\BlockB}{% Manzana B - 0079
    \draw[lot] (1243.35,6.18) -- (1243.35,79.68);
    \foreach \i in {0,...,16} {
        \calc \y {79.68 + 30 * \i}
        \draw[lot] (1279.59,\y) -- (1331.35,\y);
    }
    \draw[block] (1099.59,6.18) -- (1099.59,79.68) -- (1279.59,79.68) -- 
                 (1279.59,589.68) -- (1331.35,589.68) -- (1331.35,6.18) -- cycle;
    \ifLotText
        \foreach \lot in {1,...,17} {
            \calc \y {574.68 - 30 * (\lot - 1)}
            \node[tex] at (1305.47,\y) {\tottext{B}{\lot}};
        }
        \node[tex] at (1287.35,42.93) {\tottext{B}{18}};
        \node[tex] at (1171.47,42.93) {\tottext{B}{19}};
    \fi
}

\newcommand*{\BlockC}{% Manzana C - 0080
    % Hay un error de 9 cm en ancho, el lote es 171 y dice 171.09 en el plano
    \draw[block] (888.59,6.18) -- (888.59,79.68) -- (1059.59,79.68) -- (1059.59,6.18) -- cycle;
    \ifLotText
        \node[tex] at (974.09,42.93) {\tottext{C}{1}};
    \fi
}

\newcommand*{\BlockD}{% Manzana D - 0081
    \foreach \i in {0,...,18} {
        \calc \x {63.59 + 40 * \i}
        \draw[lot] (\x,0) -- (\x,79.68);
    }
    \foreach \i in {0,...,10} {
        \calc \y {99.68 + 40 * \i}
        \draw[lot] (0,\y) -- (63.59,\y);
    }
    \draw[block] (0,0) -- (0,604.23) -- (63.59,604.23) -- (63.59,79.68) -- 
                 (863.59,79.68) -- (863.59,0) -- cycle;
    \ifLotText
        \node[tex] at (823.59,39.84) {\tottext{D}{1}};
        \foreach \lot in {2,...,19} {
            \calc \x {83.59 + 40 * (19 - \lot)}
            \node[tex] at (\x,39.84) {\tottext{D}{\lot}};
        }
        \node[tex] at (31.795,49.84) {\tottext{D}{20}};
        \foreach \lot in {21,...,30} {
            \calc \y {119.68 + 40 * (\lot - 21)}
            \node[tex] at (31.795,\y) {\tottext{D}{\lot}};
        }
        \node[tex] at (31.795,551.955) {\tottext{D}{31}};
    \fi
}

\newcommand*{\BlockE}{% Manzana E - 0082
    \BlockEF{E}{200}
}

\newcommand*{\BlockF}{% Manzana F - 0083
    \BlockEF{F}{0}
}

\newcommand*{\BlockG}{% Manzana G - 0084
    \draw[lot] (839.59,99.68) -- (839.59,289.68);
    \foreach \y in \ListA {
        \draw[lot] (799.59,\y) -- (879.59,\y);
    }
    \draw[block] (799.59,99.68) -- (799.59,289.68) -- (879.59,289.68) -- (879.59,99.68) -- cycle;
    \ifLotText
        \node[tex] at (819.59,\ArrayA[5]) {\tottext{G}{1}};
        \foreach \lot in {2,...,7} {
            \node[tex] at (859.59,\ArrayA[7-\lot]) {\tottext{G}{\lot}};
        }
        \foreach \lot in {8,...,12} {
            \node[tex] at (819.59,\ArrayA[\lot-8]) {\tottext{G}{\lot}};
        }
    \fi
}

\newcommand*{\BlockH}{% Manzana H - 0085
    \foreach \i in {0,...,7} {
        \calc \x {438.59 + 43 * \i}
        \draw[lot] (\x,99.68) -- (\x,289.68);
    }
    \draw[lot] (438.59,194.68) -- (739.59,194.68);
    \foreach \x in {398.59,739.59} {
        \foreach \y in \ListA {
            \draw[lot] (\x,\y) -- (\x+40,\y);
        }
    }
    \draw[block] (398.59,99.68) -- (398.59,289.68) -- (779.59,289.68) -- (779.59,99.68) -- cycle;
    \ifLotText
        \node[tex] at (418.59,273.845) {\tottext{H}{1}};
        \foreach \lot in {2,...,8} {
            \calc \x {460.09 + 43 * (\lot - 2)}
            \node[tex] at (\x,242.18) {\tottext{H}{\lot}};
        }
        \foreach \lot in {9,...,14} {
            \node[tex] at (759.59,\ArrayA[14-\lot]) {\tottext{H}{\lot}};
        }
        \foreach \lot in {15,...,21} {
            \calc \x {460.09 + 43 * (21 - \lot)}
            \node[tex] at (\x,147.18) {\tottext{H}{\lot}};
        }
        \foreach \lot in {22,...,26} {
            \node[tex] at (418.59,\ArrayA[\lot-22]) {\tottext{H}{\lot}};
        }
    \fi
}

\newcommand*{\BlockI}{% Manzana I - 0086
    \draw[lot] (320.59,99.68) -- (320.59,131.35) -- (338.59,131.35);
    \foreach \i in {0,...,5} {
        \calc \x {123.59 + 43 * \i}
        \draw[lot] (\x,99.68) -- (\x,289.68);
    }
    \draw[lot] (123.59,194.68) -- (338.59,194.68);
    \foreach \x in {83.59,338.59} {
        \foreach \y in \ListA {
            \draw[lot] (\x,\y) -- (\x+40,\y);
        }
    }
    \draw[block] (83.59,99.68) -- (83.59,289.68) -- (378.59,289.68) -- (378.59,99.68) -- cycle;
    \ifLotText
        \node[tex] at (103.59,273.845) {\tottext{I}{1}};
        \foreach \lot in {2,...,6} {
            \calc \x {145.09 + 43 * (\lot - 2)}
            \node[tex] at (\x,242.18) {\tottext{I}{\lot}};
        }
        \foreach \lot in {7,...,12} {
            \node[tex] at (358.59,\ArrayA[12-\lot]) {\tottext{I}{\lot}};
        }
        \node[tex] at (329.59,115.515) {\tottext{I}{13}};
        \foreach \lot in {14,...,18} {
            \calc \x {145.09 + 43 * (18 - \lot)}
            \node[tex] at (\x,147.18) {\tottext{I}{\lot}};
        }
        \foreach \lot in {19,...,23} {
            \node[tex] at (103.59,\ArrayA[\lot-19]) {\tottext{I}{\lot}};
        }
    \fi
}

\newcommand*{\BlockJ}{% Manzana J - 0087
    \BlockJK{J}{200}
}

\newcommand*{\BlockK}{% Manzana K - 0088
    \BlockJK{K}{0}
}

\newcommand*{\BlockL}{% Manzana L - 0089
    \foreach \i in {0,...,7} {
        \calc \x {112.59 + 38 * \i}
        \draw[lot] (\x,519.68) -- (\x,604.23);
    }
    \foreach \i in {0,...,6} {
        \calc \y {339.68 + 30 * \i}
        \draw[lot] (398.59,\y) -- (459.42,\y);
    }
    \foreach \i in {0,...,7} {
        \calc \x {459.42 + 41 * \i}
        \draw[lot] (\x,309.68) -- (\x,421.47);
    }
    \foreach \i in {0,1} {
        \calc \x {787.59 + 42 * \i}
        \draw[lot] (\x,309.68) -- (\x,409.68);
    }
    \foreach \i in {0,1} {
        \calc \y {349.68 + 30 * \i}
        \draw[lot] (829.59,\y) -- (879.59,\y);
    }
    \draw[block] (73.59,519.68) -- (73.59,604.23) -- (459.42,604.23) -- (459.42,421.47) -- (787.59,421.47) -- 
                 (787.59,409.68) -- (879.59,409.68) -- (879.59,309.68) -- (398.59,309.68) -- (398.59,519.68) -- cycle;
    \ifLotText
        \node[tex] at (93.09,561.955) {\tottext{L}{1}};
        \foreach \lot in {2,...,8} {
            \calc \x {131.59 + 38 * (\lot - 2)}
            \node[tex] at (\x,561.955) {\tottext{L}{\lot}};
        }
        \node[tex] at (419.005,561.955) {\tottext{L}{9}};
        \foreach \lot in {10,...,16} {
            \calc \y {504.68 - 30 * (\lot - 10)}
            \node[tex] at (429.005,\y) {\tottext{L}{\lot}};
        }
        \foreach \lot in {17,...,24} {
            \calc \x {479.92 + 41 * (\lot - 17)}
            \node[tex] at (\x,365.575) {\tottext{L}{\lot}};
        }
        \node[tex] at (808.59,359.68) {\tottext{L}{25}};
        \foreach \lot in {26,27} {
            \calc \y {364.68 + 30 * (27 - \lot)}
            \node[tex] at (854.59,\y) {\tottext{L}{\lot}};
        }
        \node[tex] at (854.59,329.68) {\tottext{L}{28}};
    \fi
}

\newcommand*{\BlockM}{% Manzana M - 0090
    \foreach \i in {0,...,5} {
        \calc \x {123.59 + 43 * \i}
        \draw[lot] (\x,309.68) -- (\x,499.68);
    }
    \draw[lot] (123.59,404.68) -- (338.59,404.68);
    \foreach \x in {83.59,338.59} {
        \foreach \y in \ListA {
            \draw[lot] (\x,\y+210) -- (\x+40,\y+210);
        }
    }
    \draw[block] (83.59,309.68) -- (83.59,499.68) -- (378.59,499.68) -- (378.59,309.68) -- cycle;
    \ifLotText
        \node[tex] at (103.59,483.845) {\tottext{M}{1}};
        \foreach \lot in {2,...,6} {
            \calc \x {145.09 + 43 * (\lot - 2)}
            \node[tex] at (\x,452.18) {\tottext{M}{\lot}};
        }
        \foreach \lot in {7,...,12} {
            \node[tex] at (358.59,\ArrayA[12-\lot]+210) {\tottext{M}{\lot}};
        }
        \foreach \lot in {13,...,17} {
            \calc \x {145.09 + 43 * (17 - \lot)}
            \node[tex] at (\x,357.18) {\tottext{M}{\lot}};
        }
        \foreach \lot in {18,...,22} {
            \node[tex] at (103.59,\ArrayA[\lot-18]+210) {\tottext{M}{\lot}};
        }
    \fi
}

\newcommand*{\BlockN}{% Manzana N - 0091
    \foreach \i in {0,...,4} {
        \calc \y {439.68 + 30 * \i}
        \draw[lot] (1099.59,\y) -- (1259.59,\y);
    }
    \foreach \i in {0,1,2} {
        \calc \x {1139.59 + 40 * \i}
        \foreach \y in {409.68,559.68} {
            \draw[lot] (\x,\y) -- (\x,\y+30);
        }
    }
    \draw[block] (1099.59,409.68) -- (1099.59,589.68) -- (1259.59,589.68) -- (1259.59,409.68) -- cycle;
    \ifLotText
        \foreach \lot in {1,...,4} {
            \calc \x {1119.59 + 40 * (\lot - 1)}
            \node[tex] at (\x,574.68) {\tottext{N}{\lot}};
        }
        \foreach \lot in {5,...,8} {
            \calc \y {544.68 - 30 * (\lot - 5)}
            \node[tex] at (1179.59,\y) {\tottext{N}{\lot}};
        }
        \foreach \lot in {9,...,12} {
            \calc \x {1119.59 + 40 * (12 - \lot)}
            \node[tex] at (\x,424.68) {\tottext{N}{\lot}};
        }
    \fi
}

\newcommand*{\BlockO}{% Manzana O - 0092
    \BlockOTS{O}{0}{0}
}

\newcommand*{\BlockP}{% Manzana P - 0093
    \foreach \i in {0,...,5} {
        \calc \x {899.58 + 32 * \i}
        \draw[lot] (\x,1109.68) -- (\x,1159.68);
    }
    \draw[lot] (822.80,1159.68) -- (1059.59,1159.68);
    \foreach \i in {0,...,3} {
        \calc \x {1139.59 + 30 * \i}
        \draw[lot] (\x,1109.68) -- (\x,1109.68+\ArrayB[\i]);
    }
    \foreach \i in {0,...,22} {
        \calc \y {449.68 + 30 * \i}
        \draw[lot] (822.80,\y) -- (879.59,\y);
    }
    \draw[block] (812.80,1159.68) -- (822.80,1159.68) -- (822.80,419.68) -- (879.59,419.68) -- 
                 (879.59,1109.68)  -- (1259.59,1109.68) -- (1259.59,1168.08);
    \ifLotText
        \node[tex] at (976.195,1230) {\tottext{P}{1}};
        \foreach \lot in {2,...,5} {
            \calc \x {1154.59 + 30 * (\lot - 2)}
            \node[tex] at (\x,1134.68) {\tottext{P}{\lot}};
        }
        \foreach \lot in {6,...,10} {
            \calc \x {1043.59 - 32 * (\lot - 6)}
            \node[tex] at (\x,1134.68) {\tottext{P}{\lot}};
        }
        \node[tex] at (861.195,1134.68) {\tottext{P}{11}};
        \foreach \lot in {12,...,34} {
            \calc \y {1094.68 - 30 * (\lot - 12)}
            \node[tex] at (851.195,\y) {\tottext{P}{\lot}};
        }
    \fi
}

\newcommand*{\BlockQ}{% Manzana Q - 0094
    \BlockQR{Q}{200}
}

\newcommand*{\BlockR}{% Manzana R - 0095
    \BlockQR{R}{0}
}

\newcommand*{\BlockS}{% Manzana S - 0096
    \BlockOTS{S}{200}{500}
}

\newcommand*{\BlockT}{% Manzana T - 0097
    \BlockOTS{T}{0}{500}
}

\newcommand*{\BlockOTS}[3]{% text, xoffset, yoffset
    \draw[lot] (979.59 + #2,409.68 + #3) -- (979.59 + #2,589.68 + #3);
    \foreach \i in {0,...,4} {
        \calc \y {439.68 + #3 + 30 * \i}
        \draw[lot] (899.59 + #2,\y) -- (1059.59 + #2,\y);
    }
    \foreach \i in {0,1,2} {
        \calc \x {939.59 + 40 * \i + #2}
        \foreach \y in {409.68,559.68} {
            \draw[lot] (\x,\y + #3) -- (\x,\y + #3 + 30);
        }
    }
    \draw[block] (899.59 + #2,409.68 + #3) -- (899.59 + #2,589.68 + #3) -- 
                 (1059.59 + #2,589.68 + #3) -- (1059.59 + #2,409.68 + #3) -- cycle;
    \ifLotText
        \foreach \lot in {1,...,4} {
            \calc \x {919.59 + 40 * (\lot - 1) + #2}
            \node[tex] at (\x,574.68 + #3) {\tottext{#1}{\lot}};
        }
        \foreach \lot in {5,...,8} {
            \calc \y {544.68 + #3 - 30 * (\lot - 5)}
            \node[tex] at (1019.59 + #2,\y) {\tottext{#1}{\lot}};
        }
        \foreach \lot in {9,...,12} {
            \calc \x {919.59 + 40 * (12 - \lot) + #2}
            \node[tex] at (\x,424.68 + #3) {\tottext{#1}{\lot}};
        }
        \foreach \lot in {13,...,16} {
            \calc \y {454.68 + #3 + 30 * (\lot - 13)}
            \node[tex] at (939.59 + #2,\y) {\tottext{#1}{\lot}};
        }
    \fi
}

\newcommand*{\BlockEF}[2]{% text, xoffset
    \draw[lot] (899.59 + #2,248.88) -- (1059.59 + #2,248.88);
    \foreach \i in {0,...,3} {
        \calc \x {931.59 + 32 * \i + #2}
        \draw[lot] (\x,248.88) -- (\x,289.68);
    }
    \draw[block] (899.59 + #2,99.68) -- (899.59 + #2,289.68) -- 
                 (1059.59 + #2,289.68) -- (1059.59 + #2,99.68) -- cycle;
    \ifLotText
        \node[tex] at (979.59 + #2,174.28) {\tottext{#1}{6}};
        \foreach \lot in {1,...,5} {
            \calc \x {915.59 + 32 * (\lot - 1) + #2}
            \node[tex] at (\x,269.28) {\tottext{#1}{\lot}};
        }
    \fi
}

\newcommand*{\BlockJK}[2]{% text, xoffset
    \draw[lot] (899.59 + #2,349.68) -- (1059.59 + #2,349.68);
    \foreach \i in {0,...,3} {
        \calc \x {931.59 + 32 * \i + #2}
        \draw[lot] (\x,309.68) -- (\x,389.68);
    }
    \draw[block] (899.59 + #2,309.68) -- (899.59 + #2,389.68) -- 
                 (1059.59 + #2,389.68) -- (1059.59 + #2,309.68) -- cycle;
    \ifLotText
        \foreach \lot in {1,...,5} {
            \calc \x {915.59 + 32 * (\lot - 1) + #2}
            \node[tex] at (\x,369.68) {\tottext{#1}{\lot}};
        }
        \foreach \lot in {6,...,10} {
            \calc \x {915.59 + 32 * (10 - \lot) + #2}
            \node[tex] at (\x,329.68) {\tottext{#1}{\lot}};
        }
    \fi
}

\newcommand*{\BlockQR}[2]{% text, xoffset
    \draw[lot] (979.59 + #2,649.68) -- (979.59 + #2,849.68);
    \foreach \i in {0,...,5} {
        \calc \y {649.68 + 40 * \i}
        \draw[lot] (899.59 + #2,\y) -- (1059.59 + #2,\y);
    }
    \foreach \i in {0,1,2} {
        \calc \x {939.59 + 40 * \i + #2}
        \foreach \y in {609.68,849.68} {
            \draw[lot] (\x,\y) -- (\x,\y+40);
        }
    }
    \draw[block] (899.59 + #2,609.68) -- (899.59 + #2,889.68) -- 
                 (1059.59 + #2,889.68) -- (1059.59 + #2,609.68) -- cycle;
    \ifLotText
        \foreach \lot in {1,...,4} {
            \calc \x {919.59 + 40 * (\lot - 1) + #2}
            \node[tex] at (\x,869.68) {\tottext{#1}{\lot}};
        }
        \foreach \lot in {5,...,9} {
            \calc \y {829.68 - 40 * (\lot - 5)}
            \node[tex] at (1019.59 + #2,\y) {\tottext{#1}{\lot}};
        }
        \foreach \lot in {10,...,13} {
            \calc \x {919.59 + 40 * (13 - \lot) + #2}
            \node[tex] at (\x,629.68) {\tottext{#1}{\lot}};
        }
        \foreach \lot in {14,...,18} {
            \calc \y {669.68 + 40 * (\lot - 14)}
            \node[tex] at (939.59 + #2,\y) {\tottext{#1}{\lot}};
        }
    \fi
}

\begin{tikzpicture}[x=2pt,y=2pt,scale=0.16]

\tikzset{%
    diagram/.style = {gray, line width=0.5},
    limit/.style = {draw, gray!50, densely dashed},
    block/.style = {black},
    lot/.style = {gray},
    tex/.style = {black},
    %
    title/.style = {font=\Large\scshape},
    %
    compass line/.style = {draw, -Stealth, line width=3.8, gray!69},
    compass letters/.style = {rotate=7, black, font=\small\bfseries},
    %
    zone/.style = {fill=gray!30},
    zone limit/.style = {shade, left color=gray!30},
    zone thesis/.style = {fill=gray!69},
    %
    ref/.style = {anchor=base west, font=\scriptsize},
}

\path[zone] (0,299.68) rectangle (63.59,499.68);
\path[zone thesis] (0,499.68) rectangle (63.59,604.23);

\path[zone] (83.59,309.68) rectangle (300,499.68);
\path[zone limit] (299,309.68) rectangle (360,499.68);

\path[zone] (73.59,519.68) rectangle (300,604.23);
\path[zone limit] (299,519.68) rectangle (360,604.23);

«COORDINATE_FRANJA»

\path[limit] (EE) -- (FF) -- (GG) -- (HH) -- (II) -- (JJ) -- (KK) -- (LL) -- (MM) -- (NN);

\BlockA
\BlockB
\BlockC
\BlockD
\BlockE
\BlockF
\BlockG
\BlockH
\BlockI
\BlockJ
\BlockK
\BlockL
\BlockM
\BlockN
\BlockO
\BlockP
\BlockQ
\BlockR
\BlockS
\BlockT

\coordinate (A) at (  -9.800, 614.230);
\coordinate (B) at ( 469.670, 614.320);
\coordinate (C) at ( 469.420, 431.500);
\coordinate (D) at ( 812.580, 431.470);
\coordinate (E) at ( 812.523,1390.910);

«COORDINATE_LIMITE»

\coordinate (O) at (1341.35,-10);
\coordinate (P) at (-10,-10);

\draw[diagram] (A) -- (B) -- (C) -- (D) -- (E) -- (F) -- (G) -- (H) -- 
               (I) -- (J) -- (K) -- (L) -- (M) -- (N) -- (O) -- (P) -- cycle;

\coordinate (inl) at (1059.59,-10);
\coordinate (inc) at (1079.59,-10);
\coordinate (inr) at (1099.59,-10);

\draw[diagram, latex-latex] ([yshift=-25]inc) -- ([yshift=25]inc);
\draw[diagram] ([yshift=-15]inl) -- ([yshift=15]inl)
               ([yshift=-15]inr) -- ([yshift=15]inr);

\coordinate (p1) at ($(D)!.80!(E)$);
\coordinate (p2) at (p1-|A);
\coordinate (p3) at ($(p2)!.50!(p1)$);
\coordinate (p4) at ($(A)!.20!(p2)$);
\coordinate (p5) at ($(p4)!.50!(p1)$);

%         E
%         │
%  p2-p3-p1
%  │    ╱ │
%  │  p5  │
%  │ ╱    │
%  p4     │
%  │      │
%  A      │
%         D

\node[title] at (p3) {Parque Industrial Posadas};

\path[draw, zone thesis] (p4)++(30,-15) rectangle ++(40,-18) node[name=p41]{};
\path[draw, zone]        (p4)++(30,-45) rectangle ++(40,-18) node[name=p42]{};
\node[ref] at (p4) {Referencias:};
\node[ref] at (p41) {Parcela elegida para el proyecto};
\node[ref] at (p42) {Zona de actividad química};

\path[compass line] (p5) -- +(  7:52) node[compass letters, right]{E};
\path[compass line] (p5) -- +( 97:52) node[compass letters, above]{N};
\path[compass line] (p5) -- +(187:52) node[compass letters, left ]{O};
\path[compass line] (p5) -- +(277:52) node[compass letters, below]{S};
\path[fill=white] (p5) circle [radius=16];

\end{tikzpicture}
}""".replace('«COORDINATE_LIMITE»', limite).replace('«COORDINATE_FRANJA»', franja))
