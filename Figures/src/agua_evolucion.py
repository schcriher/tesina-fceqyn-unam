raw = dbf.get('raw')
ede = raw[raw.Mh == dbf.get_storer('ede').attrs.Mh]
Mh = int(ede.Mh) / 1000
Ma = Mh * fea
db = dbe.get('evf')
MAN = db.MAN.astype(float) / 1e3
MAS = db.MAS.astype(float) / 1e3
ymax = max((MAN.max(), MAS.max()))
x = db.index
g = Figura(**{
    'destino': BASE,
    'parametros': {
        'legend style': {
            'anchor': 'north',
            'at': '{(0.50,0.96)}',
            'legend columns': '3',
        },
        'height': '1.5\\figureheight',
        'width': '1.5\\figurewidth',
    },
    'conv': (
        '{}.0f'.format(len(str(math.ceil(x.max())))),
        '{}.0f'.format(len(str(math.ceil(ymax)))),
    ),
})
xtick = list(range(x.min()-1, x.max()+365, 365))
g.eje(**{
    'tipo': 'axis',
    'parametros': {
        'xlabel': r'{Años}',
        'xmin': x.min(),
        'xmax': x.max(),
        'xtick': tick(xtick),
        'xticklabels': tick(range(1, len(xtick), 2), 2),
        'x tick label as interval': None,
        'scaled x ticks': False,
        'scaled y ticks': True,
        'ylabel': r'{Agua, \si{\kg\Agua}}',
        'ymax': round(ymax * 1.04, -2),
        'ymin': 0,
        'ytick': '{0, 5000, 10000, 15000, 20000}',
        'reverse legend': None,
    },
})
g.serie(**{
    'x': x,
    'y': MAN,
    'leyenda': 'Agua utilizada',
})
g.serie(**{
    'x': x,
    'y': MAS,
    'leyenda': 'Agua de stock (\SI{25}{\meter\cubed})',
    'parametros': {'gray': None},
})
node = r'\node[gray, right, font=\tiny] at (axis cs:{0},{1:5.0f}) {{{2}}};'
draw = r'\draw[gray, densely dotted] (axis cs:0,{1:5.0f}) -- (axis cs:{0},{1:.0f}) node[right, font=\tiny] {{{2}}};'
xx = x.max()
objs = [node.format(xx, Ma, 1)]
objs.extend([draw.format(xx, Ma * i, i) for i in range(4, math.ceil(ymax / Ma), 3)])
for obj in objs:
    g.objeto(obj, inicio=True)
g.tex()
