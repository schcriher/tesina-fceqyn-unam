SAVE(FTEX, HEADER + r"""
\newcommand*{\Jj}{\vphantom{Jj}}
\newcommand*{\calc}[2]{\pgfmathparse{#2}\let#1\pgfmathresult}

\newcommand*{\caja}[4]{ % x, y, w, h
    \calc \x {#1 + #3}
    \calc \y {#2 + #4}
    \draw (#1,#2) -- (\x,#2) -- (\x,\y) --  (#1,\y) -- cycle;
}

\newcommand*{\CilPack}[2]{ % x, y
    \foreach \i in {0,1} {
        \calc \x {#1 + 0.250 * \i}
        \foreach \j in {0,1} {
            \calc \y {#2 + 0.250 * \j}
            \draw (\x,\y) circle [radius=0.122];
        }
    }
}

\newcommand*{\CilDay}[4]{ % x, y, nPackX, nPackY
    \foreach \i in {1,...,#3} {
        \calc \x {#1 + 0.534 * (\i - 1)}
        \foreach \j in {1,...,#4} {
            \calc \y {#2 + 0.534 * (\j - 1)}
            \CilPack{\x}{\y}
        }
    }
}

\newcommand*{\CilStock}[7]{ % x, y, nPackX, nPackY, nDayX, nDayY, yoffset
    \foreach \i in {1,...,#5} {
        \calc \x {#1 + (#3 * 0.550 + 3 - 0.122) * (\i - 1)}
        \foreach \j in {1,...,#6} {
            \calc \y {#2 + (#4 * 0.494 + (#4 - 1) * 0.040 + 0.157 + #7) * (\j - 1)}
            \CilDay{\x}{\y}{#3}{#4}
        }
    }
}

\newcommand*{\PuertaStock}[3]{ % x, y, w
    \calc \a {#1 - #3}
    \calc \b {#1 + #3}
    \calc \c {#2 - 0.13}
    \calc \d {#2 + 0.13}
    \draw[pattern=north east lines, pattern color=lightgray] (\a,\c) rectangle (\b,\d);
}

\newcommand*{\PuertaA}[2]{ % x, y
    %      /#
    %     / #
    %    /  #
    %  -|-*-|-
    %   b   a
    \calc \a {#1 + 0.75}
    \calc \b {#1 - 0.75}
    \calc \c {#2 - 0.16}
    \calc \d {#2 + 0.16}
    \calc \e {#2 + 1.50}
    % Limite
    \draw[line width=0.8] (\a,\c) -- (\a,\d);
    \draw[line width=0.8] (\b,\c) -- (\b,\d);
    % Puerta
    \draw (\a,#2) -- (\a,\e);
    % Arco
    \draw (\a,\e) arc (90:180:1.50);
}

\newcommand*{\PuertaB}[2]{ % x, y
    %   b   a
    %  -|-*-|-
    %    \  #
    %     \ #
    %      \#
    \calc \a {#1 + 0.75}
    \calc \b {#1 - 0.75}
    \calc \c {#2 - 0.16}
    \calc \d {#2 + 0.16}
    \calc \e {#2 - 1.50}
    % Limite
    \draw[line width=0.8] (\a,\c) -- (\a,\d);
    \draw[line width=0.8] (\b,\c) -- (\b,\d);
    % Puerta
    \draw (\a,#2) -- (\a,\e);
    % Arco
    \draw (\b,#2) arc (180:270:1.50);
}

\newcommand*{\PuertaC}[2]{ % x, y
    %       |
    %   ####-a
    %    \  |
    %     \ *
    %      \|
    %       -b
    %       |
    \calc \a {#2 + 0.75}
    \calc \b {#2 - 0.75}
    \calc \c {#1 - 0.16}
    \calc \d {#1 + 0.16}
    \calc \e {#1 - 1.50}
    % Limite
    \draw[line width=0.8] (\c,\a) -- (\d,\a);
    \draw[line width=0.8] (\c,\b) -- (\d,\b);
    % Puerta
    \draw (#1,\a) -- (\e,\a);
    % Arco
    \draw (\e,\a) arc (180:270:1.50);
}

\newcommand*{\PuertaD}[2]{ % x, y
    %       |
    %      a-####
    %       |  /
    %       * /
    %       |/
    %      b-
    %       |
    \calc \a {#2 + 0.75}
    \calc \b {#2 - 0.75}
    \calc \c {#1 - 0.16}
    \calc \d {#1 + 0.16}
    \calc \e {#1 + 1.50}
    % Limite
    \draw[line width=0.8] (\c,\a) -- (\d,\a);
    \draw[line width=0.8] (\c,\b) -- (\d,\b);
    % Puerta
    \draw (#1,\a) -- (\e,\a);
    % Arco
    \draw (#1,\b) arc (270:360:1.50);
}

\newcommand*{\PuertaE}[2]{ % x, y
    %       |
    %       -a
    %      /|
    %     / *
    %    /  |
    %   ####-b
    %       |
    \calc \a {#2 + 0.75}
    \calc \b {#2 - 0.75}
    \calc \c {#1 - 0.16}
    \calc \d {#1 + 0.16}
    \calc \e {#1 - 1.50}
    % Limite
    \draw[line width=0.8] (\c,\a) -- (\d,\a);
    \draw[line width=0.8] (\c,\b) -- (\d,\b);
    % Puerta
    \draw (#1,\b) -- (\e,\b);
    % Arco
    \draw (#1,\a) arc (90:180:1.50);
}

\begin{tikzpicture}[x=1mm,y=1mm,scale=3]

\draw[lightgray] (0,0) -- (57.59,0) -- (57.59,-10.9) --  (0,-10.9) -- cycle;

\draw (0,0) -- (57.59,0) -- (57.59,15.88) --  (0,15.88) -- cycle;

% Rampa
\draw[draw=none, fill=lightgray] (0,0) -- (7.124,0) -- (7.124,7.88) -- (12.124,7.88) -- 
                      (12.124,12.88) -- (17.944,12.88) -- (17.944,7.88) -- 
                      (22.944,7.88) -- (22.944,0) -- (29,0) -- (29,15.88) -- 
                      (0,15.88) -- cycle;
\shade[left color=lightgray, right color=transparent!0] (29,12.88) rectangle (34.5,15.88);
\draw (29,12.88) -- (34.5,12.88);

\draw[Stealth-Stealth] ([yshift=3]0,15.88) -- ([yshift=3]57.59,15.88)  node[midway,above]{\SI{57.59}{\m}};
\draw[Stealth-Stealth] ([xshift=-3]0,0) -- ([xshift=-3]0,15.88)  node[midway,left]{\SI{15.88}{\m}};
\draw[Stealth-Stealth] ([xshift=-3]0,0) -- ([xshift=-3]0,-10.9)  node[midway,left]{\SI{10.9}{\m}};

\CilStock{0.622}{0.622}{3}{3}{2}{7}{0}
\CilStock{23.566}{0.622}{2}{2}{2}{7}{0.623}

\caja{0}{12.88}{29}{3}             % Calle de trabajo

\caja{0}{0}{7.124}{12.88}          % Stock H
\caja{22.944}{0}{6.056}{12.88}     % Stock O

\caja{7.124}{7.88}{5}{5}           % Envasado H
\caja{17.944}{7.88}{5}{5}          % Envasado O

\caja{14.034}{7.88}{2}{5}          % Deposito de agua
\caja{8.939}{2.72}{12.190}{2.437}  % Electrolizador

\caja{29}{0}{11.09}{15.88}         % Mantenimiento

% Estacionamiento
\draw[pattern=north east lines, pattern color=lightgray!50!white] (40.09,10.88) rectangle (57.59,15.88);
\foreach \i in {1,...,6} {
    \calc \x {40.09 + 2.5 * \i}
    \draw[lightgray!50!white] (\x,10.88) -- (\x,15.88);
}

\node at (28.795,-5.45) {Área de circulación de camiones};

\node at (15.034,14.38) {\tiny Calle de trabajo};

\node at (3.562,6.44) {\Jj\ch{H2}};
\node at (25.972,6.44) {\Jj\ch{O2}};

\node at (15.034,10.38) {\Jj\tiny \ch{H2O}};
\node at (15.034,3.94) {\Jj\tiny Electrolizador};

\node at (9.624,10.38) {\Jj\scalebox{0.50}{\begin{tabular}{c}Envasado\\de\\\ch{H2}\end{tabular}}};
\node at (20.444,10.38) {\Jj\scalebox{0.50}{\begin{tabular}{c}Envasado\\de\\\ch{O2}\end{tabular}}};

\node at (34.545,6.44) {\scriptsize\begin{tabular}{c}Área de\\Mantenimiento\\Y Garaje\end{tabular}};
\node at (48.840,13.38) {Estacionamiento};
\node at (48.840,5.44) {Área de oficinas};

\PuertaStock{3.562}{0}{1.4}
\PuertaStock{3.562}{12.88}{1.4}
\PuertaStock{25.972}{0}{1.4}
\PuertaStock{25.972}{12.88}{1.4}
\PuertaStock{34.545}{0}{3}

\PuertaC{57.59}{9.88}
\PuertaD{40.09}{5.44}

%\draw[red] (0,0) --++ (0.5,0.0) -- ++(0.0,0.5) -- ++ (-0.5,0.0) -- cycle;
%\draw[red] (0,12.88) --++ (0.5,0.0) -- ++(0.0,-0.5) -- ++ (-0.5,0.0) -- cycle;
%\draw[red] (7.124,0) --++ (-0.5,0.0) -- ++(0.0,0.5) -- ++ (0.5,0.0) -- cycle;
%\draw[red] (7.124,12.88) --++ (-0.5,0.0) -- ++(0.0,-0.5) -- ++ (0.5,0.0) -- cycle;

%\draw[red] (22.944,0) --++ (0.5,0.0) -- ++(0.0,0.5) -- ++ (-0.5,0.0) -- cycle;
%\draw[red] (22.944,12.88) --++ (0.5,0.0) -- ++(0.0,-0.5) -- ++ (-0.5,0.0) -- cycle;
%\draw[red] (29,0) --++ (-0.5,0.0) -- ++(0.0,0.5) -- ++ (0.5,0.0) -- cycle;
%\draw[red] (29,12.88) --++ (-0.5,0.0) -- ++(0.0,-0.5) -- ++ (0.5,0.0) -- cycle;

\end{tikzpicture}
""")
