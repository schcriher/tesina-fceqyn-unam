g = Figura(**{
    'destino': BASE,
    'parametros': {
        'legend style': {
            'at': '{(0.03,0.96)}',
            'anchor': 'north west',
        },
    },
})
g.eje(**{
    'tipo': 'axis',
    'parametros': {
        'xlabel': '{Fecha, año}',
        'ylabel': '{Valor, M\si{\dolar}}',
    },
})
# Histórico
años = Mercado.extraer("CIQyP.A5:A22", int)
g.serie(**{
    'x': años,
    'y': Mercado.extraer("CIQyP.B5:B22", round),
    'leyenda': 'Consumo aparente',
    'parametros': {'mark': '*'},
})
g.serie(**{
    'x': años,
    'y': Mercado.extraer("CIQyP.C5:C22", round),
    'leyenda': 'Producción',
    'parametros': {'mark': 'o'},
})
g.serie(**{
    'x': años,
    'y': Mercado.extraer("CIQyP.D5:D22", round),
    'leyenda': 'Importaciones',
    'parametros': {'mark': 'x'},
})
g.serie(**{
    'x': años,
    'y': Mercado.extraer("CIQyP.E5:E22", round),
    'leyenda': 'Exportaciones',
    'parametros': {'mark': '+'},
})
# Proyecciones
años = Mercado.extraer("CIQyP.A22,A24:A25", int)
g.serie(**{
    'x': años,
    'y': Mercado.extraer("CIQyP.B22,B30:B31", round),
    'leyenda': 'Consumo aparente (optimista)',
    'parametros': {'mark': '*', 'dashdotdotted': True},
})
g.serie(**{
    'x': años,
    'y': Mercado.extraer("CIQyP.B22,B24:B25", round),
    'leyenda': 'Consumo aparente (conservador)',
    'parametros': {'mark': '*', 'dashed': True},
})
g.serie(**{
    'x': años,
    'y': Mercado.extraer("CIQyP.C22,C30:C31", round),
    'leyenda': 'Producción (optimista)',
    'parametros': {'mark': 'o', 'dashdotdotted': True},
})
g.serie(**{
    'x': años,
    'y': Mercado.extraer("CIQyP.C22,C24:C25", round),
    'leyenda': 'Producción (conservador)',
    'parametros': {'mark': 'o', 'dashed': True},
})
g.serie(**{
    'x': años,
    'y': Mercado.extraer("CIQyP.D22,D30:D31", round),
    'leyenda': 'Importaciones (optimista)',
    'parametros': {'mark': 'x', 'dashdotdotted': True},
})
g.serie(**{
    'x': años,
    'y': Mercado.extraer("CIQyP.D22,D24:D25", round),
    'leyenda': 'Importaciones (conservador)',
    'parametros': {'mark': 'x', 'dashed': True},
})
g.serie(**{
    'x': años,
    'y': Mercado.extraer("CIQyP.E22,E30:E31", round),
    'leyenda': 'Exportaciones (optimista)',
    'parametros': {'mark': '+', 'dashdotdotted': True},
})
g.serie(**{
    'x': años,
    'y': Mercado.extraer("CIQyP.E22,E24:E25", round),
    'leyenda': 'Exportaciones (conservador)',
    'parametros': {'mark': '+', 'dashed': True},
})
g.tex()
