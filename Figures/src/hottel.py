rad = dbd.get('nasa/Rdm')
hsp = dbd.get('hottel')
g = Figura(**{
    'destino': BASE,
    'parametros': {
        'legend style': {
            'anchor': 'south east',
            'at': '{(0.97,0.04)}',
        },
    },
    'conv': ('>3.0f', '.3f'),
})
g.eje(**{
    'tipo': 'axis',
    'parametros': {
        'xlabel': '{Mes del año}',
        'ylabel': '{Horas Solares Pico (HSP)}',
        'xmin': 1,
        'xmax': 365,
        'xtick': tick_dayofyear_12,
        'xticklabels': ticklabels_month,
        'x tick label as interval': True,
    },
})
g.serie(**{
    'x': rad.index + 1,
    'y': rad.values,
    'leyenda': 'Datos de la NASA',
    'parametros': {'only marks': True, 'mark options': '{scale=0.2}', 'mark': '*'},
})
g.serie(**{
    'x': hsp.index + 1,
    'y': hsp.values,
    'leyenda': 'Modelo de Hottel',
})
g.tex()
