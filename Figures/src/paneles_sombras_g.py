SAVE(FTEX, HEADER + r"""{%
\begin{tikzpicture}[>=latex]

\coordinate (a) at (1,3);
\coordinate (b) at (3,1);
\coordinate (c) at (a-|b);
\coordinate (d) at ([yshift=-25]a|-b);
\coordinate (e) at (b|-d);
\coordinate (f) at ([xshift=-10]a);

\newcommand*{\panelfotovoltaico}[1]{%
    \path [draw=gray!69!black, fill=lightgray, rotate=30] (#1a) --
        ++(0,-1) node[coordinate](#1b){} --
        ++(2, 0) node[coordinate](#1c){} --
        ++(0, 1) node[coordinate](#1d){} --
        cycle;
}

\coordinate (p1a) at ([yshift=-15]$(a)!0.5!(b)$);
\panelfotovoltaico{p1}

\coordinate (p2a) at (p1d);
\panelfotovoltaico{p2}

\fill[draw=none, fill=gray,fill opacity=.3] (a) -- (b) -- (e) -- (d) -- cycle;  % sombra
\draw[gray] (a) -- (b) -- (e);  % sombra
\path[pattern=north east lines] (f) rectangle (d);
\draw[line width=1] (f) -- (a) -- (d);  % edificio

\draw [<->] (a) -- (c) node [midway, above] {$S_x$};
\draw [<->] (c) -- (b) node [midway, left,xshift=1,yshift=10] {$S_y$};

\draw (p1a) -- (p1a|-p1b) -- (p1b) node [midway, below] {$d_6$};
\draw (p2a) -- (p2a-|p2d) node [midway, below] {$d_5$} -- (p2d);

\draw ([yshift=-12]p1a) arc (270:283:1) node[anchor=north,xshift=-2,yshift=0]{$\epsilon$};
\draw ([xshift=12]p1d) arc (0:13:1) node[anchor=west,xshift=-1,yshift=-2]{$\epsilon$};
\draw ([yshift=18]b) arc (95:124:1) node[anchor=north,xshift=9,yshift=6]{$\psi$};

\draw [|-|] ([xshift=-3,yshift=4]p2a) -- ([xshift=-3,yshift=4]p2d) node [midway, above] {$X_p$};
\draw [|-|] ([xshift=4,yshift=2.5]p2c) -- ([xshift=4,yshift=2.5]p2d) node [midway, right] {$Y_p$};

\coordinate (z) at ([xshift=25,yshift=-8]b-|p2c);
\coordinate (v1) at ([xshift=15,yshift=20]c|-p2d);
\coordinate (v2) at ([xshift=25]v1-|z);

\draw (z) -- ([xshift=50]z) node[midway, below] {S};
\draw (z) -- ([xshift=60, yshift=50]z) node[above] {\scriptsize Sol};
\draw ([xshift=50]z) -- ([xshift=50, yshift=42]z) node[midway, left] {H};
\draw ([xshift=50]z) -- ([xshift=60]z);
\draw ([xshift=50, yshift=42]z) -- ([xshift=60, yshift=42]z);
\path[pattern=north east lines] ([xshift=50]z) rectangle ([xshift=60, yshift=42]z);
\draw ([xshift=18]z) arc (0:26:1) node[anchor=north,xshift=-2,yshift=-2]{$\alpha$};

\node at (v1) {\scriptsize Vista Superior};
\node at (v2) {\scriptsize Vista Lateral};

%\fill [red] (a) circle (1pt) node[above] {a};
%\fill [red] (b) circle (1pt) node[above] {b};
%\fill [red] (c) circle (1pt) node[above] {c};
%\fill [red] (d) circle (1pt) node[above] {d};

%\fill [red] (p1a) circle (1pt) node[above] {p1a};
%\fill [red] (p2c) circle (1pt) node[above] {p2c};

%\fill [red] (z) circle (1pt) node[above] {z};

%\fill [red] (v1) circle (1pt) node[above] {v1};
%\fill [red] (v2) circle (1pt) node[above] {v2};

\end{tikzpicture}
}""")
