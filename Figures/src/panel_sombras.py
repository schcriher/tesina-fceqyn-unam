SAVE(FTEX, HEADER + r"""{%
\newif\ifEnergyA
\newif\ifEnergyB
\newif\ifEnergyC
\begin{tikzpicture}

\def\Xcell{0.6}
\def\Ycell{0.5}
\def\Scell{0.05}
\def\Ccell{10}
\def\Fcell{6}
\def\Xdiode{0.4}
\def\Ydiode{0.4}

\newcommand*{\calc}[2]{\pgfmathparse{#2}\let#1\pgfmathresult}

\newcommand*{\cell}[1]{%
    \path [draw=black!60, fill=lightgray, rounded corners=2pt]
          (#1) --
        ++( \Xcell,0) --
        ++( 0,\Ycell) --
        ++(-\Xcell,0) --
        cycle;
}

\calc \xdiode {\Xdiode / 2}
\calc \ydiode {\Ydiode / 2}
\newcommand*{\diode}[1]{%
    \draw [fill] (#1)
        ++(-\xdiode,-\ydiode) --
        ++(\Xdiode,0) --
        ++(-\xdiode,\Ydiode) --
        cycle;
    \draw [ultra thick] (#1)
        ++(-\xdiode,\ydiode) -- ++(\Xdiode,0);
}

\newcommand*{\panelfotovoltaico}[3]{%
    \foreach \f in {1,...,\Fcell} {
        \foreach \c in {1,...,\Ccell} {
            \calc \x {#1 + (\c - 1) * \Xcell + (\c - 1) * \Scell}
            \calc \y {#2 + (\f - 1) * \Ycell + (\f - 1) * \Scell}
            \cell{\x,\y}
        }
    }

    \calc \A {#1 - 2 * \Scell}
    \calc \B {#2 - 2 * \Scell}
    \calc \C {#1 + \Ccell * (\Xcell + \Scell) + \Scell}
    \calc \D {#2 + \Fcell * (\Ycell + \Scell) + \Scell}

    \coordinate (p1) at (\A,\B);
    \coordinate (p2) at (\C,\D);
    \coordinate (p3) at (p1-|p2);

    %\fill [red] (p1) circle (1pt) node[left]  {p1};
    %\fill [red] (p2) circle (1pt) node[right] {p2};
    %\fill [red] (p3) circle (1pt) node[right] {p3};

    \calc \x {#1 + \Xcell / 2}
    \calc \y {#2 + \Ycell / 2}

    \calc \a {(\Ccell - 1) * (\Xcell + \Scell)}
    \calc \b {(\Fcell - 1) * (\Ycell + \Scell) + \y}
    \calc \c {\Ycell + \Scell}
    \calc \d {\x - \Xcell - 4*\Scell}
    \calc \e {\y - \Ycell - 4*\Scell}
    \calc \f {\b + \Ycell + 4*\Scell}

    \coordinate (c1) at (\x,\y);
    \foreach \i in {2,...,5} {
        \calc \yy {\y + \c * (\i - 1}
        \coordinate (c\i) at (\x,\yy);
    }
    \coordinate (c6) at (\x,\b);

    \coordinate (C0) at (\d,\e);
    \coordinate (C1) at (\d,\y);
    \foreach \i in {2,...,5} {
        \calc \yy {\y + \c * (\i - 1}
        \coordinate (C\i) at (\d,\yy);
    }
    \coordinate (C6) at (\d,\b);
    \coordinate (C7) at (\d,\f);

    \coordinate (n1) at ($(c2)!.5!(c3)$);
    \coordinate (n2) at ($(c4)!.5!(c5)$);
    \coordinate (N1) at ($(C2)!.5!(C3)$);
    \coordinate (N2) at ($(C4)!.5!(C5)$);

    \coordinate (D1) at ($(C1)!.5!(N1)$);
    \coordinate (D2) at ($(N1)!.5!(N2)$);
    \coordinate (D3) at ($(N2)!.5!(C6)$);

    %\fill [red] (c1) circle (1pt) node[left] {c1};
    %\fill [red] (c2) circle (1pt) node[left] {c2};
    %\fill [red] (c3) circle (1pt) node[left] {c3};
    %\fill [red] (c4) circle (1pt) node[left] {c4};
    %\fill [red] (c5) circle (1pt) node[left] {c5};
    %\fill [red] (c6) circle (1pt) node[left] {c6};

    %\fill [red] (C0) circle (1pt) node[left] {C0};
    %\fill [red] (C1) circle (1pt) node[left] {C1};
    %\fill [red] (C2) circle (1pt) node[left] {C2};
    %\fill [red] (C3) circle (1pt) node[left] {C3};
    %\fill [red] (C4) circle (1pt) node[left] {C4};
    %\fill [red] (C5) circle (1pt) node[left] {C5};
    %\fill [red] (C6) circle (1pt) node[left] {C6};
    %\fill [red] (C7) circle (1pt) node[left] {C7};

    %\fill [red] (n1) circle (1pt) node[right] {n1};
    %\fill [red] (n2) circle (1pt) node[right] {n2};
    %\fill [red] (N1) circle (1pt) node[left] {N1};
    %\fill [red] (N2) circle (1pt) node[left] {N2};

    %\fill [red] (D1) circle (1pt) node[left] {D1};
    %\fill [red] (D2) circle (1pt) node[left] {D2};
    %\fill [red] (D3) circle (1pt) node[left] {D3};

    \ifEnergyA
        \begin{scope}[transparency group, opacity=0.2]
            \draw [ultra thick] (C1) -- (N1);
            \draw [ultra thick] (N1) -- (N2);
            \draw [ultra thick] (N2) -- (C6);
            \draw [ultra thick] (n1) -- (N1);
            \draw [ultra thick] (n2) -- (N2);

            \diode{D1}
            \diode{D2}
            \diode{D3}
        \end{scope}
        \draw [ultra thick, rounded corners=2pt]
            (C1) --
            (c1) --
            ++(\a,0) --
            ++(0,\c) --
            ++(-\a,0) --
            (n1);

        \draw [ultra thick, rounded corners=2pt]
            (n1) --
            (c3) --
            ++(\a,0) --
            ++(0,\c) --
            ++(-\a,0) --
            (n2);
    \fi
    \ifEnergyB
        \begin{scope}[transparency group, opacity=0.2]
            \draw [ultra thick] (N1) -- (N2);
            \draw [ultra thick] (N2) -- (C6);
            \draw [ultra thick] (n2) -- (N2);

            \draw [ultra thick, rounded corners=2pt]
                (C1) --
                (c1) --
                ++(\a,0) --
                ++(0,\c) --
                ++(-\a,0) --
                (n1);

            \diode{D2}
            \diode{D3}
        \end{scope}
        \draw [ultra thick] (C1) -- (N1);
        \draw [ultra thick, line cap=rect] (n1) -- (N1);

        \diode{D1}

        \draw [ultra thick, rounded corners=2pt]
            (n1) --
            (c3) --
            ++(\a,0) --
            ++(0,\c) --
            ++(-\a,0) --
            (n2);


        %\coordinate (p4) at ($(p1)!.6!(p3)$);
        %\coordinate (p5) at ($(p2)!.7!(p3)$);
        %\fill [red] (p4) circle (1pt) node[right] {p4};
        %\fill [red] (p5) circle (1pt) node[right] {p5};
        %\draw [red] (p4) -- (p5);
        %\fill [opacity=0.5] (p3) -- (p4) -- (p5) -- cycle;


        \coordinate (p4) at ($(p3)!.30!(p2)$);
        \coordinate (p5) at ($(p3)!6!105:(p4)$);
        \coordinate (p6) at (intersection of p1--p3 and p4--p5);
        \fill [opacity=0.5] (p3) -- (p4) -- (p6) -- cycle;

        %\fill [red] (p4) circle (1pt) node[right] {p4};
        %\fill [red] (p5) circle (1pt) node[right] {p5};
        %\fill [red] (p6) circle (1pt) node[right] {p6};
    \fi
    \ifEnergyC
        \begin{scope}[transparency group, opacity=0.2]
            \draw [ultra thick] (N2) -- (C6);
            \draw [ultra thick] (n1) -- (N1);

            \draw [ultra thick, rounded corners=2pt]
                (C1) --
                (c1) --
                ++(\a,0) --
                ++(0,\c) --
                ++(-\a,0) --
                (n1);

            \draw [ultra thick, rounded corners=2pt]
                (n1) --
                (c3) --
                ++(\a,0) --
                ++(0,\c) --
                ++(-\a,0) --
                (n2);

            \diode{D3}
        \end{scope}
        \draw [ultra thick] (C1) -- (N1);
        \draw [ultra thick] (N1) -- (N2);
        \draw [ultra thick, line cap=rect] (n2) -- (N2);

        \diode{D1}
        \diode{D2}

        %\coordinate (p4) at ($(p1)!.28!(p3)$);
        %\coordinate (p5) at ($(p2)!.46!(p3)$);
        %\fill [red] (p4) circle (1pt) node[right] {p4};
        %\fill [red] (p5) circle (1pt) node[right] {p5};
        %\draw [red] (p4) -- (p5);
        %\fill [opacity=0.5] (p3) -- (p4) -- (p5) -- cycle;


        \coordinate (p4) at ($(p3)!.60!(p2)$);
        \coordinate (p5) at ($(p3)!6!105:(p4)$);
        \coordinate (p6) at (intersection of p1--p3 and p4--p5);
        \fill [opacity=0.5] (p3) -- (p4) -- (p6) -- cycle;

        %\fill [red] (p4) circle (1pt) node[right] {p4};
        %\fill [red] (p5) circle (1pt) node[right] {p5};
        %\fill [red] (p6) circle (1pt) node[right] {p6};
    \fi

    \draw [ultra thick, rounded corners=2pt]
        (n2) --
        (c5) --
        ++(\a,0) --
        ++(0,\c) --
        ++(-\a,0) --
        (C6);
    \draw [very thick] (p1) rectangle (p2);
    \node [anchor=south east, font=\scriptsize\bfseries] at (p2) {Potencia #3\%};
    \draw [ultra thick, line cap=rect] (C0) -- (C1);
    \draw [ultra thick, line cap=rect] (C6) -- (C7);
}

\EnergyAtrue
\EnergyBfalse
\EnergyCfalse
\panelfotovoltaico{0}{0}{100}

\EnergyAfalse
\EnergyBtrue
\EnergyCfalse
\panelfotovoltaico{0}{-5}{66}

\EnergyAfalse
\EnergyBfalse
\EnergyCtrue
\panelfotovoltaico{0}{-10}{33}

\end{tikzpicture}
}""")
