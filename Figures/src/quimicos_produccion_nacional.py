g = Figura(**{
    'destino': BASE,
    'parametros': {
        'legend style': {
            'at': '{(0.03,0.96)}',
            'anchor': 'north west',
        },
    },
})
g.eje(**{
    'tipo': 'axis',
    'parametros': {
        'xlabel': '{Fecha, año}',
        'ylabel': '{Producción Anual Argentina, \si{\kilogram}}',
        'xmax': 2016,
    },
})
ini = 43
fin = 59
años = Mercado.extraer("Indec.A{}:A{}".format(ini, fin), int)
ton2kg = lambda l: list(i * 1000 for i in l)
graphs = (
    ("Indec.D{}:D{}".format(ini, fin), 'Urea', {'mark': '10-pointed star'}),
    ("Indec.E{}:E{}".format(ini, fin), 'Amoníaco', {'mark': 'triangle'}),
    ("Indec.C{}:C{}".format(ini, fin), 'Metanol', {'mark': 'star'}),
   #("Indec.F{}:F{}".format(ini, fin), 'Pinturas', {'mark': '+'}),
    ("Indec.M{}:M{}".format(ini, fin), 'Hidrógeno', {'mark': '*'}),
    ("Indec.B{}:B{}".format(ini, fin), 'Benceno', {'mark': 'o'}),
)
for cels, name, param in graphs:
    datos = ton2kg(Mercado.extraer(cels, int))
    g.serie(**{
        'x': años,
        'y': datos,
        'leyenda': name,
        'parametros': param,
    })
    lim = 2008
    index = años.index(lim)
    func, ajustes = recta(años[index:], datos[index:])
    printpns(ajustes['p_value'], ajustes['r_squared'])
    line = r'\draw[densely dotted] (axis cs:{},{:.2f}) -- (axis cs:{},{:.2f});'
    g.objeto(line.format(lim, func(lim), años[-1], func(años[-1])), inicio=True)
g.tex()
