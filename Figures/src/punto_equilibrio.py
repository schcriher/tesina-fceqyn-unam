hoja = "Punto de equilibrio"
data = (
    ( 6, 1),
   #(20, 2),
    (34, 3),
   #(48, 4),
    (62, 5),
   #(76, 6),
)
for row, año in data:
    nivel = Economicidad.extraer("{}.A{}:A{}".format(hoja, row, row + 1))
    fijo = Economicidad.extraer("{}.B{}:B{}".format(hoja, row, row + 1))
    variable = Economicidad.extraer("{}.C{}:C{}".format(hoja, row, row + 1))
    total = Economicidad.extraer("{}.D{}:D{}".format(hoja, row, row + 1))
    ingresos = Economicidad.extraer("{}.E{}:E{}".format(hoja, row, row + 1))
    equilibrio = round(Economicidad.extraer("{}.B{}".format(hoja, row + 2)) * 100, 3)
    g = Figura(**{
        'destino': '{}_{}'.format(BASE, año),
        'parametros': {
            'legend style': {
                'legend columns': 2,
                'anchor': 'south east',
                'at': '{(0.95,0.06)}',
            },
            #'height': r'0.55\figureheight',
            #'width': r'0.55\figurewidth',
        },
        'conv': ('3.0f', '10.2f'),
    })
    g.eje(**{
        'tipo': 'axis',
        'parametros': {
            'xlabel': '{Nivel de producción, \si{\percent}}',
            'xmin': 1,
            'xmax': 100,
            'xtick': '{0, 20, 40, 60, 80, 100}',
            'ylabel': '{Monto, \si{\peso}}',
            'ymin': 0,
            'ymax': 50000000,
            'ytick': '{0, 10000000, 20000000, 30000000, 40000000, 50000000}',
        },
    })
    g.serie(**{
        'x': nivel,
        'y': total,
        'leyenda': "Costos",
    })
    g.serie(**{
        'x': nivel,
        'y': ingresos,
        'leyenda': "Ingresos",
        'parametros': {'line width': '1.5pt'},
    })
    info = (r'\draw[darkgray, densely dashed] (axis cs:{0},{1}) -- (axis cs:{0},{2}) '
            r'node[above, fill=white, font=\footnotesize] {{\SI{{{0:.1f}}}{{\percent}}}};')
    g.objeto(info.format(equilibrio, 0, 38000000, r''.format(equilibrio)))
    node = r'\node[font=\footnotesize] at (axis cs:{},{}) {{{}}};'
    text = 'Año {}'.format(año) if año < 6 else 'Años del 6 al 10'
    g.objeto(node.format(50, 51000000, text))
    g.tex()
