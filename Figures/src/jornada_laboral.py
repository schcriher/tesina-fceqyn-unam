aa = dbd.get('twc/aadp')

S = Sol()
mds = [S.fh(0, n) for n in range(365)]
#mds = Series(mds)
#m = mds.mean()
#for h in (mds.min(), m, mds.max()):
#    print(timedelta(hours=h))
#12:27:38.649196
#12:44:00.376868
#12:58:16.837903
#for d in (-4, +4):
#    print(timedelta(hours=m+d))
# 8:44:00.376868
#16:44:00.376868

días = range(1, 366)
g = Figura(**{
    'destino': BASE,
    'parametros': {
        'legend style': {
            'at': '{(0.97,0.96)}',
            'anchor': 'north east',
        },
    },
    'conv': ('>3.0f', '.3f'),
})
g.eje(**{
    'tipo': 'axis',
    'parametros': {
        'xlabel': '{Mes del año}',
        'ylabel': '{Hora civil}',
        'xmin': 1,
        'xmax': 365,
        'xtick': tick_dayofyear_12,
        'xticklabels': ticklabels_month,
        'x tick label as interval': True,
        'ymin': 0,
        'ymax': 24,
        'ytick': tick(range(0, 25, 6)),
        'yticklabels': tick('{:02.0f}:00'.format(i) for i in range(0, 25, 6)),
    },
})
g.set_default('serie', {'smooth': True, 'gray': True})
g.serie(**{
    'x': días,
    'y': aa.Atardecer,
    'leyenda': 'Atardecer',
    'parametros': {'densely dashdotdotted': True},
})
g.serie(**{
    'x': días,
    'y': mds,
    'leyenda': 'Medio día solar',
    'parametros': {'densely dashed': True},
})
g.serie(**{
    'x': días,
    'y': aa.Amanecer,
    'leyenda': 'Amanecer',
    'parametros': {'densely dashdotted': True},
})
g.set_default('serie', {})


t = r'\node[yshift={}1ex] at (axis cs:183,{}) {{\tiny {} de la jornada laboral, {}}};'
g.serie(**{
    'x': [1, 365],
    'y': [8 + 45/60] * 2,
    'leyenda': False,
})
g.objeto(t.format('+', '8.75', 'Inicio', '08:45'))
g.serie(**{
    'x': [1, 365],
    'y': [16 + 45/60] * 2,
    'leyenda': False,
})
g.objeto(t.format('-', '16.75', 'Final', '16:45'))
g.tex()
