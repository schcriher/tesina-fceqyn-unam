g = Figura(**{
    'destino': BASE,
    'parametros': {
        'legend style': {
            'at': '{(0.03,0.96)}',
            'anchor': 'north west',
        },
    },
})
g.eje(**{
    'tipo': 'axis',
    'parametros': {
        'xlabel': '{Fecha, año}',
        'ylabel': '{Producción mundial, \si{\kilogram}}',
    },
})
g.serie(**{
    'x': Mercado.extraer("Global.B115:B125", int),
    'y': Mercado.extraer("Global.C115:C125", round),
    'leyenda': 'Histórico',
    'parametros': {'mark': '*', 'smooth': True},
})
g.tex(usar_leyendas=True)
