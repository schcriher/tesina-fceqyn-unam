stock_obj = dbf.get_storer('agua').attrs.stock_obj  # 18857000 g ≈ 18.857 m³
pre_max = dbf.get_storer('agua').attrs.pre_max      # 114 mm
keys = ('pmax010', 'pmax025', 'pmax050', 'pmax100')
db = [dbf.get(key) for key in keys]
x = min(dbf.get_storer(key).attrs.afac_obj for key in keys) * 100
y = stock_obj / da
X = [m.index * 100 for m in db]
Y = [m.values / da for m in db]
g = Figura(**{
    'destino': BASE,
    'conv': ('5.1f', '8.3f'),
})
g.eje(**{
    'tipo': 'axis',
    'parametros': {
        'xlabel': '{Uso del terreno para captación, \si{\percent}}',
        'xmin': 0,
        'xmax': 30,
        'ylabel': '{Volumen del stock de \ch{H2O}, \si{\meter\cubed}}',
        'ymin': 0,
        'ymax': 100,
    },
})
g.serie(**{
    'x': X[3],
    'y': Y[3],
    'leyenda': 'Precipitación máxima {:.1f} mm (\SI{{100}}{{\percent}})'.format(pre_max),
})
g.serie(**{
    'x': X[2],
    'y': Y[2],
    'leyenda': 'Precipitación máxima {:.1f} mm (\SI{{50}}{{\percent}})'.format(pre_max * 0.50),
    'parametros': {'dashed': None},
})
g.serie(**{
    'x': X[1],
    'y': Y[1],
    'leyenda': 'Precipitación máxima {:.1f} mm (\SI{{25}}{{\percent}})'.format(pre_max * 0.25),
    'parametros': {'dashdotted': None},
})
g.serie(**{
    'x': X[0],
    'y': Y[0],
    'leyenda': 'Precipitación máxima {:.1f} mm (\SI{{10}}{{\percent}})'.format(pre_max * 0.10),
    'parametros': {'dashdotdotted': None},
})
info = r'\draw[gray, densely dotted] (axis cs:{x1:5.1f},{y1:7.3f}) -- (axis cs:{x2:5.1f},{y2:7.3f}) {t};'
g.objeto(info.format(**{
    'x1': x,
    'y1': 100,
    'x2': x,
    'y2': 0,
    't': r'node[below, yshift=2, font=\tiny] {{{:.2f}}}'.format(x).replace('.', ','),
}), inicio=True)
g.objeto(info.format(**{
    'x1': x,
    'y1': y,
    'x2': 2,
    'y2': y,
    't': r'node[left,  xshift=2, font=\tiny] {{{:.2f}}}'.format(y).replace('.', ','),
}), inicio=True)
g.tex()
