data = (
    ('opt', dbf.get('eop'), dbf.get_storer('eop').attrs.D),
    ('def', dbf.get('ede'), dbf.get_storer('ede').attrs.D),
    ('evf', dbe.get('evf'), dbe.get_storer('evf').attrs.D),
)
for name, db, D in data:
    ymax = max((db.EV.max(), db.ES.max(), db.EE.max()))
    x = db.index
    g = Figura(**{
        'destino': '{}_{}'.format(BASE, name),
        'parametros': {
            'legend style': {
                'anchor': 'north',
                'at': '{(0.50,0.96)}',
                'legend columns': '3',
            },
            'height': '1.5\\figureheight',
            'width': '1.5\\figurewidth',
        },
        'conv': (
            '{}.0f'.format(len(str(math.ceil(x.max())))),
            '{}.0f'.format(len(str(math.ceil(ymax)))),
        ),
    })
    xtick = list(range(x.min()-1, x.max()+365, 365))
    g.eje(**{
        'tipo': 'axis',
        'parametros': {
            'xlabel': r'{Años}',
            'xmin': x.min(),
            'xmax': x.max(),
            'xtick': tick(xtick),
            'xticklabels': tick(range(1, len(xtick), 2), 2),
            'x tick label as interval': None,
            'scaled x ticks': False,
            'scaled y ticks': True,
            'ylabel': r'{Energía, \si{\mega\joule}}',
            'ymax': round(ymax * 1.04, -2),
            'ymin': 0,
            'reverse legend': None,
        },
    })
    g.serie(**{
        'x': x,
        'y': db.EV,
        'leyenda': 'Residual (Venta)',
        'parametros': {'lightgray': None},
    })
    g.serie(**{
        'x': x,
        'y': db.ES,
        'leyenda': 'Stock ({} días)'.format(D),
        'parametros': {'gray': None},
    })
    g.serie(**{
        'x': x,
        'y': db.EE,
        'leyenda': 'Producción',
        'parametros': {'line width': '1.2pt'},
    })
    node = r'\node[gray, right, font=\tiny] at (axis cs:{0},{1:5.0f}) {{{2}}};'
    draw = r'\draw[gray, densely dotted] (axis cs:0,{1:5.0f}) -- (axis cs:{0},{1:5.0f}) node[right, font=\tiny] {{{2}}};'
    xx = x.max()
    yy = db.EE.median()
    objs = [node.format(xx, yy * i, i) for i in range(2)]
    objs.extend([draw.format(xx, yy * i, i) for i in range(2, D+1)])
    for obj in objs:
        g.objeto(obj, inicio=True)
    ndias = len(db.EE)
    naños = ndias / Ndl
    etotal = db.EE.max() * ndias
    ecomprat = db.EC.sum()
    ecompram = ecomprat * 1000 / (3600 * naños * 12)  # MJ → kWh/mes
    print('   ({}) Faltante: {:6.3%}, {:7.0f} MJ, {:6.1f} kWh/mes'.format(name, ecomprat / etotal, ecomprat, ecompram))
    g.tex()
