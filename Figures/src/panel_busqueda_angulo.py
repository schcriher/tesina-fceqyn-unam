f = ffv * fin / 1e6  # eficiencias; J→MJ
B = list(range(0, 91))
E = [sum(Sol(ωi, ωf, ωs, b).E()) * f for b in B]
g = Figura(**{
    'destino': BASE,
    'conv': ('>2.0f', '>5.2f'),
})
g.eje(**{
    'tipo': 'axis',
    'parametros': {
        'xlabel': r'{Ángulo $\beta$, grados}',
        'ylabel': r'{Energía, \si{\mega\joule\per\meter\squared\per\year}}',
        'ymin': 370,
        'ymax': 930,
    },
})
g.serie(**{
    'x': B,
    'y': E,
})
MAX = dba.get_storer('E1').attrs.B   # 27
OPT = dba.get_storer('E1s').attrs.B  # 30
diff = (E[MAX] - E[OPT]) / E[MAX] * 100
node = r'\node[coordinate,pin={{[pin distance={}ex]{}:{{{}}}}}] at (axis cs:{},{:.2f}) {{}};'.format
tm = r'max {}'.format(MAX)
to = r'\parbox{{5em}}{{\centering opt {}\\[-1ex]{{\tiny-{:.3f}\%}}}}'.format(OPT, diff)
g.objeto(node('1.5', 'above', tm, MAX, E[MAX]))
g.objeto(node('2.5', 'below', to, OPT, E[OPT]))
g.tex()
