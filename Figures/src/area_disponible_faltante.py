SAVE(FTEX, HEADER + r"""
\begin{tikzpicture}[x=20pt,y=20pt,>=latex]
\draw[->] (5,6) -- (4.8,6.43) node[xshift=-1,yshift=6] {N};
\def\Y{-80pt}

\coordinate (a) at (1,0);
\coordinate (b) at (0,2);
\coordinate (c) at (12,8);
\coordinate (d) at (13,6);

\coordinate (t1) at ($(b)!.20!(c)$);
\coordinate (t2) at ($(b)!.40!(c)$);
\coordinate (t3) at ($(b)!.60!(c)$);
\coordinate (t4) at ($(b)!.80!(c)$);

\coordinate (b1) at ($(a)!.20!(d)$);
\coordinate (b2) at ($(a)!.40!(d)$);
\coordinate (b3) at ($(a)!.60!(d)$);
\coordinate (b4) at ($(a)!.80!(d)$);

\coordinate (p1) at (c-|b);
\coordinate (p2) at (b|-a);
\coordinate (p3) at (a-|d);
\coordinate (p4) at (d|-c);

\fill[draw=none,fill=gray!30] (a) -- (b) -- (c) -- (d) -- cycle;
\fill[draw=none,fill=gray!30] ([yshift=\Y]b3) -- ([yshift=\Y]t3) -- ([yshift=\Y]c)  -- ([yshift=\Y]d)  -- cycle;
\fill[draw=none,fill=white] ([yshift=\Y]a)  -- ([yshift=\Y]b)  -- ([yshift=\Y]t3) -- ([yshift=\Y]b3) -- cycle;

\foreach \y in {0,\Y}{
    \draw ([yshift=\y]a) -- ([yshift=\y]b) -- ([yshift=\y]c) -- ([yshift=\y]d) -- cycle;
    \draw ([yshift=\y]t1) -- ([yshift=\y]b1);
    \draw ([yshift=\y]t2) -- ([yshift=\y]b2);
    \draw ([yshift=\y]t3) -- ([yshift=\y]b3);
    \draw ([yshift=\y]t4) -- ([yshift=\y]b4);
}

\draw [|-|] ([xshift=2ex]d) -- ([xshift=2ex,yshift=\Y]d) node[midway,right]{$Y_{sep}$};
\draw [|-|] ([xshift=7ex]d) -- ([xshift=7ex]p3) node[midway,right]{$\Ydiff$};

\coordinate (C) at (intersection of [yshift=\Y]a--[yshift=\Y]d and p2--p3);
\draw [line cap=round,line join=round,ultra thick] (C) -- ([yshift=\Y]d) -- (p3) {};
\draw ([xshift=35]C) arc (0:45:1) node[anchor=west,xshift=5,yshift=-5.5]{$\epsilon$};

\draw[densely dashed] (p1) -- (p2) -- (p3) -- (p4) -- cycle;

\node [anchor=north west, font=\scriptsize] at (p1) {Límite construible};

\end{tikzpicture}
""")
