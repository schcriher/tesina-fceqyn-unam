SAVE(FTEX, HEADER + r"""
\begin{tikzpicture}[x=20pt,y=20pt,>=latex]
\tikzset{
    decoration=snake,
    line around/.style={decoration={pre length=#1,post length=#1}},
}
\draw[->] (5,6) -- (4.8,6.43) node[xshift=-1,yshift=6] {N};

\coordinate (a) at (1,0);
\coordinate (b) at (0,2);
\coordinate (c) at (12,8);
\coordinate (d) at (13,6);

\coordinate (t1) at ($(b)!.33!(c)$);
\coordinate (t2) at ($(b)!.66!(c)$);
\coordinate (b1) at ($(a)!.33!(d)$);
\coordinate (b2) at ($(a)!.66!(d)$);

\coordinate (p1) at (b|-a);
\coordinate (p2) at (c-|b);
\coordinate (p3) at (d|-c);
\coordinate (p4) at (a-|d);

\fill[draw=none,fill=gray!30] (a) -- (b) -- (c) -- (d) -- cycle;
\draw[decorate,line around=185] (b) -- (c);
\draw[decorate,line around=185] (a) -- (d);
\draw (a) -- (b);
\draw (c) -- (d);
\draw (t1) -- (b1);
\draw (t2) -- (b2);

\draw[densely dashed] ([yshift=-6]p1) -- (p2);
\draw[densely dashed] (p3) -- ([yshift=-6]p4);
\draw [|-|] ([xshift=-0.25,yshift=8]p2) -- ([yshift=8]p3) node[midway,above]{$X_{lim}$};

\draw (b) -- (p1) -- (a);
\draw ([yshift=-28]b) arc (-90:-53:1) node[anchor=north,xshift=-5]{$\epsilon$};
\draw [|-|] ([yshift=-12]p1) -- ([yshift=-12]a) node[midway,below]{$\Xdiff$};

\draw (a) -- (p4) -- (d);
\draw ([xshift=28]a) arc (0:36:1) node[anchor=west,yshift=-4]{$\epsilon$};
\draw [|-|]  ([xshift=-0.25,yshift=-12]a) -- ([xshift=0.25,yshift=-12]a-|d) node[midway,below]{$X_{paneles}$};

\coordinate (p7) at ($(a)!.5!(t1)$);
\coordinate (p8) at ($(c)!.5!(b2)$);

\path (p7.center) node[rotate=26.5]{Panel 1};
\path (p8.center) node[rotate=26.5]{Panel $N_X$};

%\node at (a) {a};
%\node at (b) {b};
%\node at (c) {c};
%\node at (d) {d};

%\node at (t1) {t1};
%\node at (t2) {t2};
%\node at (b1) {b1};
%\node at (b2) {b2};

%\node at (p1) {p1};
%\node at (p2) {p2};
%\node at (p3) {p3};
%\node at (p4) {p4};

\end{tikzpicture}
""")
