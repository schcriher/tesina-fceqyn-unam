g = Figura(**{
    'destino': BASE,
    'parametros': {
        'legend style': {
            'at': '{(0.03,0.96)}',
            'anchor': 'north west',
        },
    },
})
g.eje(**{
    'tipo': 'axis',
    'parametros': {
        'xlabel': '{Fecha, año}',
        'ylabel': '{Masa, \si{\kilogram}}',
    },
})
años = Mercado.extraer("Resumen.A4:A18")
g.serie(**{
    'x': años[:-1],
    'y': Mercado.extraer("Resumen.B4:B18")[:-1],
    'leyenda': 'Importaciones',
    'parametros': {'mark': '*'},
})
g.serie(**{
    'x': años,
    'y': Mercado.extraer("Resumen.E4:E18"),
    'leyenda': 'Exportaciones',
    'parametros': {'mark': 'o'},
})
g.tex()
