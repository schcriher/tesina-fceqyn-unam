g = Figura(**{
    'destino': BASE,
    'parametros': {
        'legend style': {
            'at': '{(0.50,0.96)}',
            'anchor': 'north',
            'legend columns': '3',
        },
    },
    'conv': ('.0f', '>6.3f'),
})
g.eje(**{
    'tipo': 'axis',
    'parametros': {
        'xlabel': '{Fecha, año}',
        'ylabel': '{Precios, \si{\dolar\per\kilogram}}',
        'xmin': 2002,
        'xmax': 2015,
    },
})
g.set_default('serie', {'smooth':True})
años = Mercado.extraer("Resumen.A4:A17")
g.serie(**{
    'x': años,
    'y': Mercado.extraer("Resumen.D4:D17"),
    'leyenda': 'Importación',
    'parametros': {'mark': '+'},
})
g.serie(**{
    'x': Mercado.extraer("Resumen.A4:A18"),
    'y': Mercado.extraer("Resumen.G4:G18"),
    'leyenda': 'Exportación',
    'parametros': {'mark': 'x'},
})
g.serie(**{
    'x': años,
    'y': Mercado.extraer("Resumen.H4:H17"),
    'leyenda': 'Promedio Ponderado',
    'parametros': {'mark': '*'},
})
g.tex()
