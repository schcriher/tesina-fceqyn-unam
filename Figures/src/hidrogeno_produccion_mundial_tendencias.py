g = Figura(**{
    'destino': BASE,
    'parametros': {
        'legend style': {
            'at': '{(0.03,0.96)}',
            'anchor': 'north west',
        },
    },
    'conv': ('.0f', '.0f'),
})
g.eje(**{
    'tipo': 'axis',
    'parametros': {
        'xlabel': '{Fecha, año}',
        'ylabel': '{Producción mundial, \si{\kilogram}}',
    },
})
g.set_default('serie', {'smooth': True})
# Histórico
g.serie(**{
    'x': Mercado.extraer("Global.B4:B8"),
    'y': Mercado.extraer("Global.C4:C8"),
    'leyenda': 'Histórico',
    'parametros': {'mark': '*'},
})
# Proyecciones
g.add_set_default('serie', {'densely dashed': True})
años = Mercado.extraer("Global.B8:B16")
leyenda = 'Crecimiento del \SI{{{:.1f}}}{{\percent}}'
g.serie(**{
    'x': años,
    'y': Mercado.extraer("Global.C8,D9:D16"),
    'leyenda': leyenda.format(Mercado.extraer("Global.D17") * 100),
    'etiqueta': 'Crecimiento alto',
    'parametros': {'mark': 'x'},
})
g.serie(**{
    'x': años,
    'y': Mercado.extraer("Global.C8,H9:H16"),
    'leyenda': 'Crecimiento promedio',
    'parametros': {'mark': '*'},
})
g.serie(**{
    'x': años,
    'y': Mercado.extraer("Global.C8,C9:C16"),
    'leyenda': leyenda.format(Mercado.extraer("Global.C17") * 100),
    'etiqueta': 'Crecimiento bajo',
    'parametros': {'mark': '+'},
})
g.tex()
