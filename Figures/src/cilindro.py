SAVE(FTEX, HEADER + r"""
\begin{tikzpicture}[x=1mm,y=1mm,scale=1]

\tikzset{%
    rounded line/.style={rounded corners=0.3mm},%
    dimension line/.style={line width=0.8},%
    dimension line 1/.style={dimension line, Stealth-},%
    dimension line 2/.style={dimension line, Stealth-Stealth},%
}

% FUENTE: ~/.local/soft/texlive/texmf-dist/tex/generic/pgf/libraries/pgflibrarypatterns.code.tex
%
% Base: "north west lines"
\pgfdeclarepatternformonly{case}{\pgfqpoint{-1pt}{-1pt}}{\pgfqpoint{4pt}{4pt}}{\pgfqpoint{2.88pt}{2.88pt}}%
{%
  \pgfsetlinewidth{0.4pt}
  \pgfpathmoveto{\pgfqpoint{0pt}{3pt}}
  \pgfpathlineto{\pgfqpoint{3.1pt}{-0.1pt}}
  \pgfusepath{stroke}
}
%
% Base: "horizontal lines"
\pgfdeclarepatternformonly{screw}{\pgfpointorigin}{\pgfqpoint{100pt}{1pt}}{\pgfqpoint{100pt}{1.25pt}}%
{%
  \pgfsetlinewidth{0.5pt}
  \pgfpathmoveto{\pgfqpoint{0pt}{0.5pt}}
  \pgfpathlineto{\pgfqpoint{100pt}{0.5pt}}
  \pgfusepath{stroke}
}

% Lineas para las cotas
\draw[gray] (0,108) -- (-20,108);
\draw[gray] (4,87.8) -- (20,87.8);
\draw[gray] (-20,-7) -- (20,-7);

% Soporte
% Soporte externo
\draw[rounded line]
    (-7,80.95)
    --
    (-5,85)
    --
    (-5,87.2)
    --
    (-4.5,87.8)
    --
    (4.5,87.8)
    --
    (5,87.2)
    --
    (5,85)
    --
    (7,80.95);
% Soporte interno
\draw[rounded line]
    (-6.8,81.1)
    --
    (-4.8,85)
    --
    (-4.8,87)
    --
    (-4.4,87.5)
    --
    (4.4,87.5)
    --
    (4.8,87)
    --
    (4.8,85)
    --
    (6.8,81.1);
% Linea horizontal
\draw[line width=1.12] (-3.57,87.65) -- (3.57,87.65);

% Casco
\draw[rounded line]
    (-4.8,87.4)
    --
    (-5.2,89)
    --
    (-5.2,103)
    .. controls (-5.2,105.5) and (-3,108) ..
    (0,108) % punta
    .. controls (3,108) and (5.2,105.5) ..
    (5.2,103)
    --
    (5.2,89)
    --
    (4.8,87.4);

% Orificio
\draw (0,103.5) circle (0.75mm);

% Rosca
\fill[pattern=screw]
    (1.5,81.5)
    --
    (2,87.5)
    --
    (-2,87.5)
    --
    (-1.5,81.5)%
    --
    cycle;

% Botella
\draw[pattern=case]
    (-11,-2)
    .. controls (-11,-3) and (-10.5,-4) ..
    (-9,-4)
    .. controls (-6,-4) and (-6,0) ..   
    (0,0) %---------------------------------- simetría
    .. controls (6,0) and (6,-4) ..
    (9,-4)
    .. controls (10.5,-4) and (11,-3) ..
    (11,-2)
    -- % Pared vertical, derecha, interna
    (11,71)
    .. controls (11,76) and (5.5,80.25) ..
    %%% Curva interna derecha %%%%%%%%%%%%%%%
    (2.15,80.75)%
    .. controls (2,80.75) and (1.5,81) ..
    (1.5,81.5)%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    --
    (2,87.5)
    --
    (3.5,87.5)
    --
    %%% Curva externa derecha %%%%%%%%%%%%%%%
    (3.5,84)
    .. controls (3.5,83.2) and (4.0,82.7) ..
    (4.5,82.5)
    .. controls (6,81.9) and (12,78.25) ..
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    (12,71)
    -- % Pared vertical, derecha, externa
    (12,-3)
    .. controls (12,-5.5) and (10.5,-7) ..
    (8.5,-7)
    .. controls (6,-7) and (5,-3.5) ..
    (2.1,-3.1)
    .. controls (2,-3.5) and (1.25,-4.5) ..
    (0,-4.5) %------------------------------- simetría
    .. controls (-1.25,-4.5) and (-2,-3.5) ..
    (-2.1,-3.1)
    .. controls (-5,-3.5) and (-6,-7) ..
    (-8.5,-7)
    .. controls (-10.5,-7) and (-12,-5.5) ..
    (-12,-3)
    -- % Pared vertical, izquierda, externa
    (-12,71)
    %%% Curva externa izquierda %%%%%%%%%%%%%
    .. controls (-12,78.25) and (-6,81.9) ..
    (-4.5,82.5)
    .. controls (-4.0,82.7) and (-3.5,83.2) ..
    (-3.5,84)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    --
    (-3.5,87.5)
    --
    (-2,87.5)
    --
    %%% Curva interna izquierda %%%%%%%%%%%%%
    (-1.5,81.5)
    .. controls (-1.5,81) and (-2,80.75) ..
    (-2.15,80.75)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    .. controls (-5.5,80.25) and (-11,76) ..
    (-11,71)
    -- % Pared vertical, izquierda, interna
    cycle;

% Cotas
\draw[dimension line 2] (-18,-7) -- (-18,108)  node[midway,left]{$L_t$};
\draw[dimension line 2] ( 18,-7) -- ( 18,87.8) node[midway,right]{$L_c$};
\draw[dimension line 2] (-12,30) -- ( 12,30)   node[midway,below]{$D_{\!e}$};
\draw[dimension line 1] (-11,45) -- ( -7,45)   node[right]{$E_p$};
\draw[dimension line 1] (-12,45) -- (-16,45);

% Lineas de ayuda
%\draw[red,opacity=0.3]
%    (11,71)
%    .. controls (11,76) and (5.5,81) ..
%    (0,81)
%    .. controls (-5.5,81) and (-11,76) ..
%    (-11,71)
%    ;
%\draw[red,opacity=0.3]
%    (12,71)
%    .. controls (12,78) and (6,83.5) ..
%    (0,83.5)
%    .. controls (-6,83.5) and (-12,78)..
%    (-12,71)
%    ;

\end{tikzpicture}
""")
