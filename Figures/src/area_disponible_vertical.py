SAVE(FTEX, HEADER + r"""
\begin{tikzpicture}[x=20pt,y=20pt,>=latex]
\tikzset{
    decoration=snake,
    line around/.style={decoration={pre length=#1,post length=#1}},
}
\draw[->] (5,6) -- (4.8,6.43) node[xshift=-1,yshift=6] {N};
\def\Y{-100pt}

\coordinate (a) at (1,0);
\coordinate (b) at (0,2);
\coordinate (c) at (12,8);
\coordinate (d) at (13,6);

\coordinate (t1) at ($(b)!.33!(c)$);
\coordinate (t2) at ($(b)!.66!(c)$);
\coordinate (b1) at ($(a)!.33!(d)$);
\coordinate (b2) at ($(a)!.66!(d)$);

\coordinate (p1) at (b|-a);
\coordinate (p2) at (b|-c);
\coordinate (p3) at (d|-c);
\coordinate (p4) at (a-|d);

\coordinate (p5) at (a-|b1);
\coordinate (p6) at (b2-|d);

\coordinate (p7) at ($(a)!.5!(t1)$);
\coordinate (p8) at ($(c)!.5!(b2)$);

%\coordinate (p9) at (intersection of b--$(a)!\Y!(b)$ and [yshift=\Y]b--[yshift=\Y]c);
\coordinate (p9) at ($([yshift=\Y]b)!(b)!([yshift=\Y]c)$);

\foreach \y in {0,\Y}{
    \fill[draw=none,fill=gray!30] ([yshift=\y]a) -- ([yshift=\y]b) -- ([yshift=\y]c) -- ([yshift=\y]d) -- cycle;
    \draw[decorate,line around=185] ([yshift=\y]b) -- ([yshift=\y]c);
    \draw[decorate,line around=185] ([yshift=\y]a) -- ([yshift=\y]d);
    \draw ([yshift=\y]a) -- ([yshift=\y]b);
    \draw ([yshift=\y]c) -- ([yshift=\y]d);
    \draw ([yshift=\y]t1) -- ([yshift=\y]b1);
    \draw ([yshift=\y]t2) -- ([yshift=\y]b2);
    \path ([yshift=\y]p7.center) node[rotate=26.5]{Panel 1};
    \path ([yshift=\y]p8.center) node[rotate=26.5]{Panel $N_X$};
}

\draw (b) -- (p2) -- (c);
\draw ([xshift=-40]c) arc (0:52:-1) node[anchor=east,xshift=-4,yshift=5]{$\epsilon$};
\draw [|-|] ([xshift=-12]p2) -- ([xshift=-12]b) node[midway,left]{$\Ydiff$};

\draw (b) -- ([yshift=\Y]b);
\draw [ultra thick] (b) -- (p9) node[midway,right,xshift=2,yshift=-4]{$Y_{psep}$};
\draw [ultra thick] ([yshift=\Y]b) -- (p9) node[midway,above,xshift=-3,yshift=1]{$X_{\epsilon}$};
\draw ([yshift=-30]b) arc (-90:-52:1) node[anchor=north,xshift=-5,yshift=-2]{$\epsilon$};
\draw [|-|] ([xshift=-12]b) -- ([xshift=-12,yshift=\Y]b) node[midway,left]{$Y_{sep}$};

\draw ([yshift=\Y]b) -- ([yshift=\Y]p1) -- ([yshift=\Y]a) {};
\draw ([yshift=\Y-30]b) arc (-90:-52:1) node[anchor=north,xshift=-5,yshift=-2]{$\epsilon$};
\draw [|-|] ([xshift=-12,yshift=\Y+0.25]b) -- ([xshift=-12,yshift=\Y]p1) node[midway,left]{$Y_{pp}$};

\draw[densely dashed] ([xshift=-5]p2) -- (p3) {};
\draw[densely dashed] ([xshift=-5,yshift=\Y]p1) -- ([yshift=\Y]p4) {};
\draw [|-|] ([xshift=6]p3) -- ([xshift=6,yshift=\Y]p4) node[midway,right]{$Y_{lim}$};

\end{tikzpicture}
""")
