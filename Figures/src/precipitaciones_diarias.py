db = dbd.get('inta/P')['2005':'2010']   # Precipitación, mm
g = Figura(**{
    'destino': BASE,
    'conv': ('4.0f', '5.1f'),
})
dt0 = pandas.Timestamp(dbd.root.inta.P.index[0])
g.eje(**{
    'tipo': 'axis',
    'parametros': {
        'xlabel': '{Fecha, año}',
        'ylabel': '{Precipitación diaria, \si{\milli\meter}}',
        'x tick label as interval': True,
        'xtick': tick((datetime.datetime(y, 1, 1) - dt0).days for y in range(2005, 2012)),
        'xticklabels': tick(range(2005, 2011)),
    },
})
g.serie(**{
    'x': [(dt - dt0).days for dt in db.index],
    'y': db,
})
g.tex()
