P = dbd.get('inta/P')  # Precipitación, mm
P = P[P > 0]
Pmax = P.max()
g = Figura(**{
    'destino': BASE,
    'conv': ('7.3f', '3.0f'),
})
bins = 5
parametros = {
    'bins': bins,
    'xlabel': '{Precipitación diaria, \si{\milli\meter}}',
    'ylabel': 'Ocurrencia (2005-2010)',
    'vcount': False,
    'xmin': 0,
    'xmax': Pmax,
    'xtick': tick(numpy.linspace(0, Pmax, bins + 1).round().astype(int)),
}
porcentaje = {
    'round': 0,
    'interval': True,
    'acumulative': False,
}
histograma(g, P, parametros, porcentaje)
