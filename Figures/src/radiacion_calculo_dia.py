from Library.TikZ import node, draw, fill

ŋ = 184  # Día juliano del año
ĸ = 24   # Divisiones del gráfico
ø = 4    # Numero de dígitos de redondeo

s = 3.5 / (ĸ - 1)
S = Sol(0, ωr, s, β, True)
# X
Ω = S.ω[ŋ]
T = Ω * 8 / max(Ω)
# Y
I = S.I(ffv * fin)[ŋ]
I = I - 0.9 * min(I)
P = I * 7.6 / max(I)

Tamin = 0
Tamax = 8
Pamin = 0
Pamax = 8

Taspn = [Tamin, Tamax]
Paspn = [Pamin, Pamax]

Tvmin = min(T)
Tvmax = max(T)
Pvmin = min(P)
Pvmax = max(P)

Tvspn = [Tvmin, Tvmax]
Pvspn = [Pvmin, Pvmax]

a = int(ĸ * 0.40)
b = int(ĸ * 0.53)
c = int(ĸ * 0.87)

Tmax = T[a]
Tobj = T[b]
Tmin = T[c]

Pmax = P[a]
Pobj = P[b]
Pmin = P[c]

S = numpy.where(P >= Pmax, P, 0)                  # Energía Sobrante
I = numpy.where(P <= Pmin, P, 0)                  # Energía Insuficiente
W = numpy.where(P <= Pobj, P, 0)                  # Energía Utilizable comprando energía
U = numpy.where((P >= Pmin) & (P <= Pobj), P, 0)  # Energía Utilizable sin comprar energía
C = numpy.where((P >= Pobj) & (P <= Pmax), P, 0)  # Energía Utilizable para llenar cilindros

def setpoints(x, y, X, Y):
    for i, j in zip(X, Y):
        if j:
            x.append(i)
            y.append(j)

def energía_sobrante(*args, **kwargs):
    x = [Tamin]
    y = [Pmax]
    setpoints(x, y, T, S)
    kwargs['cycle'] = True
    return fill(x, y, *args, **kwargs)

def energía_insuficiente(*args, **kwargs):
    x = [Tvmax, Tmin]
    y = [Pamin, Pamin]
    setpoints(x, y, T, I)
    kwargs['cycle'] = True
    return fill(x, y, *args, **kwargs)

def energía_utilizable_para_llenado_de_cilindros(*args, **kwargs):
    x = [Tamin, Tamin]
    y = [Pobj, Pmax]
    setpoints(x, y, T, C)
    kwargs['cycle'] = True
    return fill(x, y, *args, **kwargs)

def energía_utilizable_sin_comprar_energía(*args, **kwargs):
    x = [Tmin, Tamin, Tamin]
    y = [Pamin, Pamin, Pobj]
    setpoints(x, y, T, U)
    kwargs['cycle'] = True
    return fill(x, y, *args, **kwargs)

def energía_utilizable_comprando_energía(*args, **kwargs):
    x = [Tvmax, Tamin, Tamin]
    y = [Pamin, Pamin, Pobj]
    setpoints(x, y, T, W)
    kwargs['cycle'] = True
    return fill(x, y, *args, **kwargs)

def energía_comprada(*args, **kwargs):
    x = [Tvmax]
    y = [Pobj]
    setpoints(x, y, T, W)
    kwargs['cycle'] = True
    return fill(x, y, *args, **kwargs)

CODES = [r'''
\begin{tikzpicture}[x=22pt,y=22pt]

% FUENTE: ~/.local/soft/texlive/texmf-dist/tex/generic/pgf/libraries/pgflibrarypatterns.code.tex
%
% BASE: horizontal lines light gray
\pgfdeclarepatterninherentlycolored{process energy}
{\pgfpointorigin}
{\pgfpoint{100pt}{4pt}}
{\pgfpoint{100pt}{4pt}}
{
  \pgfsetfillcolor{black!10}
  \pgfpathrectangle{\pgfpointorigin}{\pgfpoint{100.5pt}{2.5pt}}
  \pgfusepath{fill}
  \pgfsetfillcolor{black!15}
  \pgfpathrectangle{\pgfpoint{0pt}{2pt}}{\pgfpoint{100.5pt}{2.5pt}}
  \pgfusepath{fill}
}
%
% BASE: horizontal lines gray
\pgfdeclarepatterninherentlycolored{buy energy}
{\pgfpointorigin}
{\pgfpoint{100pt}{4pt}}
{\pgfpoint{100pt}{4pt}}
{
  \pgfsetfillcolor{black!30}
  \pgfpathrectangle{\pgfpointorigin}{\pgfpoint{100.5pt}{2.5pt}}
  \pgfusepath{fill}
  \pgfsetfillcolor{black!35}
  \pgfpathrectangle{\pgfpoint{0pt}{2pt}}{\pgfpoint{100.5pt}{2.5pt}}
  \pgfusepath{fill}
}
''']
CODES.extend((
    energía_sobrante('pattern=dots'),
    energía_insuficiente('pattern=dots'),
    energía_utilizable_para_llenado_de_cilindros('pattern=north east lines'),
    energía_utilizable_sin_comprar_energía('pattern=process energy'),
    draw(Tmin, [Pamin, Pmin]),
    draw(Tvspn, Pmax, 'gray'),
    draw(Tvspn, Pobj, 'gray'),
    draw(Tvspn, Pmin, 'gray'),
    draw(T, P, 'line width=1'),
    draw([Tvmin, Tvmin, Tvmax, Tvmax], [Pamin, Pamax, Pamax, Pamin], cycle=True),
))
graphsep = 2
xoffset = Tvmax + graphsep
CODES.extend((
    energía_sobrante('pattern=dots', xoffset=xoffset),
    energía_utilizable_para_llenado_de_cilindros('pattern=north east lines', xoffset=xoffset),
    energía_utilizable_comprando_energía('pattern=process energy', xoffset=xoffset),
    energía_comprada('pattern=buy energy', xoffset=xoffset),
    draw(Tvspn, Pmax, 'gray', xoffset=xoffset),
    draw(Tvspn, Pobj, 'gray', xoffset=xoffset),
    draw(Tvspn, Pmin, 'gray, densely dotted', xoffset=xoffset),
    draw(T, P, 'line width=1', xoffset=xoffset),
    draw([Tvmin, Tvmin, Tvmax, Tvmax], [Pamin, Pamax, Pamax, Pamin], cycle=True, xoffset=xoffset),
))
x = Tvmax + graphsep / 2
CODES.extend((
    node(x, Pmax, r'\texttt{Pmax}'),
    node(x, Pobj, r'\texttt{Pobj}'),
    node(x, Pmin, r'\texttt{Pmin}'),
))
w = 1
h = 1
x = (0, w, w, 0)
y = (0, 0, h, h)
xsep = (2 * Tvmax + graphsep) / 4
xini = xsep / 2
xoffset = [xini + xsep * i - w / 2 for i in range(4)]
yoffset = -2.5
CODES.extend((
    draw(x, y, 'pattern=dots',             xoffset=xoffset[0], yoffset=yoffset, cycle=True),
    draw(x, y, 'pattern=north east lines', xoffset=xoffset[1], yoffset=yoffset, cycle=True),
    draw(x, y, 'pattern=process energy',   xoffset=xoffset[2], yoffset=yoffset, cycle=True),
    draw(x, y, 'pattern=buy energy',       xoffset=xoffset[3], yoffset=yoffset, cycle=True),
))
x = w / 2
y = yoffset
CODES.extend((
    node(x, y, 'Energía residual',   'below', xoffset=xoffset[0]),
    node(x, y, 'Energía al stock',   'below', xoffset=xoffset[1]),
    node(x, y, 'Energía de proceso', 'below', xoffset=xoffset[2]),
    node(x, y, 'Energía a comprar',  'below', xoffset=xoffset[3]),
))
x0 = Tamin - 0.3
x1 = Tvmax / 2
x2 = x1 + Tvmax + graphsep
CODES.extend((
    node(x1, Pamax + 0.5, 'Aislado de la Red Eléctrica'),
    node(x2, Pamax + 0.5, 'Conectado a la Red Eléctrica'),
    node(x1, Pamin, 'Tiempo', 'below'),
    node(x2, Pamin, 'Tiempo', 'below'),
    node(x0, Pamax / 2, 'Potencia', 'rotate=90'),
))
CODES.append(r'\end{tikzpicture}')
SAVE(FTEX, HEADER + '\n'.join(CODES))
