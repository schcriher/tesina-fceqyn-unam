hoja = "Período de reembolso anticipado"
ini = 2
fin = 12
año = Economicidad.extraer("{}.A{}:A{}".format(hoja, ini, fin))
acumulado = Economicidad.extraer("{}.E{}:E{}".format(hoja, ini, fin))
cruce = Economicidad.extraer("{}.F{}:F{}".format(hoja, ini, fin))
cruce = [i for i in cruce if i][0]
g = Figura(**{
    'destino': BASE,
    'conv': (
        '{}.0f'.format(len(str(max(año)))),
        '{}.2f'.format(len(str(max(acumulado)))),
    ),
})
g.eje(**{
    'tipo': 'axis',
    'parametros': {
        'xlabel': '{Año}',
        'ylabel': '{Flujo, \si{\peso}}',
    },
})
g.serie(**{
    'x': año,
    'y': acumulado,
})
g.objeto(r'\draw (axis cs:0,0) -- (axis cs:10,0);')
ymin = round(min(acumulado) * 0.8)
ymax = round(max(acumulado) * 0.8)
info = (r'\draw[darkgray, densely dashed] (axis cs:{0},{1}) -- (axis cs:{0},{2}) '
        r'node[above, fill=white, font=\tiny] {{\SI{{{0:.2f}}}{{\year}}}};')
g.objeto(info.format(cruce, ymin, ymax))
g.tex()
