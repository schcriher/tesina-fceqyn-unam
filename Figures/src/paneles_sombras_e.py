SAVE(FTEX, HEADER + r"""{%
\begin{tikzpicture}[x=40pt,y=40pt,>=latex]

\tikzset{
    line/.style = {<->, gray},
    cota/.style = {midway, black},
}

\coordinate (a) at (0,0);
\path [draw=black, fill=lightgray, rotate=32] (a) --
    ++( 3,0) node[coordinate](b){} --
    ++( 0,2) node[coordinate](c){} --
    ++(-3,0) node[coordinate](d){} --
    cycle;

\coordinate (e) at (a-|d);
\coordinate (f) at (a-|b);
\coordinate (g) at (b|-c);
\coordinate (h) at (c-|d);

%\coordinate (i) at (intersection of a--c and b--d);
%\node [font=\scriptsize, rotate=32] at (i) {Panel};

\draw ([yshift=-25]d) arc (270:291:1) node[anchor=north, xshift=-8, yshift=9] {$\epsilon$};

\draw [line] (e) -- (a) node [cota, below] {$x_1$};
\draw [line] (c) -- (g) node [cota, above] {$x_1$};

\draw [line] (a) -- (f) node [cota, below] {$x_2$};
\draw [line] (c) -- (h) node [cota, above] {$x_2$};

\draw [line] (d) -- (e) node [cota, left] {$y_1$};
\draw [line] (b) -- (g) node [cota, right] {$y_1$};

\draw [line] (d) -- (h) node [cota, left] {$y_2$};
\draw [line] (b) -- (f) node [cota, right] {$y_2$};

%\fill [red] (a) circle (1pt) node[above] {a};
%\fill [red] (b) circle (1pt) node[above] {b};
%\fill [red] (c) circle (1pt) node[above] {c};
%\fill [red] (d) circle (1pt) node[above] {d};

%\fill [red] (e) circle (1pt) node[above] {e};
%\fill [red] (f) circle (1pt) node[above] {f};
%\fill [red] (g) circle (1pt) node[above] {g};
%\fill [red] (h) circle (1pt) node[above] {h};

\end{tikzpicture}
}""")
