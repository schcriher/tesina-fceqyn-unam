SAVE(FTEX, HEADER + r"""{%
\begin{tikzpicture}[>=latex]

\draw[fill=gray!30] (9,3) -- (8,3) -- (8,7) -- (9,7);  % Edificio
\node[rotate=90] at (8.5,5) {Edificio};

\draw[line width=4,yshift=1.41, gray] (1,4) -- (2,3);  % Panel
\draw[line width=4,yshift=1.41, gray] (3,4) -- (4,3);  % Panel
\draw[line width=4,yshift=1.41, gray] (5,4) -- (6,3);   % Panel

\draw (0.5,3) -- (9,3);   % Piso de arriba

\draw[line width=4,yshift=1.41, gray, xshift=1.41] (8,8) -- (9,7);   % Panel, arriba

\draw[densely dotted] (1.65,3) -- (8.45,8.35) node[above] {Sol};
\draw (2.85,3) arc (0:45:1) node[anchor=north,yshift=-5.5] {$\alpha_y$};

\draw[xshift=10]  [|-|] (9,3) -- (9,7)               node[midway,right]{$H_{inst}$};
\draw[yshift=-10] [|-|] ([xshift=-1.8]6,3) -- (8,3)  node[midway,below]{$I_{hsep}$};
\draw[yshift=-10, xshift=-1.41] [|-|] (4,3) -- (6,3) node[midway,below]{$Y_{sep}$};

\end{tikzpicture}
}""")
