for b in (0, 30):
    s = Sol(-12, 12, 5/60, b)
    h = s.h()
    I = s.I()
    # horas
    TO = h[s.neo]
    TI = h[s.nsi]
    TP = h[s.nep]
    TV = h[s.nsv]
    # W/m²
    IO = I[s.neo]
    II = I[s.nsi]
    IP = I[s.nep]
    IV = I[s.nsv]
    g = Figura(**{
        'destino': '{}_b{}'.format(BASE, b),
        'parametros': {
            'legend style': {
                'at': '{(0.03,0.96)}',
                'anchor': 'north west',
            },
        },
        'conv': ('5.2f', '>6.2f'),
    })
    g.eje(**{
        'tipo': 'axis',
        'parametros': {
            'xlabel': '{Horas del día}',
            'ylabel': '{Irradiación, \si{\W\per\m\squared}}',
            'xmin': 0,
            'xmax': 24,
            'xtick': tick(range(0, 25, 6)),
            'xticklabels': tick('{:02.0f}:00'.format(i) for i in range(0, 25, 6)),
            'ymin': 0,
        },
    })
    g.set_default('serie', {'smooth':True})
    g.serie(**{
        'x': TV,
        'y': IV,
        'leyenda': 'Solsticio de verano',
        'parametros': {'densely dashed': True},
    })
    g.serie(**{
        'x': TO,
        'y': IO,
        'leyenda': 'Equinoccio de otoño',
        'parametros': {'densely dashdotdotted': True},
    })
    g.serie(**{
        'x': TP,
        'y': IP,
        'leyenda': 'Equinoccio de primavera',
        'parametros': {'densely dashdotted': True},
    })
    g.serie(**{
        'x': TI,
        'y': II,
        'leyenda': 'Solsticio de invierno',
        'parametros': {'densely dotted': True},
    })
    info = r'\draw[gray] (axis cs: {0:5.2f},{1:>6.2f}) -- (axis cs: {0:5.2f},  0.00) node[below, font=\tiny] {{{2}}};'
    ymax = max(max(IV), max(IO), max(IP), max(II))
    g.objeto(info.format( 8.75, ymax,  '8:45'))
    g.objeto(info.format(16.75, ymax, '16:45'))
    beta = r'\draw (axis cs:24.00,{:>6.2f}) node[below left, font=\scriptsize] {{$\beta = \ang{{{}}}$}};'
    g.objeto(beta.format(ymax, b))
    g.tex()
