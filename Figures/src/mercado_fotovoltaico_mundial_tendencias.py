g = Figura(**{
    'destino': BASE,
    'parametros': {
        'legend style': {
            'at': '{(0.03,0.96)}',
            'anchor': 'north west',
        },
    },
})
g.eje(**{
    'tipo': 'axis',
    'parametros': {
        'xlabel': '{Fecha, año}',
        'ylabel': '{Potencia, \si{\watt}}',
    },
})
# Histórico
años = Mercado.extraer("PV.A10:A20", int)
g.serie(**{
    'x': años,
    'y': Mercado.extraer("PV.B10:B20", round),
    'leyenda': 'Potencia acumulada',
    'parametros': {'mark': '*'},
})
g.serie(**{
    'x': años,
    'y': Mercado.extraer("PV.C10:C20", round),
    'leyenda': 'Potencia Instalada',
    'parametros': {'mark': 'o'},
})
# Proyecciones
años = Mercado.extraer("PV.A20,A21:A26", int)
g.serie(**{
    'x': años,
    'y': Mercado.extraer("PV.B20,B21:B26", round),
    'leyenda': 'Potencia acumulada (proyección)',
    'parametros': {'mark': '*', 'dashdotdotted': True},
})
g.serie(**{
    'x': años,
    'y': Mercado.extraer("PV.C20,C21:C26", round),
    'leyenda': 'Potencia Instalada (proyección)',
    'parametros': {'mark': 'o', 'dashdotdotted': True},
})
g.tex()
