g = Figura(**{
    'destino': BASE,
    'parametros': {
        'legend style': {
            'at': '{(0.50,0.96)}',
            'anchor': 'north',
            'legend columns': '3',
        },
    },
    'conv': ('.0f', '.3f'),
})
g.eje(**{
    'tipo': 'axis',
    'parametros': {
        'xlabel': '{Fecha, año}',
        'ylabel': '{Precios, \si{\dolar\per\kilogram}}',
        'ymin': 0.0,
        'ymax': 1.0,
    },
})
g.set_default('serie', {'smooth': True})
años = Mercado.extraer("Resumen.A20:A34")
g.serie(**{
    # Los dos primeros son desproporcionados
    'x': años[2:],
    'y': Mercado.extraer("Resumen.D20:D34")[2:],
    'leyenda': 'Importación',
    'parametros': {'mark': '+'},
})
g.serie(**{
    'x': años,
    'y': Mercado.extraer("Resumen.G20:G34"),
    'leyenda': 'Exportación',
    'parametros': {'mark': 'x'},
})
g.serie(**{
    'x': años,
    'y': Mercado.extraer("Resumen.H20:H34"),
    'leyenda': 'Promedio Ponderado',
    'parametros': {'mark': '*'},
})
g.tex()
