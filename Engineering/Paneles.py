#-*- coding: utf-8 -*-

from Library.Panel import Paneles, Xlc, Ylc, ε, cosε

exec(open('Variables.py').read())

Id = 5
*_, Xlote, Ylote, Pinst, Ihsep, Ypsep = Lotes[Id]

Alote = Xlote * Ylote
Ainst = 0.202 * Alote # 0.107
Ysep = Ypsep / cosε
P = Paneles(Xlote, Ylote, Pinst, Ainst, Hinst, Xpanel, Ypanel, Dpanel, β)
z1, z2 = P.distribución(Ihsep, Ysep)
Ainst = z1.xlim * z1.ylim

f = lambda v: round(v * 1000, 4)  # m → mm
data = {
    'xlim': f(Xlc),
    'ylim': f(Ylc),
    'Ysep': f(Ysep),
    'Ihsep': f(Ihsep),
    'Xlote': f(Xlote),
    'Ylote': f(Ylote),
    'Xpanel': f(Xpanel),
    'Ypanel': f(Ypanel),
    'E': round(ε * frg),
    'B': β,
}

limites = r"""
lote = Draft.makeRectangle(length=Xlote, height=Ylote)
lote.Label = "Limite del Lote"
lote.MakeFace = False
pl = App.Placement(App.Vector(xlim, ylim, 0), App.Rotation(p0, 0))
construible = Draft.makeRectangle(length=Xlote-2*xlim, height=Ylote-2*ylim, placement=pl)
construible.Label = "Limite Construible"
construible.MakeFace = False
Gui.ActiveDocument.getObject(construible.Name).DrawStyle = 'Dotted'
""".strip()

grupos = r"""
ZS = App.ActiveDocument.addObject("App::DocumentObjectGroup", "Paneles")
ZA = App.ActiveDocument.addObject("App::DocumentObjectGroup", "ZonaA")
ZB = App.ActiveDocument.addObject("App::DocumentObjectGroup", "ZonaB")
ZS.addObject(ZA)
ZS.addObject(ZB)
""".strip()

fila = r"""
panel = Draft.makeRectangle(length=-Xpanel, height=-Ypanel, face=True)
panel.Placement = App.Placement(p, App.Rotation(px, -B))
panel.Label = "P{1}"
fila = Draft.makeArray(panel, App.Vector(-Xpanel, 0, 0), p0, {0}, 1)
fila.Placement = App.Placement(p0, App.Rotation(pz, E), p)
fila.Label = "F{1}"
""".strip().format  # nx, label

columnas = r"""
columnas = Draft.makeArray(fila, p0, App.Vector(0, Ysep, 0), 1, {0})
columnas.Label = "C{1}"
""".strip().format  # ny, label

pvector = r"p = App.Vector({}, {}, {})".format

base = r"""
import Draft

if App.ActiveDocument is None:
    App.newDocument("Sin nombre")
    App.setActiveDocument("Sin_nombre")
    App.ActiveDocument = App.getDocument("Sin_nombre")
    Gui.ActiveDocument = Gui.getDocument("Sin_nombre")

xlim = {xlim}
ylim = {ylim}
Ysep = {Ysep}
Ihsep = {Ihsep}
Xlote = {Xlote}
Ylote = {Ylote}
Xpanel = {Xpanel}
Ypanel = {Ypanel}

E = {E}
B = {B}

p0 = App.Vector(0, 0, 0)
px = App.Vector(1, 0, 0)
pz = App.Vector(0, 0, 1)
""".strip().format(**data)

codes = [base]
fa = codes.append

#fa(limites)
#fa(r"")
fa(grupos)
fa(r"")
zonas = (
    ('A', z1,               0, Hinst),
    ('B', z2, z1.ylim + Ihsep,     0),
)
for nz, zona, yoffset, zoffset in zonas:
    fa('# Zona {}'.format(nz))
    addf = 'Z{}.addObject(fila)'.format(nz)
    addc = 'Z{}.addObject(columnas)'.format(nz)
    # esquina superior derecha
    X = Xlote - Xlc - zona.x1
    Y = Ylote - Ylc - yoffset
    fa('# filas incompletas (arriba)')
    for i, nx in enumerate(reversed(zona.nxa), 1):
        n = zona.nx - nx
        x = f(X - zona.x2 * n)
        y = f(Y - zona.y2 * n + Ysep * i)
        z = f(zoffset)
        fa(pvector(x, y, z))
        fa(fila(nx, '{}1{}'.format(i, nz)))
        fa(addf)
    fa('# filas completas')
    x = f(X)
    y = f(Y - Ysep * (zona.ny - 1))
    z = f(zoffset)
    l = '02{}'.format(nz)
    fa(pvector(x, y, z))
    fa(fila(zona.nx, l))
    fa(columnas(zona.ny, l))
    fa(addc)
    fa('# filas incompletas (abajo)')
    for i, nx in enumerate(reversed(zona.nxb), 1):
        x = f(X)
        y = f(Y - Ysep * (zona.ny + i - 1))
        z = f(zoffset)
        fa(pvector(x, y, z))
        fa(fila(nx, '{}3{}'.format(i, nz)))
        fa(addf)
    fa(r"")

fa(r"App.ActiveDocument.recompute()")

with open('CAD/paneles.py', encoding='ascii', mode='w') as fid:
    fid.write('\n'.join(codes))

