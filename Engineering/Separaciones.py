#-*- coding: utf-8 -*-

from math import sin, cos, tan
from numpy import linspace, arange
from pandas import HDFStore, DataFrame
from os.path import splitext, basename
from itertools import product
from scipy.integrate import simps

from Library.Sol import Sol
from Library.Panel import Paneles
from Library.Utils import Progress, fceil

exec(open('Variables.py').read())

S = Sol(ωi, ωf, ωs, β)  # el área efectiva no es simétrica respecto de ψ
α = S.α()               # rad
ψ = S.ψ()               # rad
I = S.I(ffv)            # W/m²

f = frs / 1e6           # rad→segundo;J→MJ

Yp = Ypanel * cos(β * fgr)
Zp = Ypanel * sin(β * fgr)
tα = tan(min(a.min() for a in α))
Sp = Zp / tα            # Distancia de "cero sombra" para los paneles
Di = (Hinst + Zp) / tα  # Distancia de "cero sombra" para la construcción

filename = '{[0]}.h5'.format(splitext(basename(__file__)))
db = HDFStore(filename, mode='w')

progress = Progress(len(Lotes), __file__)

def calc(Xlote, Ylote, Pinst, Ainst, IHSEP, YPSEP):
    data = {
        'Ihsep': [],    # separación horizontal o vertical de la instalación, m
        'Ypsep': [],    # separación entre paneles, m
        'E': [],        # energía total, MJ
        'N': [],        # número totales de paneles 
    }
    for Ihsep, Ypsep in product(IHSEP, YPSEP):
        E = []
        for d in range(S.neo, S.nep, 3):  # entre los equinoccio de otoño y primavera
            P = Paneles(Xlote, Ylote, Pinst, Ainst, Hinst, Xpanel, Ypanel, Dpanel, β)
            P.setαψ(α[d], ψ[d])
            a, n = P.área_efectiva(Ihsep, Ypsep)  # el área efectiva varía por las sombras
            E.append(simps(I[d] * a, S.ω[d]) * f)
        data['Ihsep'].append(Ihsep)
        data['Ypsep'].append(Ypsep)
        data['E'].append(sum(E))
        data['N'].append(n)
    return DataFrame(data)

def get_opt(data):
    group = data[data.E > data.E.max() * 0.95]
    group = group[group.Ihsep == group.Ihsep.max()]
    group = group[group.Ypsep == group.Ypsep.max()]
    group = group[group.N == group.N.min()]
    return group.head(1)

for Id, mp, Xlote, Ylote, Pinst, *_ in Lotes:
    progress(mp)
    Ainst = 0.15 * Xlote * Ylote

    a = fceil(Yp, 0)
    b = fceil(Di / 2, 0)
    c = fceil(Yp, 1)
    d = fceil(Sp, 1)
    IHSEP = arange(a, b + 1e-6, 0.50).round(1).tolist()
    YPSEP = arange(c, d + 1e-6, 0.10).round(1).tolist()
    data0 = calc(Xlote, Ylote, Pinst, Ainst, IHSEP, YPSEP)

    opt = get_opt(data0)

    a = opt.Ihsep - 0.40
    b = opt.Ihsep + 0.40
    c = opt.Ypsep - 0.09
    d = opt.Ypsep + 0.09
    IHSEP = [i for i in arange(a, b + 1e-6, 0.10).round(1) if i not in IHSEP]
    YPSEP = [i for i in arange(c, d + 1e-6, 0.01).round(2) if i not in YPSEP]
    data1 = calc(Xlote, Ylote, Pinst, Ainst, IHSEP, YPSEP)

    data = data0.append(data1, ignore_index=True)
    data = data.sort_values(['Ihsep', 'Ypsep', 'N', 'E']).reset_index(drop=True)

    opt = get_opt(data)

    key = 'mp{}'.format(Id)
    db.put(key, data)
    db.get_storer(key).attrs.Ihsep = float(opt.Ihsep)
    db.get_storer(key).attrs.Ypsep = float(opt.Ypsep)

db.close()
