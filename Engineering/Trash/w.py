ωi = -12
ωf = +12
S = Sol(ωi, ωf, ωs, β)  # el área efectiva no es simétrica respecto de ψ
α = S.α()               # rad
ψ = S.ψ()               # rad

any([any((a >= β) & ((u <= -π2) | (u >= π2))) for a, u in zip(α, ψ)])


any([any((u <= -π2) | (u >= π2)) for a, u in zip(α, ψ)])
