#-*- coding: utf-8 -*-

DEBUG = True

import os
import sys

os.chdir(os.path.join(os.path.dirname(os.path.abspath(__file__)), '..')

from math import sin, cos, sqrt
from numpy import where, sin as nsin, cos as ncos, tan as ntan
from subprocess import Popen, PIPE
from multiprocessing import Pool, cpu_count

from Library.Sol import Sol
from Library.TikZ import getrectangle, point, node, path, draw, fill, filldraw
from Library.Panel import Paneles, ε, sinε, cosε, π, π2
from Library.Utils import Progress, plantilla, getday

exec(open('Variables.py').read())

#Xpanel = 2
#Ypanel = 1
#Xlote = 40
#Ylote = 30
#Hinst = 5
#Pinst = "arriba" # izquierda derecha arriba abajo
#Ihsep = 5.00
#Ypsep = 1.50

#Alote = Xlote * Ylote               # m²
#Ainst = 0.250 * Alote               # m² (mínimo)
#Ysep = Ypsep / cosε
#P = Paneles(Xlote, Ylote, Pinst, Ainst, Hinst, Xpanel, Ypanel, Dpanel, β)
#z1, z2 = P.distribución(Ihsep, Ysep)
#Ainst = z1.xlim * z1.ylim           # m² (real)

# Manzana D, lote 31 del PIP
Id = 5
*_, Xlote, Ylote, Pinst, Ihsep, Ypsep = Lotes[Id]
Pinst = "arriba"



#print("Hinst={}, Ihsep={}, Ypsep={}".format(Hinst, Ihsep, Ypsep))
Hinst = 4.0 * Hinst
Ihsep = 0.2 * Ihsep
#Ypsep = 1.2 * Ypsep
#print("Hinst={}, Ihsep={}, Ypsep={}".format(Hinst, Ihsep, Ypsep))



Alote = Xlote * Ylote               # m²
Ainst = 0.118 * Alote               # m² (mínimo)
Ysep = Ypsep / cosε
P = Paneles(Xlote, Ylote, Pinst, Ainst, Hinst, Xpanel, Ypanel, Dpanel, β)
z1, z2 = P.distribución(Ihsep, Ysep)
Ainst = z1.xlim * z1.ylim           # m² (real)

HZ = P.Hinst + P.Zp
NP = z1.n + z2.n

ωi = -5.5
ωf = +5.5
ωs = 120/3600

S = Sol(ωi, ωf, ωs, β)      # el área efectiva no es simétrica respecto de ψ
α = S.α()                   # rad
ψ = S.ψ()                   # rad
t = S.h()                   # horas

#ΨΕ = [u + ε for u in ψ]
#sinψε = [nsin(ue) for ue in ΨΕ]
#cosψε = [ncos(ue) for ue in ΨΕ]

#ψεx = [sinue.tolist() for sinue in sinψε]
#ψεy = [cosue.tolist() for cosue in cosψε]

sinψ = [nsin(u) for u in ψ]
cosψ = [ncos(u) for u in ψ]

tanα = [ntan(a) for a in α]

IS = [HZ / tana for tana in tanα]
if P.Binst:
    IS = [where((a > -π2) & (a < π2), som, P.Hinst/tana) for a, tana, som in zip(α, tanα, IS)]
ISx = [(som * sinue).tolist() for som, sinue in zip(IS, sinψ)]
ISy = [(som * cosue).tolist() for som, cosue in zip(IS, cosψ)]

PS = [P.Zp / tana for tana in tanα]
PSx = [som * sinue for som, sinue in zip(PS, sinψ)]
PSy = [som * cosue for som, cosue in zip(PS, cosψ)]

#NN = list(range(0, 365))
TT = list(range(0, S.size, 10))
#NN = [1, 90, 181]
#TT = [40, 41, 42, 43, 44, 45, 46, 47, 48]
NN = [1]
#TT = [0, 10, 20, 30, 40, 50]
#tt = 10
#TT = []
#TT += list(range(0, tt))
#TT += list(range(S.size-tt, S.size))
#TT = [78, 79, 80, 81, 82, 82, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93]
#TT = [8, 9, 10, 11, 12, 13, 14, 15, 16, 17]
#TT = list(range(int(S.size*0.8), S.size, 10))
#TT = list(range(int(S.size*0.4), S.size))
#TT = list(range(int(S.size*0.6)))

fn = lambda i: len(str(max(i)))
FN = 'frame_{{:0{}}}_{{:0{}}}'.format(fn(NN), fn(TT)).format

progress = Progress(len(NN) * len(TT), __file__)

xf = 0, 1.50 * Xlote
yf = 0, Ylote
XF, YF = getrectangle(xf, yf)   # Límite del frame

xl = 0, Xlote
yl = 0, Ylote
XL, YL = getrectangle(xl, yl)   # Límite del lote

xc = Xlc, Xlote - Xlc
yc = Ylc, Ylote - Ylc
XC, YC = getrectangle(xc, yc)   # Límite construible

xxx = (xf[1] - xl[1]) / 2
xx0 = xl[1] + xxx
yy1 = 0
yy2 = yf[1] * 4.1 / 5
yy3 = yf[1] * 3.1 / 5
yy4 = yf[1]

coordinates = '\n'.join(r'\coordinate ({}) at {};'.format(l, point(i, j))
    for l, i, j in zip('ABCD', XF, YF))

nodes = '\n'.join(r'\node at ({}) {{}};'.format(l) for l in 'ABCD')

zr = xxx / 3
za = sinε * zr
zb = cosε * zr
zn = point(xx0 - za, yy3 + zb)
zs = point(xx0 + za, yy3 - zb)
ze = point(xx0 + zb, yy3 + za)
zo = point(xx0 - zb, yy3 - za)
azimut = r'''
\coordinate (E) at {0};
\draw[lightgray] (E) circle [radius={1:.3f}];
\draw[lightgray,->] (E) -- {2} node[above,rotate={6:.0f},gray] {{\scriptsize N}};
\draw[lightgray,->] (E) -- {3} node[below,rotate={6:.0f},gray] {{\scriptsize S}};
\draw[lightgray,->] (E) -- {4} node[right,rotate={6:.0f},gray] {{\scriptsize E}};
\draw[lightgray,->] (E) -- {5} node[left, rotate={6:.0f},gray] {{\scriptsize O}};
'''.strip().format(point(xx0, yy3), zr, zn, zs, ze, zo, ε * frg)

start = r'''
\documentclass[border=0.5mm]{standalone}
\usepackage[utf8]{inputenc}
\usepackage[TS1,T1]{fontenc}
\usepackage{microtype}
\usepackage{lmodern}
\usepackage[spanish,es-noshorthands]{babel}
\usepackage{icomma}
\usepackage{textcomp}
\usepackage{array}
\usepackage{tikz}
\usetikzlibrary{calc}
\begin{document}
\begin{tikzpicture}[x=3pt,y=3pt,>=latex]
\tikzset{
    lote/.style  = {lightgray},
    lims/.style  = {lote, draw opacity=.5},
    light/.style = {fill=white},
    dark/.style  = {fill=gray},
    sp/.style    = {ultra thin, draw=gray, fill=gray},
    dlims/.style = {ultra thin, green!50!black, draw opacity=.5},
    debug/.style = {draw opacity=.5, fill=none},
}
'''.strip()

end = r'''
\end{tikzpicture}
\end{document}
'''.strip()

config = plantilla(r'''
    \begin{tabular}{l@{ }r}
        Xlote   & «:.2f»\\
        Ylote   & «:.2f»\vspace{1.4ex}\\
        Pinst   & «»\\
        Ainst   & «:.2f»\\
        Hinst   & «:.2f»\\
        Ihsep   & «:.2f»\\
        Ypsep   & «:.2f»\vspace{1.4ex}\\
        Xpanel  & «:.3f»\\
        Ypanel  & «:.3f»\\
        Dpanel  & «»\vspace{1.4ex}\\
        $\beta$ & «»\\
        NP      & «»\\
    \end{tabular}
''').format(Xlote, Ylote, Pinst, Ainst, Hinst, Ihsep, Ypsep, Xpanel, Ypanel, Dpanel, β, NP)
config = node(xx0, yy1, config, 'anchor=south')

s1 = 'xshift=-1pt,yshift=-1pt'
s2 = 'xshift=1pt,yshift=1pt'
frameclip = r'\clip {} rectangle {};'.format(point(xf[0], yf[0], s1), point(xf[1], yf[1], s2))

document = plantilla('\n'.join((
    start,
    coordinates,
    nodes,
    #config,
    #azimut,
    frameclip if DEBUG else '% frame clip debug',
    #node(xx0, yy4, r'\tiny Schmidt Cristian Hernán', 'anchor=north'),
    #node(xx0, yy4, r'\tiny Schcriher', 'anchor=north,yshift=-6'),
    draw(XL, YL, 'lote', cycle=True),
    draw(XC, YC, 'dlims', cycle=True) if DEBUG else '% draw(XC, YC) for debug',
    '«»',
    end,
)))

def panel(x1, y1, l, norm, full, Δx, Δy):
    #   3/\
    #   / /\5
    #  / / /\2
    # 4\/ / /
    #  6\/ /
    #    \/1
    x2 = x1 + P.x2
    x3 = x2 - P.x1
    x4 = x1 - P.x1
    y2 = y1 + P.y2
    y3 = y2 + P.y1
    y4 = y1 + P.y1
    x = [x1, x2, x3, x4]
    y = [y1, y2, y3, y4]
    if l in (0, Dpanel):
        style = 'dark' if l == 0 else 'light'
        code = filldraw(x, y, style, cycle=True)
    else:
        f = l / Dpanel
        X = f * P.x1
        Y = f * P.y1
        if norm:
            x5 = x2 - X
            x6 = x1 - X
            y5 = y2 + Y
            y6 = y1 + Y
            sa = 'dark'
            sb = 'light'
        else:
            x5 = x3 + X
            x6 = x4 + X
            y5 = y3 - Y
            y6 = y4 - Y
            sa = 'light'
            sb = 'dark'
        dx = [x6, x5, x3, x4]
        dy = [y6, y5, y3, y4]
        lx = [x1, x2, x5, x6]
        ly = [y1, y2, y5, y6]
        code = '\n'.join((
            fill(dx, dy, sa, cycle=True),
            fill(lx, ly, sb, cycle=True),
            draw(x, y, cycle=True),
        ))
    xx = [x1 + Δx, x2 + Δx, x3, x4]
    yy = [y1 - Δy, y2 - Δy, y3, y4]
    som = path(xx, yy, 'sp', cycle=True)
    return code, som

status = plantilla(r'''
    \begin{tabular}{c}
        Día «»\vspace{0.8ex}\\
        «»\vspace{0.8ex}\\
        \begin{tabular}{c@{ }c@{}r@{:}c@{:}c}
            $t$      & «»\\
            $\alpha$ & «»\\
            $\psi$   & «»\\
        \end{tabular}
    \end{tabular}
''').format

num = plantilla(r'\texttt{«:02.0f»}').format

def hour2str(n, sign=False):
    if sign:
        sign = '+' if n > 0 else '-' if n < 0 else ' '
        n = abs(n)
    h = int(n)
    m = int((n - h) * 60)
    s = round((n - h - m / 60) * 3600)
    if s >= 60:
        s -= 60
        m += 1
    if m >= 60:
        m -= 60
        h += 1
    if sign:
        nums = [r'\texttt{{{}}}'.format(sign)]
    else:
        nums = ['']
    nums.extend(num(i) for i in (h, m, s))
    return ' & '.join(nums)

def getstatus(n, i):
    dd = getday(n + 1)[-1]
    tt = hour2str(t[n][i])
    αα = hour2str(α[n][i] * frg)
    ψψ = hour2str(ψ[n][i] * frg, sign=True)
    return node(xx0, yy2, status(n, dd, tt, αα, ψψ))

if P.Xinst:
    XFC = 0
    YFC = 0 # (Ylote - 2 * Ylc - P.y1 - P.y2 * z1.ny) / 2  # correción de centrado en Y
else:
    XFC = 0 # (Xlote - 2 * Xlc - P.x1 - P.x2 * z1.nx) / 2  # correción de centrado en X
    YFC = 0

if P.Rinst:
    XFC = z1.xoffset

xx = xc[0] + XFC + P.x1
yy = yc[1] + YFC - P.y1 - P.y2

if DEBUG:
    index = []
    index_xsep = 5 * P.x1
    index_ysep = 4 * P.x1
    index_node = lambda x, y, n: node(x, y, r'\scalebox{{0.5}}{{{}}}'.format(n), 'debug')

if P.Xinst:
    if P.Linst:
        Ihsep += z2.xoffset

    xs = z1.xlim + Ihsep
    ΔX = [xx, xx + xs]
    ΔY = [yy - z.getypos1(0, z.getrow1(0)) for z in (z1, z2)]
    x1 = xc[0] + z1.xlim
    x2 = x1 + Ihsep
    xl1 = [x1, x1]
    xl2 = [x2, x2]
    yl1 = yc
    yl2 = yc

    if DEBUG:
        for Z, X, Y in ((z1, 0, ΔY[0]), (z2, xs, ΔY[1])):
            x = xc[0] + P.x2 / 2 + X
            y = yc[1] + index_ysep
            index.append('\n'.join(index_node(x + i * P.x2, y, i) for i in range(Z.nx)))
            x = xc[0] - index_xsep + X
            y = Y + P.y1 / 2
            index.append('\n'.join(index_node(x, y - i * Ysep, i) for i in range(Z.Ny)))

    if P.Rinst:
        XI, YI = getrectangle([x2, xc[1]], yc)
        sx = x2
    else:
        XI, YI = getrectangle([xc[0], x1], yc)
        sx = x1

    def getsom(Δx, Δy, param='sp'):
        Δx += sx
        x = [sx,       sx,         Δx,         Δx]
        y = [yc[0], yc[1], yc[1] - Δy, yc[0] - Δy]
        return path(x, y, param, cycle=True)
else:
    #if P.Tinst:
    #    Ihsep += z2.yoffset

    yy -= z1.getypos1(0, z1.getrow1(0))  # igual que con z2
    ys = z1.ylim + Ihsep
    ΔX = [xx, xx]
    ΔY = [yy, yy - ys]
    y1 = yc[1] - z1.ylim
    y2 = y1 - Ihsep
    xl1 = xc
    xl2 = xc
    yl1 = [y1, y1]
    yl2 = [y2, y2]

    if DEBUG:
        for Z, Y1, Y2 in ((z1, 0, ΔY[0]), (z2, ys, ΔY[1])):
            x = xc[0] + P.x2 / 2
            y = yc[1] + index_ysep - Y1
            index.append('\n'.join(index_node(x + i * P.x2, y, i) for i in range(Z.nx)))
            x = xc[0] - index_xsep
            y = Y2 + P.y1 / 2
            index.append('\n'.join(index_node(x, y - i * Ysep, i) for i in range(Z.Ny)))

    if P.Tinst:
        XI, YI = getrectangle(xc, [y1, yc[1]])
        sy = y1
    else:
        XI, YI = getrectangle(xc, [yc[0], y2])
        sy = y2

    def getsom(Δx, Δy, param='sp'):
        Δy = sy - Δy
        x = [xc[0], xc[1], xc[1] + Δx, xc[0] + Δx]
        y = [   sy,    sy,         Δy,         Δy]
        return path(x, y, param, cycle=True)

clip = r'\clip {} rectangle {};'.format(point(xl[0], yl[0]), point(xl[1], yl[1]))

def getazimut(x, y, param=None):
    zx = xx0 - x * zr
    zy = yy3 + y * zr
    param = '[{}]'.format(param) if param else ''
    return r'\draw{} (E) -- {};'.format(param, point(zx, zy))

if DEBUG:
    pdflatex = ['pdflatex', '-file-line-error', '-interaction', 'nonstopmode']
else:
    pdflatex = ['pdflatex', '-file-line-error', '-interaction', 'batchmode', '-halt-on-error']
pdftocairo = ['pdftocairo', '-singlefile', '-png', '-r', '400']
#pdftocairo = ['pdftocairo', '-singlefile', '-png']
ffmpeg = ['ffmpeg', '-r', '6', '-pattern_type', 'glob', '-i', "*.png", 'output.mp4']

base = os.path.expanduser('~/.cache/thesis/video')
dtex = os.path.join(base, 'tex')
dpng = os.path.join(base, 'png')

os.makedirs(dtex, exist_ok=True)
os.makedirs(dpng, exist_ok=True)

oldwd = os.getcwd()
os.chdir(dtex)

def frame(N):
    P.setαψ(α[N], ψ[N])
    m1, m2 = P.sombras(Ihsep, Ypsep, Ysep, z1, z2)
    #zif = '\n'.join(getazimut(ψεx[N][i], ψεy[N][i], 'densely dotted') for i in (0, -1))
    for T in TT:
        name = FN(N, T)
        progress(name)

        tex = name + '.tex'
        pdf = name + '.pdf'
        png = name + '.png'

        img = os.path.join(dpng, png)
#        if os.path.exists(img):
#            continue

        #ψε = ΨΕ[N][T]
        #am = ψε < 0
        #cd = -π2 < ψε < π2
        #pn = True if cd else False
        u = ψ[N][T]
        am = u < 0
        cd = -π2 < u < π2
        pn = True

        if P.Tinst:
            som = cd
        elif P.Binst:
            som = not cd
        elif P.Linst:
            som = not am
        else:
            som = am

        lines = []
        if DEBUG:
            lines.append(draw(xl1, yl1, 'dlims'))
            lines.append(draw(xl2, yl2, 'dlims'))
        lines.append(draw(XI, YI, 'lims', cycle=True))
        lines.append(getstatus(N, T))
        #lines.append(getazimut(ψεx[N][T], ψεy[N][T]))
        #lines.append(zif)

        if DEBUG:
            lines.append('% Debug (índices de las filas y las columnas)')
            lines.append('\n'.join(index))

        pans = []
        soms = []
        for M, Δx, Δy in zip((m1, m2), ΔX, ΔY):
            for r, row in enumerate(M[T]):
                δy = Δy - r * Ysep
                ŋ = len(row) - 1
                ł = row[0] == 0 if P.Linst else row[0] != 0
                for c, l in enumerate(row):
                    if 0 <= l <= Dpanel:
                        if am:
                            full = c == 0 or row[c - 1] == -1
                        else:
                            full = c == ŋ or row[c + 1] == -1
                        x = Δx + c * P.x2
                        y = δy + c * P.y2
                        p, s = panel(x, y, l, pn, full, PSx[N][T], PSy[N][T])
                        pans.append(p)
                        soms.append(s)

        lines.append('% Sombras')
        lines.append(r'\begin{scope}[transparency group, opacity=.4]')
        lines.append(clip)
        lines.append('\n'.join(soms))
        if som:
            lines.append(getsom(ISx[N][T], ISy[N][T]))
        lines.append(r'\end{scope}')

        lines.append('% Paneles')
        lines.append('\n'.join(pans))

        if DEBUG and som:
            lines.append('% Debug (línea azul en la sombra de la instalación)')
            lines.append(r'\begin{scope}')
            lines.append(clip)
            lines.append(getsom(ISx[N][T], ISy[N][T], 'debug'))
            lines.append(r'\end{scope}')

        with open(tex, encoding='utf-8', mode='w') as f:
            f.write(document.format('\n'.join(lines)))

        proc = Popen(pdflatex + [tex], stdout=PIPE, stderr=PIPE).communicate()
        err = proc[1].decode().strip()
        if err:
            raise ValueError('pdflatex, N={}, T={}: {}'.format(N, T, err))

        proc = Popen(pdftocairo + [pdf], stdout=PIPE, stderr=PIPE).communicate()
        err = proc[1].decode().strip()
        if err:
            raise ValueError('pdftocairo, N={}, T={}: {}'.format(N, T, err))

        err = os.system("mv {} {}".format(png, img))
        if err != 0:
            raise ValueError('mv, N={}, T={}: {}'.format(N, T, err))

with Pool(cpu_count()) as pool:
    pool.map(frame, NN)

#for N in NN:
#    frame(N)

#os.chdir(dpng)

#proc = Popen(ffmpeg, stdout=PIPE, stderr=PIPE).communicate()
#for p in proc:
#    with open('video.log', encoding='utf-8', mode='w') as f:
#        f.write(p.decode().strip())

os.chdir(oldwd)
