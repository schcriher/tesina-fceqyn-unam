#-*- coding: utf-8 -*-

from numpy import std, inf
from pandas import HDFStore, Series
from os.path import splitext, basename
from itertools import product

from Library.Sol import Sol

exec(open('Variables.py').read())

B = range(0, 91)
D = {b: Sol(ωi, ωf, ωs, b).E() for b in B}

# Paneles horizontales
β0 = 0
E0 = D[0]

# Paneles con un ángulo de inclinación
# Basado en energía máxima
β1 = 0
Et = 0
for b in B:
    E = sum(D[b])
    if E > Et:
        β1 = b
        Et = E
E1 = D[β1]
# Basado en la menor dispersión (mejor en invierno)
β1s = 0
S = inf
for b in B:
    s = std(D[b])
    if s < S:
        β1s = b
        S = s
E1s = D[β1s]

# Paneles con dos ángulos de inclinación
β2 = (0, 0)
Et = 0
for b1, b2 in product(B, B):
    E = sum(e1 if e1 > e2 else e2 for e1, e2 in zip(D[b1], D[b2]))
    if E > Et:
        β2 = (b1, b2)
        Et = E
b1, b2 = β2
E2 = []
days = []
prev = 1 if D[b1][0] >= D[b1][0] else 2
for n in range(365):
    e1 = D[b1][n]
    e2 = D[b2][n]
    if e1 > e2:
        E2.append(e1)
        if prev != 1:
            days.append(n)
            prev = 1
    else:
        E2.append(e2)
        if prev != 2:
            days.append(n)
            prev = 2

# Guardado de los datos
filename = '{[0]}.h5'.format(splitext(basename(__file__)))
db = HDFStore(filename, mode='w')
db.put('E0', Series(E0))
db.put('E1', Series(E1))
db.put('E1s', Series(E1s))
db.put('E2', Series(E2))
db.get_storer('E0').attrs.B = β0
db.get_storer('E1').attrs.B = β1
db.get_storer('E1s').attrs.B = β1s
db.get_storer('E2').attrs.B = β2
db.get_storer('E2').attrs.D = days
db.close()
