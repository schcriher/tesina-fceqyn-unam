#-*- coding: utf-8 -*-

from math import ceil, floor
from numpy import linspace, where
from pandas import HDFStore
from itertools import cycle
from scipy.integrate import simps

from Library.Sol import Sol
from Library.Panel import Paneles
from Library.Utils import Cache

exec(open('Variables.py').read())

@Cache
def energía_día(n, lote, Ve):
    Pmax = Ve * fcvp                        # Potencia máxima utilizable 100%, W
    Pmin = Pmax * 0.10                      # Potencia mínima utilizable  10%, W
    P = pot[lote][n]
    Ω = ω[n]
    i = where(P < Pmin, P, 0)               # Energía insuficiente, W
    s = where(P > Pmax, P - Pmax, 0)        # Energía sobrante, W
    Eu = simps(P - (i + s), Ω) * fcae       # máxima energía utilizable, MJ
    Ei = simps(i, Ω) * fcae                 # Energía insuficiente, MJ
    Es = simps(s, Ω) * fcae                 # Energía sobrante, MJ
    return Eu, Ei, Es                       # Energías diarias, MJ

def energía_serie(lote, Ve, Dstock, Mhobj, dias=NL, full=True):
    Eeobj = Mhobj / feh                     # "Energía" objetivo, MJ/día
    EMstock = Eeobj * Dstock                # Capacidad del stock, MJ
    stock = EMstock if full else 0          # Estado inicial del stock, MJ
    EE = []                                 # Energía al electrolizador, MJ
    ES = []                                 # Energía del stock, MJ
    ER = []                                 # Energía residual, MJ
    EV = []                                 # Energía vendible, MJ
    EC = []                                 # Energía a comprar, MJ
    for n in dias:
        Eutil, Einsuficiente, Esobrante = energía_día(n, lote, Ve)
        Er = Einsuficiente + Esobrante
        if Eutil >= Eeobj:
            # 1) Alcanza justo: no recargo el stock (Eutil = Eeobj)
            # 2) Sobra energía: recargo el stock
            Ee = Eeobj
            S = Eutil - Eeobj               # Energía sobrante del día (>=0)
            F = EMstock - stock             # Energía faltante en el stock
            if S >= F:
                # Si S>0: se llena el stock
                stock = EMstock
                Er += (S - F)
            else:
                # Se recarga el stock
                stock += S
            Ev = Er
            Ec = 0  # ¡No se compra para rellenar el stock!
        else:
            # 3) Falta energía: retiro del stock
            E = Eeobj - Eutil               # Energía faltante para Eeobj (>0)
            if E <= stock:
                # Se cumple el objetivo con el stock
                Ee = Eeobj
                stock -= E
                Ev = Er
                Ec = 0
            else:
                # No se cumple el objetivo, se vacía el stock, se compra energía
                Ee = Eutil + stock
                stock = 0
                Ev = Esobrante
                Ec = Eeobj - Ee - Einsuficiente
        EE.append(Ee)
        ES.append(stock)
        ER.append(Er)
        EV.append(Ev)
        EC.append(Ec)
    return EE, ES, ER, EV, EC               # Energías (día por día)


def inversión(L, Ve, Dstock, Mhobj, A):
    ncil = ceil(Mhobj * Dstock / mhcil)                     # H2
    ncil += ceil(Mhobj * mho * Dstock / mocil)              # O2
    return Cf * A + Ce * Ve + Ct * L + Ccil * ncil          # $


def ganancia(Ee, Ev):
    Mh = Ee * feh
    Mo = Mh * feo
    return Ch * Mh + Co * Mo + Cv * Ev                      # $

def amortización(I, EE, EV):
    ne = len(EE) if hasattr(EE, '__len__') else 0
    if ne and ne > Ndl:
        # Mas de 1 año de análisis
        ingresos = 0
        for año in range(floor(ne / Ndl)):
            i = Ndl * año
            f = Ndl * (año + 1)
            Ee = sum(EE[i:f])
            Ev = sum(EV[i:f]) if EV else 0
            G = ganancia(Ee, Ev)
            ingresos += G
            if ingresos >= I:
                break
        A = año + 1 - (ingresos - I) / G
    else:
        Ee = sum(EE) if ne else EE
        Ev = sum(EV) if EV else 0
        G = ganancia(Ee, Ev)
        A = I / G
    return A  # años

def agua_serie(EE, ES, EC, P, PMAX, AREA, STOCK):
    MAN = []                                        # Masa de agua necesaria
    MAF = []                                        # Masa de agua faltante
    MAE = []                                        # Masa de agua en exceso
    MAS = []                                        # Masa de agua del stock
    MAD = []                                        # Masa de agua de desbordamiento
    stock = STOCK                                   # gH2O, comenzamos lleno
    es_prev = 0                                     # valor anterior del stock
    fac = AREA * da / 1000                          # factor de conversión mm → g
    for ee, es, ec, p in zip(EE, ES, EC, cycle(P)):
        ### Utilización del agua
        es_step = es - es_prev
        es_prev = es
        man = (ee + ec + es_step) * feh * fea       # gH2O (necesaria)
        if man > stock:
            # Falta agua
            maf = man - stock
            stock = 0
        else:
            # El agua alcanza
            maf = 0
            stock -= man
        ### Recarga del agua
        pdiff = p - PMAX
        if pdiff > 0:
            p = PMAX
            mad = pdiff * fac
        else:
            mad = 0
        rain = p * fac                              # gH2O (p: lluvia [mm])
        space = STOCK - stock
        if rain > space:
            stock = STOCK       # stock lleno
            mae = rain - space  # sobra lluvia
        else:
            stock += rain
            mae = 0
        ### Información
        MAN.append(man)
        MAF.append(maf)
        MAE.append(mae)
        MAS.append(stock)
        MAD.append(mad)
    return MAN, MAF, MAE, MAS, MAD                  # gH2O (día por día)

def find1(f, ftol, xini, xend, xtol):
    """ Función de búsqueda:   \
            f(xini)    != 0     \___
            f(xend)    == 0     /
            f(x>=xopt) == 0    /
    """
    while xend - xini > xtol:
        x = (xend + xini) / 2
        y = f(x)
        if abs(y) < ftol:  # y ≈ 0
            xend = x
        else:
            xini = x
    return (xend + xini) / 2

def find2(f, ftol, xini, xend, xtol):
    """ Función de búsqueda:       /
            f(xini)    == 0    ___/
            f(xend)    != 0       \
            f(x<=xopt) == 0        \
    """
    while xend - xini > xtol:
        x = (xend + xini) / 2
        y = f(x)
        if abs(y) < ftol:  # y ≈ 0
            xini = x
        else:
            xend = x
    return (xend + xini) / 2

def max_number_of_days_in_low(data, limit):
    """ Cuenta la cantidad maxima de días seguidos que los valores 
        en 'data' están por debajo del porcentaje 'limit' de la media
    """
    dlim = data.mean() * limit
    prev = data[0] < dlim
    days = 1 if prev else 0
    count = []
    for d in data[1:]:
        if d < dlim:
            days += 1
        else:
            if days:
                count.append(days)
            days = 0
    if days:
        count.append(days)
    return max(count) if count else 0
