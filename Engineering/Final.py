#-*- coding: utf-8 -*-

from math import ceil, floor
from numpy import linspace
from pandas import HDFStore, DataFrame, Series
from os.path import splitext, basename
from itertools import cycle

from Library.Utils import Progress

exec(open('Modelo.py').read())

dbd = HDFStore('Data.h5', mode='r')
rad = dbd.get('nasa/R')['1984':'2014']  # Datos reales (31 años completos)
pre = dbd.get('inta/P')                 # Precipitación, mm
hsp = dbd.get('hottel')                 # horas solares pico (kWh/(m²·día))
dbd.close()

# Optimización
Ve = 60                                 # Capacidad del electrolizador, Nm³H2/h
Dop = 3                                 # Stock de optimización, días
Mop = 29531                             # Producción de optimización, gH2/día
Npanel = 1551                           # Número de paneles
Af = Npanel * Apanel                    # Área fotovoltaica, m²

# Manzana D, lote 31 del PIP
Id = 5
*_, Xlote, Ylote, Pinst, Ihsep, Ypsep = Lotes[Id]
lote = 'mp{}'.format(Id)
Alote = Xlote * Ylote                   # m²

Ndlpm = round(Ndl / 12)                 # Número de días laborales por mes
EMSAlim = 2000 * 3600 / 1000            # EMSA: 3.a 1 'Menor a 2000 kWh'

NR = len(rad.resample('A'))
dias = [d + 365 * (i // Ndl) for i, d in enumerate(NL * NR)]

# Variables para la función "energía_día"
dbp = HDFStore('Potencias.h5', mode='r')
pot = dbp.get(lote)
dbp.close()
POT = {}
for d, nl in zip(dias, cycle(NL)):
    POT[d] = pot[nl] * rad[d] / hsp[nl]
pot = {lote:POT}
sol = Sol(ωi, ωf, ωs, β)
ω = {d: sol.ω[nl] for d, nl in zip(dias, cycle(NL))}

progress = Progress(3, __file__)
progress('P1')

def años_autosuficientes(EC):
    n = 0  # número de años autosuficientes
    for año in range(NR):
        i = Ndl * año
        f = i + Ndl
        if sum(EC[i:f]) == 0:
            n += 1
        for mes in range(12):
            ii = i + Ndlpm * mes
            ff = ii + Ndlpm
            if sum(EC[ii:ff]) > EMSAlim:
                return False
    return n

def alternativa(Dstock, Mh, data, naamin=0):
    EE, ES, ER, EV, EC = energía_serie(lote, Ve, Dstock, Mh, dias, full=False)
    naa = años_autosuficientes(EC)
    if naa is not False and naa >= naamin:
        I = inversión(Alote, Ve, Dstock, Mh, Af)
        A1 = amortización(I, EE, EV)
        A2 = amortización(I, EE, 0)
        data['NA'].append(naa)
        data['Mh'].append(Mh)
        data['D'].append(Dstock)
        data['I'].append(ceil(I))
        data['A1'].append(A1 * 12)
        data['A2'].append(A2 * 12)
        data['IM'].append(ceil(I * 1000 / Mh))
        data['EE'].append(round(sum(EE)))
        data['ER'].append(round(sum(ER)))
        data['EC'].append(round(sum(EC)))
        return naa

# Producción objetivo: busqueda
DSTOCK = tuple(range(Dop, 31, 1))   # 3 a 30 días
data = {
    'NA': [],  # número de años autosuficientes
    'Mh': [],  # Producción de H2, g/día
    'D':  [],  # Stock, días
    'I':  [],  # Inversión, $
    'A1': [],  # Amortización vendiendo la energía residual, meses
    'A2': [],  # Amortización sin vender la energía residual, meses
    'IM': [],  # Inversión relativa, $/(kg/día)
    'EE': [],  # Energía al electrolizador, MJ
    'ER': [],  # Energía residual, MJ
    'EC': [],  # Energía a comprar, MJ
}
for D in DSTOCK:
    def fobj(Mh):
        EE, ES, ER, EV, EC = energía_serie(lote, Ve, D, Mh, dias, full=False)
        naa = años_autosuficientes(EC)
        if naa is not False:
            a = 1  # se acepta 1 año sin autosuficiencia
            return NR - naa - a if naa < NR - a else 0
        else:
            return NR
    Mh = int(floor(find2(fobj, 0.1, 1, Mop, 0.1)))
    alternativa(D, Mh, data)
raw = DataFrame(data)

# Producción objetivo: Condición definitiva
M = raw.iloc[raw.IM.idxmin()].Mh        # Mh para la mínima inversión relativa
Mh = int(round(M / mhcil) * mhcil)      # Redondeo en base al número de cilindros
for D in DSTOCK:
    if alternativa(D, Mh, data, NR - 1):
        Dstock = D
        break
raw = DataFrame(data).sort_values(['Mh', 'D'])

def evolución(Dstock, Mh):
    EE, ES, ER, EV, EC = energía_serie(lote, Ve, Dstock, Mh, dias, full=False)
    data = {'EE': EE, 'ES': ES, 'ER': ER, 'EV': EV, 'EC': EC}
    viable = años_autosuficientes(EC) != False  # no excede el límite energético
    return DataFrame(data, dias), viable

# Evolución para las condiciones de la optimización
eop, eop_viable = evolución(Dop, Mop)

# Evolución para las condiciones definitivas
ede, ede_viable = evolución(Dstock, Mh)


progress('P2')

def agua_stock(EE, ES, EC, PMAX, AREA):
    def f(stock):
        MAN, MAF, MAE, MAS, MAD = agua_serie(EE, ES, EC, pre, PMAX, AREA, stock)
        return sum(MAF)
    n = 1
    stock = 50e6  # 50000000g ~50 m³ (punto de partida)
    while True:
        stock *= n
        if f(stock) == 0:
            break
        n *= 1.618
    return int(ceil(find1(f, 0.01, 1, stock, 0.5)))

def agua_area(EE, ES, EC, PMAX, stock_obj):
    def f(area):
        stock = agua_stock(EE, ES, EC, PMAX, area)
        # Siempre stock >= stock_obj
        return stock - stock_obj
    return int(ceil(find1(f, 0.01, 1, Alote, 0.5)))

def find_and_trace(func, ini, end, tol):
    xini = ini
    yobj = func(end)
    X = [end]
    Y = [yobj]
    while abs(end - ini) > tol:
        x = (end + ini) / 2
        y = func(x)
        X.append(x)
        Y.append(y)
        if y == yobj:       # Forma de
            end = x         # la gráfica
        else:               #  \
            ini = x         #   \___
    xobj = (end + ini) / 2
    yobj = func(xobj)
    X.append(xobj)
    Y.append(yobj)
    for x in linspace(xini, xobj, 29):
        if x not in X:
            X.append(x)
            Y.append(func(x))
    XY = Series(Y, X)
    return xobj, yobj, XY.sort_index()


# Depósito de agua
EE = ede.EE
ES = ede.ES
EC = ede.EC
pre_max = pre.max()
stock_obj = agua_stock(EE, ES, EC, pre_max, Alote)

# Variación del área requerida en función del límite de captación de lluvia
fobj = lambda pmax: agua_area(EE, ES, EC, pmax, stock_obj)
pmax_obj, area_obj, agua = find_and_trace(fobj, 0.1, pre_max, 0.01)

# Variaciones del volumen del stock para varios límites de captación de lluvia
stocks = {}
for pfac in (0.10, 0.25, 0.50, 0.75, 1.00):
    key = 'pmax{:4.2f}'.format(pfac).replace('.', '')
    pmax = pfac * pre_max
    fobj = lambda afac: agua_stock(EE, ES, EC, pmax, Alote * afac)
    stocks[key] = find_and_trace(fobj, 0.01, 1.000, 0.01)

progress('P3')

# Guardado de los datos
filename = '{[0]}.h5'.format(splitext(basename(__file__)))
db = HDFStore(filename, mode='w')
db.put('raw', raw)
db.put('eop', eop)
db.put('ede', ede)
db.put('agua', agua)
for k, d in stocks.items():
    db.put(k, d[2])
    db.get_storer(k).attrs.afac_obj = d[0]
    db.get_storer(k).attrs.stock_obj = d[1]
db.get_storer('eop').attrs.Mh = Mop
db.get_storer('eop').attrs.D = Dop
db.get_storer('eop').attrs.viable = eop_viable
db.get_storer('ede').attrs.Mh = Mh
db.get_storer('ede').attrs.D = Dstock
db.get_storer('ede').attrs.viable = ede_viable
db.get_storer('agua').attrs.pre_max = pre_max
db.get_storer('agua').attrs.stock_obj = stock_obj
db.get_storer('agua').attrs.pmax_obj = pmax_obj
db.get_storer('agua').attrs.area_obj = area_obj
db.close()
