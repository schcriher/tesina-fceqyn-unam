#-*- coding: utf-8 -*-

from math import ceil
from pandas import HDFStore, DataFrame, Series
from os.path import splitext, basename

from Library.Sol import Sol
from Library.Panel import Paneles

exec(open('Variables.py').read())

S = Sol(ωi, ωf, ωs, β)  # el área efectiva no es simétrica respecto de ψ
α = S.α()               # rad
ψ = S.ψ()               # rad
I = S.I(ffv)            # W/m² (potencia disponible)

filename = '{[0]}.h5'.format(splitext(basename(__file__)))
db = HDFStore(filename, mode='w')

for Id, mp, Xlote, Ylote, Pinst, Ihsep, Ypsep in Lotes:
    Ainst = 0.15 * Xlote * Ylote
    P = Paneles(Xlote, Ylote, Pinst, Ainst, Hinst, Xpanel, Ypanel, Dpanel, β)
    POT = {}
    for d in range(365):
        P.setαψ(α[d], ψ[d])
        a, n = P.área_efectiva(Ihsep, Ypsep)
        POT[d] = I[d] * a * fin                         # Potencia, W
    key = 'mp{}'.format(Id)
    db.put(key, DataFrame(POT))
    db.get_storer(key).attrs.N = n                      # número de paneles

db.close()
