#-*- coding: utf-8 -*-

from math import floor, ceil
from numpy import inf
from pandas import HDFStore, DataFrame
from os.path import splitext, basename
from itertools import product

from Library.Utils import Progress, ffloor

exec(open('Modelo.py').read())

pot = HDFStore('Potencias.h5', mode='r')
sol = Sol(ωi, ωf, ωs, β)    # el área efectiva no es simétrica respecto de ψ
ω = sol.ω


def búsqueda(VE, DSTOCK, MHMAX):
    data = {
        'Id': [],  # Id del lote
        'Ve': [],  # Capacidad del electrolizador, Nm³H2/h
        'D':  [],  # Stock, días
        'Mh': [],  # Producción de H2, g/año
        'I':  [],  # Inversión, $
        'A':  [],  # Amortización, meses
        'IM': [],  # Inversión relativa, $/(kg/día)
        'N':  [],  # Número de paneles fotovoltaicos
    }
    progress = Progress(len(Lotes) * len(VE) * len(DSTOCK), __file__)
    for Id, mp, Xlote, Ylote, *_ in Lotes:
        lote = 'mp{}'.format(Id)
        N = pot.get_storer(lote).attrs.N    # Número de paneles
        L = Xlote * Ylote                   # Área del lote, m²
        Af = N * Apanel                     # Área fotovoltaica, m²
        for Ve, Dstock in product(VE, DSTOCK):
            progress('Ve={:2}, Dstock={:2}, mp={}'.format(Ve, Dstock, mp))

            def fobj(Mh):
                ee = sum(energía_serie(lote, Ve, Dstock, Mh)[0])
                return round(Mh - ee * feh / Ndl, 2)

            Mh = floor(find2(fobj, 0.01, 100, MHMAX, 0.01))

            EE, ES, ER, EV, EC = energía_serie(lote, Ve, Dstock, Mh)
            Ee = sum(EE)
            I = inversión(L, Ve, Dstock, Mh, Af)
            A = amortización(I, Ee, ER)
            # Se usan enteros, por ser más rápidos de procesar
            data['Id'].append(Id)
            data['Ve'].append(Ve)
            data['D'].append(Dstock)
            data['Mh'].append(Mh)
            data['I'].append(ceil(I))
            data['A'].append(ceil(A * 12))
            data['IM'].append(ceil(I * 1000 / Mh))
            data['N'].append(N)
    return DataFrame(data, dtype=int)


def óptimos(df):
    op = DataFrame()
    for Id, *_ in Lotes:
        # Por cada lote se busca
        l = df[df.Id == Id]
        # las mínimas amortizaciones
        a = l[l.A < l.A.min() * 1.04]
        a = a[a.Mh == a.Mh.max()]
        # las mínimas inversiones relativas
        i = a[a.IM < a.IM.min() * 1.04]
        i = i[i.Mh == i.Mh.max()]
        op = op.append(i)
    return op


# Optimización
VE = tuple(range(15, 91, 15))               # 15 a 90 Nm³H2/h
DSTOCK = tuple(range(0, 31, 1))             # 0 a 30 días
MHMAX = 78750                               # g/día, 78.75 kg/día = 18900 kgH2/año
raw = búsqueda(VE, DSTOCK, MHMAX)
op = óptimos(raw)

# Análisis de la evolución promedio
dias = NL * 3                               # 3 años de análisis
m = op.loc[op.IM.idxmin()]                  # mínima inversión relativa
lote = 'mp{}'.format(m.Id)
EE, ES, ER, EV, EC = energía_serie(lote, m.Ve, m.D, m.Mh, dias, full=False)
data = {'EE': EE, 'ES': ES, 'ER': ER, 'EV': EV, 'EC': EC}
index = [d + 365 * (i // Ndl) for i, d in enumerate(dias)]
ep = DataFrame(data, index, dtype=int)

# Guardado de los datos
pot.close()
filename = '{[0]}.h5'.format(splitext(basename(__file__)))
db = HDFStore(filename, mode='w')
db.put('raw', raw)
db.put('op', op)
db.put('ep', ep)
db.get_storer('ep').attrs.lote = lote
db.close()
