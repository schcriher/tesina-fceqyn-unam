#-*- coding: utf-8 -*-

from math import ceil, floor
from numpy import linspace
from pandas import HDFStore, DataFrame, Series
from os.path import splitext, basename
from itertools import cycle

from Library.Panel import Paneles, cosε
from Library.Utils import Progress

exec(open('Modelo.py').read())

dbd = HDFStore('Data.h5', mode='r')
rad = dbd.get('nasa/R')['1984':'2014']  # Datos reales (31 años completos)
pre = dbd.get('inta/P')                 # Precipitación, mm
hsp = dbd.get('hottel')                 # horas solares pico (kWh/(m²·día))
dbd.close()

# Optimización
Ve = 60                                 # Capacidad del electrolizador, Nm³H2/h
Ds = 13                                 # Stock de optimización, días
Mh = 24920                              # Producción archivo Final.py, gH2/día
Npanel = 1551                           # Número de paneles
Af = Npanel * Apanel                    # Área fotovoltaica, m²
Astock = 22 * 1e6                       # Depósito de agua, gH2O
Pmax = 15                               # Precipitación máxima, mm (mínimo 6.7)

# Manzana D, lote 31 del PIP
Id = 5
*_, Xlote, Ylote, Pinst, Ihsep, Ypsep = Lotes[5]
lote = 'mp{}'.format(Id)
Alote = Xlote * Ylote                   # m²
Ainst = 0.118 * Alote                   # m² (mínimo)
Ysep = Ypsep / cosε
P = Paneles(Xlote, Ylote, Pinst, Ainst, Hinst, Xpanel, Ypanel, Dpanel, β)
z1, z2 = P.distribución(Ihsep, Ysep)
Ainst = z1.xlim * z1.ylim               # m² (real)

NR = len(rad.resample('A'))
dias = [d + 365 * (i // Ndl) for i, d in enumerate(NL * NR)]

# Variables para la función "energía_día"
dbp = HDFStore('Potencias.h5', mode='r')
pot = dbp.get(lote)
dbp.close()
POT = {}
for d, nl in zip(dias, cycle(NL)):
    fac = rad[d] / hsp[nl]
    POT[d] = pot[nl] * fac
pot = {lote: POT}
sol = Sol(ωi, ωf, ωs, β)
ω = {d: sol.ω[nl] for d, nl in zip(dias, cycle(NL))}

# Evolución final (evf)
EE, ES, ER, EV, EC = energía_serie(lote, Ve, Ds, Mh, dias, full=False)
MAN, MAF, MAE, MAS, MAD = agua_serie(EE, ES, EC, pre, Pmax, Ainst, Astock)
I = inversión(Alote, Ve, Ds, Mh, Af)
A1 = amortización(I, EE, EV)
A2 = amortización(I, EE, 0)

data = {'EE': EE, 'ES': ES, 'ER': ER, 'EV': EV, 'EC': EC,
    'MAN': MAN, 'MAF': MAF, 'MAE': MAE, 'MAS': MAS, 'MAD': MAD}
ev = DataFrame(data, dias)

filename = '{[0]}.h5'.format(splitext(basename(__file__)))
db = HDFStore(filename, mode='w')
db.put('evf', ev)
db.get_storer('evf').attrs.D = Ds
db.get_storer('evf').attrs.I = I
db.get_storer('evf').attrs.A1 = A1
db.get_storer('evf').attrs.A2 = A2
db.close()
