#!/usr/bin/env python3
#-*- coding: utf-8 -*-

import os
import sys
import pandas

db_file = os.path.join(os.path.dirname(__file__), '..', 'Data.h5')
db = pandas.HDFStore(db_file, mode='r')

m = '''
{}:
    Desde {} \ 
    Hasta {} / {} días = {:0.1f} años (útil: {} años)'''

for fuente in ('nasa', 'inta', 'twc'):
    d = db.get('{}/T'.format(fuente))
    n = db.get('{}/Ta'.format(fuente)).count()
    tini = d.first_valid_index()
    tfin = d.last_valid_index()
    ntot = (tfin - tini).days / 365.25
    print(m.format(fuente.upper(), tini, tfin, d.count(), ntot, n))
print()
db.close()
