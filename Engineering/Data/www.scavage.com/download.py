#!/usr/bin/env python3
#-*- coding: utf-8 -*-

import os
import sys
import time
import traceback

from random import random
from http.cookiejar import CookieJar
from urllib.request import HTTPCookieProcessor, build_opener

PATH, NAME = os.path.split(os.path.abspath(__file__))
BASE, EXT = os.path.splitext(NAME)
LOGS = os.path.join(PATH, '{}.log'.format(BASE))
SCRIPT = os.path.basename(PATH)
DATA = os.path.join(PATH, 'SCAVAGE.csv')
ROOT = os.path.abspath(os.path.join(PATH, '..', '..'))

sys.path.insert(0, ROOT)
from Library.Xml import ParserXML


FUN_TRANF = lambda d: float(d.replace(',', '.'))
SET_DATOS = (
    #i  nombre          unidad  incluir     ignorar  limpiar  conversión  redondeo
    (0, 'Tipo',         '',     '<img><a>', False,   (),      None,       None   ),
    (2, 'Valor FOB',    'USD',  False,      False,   ('.',),  FUN_TRANF,  2      ),
    (3, 'Valor Flete',  'USD',  False,      False,   ('.',),  FUN_TRANF,  2      ),
    (4, 'Valor CIF',    'USD',  False,      False,   ('.',),  FUN_TRANF,  2      ),
    (5, 'Peso neto',    'kg',   False,      False,   ('.',),  FUN_TRANF,  2      ),
    (6, 'Movimientos',  '',     False,      False,   ('.',),  FUN_TRANF,  0      ),
    #0  1               2       3           4        5        6           7
)
URL = 'http://www.scavage.com/trade?menu=ar.{}/definitive&query=product:{}+period:{}&inquiry={}'
INTENTOS_MAXIMOS_AL_SERVIDOR = 10


def escribir_datos(datos, mode='a'):
    with open(DATA, mode=mode, encoding='utf-8') as fichero:
        fichero.write('{}\n'.format(';'.join(map(str, datos))))


def escribir_errores(error, mode='a'):
    with open(LOGS, mode=mode, encoding='utf-8') as fichero:
        fichero.write('{}\n'.format(error))


cookies = CookieJar()
urlopener = build_opener(HTTPCookieProcessor(cookies))

def obtener_datos(producto, flujo, año, tipo, reintento=False):
    try:
        response = urlopener.open(URL.format(flujo, producto, año, tipo))
    except:
        if not reintento:
            for i in range(INTENTOS_MAXIMOS_AL_SERVIDOR):
                time.sleep(10 + 10 * (i + random()))
                try:
                    print('    Reintento {}'.format(i))
                    return obtener_datos(producto, flujo, año, tipo, reintento=True)
                except:
                    pass
            error = 'ERROR EN EL SERVIDOR (día:{}).\n{}\n'.format(día, traceback.format_exc().strip().replace('\n', '    \n'))
            escribir_errores(error)
        response.close()
        raise
    else:
        codigo = response.read().decode()
        response.close()
        html = ParserXML()
        html.cargar(codigo)
        return html


def main():
    if os.path.isfile(DATA):
        raise OSError('Ya existe el archivo {}'.format(DATA))
    
    unidad = lambda u: ' ({})'.format(u) if u else ''
    encabezado = ['Producto', 'Flujo', 'Año', 'Tipo', 'Valor de Tipo'] + ['{}{}'.format(i[1], unidad(i[2])) for i in SET_DATOS[1:]]
    escribir_datos(encabezado, mode='w')
    escribir_errores('', mode='w')
    
    for producto in ('28044000000',):       # ('28041000000', '28044000000')
        for flujo in ('export',):           # ('import', 'export')
            for año in range(2007, 2015):   # range(2000, 2015)
                for tipo in ('via', 'procedence', 'customs', 'country'):
                    mensaje = '{:>10} {:>8} {:>6} {:>12}'.format(producto, flujo, año, tipo)
                    print(mensaje)
                    try:
                        html = obtener_datos(producto, flujo, año, tipo)
                        filas = html.buscar('<table id="projected"><tr>')
                        if filas:
                            for f, fila in enumerate(filas, 1):
                                celdas = html.buscar('<td class="detail">', nodos=fila)
                                if celdas:
                                    datos = []
                                    #--- Extracción, Limpieza y Conversión --------------------------------------------------#
                                    for (i, nombre, unidad, incluir, ignorar, limpiar, conversión, redondeo) in SET_DATOS:
                                        d = html.extraer(incluir=incluir, ignorar=ignorar, nodos=celdas[i])[0]
                                        for l in limpiar:
                                            d = d.replace(l, '')
                                        d = d.strip()
                                        if conversión:
                                            d = conversión(d)
                                            d = round(d, redondeo)
                                        datos.append(d)
                                    #----------------------------------------------------------------------------------------#
                                    
                                    escribir_datos([producto, flujo, año, tipo] + datos)
                                    
                                elif f != 1:
                                    error = 'SIN DATOS EN CELDA ({})'.format(mensaje)
                                    print(error)
                                    escribir_errores(error)
                        else:
                            error = 'SIN DATOS ({})'.format(mensaje)
                            print(error)
                            escribir_errores(error)
                    except:
                        error = 'ERROR FATAL ({})\n{}'.format(mensaje, traceback.format_exc())
                        print(error)
                        escribir_errores(error)
                    
                    time.sleep(3 + 3 * random())
            time.sleep(30)


if __name__ == '__main__':
    #main()  # no se puede descargar todo de una vez, el server no lo permite
    sys.stdout.write('\n{} ({}) finalizado.\n'.format(NAME, SCRIPT))
