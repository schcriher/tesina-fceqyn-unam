#!/usr/bin/env python3
#-*- coding: utf-8 -*-

import os
import sys

PATH, NAME = os.path.split(os.path.abspath(__file__))
SCRIPT = os.path.basename(PATH)
SOURCE = os.path.join(PATH, 'SCAVAGE.csv')
DATA = os.path.join(PATH, 'SCAVAGE_procesado.csv')


def main(fuente, destino):
    información = {}
    with open(fuente, encoding='utf-8', mode='r') as fichero:
        for i, linea in enumerate(fichero, 1):
            if 2 <= i <= 705:
                datos = linea.split(';')
                if datos[3] == 'country':  # tipo
                    producto = datos[0]
                    flujo = datos[1]
                    año = datos[2]
                    valor = float(datos[5] if flujo == 'export' else datos[7])  # FOB para export
                    masa = float(datos[8])
                    información.setdefault(producto, {}).setdefault(año, {}).setdefault(flujo, {}).setdefault('valor', []).append(valor)
                    información.setdefault(producto, {}).setdefault(año, {}).setdefault(flujo, {}).setdefault('masa', []).append(masa)
    
    with open(destino, encoding='utf-8', mode='w') as fichero:
        fichero.write('Año;Importación Masa (kg);Importación Valor CIF (USD);Exportación Masa (kg);Exportación Valor FOB (USD)\n')
        for producto in sorted(información):
            fichero.write('\n{}\n'.format(producto))
            for año in sorted(información[producto]):
                import_masa = sum(información[producto][año]['import']['masa'])
                import_valor = sum(información[producto][año]['import']['valor'])
                export_masa = sum(información[producto][año]['export']['masa'])
                export_valor = sum(información[producto][año]['export']['valor'])
                fichero.write('{};{};{};{};{}\n'.format(año, import_masa, import_valor, export_masa, export_valor))


if __name__ == '__main__':
    main(SOURCE, DATA)
    sys.stdout.write('  {}/{}\n'.format(SCRIPT, NAME))
