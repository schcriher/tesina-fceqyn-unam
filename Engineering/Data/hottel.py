#!/usr/bin/env python3
#-*- coding: utf-8 -*-

import os
import sys
import pandas

PATH, NAME = os.path.split(os.path.abspath(__file__))
ROOT = os.path.abspath(os.path.join(PATH, '..'))
DATA = os.path.join(ROOT, 'Data.h5')

sys.path.insert(0, ROOT)
from Library.Sol import Sol

s = 10/3600                   # salto de 10 segundos
S = Sol(-12, 12, s, 0, True)  # radiación para todo el día sobre placa horizontal
E = S.E(1 / (1000 * 3600))    # J/m² * Ws/J * 1kW/1000W * 1h/3600s = kWh/(m²·day)

hsp = pandas.Series(E, S.n, dtype='float')  # horas solares pico (kWh/(m²·día))

db = pandas.HDFStore(DATA)
db.put('hottel', hsp) 
db.close()

print('  hottel.py')
