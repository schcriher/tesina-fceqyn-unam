#!/usr/bin/env python3
#-*- coding: utf-8 -*-

import os
import re
import sys
import time
import requests
import traceback

from random import random
from datetime import datetime
from argparse import ArgumentParser

PATH, NAME = os.path.split(os.path.abspath(__file__))
BASE, EXT = os.path.splitext(NAME)
LOGS = os.path.join(PATH, '{}.log'.format(BASE))
SCRIPT = os.path.basename(PATH)
DATA = os.path.join(PATH, 'ARMS0118.csv')

sys.path.insert(0, os.path.abspath(os.path.join(PATH, '..', '..')))
from Library.Xml import ParserXML
from Library.Utils import meses_nom_num

http = requests.Session()
http.cookies.set("CTOF", "E")  # Unidades inglesas: T=°F, V=millas, etc

FECHA_INI = '16/10/2006'   #
#FECHA_INI = '01/01/2007'  # \  8
FECHA_FIN = '31/12/2014'   # / años
FECHA_FORMATO = "%d/%m/%Y"

Temperatura = lambda i: (float(i) - 32) * 5 / 9     # °F       → °C             MEJOR PRESICIÓN
Visibilidad = lambda i: float(i) * 1.609344         # mille    → Km             http://www.convertworld.com/es/longitud/
Presión = lambda i: float(i) * 0.033863787          # inHg     → bar            http://www.convertworld.com/es/presion/
def Viento(i):                                      # mille/h  → Km/h
    d, v, u = i.split()
    return '{} {} km/h'.format(d.replace('W', 'O'), round(float(v) * 1.609344))

SET_DATOS = (
    #i  nombre                  unidad    incluir     ignorar    limpiar                    conversión     redondeo
    (0, 'Hora y Minuto',        'HH:MM', '<strong>',  False,     (),                        None,          None   ),
    (2, 'Condición',            '',       False,     '<strong>', ('<br>',),                 None,          None   ),
    (2, 'Temperatura',          '°C',    '<strong>',  False,     ('°',),                    Temperatura,   1      ),
    (3, 'Sensación Térmica',    '°C',     False,      False,     ('°',),                    Temperatura,   1      ),
    (4, 'Punto de Rocío',       '°C',     False,      False,     ('°',),                    Temperatura,   1      ),
    (5, 'Humedad',              '%',      False,      False,     ('%',),                    int,           None   ),
    (6, 'Visibilidad',          'Km',     False,      False,     ('mi'),                    Visibilidad,   1      ),
    (7, 'Presión',              'bar',    False,      False,     ('pulg.', '↑', '↓', '→'),  Presión,       5      ),
    (8, 'Velocidad del Viento', '',       False,      False,     (),                        Viento,        None   ),
    #0  1                       2         3           4          5                          6              7
)
URL = "http://espanol.weather.com/weather/almanacHourly-Posadas-(aeropuerto)-ARMS0118"
INTENTOS_MAXIMOS_AL_SERVIDOR = 10
AMANECER_ATARDECER = re.compile('.*?(amanecer|atardecer).*?(\d{2}:\d{2}).*', re.IGNORECASE).match


def escribir_datos(datos, mode='a'):
    with open(DATA, mode=mode) as fichero:
        fichero.write('{}\n'.format(';'.join(map(str, datos))))


def escribir_errores(error, mode='a'):
    with open(LOGS, mode=mode, encoding='utf-8') as fichero:
        fichero.write('{}\n'.format(error))


def obtener_día(día, reintento=False):
    try:
        response = http.get(URL, params={"day": día})
    except:
        if not reintento:
            for i in range(INTENTOS_MAXIMOS_AL_SERVIDOR):
                time.sleep(10 + 10 * (i + random()))
                try:
                    print('    Reintento {}'.format(i))
                    return obtener_día(día, reintento=True)
                except:
                    pass
            error = 'ERROR EN EL SERVIDOR (día:{}).\n{}\n'.format(
                día, traceback.format_exc().strip().replace('\n', '    \n'))
            escribir_errores(error)
        raise
    else:
        html = ParserXML()
        html.cargar(response.content.decode())
        return html


def main(args, números):
    if os.path.isfile(DATA) and not args.s and not args.a:
        raise OSError('Ya existe el destino {}'.format(DATA))

    if not args.a:
        unidad = lambda u: ' ({})'.format(u) if u else ''
        encabezado = ['Fecha'] + ['{}{}'.format(i[1], unidad(i[2])) for i in SET_DATOS[1:]]
        escribir_datos(encabezado, mode='w')

    escribir_errores('', mode=('a' if args.a else 'w'))

    keys = set()  # lista de 'fecha_hora'

    nmax = max(números)
    nmin = min(números)
    ntot = len(números)
    plural = ('n', 's') if ntot > 1 else ('', '')
    rango = 'de {} a {}'.format(nmax, nmin) if ntot > 1 else nmin
    print('Se procesará{} {} número{} ({})\n\n  Número  Porcentaje'.format(plural[0], ntot, plural[1], rango))

    base = '<html><body><div id="content"><div id="content_left">'
    for número in números:
        print('{: >8} {: >11.2%}'.format(número, (ntot + nmin - número) / ntot))
        try:
            html = obtener_día(número)
            día, mes, año = html.extraer(base + '<h2>')[0].strip().split()
            fecha = '{}{:0>2}{}'.format(año, meses_nom_num[mes.lower()], día)
            horas = html.buscar(base + '<div id="almanac_hourly"><table class="tblAlmanacHourly"><tr>')
            filas = []
            for hora in horas:
                celdas = html.buscar('.<td>', nodos=hora)
                if len(celdas) == 9:
                    datos = []
                    #--- Extracción, Limpieza y Conversión --------------------------------------------------#
                    for (i, nombre, unidad, incluir, ignorar, limpiar, conversión, redondeo) in SET_DATOS:
                        d = html.extraer(incluir=incluir, ignorar=ignorar, nodos=celdas[i])[0]
                        for l in limpiar:
                            d = d.replace(l, '')
                        d = d.strip()
                        if conversión:
                            try:
                                d = conversión(d)
                                if redondeo is not None:
                                    if redondeo is 0:
                                        d = round(d)  # devuelve entero
                                    else:
                                        d = round(d, redondeo)
                            except ValueError:
                                d = 'ND'
                        datos.append(d)
                    #----------------------------------------------------------------------------------------#
                    hora, minuto = datos[0].split(':')
                    hora, minuto = int(hora), int(minuto)
                    if minuto <= 10 or (minuto >= 50 and not hora >= 23):
                        # minuto
                        #   0~10 hora actual
                        #  11~49 ignorado
                        #  50~59 hora siguiente (se ignora si hora >= 23)
                        fecha_hora = '{}{:0>2}00'.format(fecha, hora if minuto <= 10 else hora + 1)
                        if fecha_hora not in keys:
                            filas.append([fecha_hora] + datos[1:])
                            keys.add(fecha_hora)

                elif len(celdas) == 1:
                    amanecer_ocaso = html.extraer(nodos=celdas[0])[0]
                    aa = AMANECER_ATARDECER(amanecer_ocaso)
                    if aa:
                        tipo, hm = aa.groups()
                        fecha_hora = '{}{}'.format(fecha, hm).replace(':', '')
                        filas.append([fecha_hora, tipo.capitalize()])
                    else:
                        no_información = html.extraer(incluir='<strong>', nodos=celdas[0])
                        if no_información:
                            info = no_información[0]
                        else:
                            info = amanecer_ocaso
                        error = 'Número {} {} {}'.format(número, fecha, info)
                        print(' ' * 10 + error)
                        escribir_errores(error)

            for fila in filas:
                escribir_datos(fila)

        except:
            error = 'ERROR FATAL EN EL DÍA {}\n{}'.format(número, traceback.format_exc())
            print(' ' * 10 + error)
            escribir_errores(error)

    print('')


if __name__ == '__main__':
    parser = ArgumentParser(description='Descarga los datos de espanol.weather.com.')
    parser.add_argument('-a', help='Actualizar a los datos existentes', default=False, action='store_true')
    parser.add_argument('-s', help='Silenciar si el archivo ya existe', default=False, action='store_true')
    args = parser.parse_args()

    hoy = datetime.strptime(datetime.now().strftime(FECHA_FORMATO), FECHA_FORMATO)
    ini = (hoy - datetime.strptime(FECHA_INI, FECHA_FORMATO)).days
    fin = (hoy - datetime.strptime(FECHA_FIN, FECHA_FORMATO)).days
    main(args, range(ini, fin-1, -1))

    sys.stdout.write('{} ({}) finalizado.\n'.format(NAME, SCRIPT))
