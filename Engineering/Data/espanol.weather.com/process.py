#!/usr/bin/env python3
#-*- coding: utf-8 -*-

import os
import sys
import numpy
import pandas
import datetime

PATH, NAME = os.path.split(os.path.abspath(__file__))
SCRIPT = os.path.basename(PATH)
SOURCE = os.path.join(PATH, 'ARMS0118.csv')
DATA = os.path.abspath(os.path.join(PATH, '..', '..', 'Data.h5'))


year = lambda y: (y % 4 == 0 and not y % 100 == 0) or (y % 400 == 0)
day = lambda t: t.dayofyear + (-2 if t.month >= 3 and year(t.year) else -1) # 0-364


def mensaje():
    global offset1
    texto = '  {} '.format(SCRIPT)
    offset1 = len(texto)
    sys.stdout.write(texto)
    sys.stdout.flush()


def progreso(i):
    global offset2
    sys.stdout.write('\b' * offset2)
    porcentaje = '{:.0%} '.format(i)
    offset2 = len(porcentaje)
    sys.stdout.write(porcentaje)
    sys.stdout.flush()


limite_t = lambda T: -30 < T < 60  # °C


def main(fuente, destino):
    mensaje()
    index = []
    horarios = {}
    temperatura = []
    with open(fuente, mode='r') as fichero:
        N = sum(1 for l in fichero)
        fichero.seek(0)
        for i, linea in enumerate(fichero, 1):
            progreso(i / N)
            try:
                d = linea.strip().split(';')
                i = d[0]
                if len(d) == 2:
                    fecha, hora, minuto = i[:8], i[8:10], i[10:12]
                    f = pandas.Timestamp(fecha)
                    condicion = d[1].strip()
                    horaminuto = int(hora) + int(minuto) / 60
                    horarios.setdefault(f, {})[condicion] = horaminuto
                else:
                    f = pandas.Timestamp(i)
                    t = float(d[2])  # Temperatura (°C)
                    if limite_t(t):
                        index.append(f)
                        temperatura.append(t)
            except (ValueError, IndexError):
                pass

    db = pandas.HDFStore(destino)

    # Temperaturas
    ts_t = pandas.Series(temperatura, index)
    db.put('twc/T', ts_t.resample('D').mean())  # los datos son por hora
    db.put('twc/Ta', ts_t['2007':'2014'].resample('A').mean())

    # Horarios
    index = []
    data = {'Amanecer': [], 'Atardecer':[]}
    for f, h in horarios.items():
        if len(h) == 2:
            index.append(f)
            data['Amanecer'].append(h['Amanecer'])
            data['Atardecer'].append(h['Atardecer'])
    df_aa = pandas.DataFrame(data, index)
    #db.put('twc/aa', df_aa.astype('float'))
    df_aa_gd = df_aa.groupby(day)
    db.put('twc/aadp', df_aa_gd.mean().astype('float'))
    db.put('twc/aadstd', df_aa_gd.std().astype('float'))

    db.close()


if __name__ == '__main__':
    offset1 = 0
    offset2 = 0
    main(SOURCE, DATA)
    sys.stdout.write('\b' * (offset1 + offset2))
    sys.stdout.write('  {}/{}\n'.format(SCRIPT, NAME))
