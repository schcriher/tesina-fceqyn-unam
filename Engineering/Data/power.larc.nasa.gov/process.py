#!/usr/bin/env python3
#-*- coding: utf-8 -*-

import os
import pandas

os.chdir(os.path.dirname(__file__))

year = lambda y: (y % 4 == 0 and not y % 100 == 0) or (y % 400 == 0)
day = lambda t: t.dayofyear + (-2 if t.month >= 3 and year(t.year) else -1) # 0-364

R = lambda R:   0 < R <  20  # kWh/(m²·day)
T = lambda T: -30 < T <  60  # °C
P = lambda P:   0 < P < 200  # mm

sets = (
    ('R.txt', (('R', R), ('Rm',   R), ('P',    P))),
    ('T.txt', (('T', T), ('Tmin', T), ('Tmax', T), ('P2', P))),
)
data = {'t': {}, 'd': {}}

for name, varslimit in sets:
    data['t'][name] = {}
    data['d'][name] = {}
    for var, limit in varslimit:
        data['t'][name][var] = []
        data['d'][name][var] = []
    with open(name, mode='r') as fid:
        for line in fid:
            d = line.strip().split()
            try:
                t = pandas.Timestamp(''.join(d[:3]))
                for index, (var, limit) in enumerate(varslimit, 3):
                    try:
                        n = float(d[index])
                        if limit(n):
                            data['t'][name][var].append(t)
                            data['d'][name][var].append(n)
                    except (ValueError, IndexError):
                        pass
            except (ValueError, IndexError):
                pass

d = data['d']['R.txt']['R']
t = data['t']['R.txt']['R']
s = pandas.Series(d, t)
Rdm = s.groupby(day).mean()

d = data['d']['T.txt']['T']
t = data['t']['T.txt']['T']
s = pandas.Series(d, t)
Ta = s['1981':'2014'].resample('A').mean()

db = pandas.HDFStore('../../Data.h5')
for name, varslimit in sets:
    for var, _ in varslimit:
        k = 'nasa/{}'.format(var)
        d = data['d'][name][var]
        t = data['t'][name][var]
        s = pandas.Series(d, t)
        db.put(k, s.astype('float'))
db.put('nasa/Rdm', Rdm.astype('float'))
db.put('nasa/Ta', Ta.astype('float'))
db.close()

print('  power.larc.nasa.gov/process.py')
