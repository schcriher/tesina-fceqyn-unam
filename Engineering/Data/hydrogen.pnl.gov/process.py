#!/usr/bin/env python3
#-*- coding: utf-8 -*-

import os
import sys
import pandas

PATH, NAME = os.path.split(os.path.abspath(__file__))
SCRIPT = os.path.basename(PATH)
ROOT = os.path.abspath(os.path.join(PATH, '..', '..'))
DATA = os.path.join(ROOT, 'Data.h5')

sys.path.insert(0, ROOT)
from Library.Xls import XLS

ARCHIVO = 'International_Operational_Hydrogen_Fueling_Stations.xlsx'
HOJA = 'Intl. Hydrogen Stations'


def mensaje():
    global offset1
    texto = '  {} '.format(SCRIPT)
    offset1 = len(texto)
    sys.stdout.write(texto)
    sys.stdout.flush()


def progreso(i):
    global offset2
    sys.stdout.write('\b \b' * offset2)
    porcentaje = '{:.0%} '.format(i)
    offset2 = len(porcentaje)
    sys.stdout.write(porcentaje)
    sys.stdout.flush()


def main(fuente, destino):
    mensaje()
    data_t = {}
    data_e = {}

    N = fuente.nrows(HOJA)
    for i in range(4, N + 1):
        progreso(i/N)
        pais = fuente.extraer("{}.A{}".format(HOJA, i)).lower().strip().title()
        status = fuente.extraer("{}.H{}".format(HOJA, i)).lower().strip()
        producción = fuente.extraer("{}.P{}".format(HOJA, i))

        data_t.setdefault(status, {}).setdefault(pais, 0)
        data_e.setdefault(status, {}).setdefault(pais, 0)
        
        data_t[status][pais] += 1
        if producción.lower().find('electroly') >= 0:
            data_e[status][pais] += 1

    db = pandas.HDFStore(destino)
    df_t = pandas.DataFrame(data_t).fillna(0)
    df_e = pandas.DataFrame(data_e).fillna(0)
    db.put('pnl/estaciones/totales', df_t.astype('int32'))
    db.put('pnl/estaciones/electroliticas', df_e.astype('int32'))
    db.close()


if __name__ == '__main__':
    fuente = XLS(os.path.join(PATH, ARCHIVO))
    offset1 = 0
    offset2 = 0
    main(fuente, DATA)
    sys.stdout.write('\b \b' * (offset1 + offset2))
    sys.stdout.write('  {}/{}\n'.format(SCRIPT, NAME))
