#!/usr/bin/env python3
#-*- coding: utf-8 -*-

import os
import sys
import numpy
import pandas

PATH, NAME = os.path.split(os.path.abspath(__file__))
SCRIPT = os.path.basename(PATH)
SOURCE = os.path.join(PATH, 'POSADAS.txt')
DATA = os.path.abspath(os.path.join(PATH, '..', '..', 'Data.h5'))


def mensaje():
    global offset1
    texto = '  {} '.format(SCRIPT)
    offset1 = len(texto)
    sys.stdout.write(texto)
    sys.stdout.flush()


def progreso(i):
    global offset2
    sys.stdout.write('\b' * offset2)
    porcentaje = '{:.0%} '.format(i)
    offset2 = len(porcentaje)
    sys.stdout.write(porcentaje)
    sys.stdout.flush()


limite_t = lambda t: -30 < t < 60  # °C
limite_p = lambda p: p >= 0        # mm


def main(fuente, destino):
    mensaje()
    Tindex, T = [], []
    Pindex, P = [], []
    with open(fuente, mode='r') as fichero:
        N = sum(1 for l in fichero)
        fichero.seek(0)
        for i, linea in enumerate(fichero, 1):
            #progreso(i / N)
            try:
                d = linea.strip().split()
                f = pandas.Timestamp(d[0])
                t = float(d[2])  # Temperatura media, °C
                p = float(d[4])  # Precipitación, mm
                if limite_t(t):
                    Tindex.append(f)
                    T.append(t)
                if limite_p(p):
                    Pindex.append(f)
                    P.append(p)
            except (ValueError, IndexError):
                pass

    db = pandas.HDFStore(destino)

    ts_t = pandas.Series(T, Tindex)
    db.put('inta/T', ts_t.resample('D').mean())
    db.put('inta/Ta', ts_t['2005':'2010'].resample('A').mean())

    ts_p = pandas.Series(P, Pindex)
    db.put('inta/P', ts_p)

    db.close()


if __name__ == '__main__':
    offset1 = 0
    offset2 = 0
    main(SOURCE, DATA)
    sys.stdout.write('\b' * (offset1 + offset2))
    sys.stdout.write('  {}/{}\n'.format(SCRIPT, NAME))
