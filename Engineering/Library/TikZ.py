# -*- coding: utf-8 -*-
# Copyright 2012 Schmidt Cristian Hernán

from itertools import cycle as cycler

ø = 3   # Número de dígitos de redondeo
ł = 6   # Número de point en la primera línea

def ensurelen(v):
    if hasattr(v, '__len__'):
        return v
    if hasattr(v, '__iter__'):
        return list(v)
    return [v]

def getrectangle(x, y):
    # x = (x_ini, y_end)
    # y = (y_ini, y_end)
    #       4 ← 3 (end)
    #           ↑
    # (ini) 1 → 2
    xx = x[0], x[1], x[1], x[0]
    yy = y[0], y[0], y[1], y[1]
    return xx, yy

def point(x, y, param=None):
    param = '[{}]'.format(param) if param else ''
    return r'({}{:.{ø}f},{:.{ø}f})'.format(param, x, y, ø=ø)

def node(x, y, text, param=None, xoffset=0, yoffset=0):
    x += xoffset
    y += yoffset
    param = '[{}]'.format(param) if param else ''
    return r'\node{} at {} {{{}}};'.format(param, point(x, y), text)

def path(x, y, param=None, type='draw', sep=' -- ', margin=4, xoffset=0, yoffset=0, cycle=False):
    x = ensurelen(x)
    y = ensurelen(y)
    if len(x) >= len(y):
        y = cycler(y)
    else:
        x = cycler(x)
    points = [point(i+xoffset, j+yoffset) for i,j in zip(x,y)]
    if len(points) > ł:
        mar = ' ' * margin
        ini = '\n{}'.format(mar)
        sep = '{}\n{}'.format(sep, mar)
    else:
        ini = ''
    end = (sep + 'cycle') if cycle else ''
    data = ini + sep.join(points) + end
    param = '[{}]'.format(param) if param else ''
    return r'\{}{} {};'.format(type, param, data)

def draw(x, y, param=None, **kwarg):
    kwarg['type'] = 'draw'
    return path(x, y, param, **kwarg)

def fill(x, y, param=None, **kwarg):
    kwarg['type'] = 'fill'
    return path(x, y, param, **kwarg)

def filldraw(x, y, param=None, **kwarg):
    kwarg['type'] = 'filldraw'
    return path(x, y, param, **kwarg)

