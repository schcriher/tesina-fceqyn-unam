# -*- coding: utf-8 -*-
# Copyright 2012 Schmidt Cristian Hernán

class FiltrosXML:
    """ Estructura de datos para los filtros del código XML. Basado en http://www.w3.org/TR/2008/REC-xml-20081126/

        ATRIBUTOS:
            Etiquetas                   Expresión regular para la extracción de etiquetas en el código (Clase SRE_Pattern)
            Atributos                   Expresión regular para la extracción de atributos en las etiquetas (Clase SRE_Pattern)
            CharEntityRef               Expresión regular para la sustitución de las entidades XML (Clase SRE_Pattern)
    """

    def __init__(self, flexible=False):
        """ Inicializa el objeto objFiltrosXML """
        Char            = '[\u0009\u000A\u000B\u000C\u000D\u0020-\uD7FF\uE000-\uFFFD]'
        NameStartChar   = '[:A-Z_a-z\u00C0-\u00D6\u00D8-\u00F6\u00F8-\u02FF\u0370-\u037D\u037F-\u1FFF\u200C-\u200D\u2070-\u218F\u2C00-\u2FEF\u3001-\uD7FF\uF900-\uFDCF\uFDF0-\uFFFD]'  
        NameChar        = NameStartChar[:-1]+'\-.0-9\u00B7\u0300-\u036F\u203F-\u2040]'
        Name            = '('+NameStartChar+NameChar+'*?)'                                      # 1 grupo: nombre
        S               = '[\u0009\u000A\u000B\u000C\u000D\u0020]' # u'\t\n\v\f\r '
        AttValue        = '(?:"[^"]*?"|\'[^\']*?\'|[^\'">'+S[1:-1]+']+)'
        Eq              = S+'*?='+S+'*?'
        if flexible:
            # Ejemplo:  <td nowrap>
            Attribute   = Name[1:-1]+'(?:'+Eq+AttValue+')?'
        else:
            Attribute   = Name[1:-1]+Eq+AttValue
        PEReference     = '%'+Name+';'
        CharRef         = '(?:&#(?:([0-9]+)|x([0-9a-fA-F]+));)'
        EntityRef       = '(?:&'+Name+';)'

        # TIPO <? ?>  
        ET1             = '(?:<\?'+Name+'(?:'+S+'+?('+Char+'*?))?\?>)'                          # 2 grupos [XMLDeclPI]: nombre, contenido

        # TIPO <!  >
        ET2_1           = '(--)('+Char+'*?)--'                                                  # 2 grupo: '--', comentario (para capturar comentarios vacios <!---->)
        ET2_2           = Name+'(?:'+S+'+?('+Char+'*?(?:\['+Char+'*?\])?)'+S+'*?)?'             # 2 grupos [Ej: DOCTYPE]
        ET2_3           = '\['+S+'*?(?:'+Name+'|'+PEReference+')'+S+'*?\[('+Char+'*?)\]\]'      # 3 grupos [Ej: CDATA]
        ET2             = '(?:<!(?:'+ET2_1+'|'+ET2_2+'|'+ET2_3+')>)'                            # 7 grupos: nombre, comentario | nombre, contenido | nombre, nombre·ref, contenido

        # TIPO <   >
        ET3             = '(?:<'+Name+'((?:'+S+'+?'+Attribute+')*?)'+S+'*?>)'                   # 2 grupos [STag]: nombre, atributos

        # TIPO </  >
        ET4             = '(?:</'+Name+'>)'                                                     # 1 grupo [ETag]: nombre

        # TIPO <  />
        ET5             = '(?:<'+Name+'((?:'+S+'+?'+Attribute+')*?)'+S+'*?/>)'                  # 2 grupos [EmptyElemTag]: nombre, atributos

        import re
        self.Etiquetas = re.compile(ET1+'|'+ET2+'|'+ET3+'|'+ET4+'|'+ET5)                        # 14 elementos
        self.Atributos = re.compile(Name+Eq+'('+AttValue[3:])                                   # 2 elementos
        self.CharEntityRef = re.compile(CharRef+'|'+EntityRef)                                  # 3 elementos


    def __repr__(self):
        """ Representación del objeto objFiltrosXML, formato: 'FiltrosXML'
                repr(objFiltrosXML)
        """
        return 'FiltrosXML'


class NodoXML:
    """ Estructura de datos para cada nodo dentro del árbol del código XML.

        ATRIBUTOS:
            nombre                      Nombre del nodo (str)
            tipo                        Tipo de nodo, definidos en los distintos filtros (str)
                                        'Raiz', 'XMLDecl', 'PI', 'C', 'DTMD', 'CD', 'STag', 'ETag', 'EETag'
            indice                      Numero de aparición de la etiqueta, se cuentan todas excepto las de cierre
            nivel                       Nivel de profundidad (raiz = 0). Ejemplo: html = 1; head, body = 2 (normalmente)
            atributos                   Diccionario o cadena de caracteres con los atributos de la etiqueta
            posiciones                  Lista con las 6 posiciones de la etiqueta de apertura, del contenido y 
                                        de la etiqueta de cierre, inicio y final de cada una (lista)
            padre                       Nodo padre (objNodoXML)
            hijos                       Lista de Nodos de hijos (objNodoXML)
    """

    def __init__(self, nombre, tipo, indice, nivel, atributos, posiciones, padre, hijos):
        self.nombre = nombre
        self.tipo = tipo
        self.indice = indice
        self.nivel = nivel
        self.atributos = atributos
        self.posiciones = posiciones
        self.padre = padre
        self.hijos = hijos


    def __repr__(self):
        """ Representación del objeto objNodoXML, formato: 'NodoXML:' índice ':' nombre ':' tipo
                repr(objNodoXML)
        """
        return 'NodoXML:{0}:{1}:{2}'.format(self.indice, self.nombre, self.tipo)


    def __eq__(self, obj):
        """ Verifica la igualdad de dos objNodoXML, si uno no es objNodoXML devuelve False
                objNodoXML1 == objNodoXML2
                objNodoXML in "ALGO"
        """
        if isinstance(obj, NodoXML):
            if self.nombre == obj.nombre and \
                    self.tipo == obj.tipo and \
                    self.indice == obj.indice and \
                    self.nivel == obj.nivel and \
                    self.atributos == obj.atributos and \
                    self.posiciones == obj.posiciones and \
                    len(self.hijos) == len(obj.hijos):
                # No se comprueba la igualdad del padre ni los hijos por ser referencias recursivas
                # Con "posiciones" sería tecnicamente suficiente, pero por generalización se mantienen las demas comprobaciones
                return True
        return False


    def __ne__(self, obj):
        """ Verifica la desigualdad de dos objNodoXML, si uno no es objNodoXML devuelve True
                objNodoXML1 != objNodoXML2
        """
        if isinstance(obj, NodoXML):
            if self == obj:
                return False
        return True


    def __bool__(self):
        """ Verifica el valor boleano del nodo
                if objNodoXML
        """
        return any([self.nombre,
                    self.tipo,
                    self.indice,
                    self.nivel,
                    self.atributos,
                    self.posiciones,
                    self.padre,
                    self.hijos])


    # #####
    # FIXME NO ESTA EN LOS UNITEST!!!
    # #####
    def __getitem__(self, nombre):
        """ Permite obtener un atributo del nodo con su nombre
                nodo = objNodoXML['class']
                nodo = objNodoXML['../class']               # para aceder a la propiedad 'class' del padre                      NO IMPLEMENTADO
                nodo = objNodoXML['../../class']            # para aceder a la propiedad 'class' del padre del padre            NO IMPLEMENTADO
                nodo = objNodoXML['./<div id=1>/1/class']   # para aceder a la propiedad 'class' del hijo 1 del hijo div id=1   NO IMPLEMENTADO
                nodo = objNodoXML['./1/1/class']            # para aceder a la propiedad 'class' del hijo 1 del hijo 1          NO IMPLEMENTADO
        """
        if nombre in self.atributos:
            return self.atributos[nombre]
        else:
            raise ValueError('El nombre "{0}" no fue encontrado en los atributos del nodo "{1}"'.format(nombre, str(self)))


    def mostrar(self):
        """ Muestra toda la información de un objNodoXML
                objNodoXML.mostrar()
        """
        print('Nombre:     {0}'.format(self.nombre))
        print('Tipo:       {0}'.format(self.tipo))
        print('Indice:     {0}'.format(self.indice))
        print('Nivel:      {0}'.format(self.nivel))
        print('Atributos:  {0}'.format(self.atributos))
        print('Posiciones: {0}'.format(self.posiciones))
        print('Padre:      {0}'.format(self.padre))
        print('Hijos:      {0}'.format(self.hijos))


class ParserXML:
    """ Parser XML genérico, pensado para usarse con XML y HTML.
        Sigue de forma aproximada las recomendaciones de la W3C, no realiza validación de sintaxis.

            ATRIBUTOS:

                excepciones_etiquetas       En modo 'flexible' estas etiquetas se consideran de elemento vacío, aunque aparezcan 
                                            como de apertura
                excepciones_atributos       En modo 'atributo_insensible' los valores de estos atributos no se modifican,
                                            útil por ejemplo para no modificar una URI

                filtros                     Expresiones regulares para las búsquedas (objFiltrosXML)
                arbol                       Nodos organizados por listas: arbol[nº etiqueta][nº de aparición] == objNodoXML
                niveles                     Condición de los niveles de las etiquetas: 'incompletos' | 'completos',
                                            si todas las etiquetas fueron cerradas sera 'completos', 
                                            pueden haber errores igual por las etiquetas de cierre sin apertura
                etiquetas                   Diccionario con todas las etiquetas encontradas, con el nombre devuelve nº etiqueta
                contador                    Cuenta la cantidad de etiquetas, usado para fijar el índice de cada objNodoXML
                incorrectos                 Nodos incorrectos: inicio y final dentro del código e explicación
                                            len(incorrectos) == errores_totales - errores_corregidos
                raiz                        Contiene el nodo raíz, sus hijos son las primeras etiquetas del código (de nivel = 1)
                errores_totales             Numero total de errores encontrados
                errores_corregidos          Numero de errores corregidos, si profundidad_max_error = 0 sera errores_corregidos = 0
                clases_etiquetas            Contiene los nombres de todos los tipos de etiquetas
                                            ('Raiz', 'XMLDecl', 'PI', 'C', 'DTMD', 'CD', 'STag', 'ETag', 'EETag')
                numeros                     Diccionario con la cantidad de etiquetas de cada clase
                sumar_clase                 Las clases de etiquetas a sumar cuando se invoca a len(objParserXML),
                                            normalmente: sumar_clase = ['XMLDecl', 'PI', 'C', 'DTMD', 'CD', 'STag', 'EETag']
                nombre_comentario           Nombre a usar para los comentario, se mostrara en el arbol
                lista                       Contiene todos los nodos ordenados en orden de aparición en el código
                                            lista[10] devuelve el nodo con índice 10, lista[0] = nodo raíz
                flexible                    Modo flexible todas las etiquetas dentro de excepciones_etiquetas son de elemento vacío
                nombre_insensible           Convierte los nombres de etiqueta y atributo a minúscula
                nombre_patron               Considera un patron 're' los nombres de las etiqueta y atributo en las busquedas
                atributo_insensible         Convierte los valores de los atributos a minúscula, 
                                            excepto aquellos que están en 'excepciones_atributos'
                atributo_patron             Considera un patron 're' los valores de los atributos en las busquedas
                profundidad_max_error       Al encontrar una etiqueta mal cerrada buscar el cierre en la jerarquía, 
                                            hasta esta cantidad de niveles hacia arriba,
                                            para eliminar el limite: profundidad_max_error = 'inf'

                nivel_max_ultima_busqueda   En la ultima búsqueda, la cantidad de niveles que contuvo la ruta
                fin_nivel_ultima_busqueda   En la ultima búsqueda, el nivel máximo alcanzado
                entidades                   Diccionario con los nombre y los caracteres de las entidades XML,
                                            para agregar otra entidad: objParserXML.entidades['nombre'] = caracter/es
                                            Ejemplo:
                                                objParserXML.entidades['ZETA'] = 'Z\u005a'
                                                Cuando se encuentre en el código "&ZETA;" se reemplaza por "ZZ"

                codigo                      Contiene todo el código sin modificaciones (str)

                EN los NodosXML             TIPOS: 'Raiz'                      ATRIBUTOS: None
                                            TIPOS: 'XMLDecl', 'STag', 'EETag'  ATRIBUTOS: Diccionario
                                            TIPOS: 'PI', 'C', 'DTMD', 'CD'     ATRIBUTOS: Cadena de caracteres
    """
    excepciones_etiquetas = ['area', 'base', 'basefont', 'br', 'col', 'frame', 'hr', 'img', 'input', 'link', 'meta', 'p', 'param']
    excepciones_atributos = ['accesskey', 'alt', 'archive', 'cite', 'class', 'classid', 'codebase', 'content', 'data', 'href', 
                             'id', 'label', 'longdesc', 'name', 'scheme', 'src', 'standby', 'summary', 'title', 'usemap', 'value']

    def __init__(self, flexible=True, nombre_insensible=True, atributo_insensible=False,
                    profundidad_max_error=0, nombre_patron=False, atributo_patron=False,
                    nombre_comentario='<>'):
        """ Inicializa el objeto objParserXML """
        
        self.filtros = FiltrosXML(flexible=flexible)

        self.arbol = []  # FIXME CONVERTIR A DICCIONARIO, eliminar self.etiquetas, llamar etiquetas en vez de arbol (desaparece arbol)
        self.niveles = 'incompletos'
        self.etiquetas = {}
        self.contador = 0
        self.incorrectos = []
        self.raiz = NodoXML('Raiz', 'Raiz', self.contador, 0, None, None, None, [])
        self.errores_totales = 0
        self.errores_corregidos = 0
        self.clases_etiquetas = ('Raiz', 'XMLDecl', 'PI', 'C', 'DTMD', 'CD', 'STag', 'ETag', 'EETag')
        self.numeros = {'Raiz':1, 'XMLDecl':0, 'PI':0, 'C':0, 'DTMD':0, 'CD':0, 'STag':0, 'ETag':0, 'EETag':0}
        self.sumar_clase = ['XMLDecl', 'PI', 'C', 'DTMD', 'CD', 'STag', 'EETag']
        self.nombre_comentario = nombre_comentario
        self.lista = [self.raiz]

        self.flexible = flexible
        self.nombre_insensible = nombre_insensible
        self.nombre_patron = nombre_patron                  # FIXME VER NO PROBADO - FALTA AGREGAR
        self.atributo_insensible = atributo_insensible
        self.atributo_patron = atributo_patron              # FIXME VER NO PROBADO - AGREGADO
        self.profundidad_max_error = profundidad_max_error
        self.nivel_max_ultima_busqueda = False
        self.fin_nivel_ultima_busqueda = False

        from html.entities import entitydefs
        self.entidades = entitydefs

        self.__diff_nivel = 'no'  # USO INTERNO en self.mostrar_arbol()


    def __repr__(self):
        """ Representación del objeto objParserXML, formato: 'ParserXML'
                repr(objParserXML)
        """
        return 'ParserXML'


    def __len__(self):
        """ Devuelve el numero total de etiquetas de las clases dadas en "sumar_clase"
                numero_etiquetas = len(objParserXML)
                EJEMPLO:    Siendo objParserXML.sumar_clase = ['XMLDecl', 'PI', 'C', 'DTMD', 'CD', 'STag', 'EETag']
                            '<html><head></head><body><div>Texto de prueba</div></body></html>' ->  numero_etiquetas = 4
                            '<html><head/><body><div>Texto de prueba</div></body></html>'       ->  numero_etiquetas = 4
                            '<html><!-- Comentarios --></html>'                                 ->  numero_etiquetas = 2
        """
        contador = 0
        for clase in self.sumar_clase:
            contador += self.numeros[clase]
        return contador


    def __contains__(self, nodos):
        """ Comprueba la existencia de un nodo o un interable (lista, tupla, etc) de nodos dentro del parser, 
            en las listas de nodos se comprueba la existencia de todos

                objNodoXML in objParserXML
                [objNodoXML1, objNodoXML2] in objParserXML
        """

        # FIXME UNIFICAR LOS CASOS, EL PRIMERO DEBERIA CONVERTIRSE EN 'nodos = [nodos]'
        # FIXME VER SI HACER UN BUCLE SOBRE TODA LA LISTA ELIMINANDO 'self.arbol', O DAR LA OPCION EN '__init__' DE CREAR O NO EL ARBOL

        if isinstance(nodos, NodoXML):
            nodo = nodos
            if nodo.nombre in self.etiquetas:
                if nodo in self.arbol[self.etiquetas[nodo.nombre]]:
                    return True

            elif nodo.tipo == 'Raiz':
                if nodo == self.raiz:
                    return True

        elif hasattr(nodos, '__iter__'):
            for nodo in nodos:
                if isinstance(nodo, NodoXML):
                    if nodo.nombre in self.etiquetas:
                        if nodo not in self.arbol[self.etiquetas[nodo.nombre]]:
                            return False
                    elif nodo.tipo == 'Raiz':
                        if nodo != self.raiz:
                            return False
                    else:
                        return False
                else:
                    return False
            else:
                return True

        return False


    def __bool__(self):
        """ Verifica el valor boleano del objParserXML, objParserXML es True si objParserXML.raiz es True
                if objParserXML
        """
        return bool(self.raiz)


    def __getitem__(self, indice):
        """ Permite obtener un nodo con su indice (objNodoXML.indice)
                nodo = objParserXML[indice]
        """
        if isinstance(indice, int):
            return self.lista[indice]
        else:
            raise TypeError('El índice debe ser un entero')


    def __atributos_a_diccionario(self, codigo_atributos):
        """ Devuelve un diccionario con todos los atributos y sus valores del "codigo_atributos"
            Tiene en cuenta las banderas "nombre_insensible" y "atributo_insensible" y se usa la lista "excepciones_atributos"

                Ejemplo:
                    Con:        "content='text/html; charset=UTF-8' http-equiv='Content-Type'"
                    Devuelve:   {'content':'text/html; charset=UTF-8', 'http-equiv':'Content-Type'}
        """
        if not isinstance(codigo_atributos, str):
            raise TypeError('La variable "codigo_atributos" debe ser una cadena de caracteres (str)')

        atributos = {}

        for (clave, valor) in self.filtros.Atributos.findall(codigo_atributos):
            if valor[0] == '"':
                valor = valor.strip('"')
            elif valor[0] == "'":
                valor = valor.strip("'")
            if self.nombre_insensible:
                if self.atributo_insensible:
                    if clave.lower() in self.excepciones_atributos:
                        atributos[clave.lower()] = valor
                    else:
                        atributos[clave.lower()] = valor.lower()
                else:
                    atributos[clave.lower()] = valor
            else:
                if self.atributo_insensible:
                    if clave.lower() in self.excepciones_atributos:
                        atributos[clave] = valor
                    else:
                        atributos[clave] = valor.lower()
                else:
                    atributos[clave] = valor
        return atributos


    def cargar(self, codigo, codificacion='utf-8'):
        """ Carga y parsea el código """
        if isinstance(codigo, str):
            self.codigo = codigo
        elif isinstance(codigo, (bytes, bytearray)):
            self.codigo = codigo.decode(codificacion)
        else:
            raise TypeError('El código debe ser de clase "str", "bytes" o "bytearray"\n  type(codigo) = {0}'.format(type(codigo)))

        jerarquia = []  # Lista de nodos con las etiquetas abiertas (Lista LIFO)

        for etiq in self.filtros.Etiquetas.finditer(self.codigo):
            # ET1                       ET2                                          ET3             ET4     ET5
            ( n·XMLDeclPI, c·XMLDeclPI, n·C, c·C, n·DTMD, c·DTMD, n·CD, nr·CD, c·CD, n·STag, c·STag, n·ETag, n·EETag, c·EETag ) = etiq.groups()
            # n·            nombre de
            # nr·           nombre en referencia de [PEReference]
            # c·            contenido de
            #
            # XMLDecl :     Declaracion XML [XML declaration]
            # PI :          Instrucciones de procesado [Processing instructions]
            # XMLDeclPI     XMLDecl o PI
            # Comment       Comentarios [Comments]
            # DTMD          Declaracion de tipo de documento y marcado [document type declaration and markup declaration]
            # CD            [character data]
            # STag          Etiqueta de apertura [start-tags]
            # ETag          Etiqueta de cierre [end-tags]
            # EETag         Etiqueta de elemento vacio [empty-element tag]

            (ini, fin) = etiq.span()

            if n·ETag:
                if self.nombre_insensible:
                    nombre = n·ETag.lower()
                else:
                    nombre = n·ETag

                if jerarquia and jerarquia[-1].nombre == nombre:
                    nodo = jerarquia.pop()
                    nodo.posiciones.extend([ini, ini, fin])  # Termina el 'STag'
                    self.numeros['ETag'] += 1
                else:
                    # En caso de ERROR o STag que son EETag
                    profundidad = 0
                    for nodo in jerarquia[::-1]:
                        if hasattr(nodo, 'posible_EETag'):
                            delattr(nodo, 'posible_EETag')
                            nodo = jerarquia.pop()
                            nodo.posiciones[2] = None                   # \ Se corrige el 'STag'
                            nodo.posiciones.extend([None, None, None])  # /  que es un 'EETag'
                            self.numeros[nodo.tipo] -= 1
                            nodo.tipo = 'EETag'
                            self.numeros['EETag'] += 1
                        else:
                            if nodo.nombre == nombre:
                                cierre_nodo = True
                                break
                            profundidad += 1
                    else:
                        cierre_nodo = False

                    if cierre_nodo and (self.profundidad_max_error == 'inf' or profundidad <= self.profundidad_max_error):
                        for ł in range(profundidad):
                            nodo = jerarquia.pop()
                            self.numeros[nodo.tipo] -= 1
                            padre = nodo.padre
                            for i, n in enumerate(padre.hijos):
                                if n == nodo:
                                    del padre.hijos[i]
                                    break
                            del nodo
                        self.errores_corregidos += profundidad
                        self.errores_totales += profundidad
                        nodo = jerarquia.pop()
                        nodo.posiciones.extend([ini, ini, fin])  # Termina el 'STag'
                        self.numeros['ETag'] += 1
                    else:
                        self.errores_totales += 1
                        if cierre_nodo:
                            informacion_error = (ini, fin, 'Limite de profundidad alcanzado')
                        else:
                            informacion_error = (ini, fin, 'Nodo no fue abierto')
                        self.incorrectos.append(informacion_error)

            else:
                self.contador += 1
                posible_EETag = None

                if jerarquia:
                    padre = jerarquia[-1]
                    nivel = len(jerarquia) + 1
                else:
                    padre = self.raiz
                    nivel = 1

                if n·STag:
                    nombre = n·STag
                    atributos = self.__atributos_a_diccionario(c·STag)
                    if self.flexible and nombre.lower() in self.excepciones_etiquetas:
                        posible_EETag = True
                    else:
                        posible_EETag = False
                    tipo = 'STag'
                    posiciones = [ini, fin, fin]  # Se completará en 'ETag'
                    nuevo_nivel = True
                    self.numeros['STag'] += 1

                else:
                    posiciones = [ini, fin, None, None, None, None]
                    nuevo_nivel = False
                    if n·XMLDeclPI:
                        nombre = n·XMLDeclPI
                        if nombre.lower() == 'xml':
                            tipo = 'XMLDecl'
                            self.numeros['XMLDecl'] += 1
                            if c·XMLDeclPI:
                                atributos = self.__atributos_a_diccionario(c·XMLDeclPI)
                            else:
                                atributos = {}
                        else:
                            tipo = 'PI'
                            self.numeros['PI'] += 1
                            if c·XMLDeclPI:
                                if self.atributo_insensible:
                                    atributos = c·XMLDeclPI.lower()
                                else:
                                    atributos = c·XMLDeclPI
                            else:
                                atributos = ''

                    elif n·C:
                        nombre = self.nombre_comentario
                        if self.atributo_insensible:
                            atributos = c·C.lower()
                        else:
                            atributos = c·C
                        tipo = 'C'
                        self.numeros['C'] += 1

                    elif n·DTMD:
                        nombre = n·DTMD
                        if c·DTMD:
                            if self.atributo_insensible:
                                atributos = c·DTMD.lower()
                            else:
                                atributos = c·DTMD
                        else:
                            atributos = ''
                        tipo = 'DTMD'
                        self.numeros['DTMD'] += 1

                    elif n·CD or nr·CD:
                        if n·CD:  # Para futuras interpretaciones de nombres en referencia
                            nombre = n·CD
                        else:
                            nombre = '%' + nr·CD + ';'
                        if self.atributo_insensible:
                            atributos = c·CD.lower()
                        else:
                            atributos = c·CD
                        tipo = 'CD'
                        self.numeros['CD'] += 1

                    elif n·EETag:
                        nombre = n·EETag
                        tipo = 'EETag'
                        self.numeros['EETag'] += 1
                        atributos = self.__atributos_a_diccionario(c·EETag)

                if self.nombre_insensible:
                    nombre = nombre.lower()

                nodo = NodoXML(nombre, tipo, self.contador, nivel, atributos, posiciones, padre, [])  # Nodo actual
                padre.hijos.append(nodo)  # Nodo actual como hijo de su padre
                self.lista.append(nodo)

                if posible_EETag:
                    # Un STag que puede ser un EETag
                    nodo.posible_EETag = True

                if nuevo_nivel:
                    jerarquia.append(nodo)

                if nombre in self.etiquetas:
                    self.arbol[self.etiquetas[nombre]].append(nodo)
                else:
                    self.etiquetas[nombre] = len(self.arbol)
                    self.arbol.append([nodo])
        else:
            for nodo in self.lista:
                if hasattr(nodo, 'posible_EETag'):
                    delattr(nodo, 'posible_EETag')
                    nodo.posiciones[2] = None                   # \ Se corrige el 'STag'
                    nodo.posiciones.extend([None, None, None])  # /  que es un 'EETag'
                    self.numeros[nodo.tipo] -= 1
                    nodo.tipo = 'EETag'
                    self.numeros['EETag'] += 1

        if not jerarquia:
            self.niveles = 'completos'


    def comprobar(self, nodos, nombre, tipo, atributos):
        """ Devuelve una lista de nodos, con los nodos que corresponden al 'nombre' y a los 'atributos'
                nodos = objParserXML.comprobar(nodos, nombre, tipo, atributos)
        """
        if isinstance(nodos, NodoXML):
            nodos = [nodos]
        elif isinstance(nodos, list):
            for nodo in nodos:
                if not isinstance(nodo, NodoXML):
                    raise TypeError('Al suministrar una lista como variable "nodos", debe contener solo objetos objNodoXML')
        else:
            raise TypeError('La variable "nodos" debe ser un objNodoXML o una lista de objNodoXML')

        if not isinstance(nombre, str):
            raise TypeError('La variable "nombre" debe ser una cadena de caracteres (str)')

        if not isinstance(tipo, str):
            raise TypeError('La variable "tipo" debe ser una cadena de caracteres (str)')

        nodos_validos = []

        for nodo in nodos:
            #
            # FIXME VER CUANDO 'self.nombre_patron=True'
            #
            if nodo.nombre == nombre and nodo.tipo == tipo:
                if isinstance(atributos, dict):
                    #
                    # FIXME VER CUANDO 'self.nombre_patron=True'
                    #
                    for clave in atributos:
                        if self.atributo_patron:
                            import re
                            if not (clave in nodo.atributos and re.match(atributos[clave], nodo.atributos[clave])):
                                break
                        else:
                            if not (clave in nodo.atributos and atributos[clave] == nodo.atributos[clave]):
                                break
                    else:
                        nodos_validos.append(nodo)
                elif isinstance(atributos, str):
                    if atributos in nodo.atributos:
                        nodos_validos.append(nodo)
                else:
                    raise TypeError('La variable "atributos" debe ser un diccionario (dict) o una cadena de caracteres (str)')

        return nodos_validos


    def hijos(self, nodos, nodos_ignorar=False):
        """ Devuelve una lista de nodos con todos los hijos de cada nodo de "nodos" excepto los nodos "nodos_ignorar"
                nodos = objParserXML.hijos(nodos)
        """
        if isinstance(nodos, NodoXML):
            nodos = [nodos]
        elif isinstance(nodos, list):
            for nodo in nodos:
                if not isinstance(nodo, NodoXML):
                    raise TypeError('Al suministrar una lista como variable "nodo", debe contener solo objetos objNodoXML')
        else:
            raise TypeError('La variable "nodo" debe ser un objNodoXML o una lista de objNodoXML')

        if not nodos_ignorar:
            nodos_ignorar = []
        if isinstance(nodos_ignorar, NodoXML):
            nodos_ignorar = [nodos_ignorar]
        elif isinstance(nodos_ignorar, list):
            for nodo_ignorar in nodos_ignorar:
                if not isinstance(nodo_ignorar, NodoXML):
                    raise TypeError('Al suministrar una lista como variable "nodos_ignorar", debe contener solo objetos objNodoXML')
        else:
            raise TypeError('La variable "nodos_ignorar" debe ser un objNodoXML, una lista de objNodoXML o False')

        nodos_hijos = []

        for nodo in nodos:
            if nodo not in nodos_ignorar:
                nodos_hijos.extend(nodo.hijos)
        
        return nodos_hijos


    def buscar(self, ruta, nodos=False):
        """ Busca la ruta en los nodos suministrados, "nodos" puede ser un objNodoXML, una lista de objNodoXML, o False (Raiz)
            Si ruta comienza con un punto se considera una ruta absoluta a partir de cada nodo, sino se busca la primer aparición
                nodos = objParserXML.buscar("<body><div class='main'>")
        """
        # TODO Permitir patrones en las busquedas, ejemplo:
        #       CODIGO:     <div id="IDComment141952399" class="idc-c idc-anonymous ">
        #       BUSQUEDA    <div id="IDComment*" class="idc-c *">

        if not isinstance(ruta, str):
            raise TypeError('La "ruta" debe ser un objeto str')

        if not nodos:
            nodos = [self.raiz]
        elif isinstance(nodos, NodoXML):
            nodos = [nodos]
        elif isinstance(nodos, list):
            for nodo in nodos:
                if not isinstance(nodo, NodoXML):
                    raise TypeError('Al suministrar una lista como variable "nodo", debe contener solo objetos objNodoXML')
        else:
            raise TypeError('La variable "nodo" debe ser un objNodoXML, una lista objNodoXML o False')

        nivel_max = self.filtros.Etiquetas.subn('', ruta)[1]
        self.nivel_max_ultima_busqueda = nivel_max
        nivel = 0

        for etiq in self.filtros.Etiquetas.finditer(ruta):
            nivel += 1

            # Nombres y atributos
            ( n·XMLDeclPI, c·XMLDeclPI, n·C, c·C, n·DTMD, c·DTMD, n·CD, nr·CD, c·CD, n·STag, c·STag, n·ETag, n·EETag, c·EETag ) = etiq.groups()
            #
            if n·XMLDeclPI:
                nombre = n·XMLDeclPI
                if nombre.lower() == 'xml':
                    tipo = 'XMLDecl'
                    if c·XMLDeclPI:
                        atributos = self.__atributos_a_diccionario(c·XMLDeclPI)
                    else:
                        atributos = {}
                else:
                    tipo = 'PI'
                    if c·XMLDeclPI:
                        if self.atributo_insensible:
                            atributos = c·XMLDeclPI.lower()
                        else:
                            atributos = c·XMLDeclPI
                    else:
                        atributos = ''
            elif n·C:
                nombre = self.nombre_comentario
                tipo = 'C'
                if self.atributo_insensible:
                    atributos = c·C.lower()
                else:
                    atributos = c·C
            elif n·DTMD:
                nombre = n·DTMD
                tipo = 'DTMD'
                if c·DTMD:
                    if self.atributo_insensible:
                        atributos = c·DTMD.lower()
                    else:
                        atributos = c·DTMD
                else:
                    atributos = ''
            elif n·CD or nr·CD:
                if n·CD:
                    nombre = n·CD
                else:
                    nombre = '%' + nr·CD + ';'
                tipo = 'CD'
                if self.atributo_insensible:
                    atributos = c·CD.lower()
                else:
                    atributos = c·CD
            elif n·STag:
                nombre = n·STag
                atributos = self.__atributos_a_diccionario(c·STag)
                if self.flexible and nombre.lower() in self.excepciones_etiquetas:
                    # Entonces es una Etiqueta de Elemento Vacio
                    tipo = 'EETag'
                else:
                    tipo = 'STag'
            elif n·ETag:  # Asi es mas versatil
                nombre = n·ETag
                tipo = 'STag'
                atributos = {}
            elif n·EETag:
                nombre = n·EETag
                tipo = 'EETag'
                atributos = self.__atributos_a_diccionario(c·EETag)

            if self.nombre_insensible:
                nombre = nombre.lower()

            # Primer etiqueta en ruta
            if nivel == 1:
                primeros_nodos = []
                if ruta[0] == '.':
                    for n in nodos:
                        primeros_nodos.extend(n.hijos)
                else:
                    n = self.hijos(nodos)
                    while n:
                        n_si = self.comprobar(n, nombre, tipo, atributos)
                        primeros_nodos.extend(n_si)
                        n = self.hijos(n, n_si)
                nodos = primeros_nodos

            nodos = self.comprobar(nodos, nombre, tipo, atributos)

            # Todas las etiqueta en ruta excepto la ultima
            if nivel < nivel_max:
                nodos = self.hijos(nodos)

            # Si no hay hijos, ya no hay nada que buscar
            if not nodos:
                self.fin_nivel_ultima_busqueda = nivel
                return []

        if nivel:
            self.fin_nivel_ultima_busqueda = nivel
            return nodos
        else:
            self.fin_nivel_ultima_busqueda = 0
            return []


    def extraer(self, incluir=False, ignorar=False, etiquetas=False, nodos=False, reemplazar_entidades=True):
        """ 
            Extrae todo el código que esta en "incluir" o en "nodos", excepto lo que esta en "ignorar"
            Si se dan simultáneamente "incluir" y "nodos" se busca la ruta ("incluir") en los nodos y luego se ignora "ignorar"
            Para extraer las etiquetas de "incluir" poner etiquetas=True
            Para reemplazar las entidades XML poner reemplazar_entidades=True (Defaul)
                nodos = objParserXML.buscar("<body><div class='main'>", "<!---->")
        """
        if incluir:
            nodos_incluir = self.buscar(incluir, nodos)
        elif isinstance(nodos, NodoXML):
            nodos_incluir = [nodos]
        elif hasattr(nodos, '__iter__'):
            for nodo in nodos:
                if not isinstance(nodo, NodoXML):
                    raise TypeError('Al suministrar un iterable (lista, tupla, etc) como variable "nodos" debe contener solo objetos objNodoXML')
            nodos_incluir = nodos
        else:
            raise ValueError('Para extraer un dato debe suministrar una ruta en "incluir", uno o mas nodos en "nodos", o ambos')

        if isinstance(ignorar, str):
            ignorar = [ignorar]
        elif hasattr(ignorar, '__iter__'):
            for dato in ignorar:
                if not isinstance(dato, str):
                    raise TypeError('Al suministrar un iterable (lista, tupla, etc) como variable "ignorar" debe contener solo objetos str')
        else:
            ignorar = []

        codigos = []

        for nodo in nodos_incluir:
            codigo = ''

            if etiquetas:
                (ini, fin) = nodo.posiciones[0:2]  # posiciones etiqueta de apertura
                codigo += self.codigo[ini:fin]

            if nodo.tipo == 'STag' and len(nodo.posiciones) == 6:
                (ini, fin) = nodo.posiciones[2:4]  # posiciones del contenido de la etiqueta
                ini_tmp = ini

                nodos_ignorar = []
                for dato_ignorar in ignorar:
                    nodos_ignorar.extend(self.buscar(dato_ignorar, nodo))
                nodos_ignorar = sorted(nodos_ignorar, key=lambda nodo: nodo.indice)

                for nodo_ignorar in nodos_ignorar:
                    if nodo_ignorar.tipo == 'STag':
                        (ini_ignorar, fin_ignorar) = nodo_ignorar.posiciones[::5]
                    else:
                        (ini_ignorar, fin_ignorar) = nodo_ignorar.posiciones[0:2]
                    if ini_ignorar >= ini_tmp:  # Para evitar nodos anidados en los distintos elementos de "ignorar"
                        codigo += self.codigo[ini_tmp:ini_ignorar]
                        ini_tmp = fin_ignorar

                codigo += self.codigo[ini_tmp:fin]

                if etiquetas:
                    (ini, fin) = nodo.posiciones[4:6]  # posiciones etiqueta de cierre
                    codigo += self.codigo[ini:fin]

            if reemplazar_entidades:
                codigo = self.filtros.CharEntityRef.sub(self.reemplazo_entidades, codigo)

            codigos.append(codigo)

        return codigos


    def reemplazo_entidades(self, objeto_match):
        """ Reemplaza las entidades XML """
        decimal, hexadecimal, nombre = objeto_match.groups()
        if decimal:
            return chr(int(decimal, 10))
        elif hexadecimal:
            return chr(int(hexadecimal, 16))
        else:
            return self.entidades[nombre]


    def mostrar_arbol(self, sep='|  ', nodo=False, indice=True, atributos=False, margen=''):
        """ Imprime en pantalla el árbol identando con sep a partir de nodo
                Para mostrar desde la raiz poner nodo=False (Defaul)
                Para mostrar el índice poner indice=True (Defaul)
                Para mostrar los atributos poner atributos=True
        """
        if not isinstance(sep, str):
            raise TypeError('La variable "sep" debe ser una cadena de caracteres (str)')

        if not isinstance(margen, str):
            raise TypeError('La variable "margen" debe ser una cadena de caracteres (str)')

        if not nodo or isinstance(nodo, NodoXML):
            if not nodo:
                nodo = self.raiz

            if self.__diff_nivel == 'no':
                diff_nivel = nodo.nivel
                self.__diff_nivel = diff_nivel
            else:
                diff_nivel = self.__diff_nivel

            mostrar = '{0}'.format(nodo.nombre)
            if indice:
                mostrar += ' ({0})'.format(nodo.indice)
            if atributos:
                mostrar += ' "{0}"'.format(nodo.atributos)
            print('{0}{1}{2}'.format(margen, sep * (nodo.nivel - diff_nivel), mostrar))

            if nodo.hijos:
                for n in nodo.hijos:
                    self.mostrar_arbol(sep, n, indice, atributos, margen)
                    self.__diff_nivel = diff_nivel

            self.__diff_nivel = 'no'
        else:
            raise TypeError('El nodo debe ser un objNodoXML o False')


    def no_procesados(self, grupos=True, rangos=True):
        """ Devuelve el indice (en el codigo) de todos los caracteres que no pasaron los filtros, 
            en condiciones normales "no_procesados" debe devolver []
        """
        conjunto_completo = set(range(len(self.codigo)))
        conjunto_procesado = set()

        for nodo in self.lista[1:]:
            if nodo.tipo == 'STag':
                if len(nodo.posiciones) == 6:
                    (ini, fin) = nodo.posiciones[::5]
                else:
                    #
                    # FIXME VER QUE HACER EN ESTE CASO
                    #
                    print('{0} ({1}) "{2}"   nodo.posiciones: {3}'.format(nodo.nombre, nodo.indice, nodo.atributos, nodo.posiciones))
            else:
                (ini, fin) = nodo.posiciones[0:2]
            conjunto_procesado.update(set(range(ini, fin)))

        sin_procesar = sorted(conjunto_completo - conjunto_procesado)

        if grupos:
            lista = []
            ini = sin_procesar[0]
            anterior = ini
            for n in sin_procesar[1:]:
                if n == anterior+1:
                    anterior = n
                else:
                    if ini == anterior:
                        lista.append(ini)
                    else:
                        if rangos:
                            lista.append([ini, anterior+1])  # Para poder usar en range()
                        else:
                            lista.append([ini, anterior])
                    ini = n
                    anterior = ini
            if ini == anterior: # Para el ultimo 'n'
                lista.append(ini)
            else:
                if rangos:
                    lista.append([ini, anterior+1])  # Para poder usar en range()
                else:
                    lista.append([ini, anterior])
        else:
            lista = sin_procesar

        return lista


    def mostrar_no_procesados(self, sep='-'*20, grupos=True, rangos=True):
        """ Imprime el texto no procesado """
        for i in self.no_procesados(grupos=grupos, rangos=rangos):
            print('{} {}'.format(i, sep))
            if isinstance(i, list):
                print(self.codigo[i[0]:i[1]])
            else:
                print(self.codigo[i])
        print(sep)
