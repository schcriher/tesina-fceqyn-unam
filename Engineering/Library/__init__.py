# -*- coding: utf-8 -*-
# Copyright 2012 Schmidt Cristian Hernán

tick = lambda i: '{{{}}}'.format(', '.join(map(str, i)))

import numpy

from .Files import Localizador
loc = Localizador()

from .Utils import sturges

from .Xls import XLS as _XLS, xls

class XLS(_XLS):
    def __init__(self, archivo, *args, **kwargs):
        try:
            f = loc(archivo, tipo='xls')
        except:
            try:
                f = loc(archivo, tipo='xlsx')
            except:
                f = None
        if f is None:
            err = "Archivo inexistente o sin acceso de lectura. Type={}, File={}"
            raise ValueError(err.format('xls[x]', archivo))
        super().__init__(f, *args, **kwargs)

from .Latex import Tabular, Cuadro as _Cuadro, Figura as _Figura

_loc_dfg = dict(tfile=False, tparent=False, cparent=True)

class Cuadro(_Cuadro):
    def __init__(self, destino, *args, **kwargs):
        destino = loc(destino, tipo='tables', **_loc_dfg)
        super().__init__(destino, *args, **kwargs)

class Figura(_Figura):
    def __init__(self, destino, *args, **kwargs):
        destino, tikzpath = loc(destino, tipo='figures', **_loc_dfg)
        super().__init__(destino, tikzpath, *args, **kwargs)

def histograma(archivo, datos, parametros=None, porcentaje=None):
    if isinstance(archivo, Figura):
        g = archivo
    elif isinstance(archivo, str):
        g = Figura(destino=archivo)
    else:
        raise TypeError('"archivo" debe ser un str o un Gráfico')
    g.parametros['set layers'] = None
    parametros = parametros or {}
    bins = int(parametros.get('bins', sturges(len(datos))))
    hist, edges = numpy.histogram(datos, bins=bins)
    mids_edges = [(edges[i] + edges[i - 1]) / 2 for i in range(len(edges))[1:]]
    xmin = parametros.get('xmin', min(datos))
    xmax = parametros.get('xmax', max(datos))
    ymin = parametros.get('ymin', min(hist))
    ymax = parametros.get('ymax', max(hist))
    parametros_eje = {}
    for eje in ('x', 'y'):
        for par in ('label', 'tick', 'ticklabels'):
            key = eje + par
            if key in parametros:
                parametros_eje[key] = parametros[key]
    if parametros.get('vcount'):
        parametros_eje['nodes near coords'] = None
        parametros_eje['every node near coord/.style'] = '{font=\\tiny}'
    
    parametros_eje.update({
        'ybar': None,
        'point meta': 'y',
        'xtick align': 'inside',
        'bar width': '{:.6f}'.format(edges[1] - edges[0]),
        'xmin': xmin,
        'xmax': xmax,
        'ymin': ymin,
        'ymax': ymax,
    })
    if porcentaje:
        parametros_eje['axis x line*'] = 'bottom'
    g.eje(**{
        'tipo': 'axis',
        'parametros': parametros_eje,
        'orden': (None, 'xtick align'),
        
    })
    g.serie(**{
        'x': mids_edges,
        'y': hist,
        'leyenda': parametros.get('leyenda'),
        'etiqueta': parametros.get('etiqueta'),
        'parametros': {
            'draw': 'gray!33!black',
            'fill': 'gray!33!white',
        },
    })
    if porcentaje:
        digit = int(porcentaje.get('round', 0))
        interval = bool(porcentaje.get('interval'))
        acumulative = bool(porcentaje.get('acumulative'))
        xtick = porcentaje.get('xtick')
        if not xtick:
            xtick = parametros.get('xtick')
            if not xtick:
                xtick = tick(numpy.linspace(xmin, xmax, 6).astype(int))
        bins = xtick.count(',')
        hist = numpy.histogram(datos, bins=bins)[0]
        if not interval:
            hist = numpy.concatenate(([0], hist))
        data = ((hist.cumsum() if acumulative else hist) / hist.sum() * 100).round(digit)
        diff = data.sum() - 100
        if not acumulative and diff != 0:
            if not (-2 < diff < 2):
                print(('PRECAUCIÓN (histograma): La diferencia en el cálculo de los '
                    'porcentajes da una diferencia mayor a uno ({}).').format(diff))
            index = data.argmax()
            data[index] = data[index] - diff
        xticklabels = tick('\SI{{{:.{d}f}}}{{\percent}}'.format(i, d=digit) for i in data)
        xticklabelstyle = porcentaje.get('xticklabelstyle', '{font=\\tiny}')
        g.eje(**{
            'tipo': 'axis',
            'parametros': {
                'axis x line*': 'top',
                'axis y line': 'none',
                'xtick': xtick,
                'xticklabels': xticklabels,
                'x tick label as interval': interval,
                'every x tick label/.style': xticklabelstyle,
                'xmin': xmin,
                'xmax': xmax,
                'ymin': 0,
                'ymax': 100,
            },
        })
    g.tex(parametros.get('usar_leyendas'))

paises_en_es = {
    'Argentina':        'Argentina',
    'Australia':        'Australia',
    'Austria':          'Austria',
    'Belgium':          'Bélgica',
    'Brazil':           'Brasil',
    'Canada':           'Canadá',
    'China':            'China',
    'Costa Rica':       'Costa Rica',
    'Czech Republic':   'República Checa',
    'Denmark':          'Dinamarca',
    'England':          'Inglaterra',
    'Finland':          'Finlandia',
    'France':           'Francia',
    'Germany':          'Alemania',
    'Greece':           'Grecia',
    'Hong Kong':        'Hong Kong',
    'Iceland':          'Islandia',
    'India':            'India',
    'Italy':            'Italia',
    'Japan':            'Japón',
    'Netherlands':      'Países Bajos',
    'Norway':           'Noruega',
    'Scotland':         'Escocia',
    'Singapore':        'Singapur',
    'Slovenia':         'Eslovenia',
    'South Korea':      'Corea del Sur',
    'Spain':            'España',
    'Sweden':           'Suecia',
    'Switzerland':      'Suiza',
    'Taiwan':           'Taiwan',
    'Thailand':         'Tailandia',
    'Turkey':           'Turquía',
    'Wales':            'Gales',
}
