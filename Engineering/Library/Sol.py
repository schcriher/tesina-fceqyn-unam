# -*- coding: utf-8 -*-
# Copyright 2012 Schmidt Cristian Hernán

import math
from numpy import arange, linspace, zeros, concatenate, ndarray
from numpy import pi, sin, cos, tan, arcsin, arccos, arctan2, exp
from scipy.integrate import simps

tpi = 2 * pi            # 2·π

fdr = tpi / 365         # factor de días a radianes
frd = 365 / tpi         # factor de radianes a días
fgr = tpi / 360         # factor de grados a radianes
frg = 360 / tpi         # factor de radianes a grados
frh = 24 / tpi          # factor de radianes a horas
fhr = tpi / 24          # factor de horas a radianes

fds = 24 * 3600         # factor de dias a segundos
fsd = 1 / fds           # factor de segundos a dias
frs = fds / tpi         # factor de radianes a segundos
fsr = tpi / fds         # factor de segundos a radianes

Gsc = 1367              # W/m², extraterrestre
Ls = -3 * 15 * fgr      # rad, longitud geográfica de referencia, UTC-3 (15°/h)
Le = -56.002 * fgr      # rad, longitud geográfica local                            
φ = -27.367 * fgr       # rad, latitud geográfica

a0 = 0.3334             # \
a1 = 9.1815             #  > Ajustados con datos de la NASA
k  = 4.2049             # /

sinφ = math.sin(φ)
cosφ = math.cos(φ)
tanφ = math.tan(φ)

class Sol:

    def __init__(self, start=None, stop=None, step=None, slope=0, sun=False):
        """ start, stop, step: línea temporal (horas)  -12 <= start/stop <=  12
            slope: pendiente del panel (grados)          0 <=   slope    <= 180
            sun: start y stop basado en la hora solar (False 'hora civil')
            start y stop: '<0' en la mañana, '0>' en la tarde"""

        # β: Ángulo de inclinación de la superficie (0=piso; 90=pared)
        self.β = slope * fgr
        sinφβ = math.sin(φ + self.β)
        cosφβ = math.cos(φ + self.β)
        tanφβ = math.tan(φ + self.β)

        # n: Número de día del año (0 < n < 364)
        #   0 =  1 de enero
        # 364 = 31 de diciembre
        self.n = range(365)
        n = arange(365)

        # Γ: ángulo diario, radianes
        Γ = n * fdr
        sin1Γ = sin(    Γ); cos1Γ = cos(    Γ)
        sin2Γ = sin(2 * Γ); cos2Γ = cos(2 * Γ)
        sin3Γ = sin(3 * Γ); cos3Γ = cos(3 * Γ)

        # Spencer (1971) error máximo: 0.0006 rad
        # δ: declinación solar, radianes
        δ = (0.006918
            - 0.399912 * cos1Γ + 0.070257 * sin1Γ
            - 0.006758 * cos2Γ + 0.000907 * sin2Γ
            - 0.002697 * cos3Γ + 0.001480 * sin3Γ
        )
        sinδ = sin(δ)
        cosδ = cos(δ)
        tanδ = tan(δ)

        # ωr: ángulo horario de salida del sol (sunrise), radianes
        # ωs: ángulo horario de puesta del sol (sunset), radianes
        # ωr = -ωs
        self.ωs = list(arccos(-tanφ * tanδ))
        self.ωsβ = list(arccos(-tanφβ * tanδ))

        # Spencer (1971) error máximo: 0.0001
        # ρ: factor de corrección de la distancia Tierra-Sol
        self.ρ = list(1.000110
            + 0.034221 * cos1Γ + 0.001280 * sin1Γ
            + 0.000719 * cos2Γ + 0.000077 * sin2Γ
        )
        
        # Spencer (1971) error máximo de 0.0025 rad
        # E: ecuación del tiempo, radianes
        self.t = list((0.0000075                       # ecuación del tiempo
                + 0.001868 * cos1Γ - 0.032077 * sin1Γ
                - 0.014615 * cos2Γ - 0.040849 * sin2Γ
            ) - (Ls - Le)                              # corrección por longitud
        )

        # Para θ: ángulo zenital
        self.θa = sinδ * sinφβ
        self.θb = cosδ * cosφβ

        # Para α: elevación solar, radianes
        # αm cuando ω=0 igual a αm = π/2 + φ - δ
        self.αa = sinφ * sinδ
        self.αb = cosφ * cosδ

        # Para ψ: Ángulo azimutal solar, radianes
        # ψ<0: Este, ψ>0: Oeste, ψ=0: Norte
        self.ψa = cosφ * tanδ

        # Solsticios y Equinoccios
        self.nsi = δ.argmax()           # solsticio de invierno
        self.nsv = δ.argmin()           # solsticio de verano
        mask = δ >= 0
        i = mask.argmax()
        j = mask[i:].argmin() + i
        self.neo = i - 1                # equinoccio de otoño
        self.nep = j - 1                # equinoccio de primavera

        # ω: Desplazamiento angular del sol sobre el plano de su trayectoria
        #    ω < 0 antes del mediodía solar, radianes
        if stop is not None:
            start = 0 if start is None else start
            step = stop if step is None else step
            num = round((stop - start) / step) + 1
            ω, ωstep = linspace(start * fhr, stop * fhr, num, retstep=True)
            self.step = ωstep * frh
            self.size = ω.size
            if sun:
                self.ω = [ω for n in self.n]
            else:
                self.ω = [ω + self.t[n] for n in self.n]

    def sunrise(self):
        "Atardecer, ω = ωr = -ωs"
        return [self.fh(-self.ωs[n], n) for n in self.n]

    def noon(self):
        "Mediodía solar, ω = 0"
        return [self.fh(0, n) for n in self.n]

    def sunset(self):
        "Amanecer, ω = ωs"
        return [self.fh(self.ωs[n], n) for n in self.n]

    def h(self):
        "Hora civil, horas"
        return [self.fh(ω, n) for ω, n in zip(self.ω, self.n)]

    def α(self):
        "Altura solar, radianes"
        return [self.fα(ω, n).clip(0) for ω, n in zip(self.ω, self.n)]

    def ψ(self):
        "Ángulo azimutal solar, radianes"
        return [self.fψ(ω, n) for ω, n in zip(self.ω, self.n)]

    def I(self, fac=1):
        "Irradiancia, W/m² * fac"
        return [self.fI(ω, n) * fac for ω, n in zip(self.ω, self.n)]

    def E(self, fac=1):
        "Irradianción, J/m² * fac"
        return [simps(i, ω) * frs * fac for i, ω in zip(self.I(), self.ω)]

    def fh(self, ω, n):
        """ h:  hora civil
            Ls: longitud estándar, radianes
            Le: longitud local, radianes
            ω < 0 en la mañana
        """
        return 12 + (ω - self.t[n]) * frh

    def fα(self, ω, n):
        """ α: elevación solar, radianes
            αm cuando ω=0 igual a αm = π/2 + φ - δ
            α = arcsin(sin(φ) * sin(δ) + cos(φ) * cos(δ) * cos(ω))
        """
        return arcsin(self.αa[n] + self.αb[n] * cos(ω))

    def fψ(self, ω, n):
        """ ψ: Ángulo azimutal solar, radianes
            ψ<0: Este, ψ>0: Oeste, ψ=0: Norte
            ψ = -arctan2(-sin(ω), cos(φ) * tan(δ) - sin(φ) * cos(ω))
        """
        return -arctan2(-sin(ω), self.ψa[n] - sinφ * cos(ω))

    def fI(self, ω, n):
        """ I: irradiancia solar, W/m²
            I!=0: ωr < ω < ωs
        """
        ωs = self.ωsβ[n]
        array = isinstance(ω, ndarray)
        if array:
            r = ω <= -ωs  # sunrise
            s = ω >=  ωs  # sunset
            Gr = zeros(r.sum())
            Gs = zeros(s.sum())
            cosω = cos(ω[~r & ~s])
        else:
            if -ωs < ω < ωs:
                cosω = math.cos(ω)
            else:
                return 0

        # Duffie and Beckman (1980)
        # θ: ángulo zenital
        # Para el Hemisferio Sur (φ<0) y mirando al norte (γ=180):
        # cosθ = sin(δ) * sin(φ + β) + cos(δ) * cos(φ + β) * cos(ω)
        cosθ = self.θa[n] + self.θb[n] * cosω

        # Hottel (1976) transmitancia atmosférica para la radiación directa
        τb = a0 + a1 * exp(-k / cosθ)

        # Liu y Jordan (1960) transmitancia atmosférica para la radiación difusa
        τd = 0.2710 - 0.2939 * τb

        Go = Gsc * self.ρ[n] * cosθ     # Irradiancia directa sobre el panel
        Gcb = τb * Go                   # Irradiancia, componente directa
        Gcd = τd * Go                   # Irradiancia, componente difusa
        G = Gcb + Gcd                   # Irradiancia global, W/m²

        if array:
            return concatenate((Gr, G, Gs))
        else:
            return G
