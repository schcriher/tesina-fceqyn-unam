# -*- coding: utf-8 -*-
# Copyright 2012 Schmidt Cristian Hernán

from os import access, R_OK, W_OK, F_OK
from os.path import dirname, abspath, join, isdir, isfile, split, splitext
from datetime import datetime

TABLES = 'Tables/tex'
FIGURES = 'Figures/tex'
ENGINEERING = 'Engineering'

XLSTESTFILE = True
H5TESTFILE = True

ERROR = "Archivo inexistente o sin acceso de lectura{}. Type={}, File={}"

class Localizador:
    """
        Localizador de archivos en la Tesina.

        Para tipo None:
            a1              ->  ROOT/a1
            c1/a1           ->  ROOT/c1/a1
            c1/a1/b.ext     ->  ROOT/c1/a1/b.ext

        Para tipo 'h5':
            a1              ->  ROOT/ENGINEERING/a1.h5
            c1/a1           ->  ROOT/ENGINEERING/c1/a1.h5
            c1/a1/b.ext     ->  ROOT/ENGINEERING/c1/a1/b.ext.h5

        Para tipo 'xls':
            a1              ->  ROOT/ENGINEERING/a1.xls
            c1/a1           ->  ROOT/ENGINEERING/c1/a1.xls
            c1/a1/b.ext     ->  ROOT/ENGINEERING/c1/a1/b.ext.xls

        Para tipo 'xlsx':
            a1              ->  ROOT/ENGINEERING/a1.xlsx
            c1/a1           ->  ROOT/ENGINEERING/c1/a1.xlsx
            c1/a1/b.ext     ->  ROOT/ENGINEERING/c1/a1/b.ext.xlsx

        Para tipo 'csv':
            a1              ->  ROOT/ENGINEERING/a1.csv
            c1/a1           ->  ROOT/ENGINEERING/c1/a1.csv
            c1/a1/b.ext     ->  ROOT/ENGINEERING/c1/a1/b.ext.csv

        Para tipo 'tables':
            a1              ->  ROOT/TABLES/a1.tex
            c1/a1           ->  ROOT/TABLES/c1/a1.tex
            c1/a1/b.ext     ->  ROOT/TABLES/c1/a1/b.ext.tex

        Para tipo 'figures':
            a1              ->  ROOT/FIGURES/a1.tex
            c1/a1           ->  ROOT/FIGURES/c1/a1.tex
            c1/a1/b.ext     ->  ROOT/FIGURES/c1/a1/b.ext.tex
    """
    paths = {
        'h5':       ENGINEERING,
        'csv':      ENGINEERING,
        'xls':      ENGINEERING,
        'xlsx':     ENGINEERING,
        'pickle':   ENGINEERING,
        'tables':   TABLES,
        'figures':  FIGURES,
    }
    exts = {
        'h5':       'h5',
        'csv':      'csv',
        'xls':      'xls',
        'xlsx':     'xlsx',
        'pickle':   'pickle',
        'tables':   'tex',
        'figures':  'tex',
    }

    def __init__(self):
        self.raiz = abspath(join(dirname(__file__), '..', '..'))
        self.calculos = abspath(join(self.raiz, ENGINEERING))
        if not isdir(self.calculos):
            raise Exception("No se encontró la carpeta: {}".format(self.calculos))
        self.figuras = abspath(join(self.raiz, FIGURES))
        self.cuadros = abspath(join(self.raiz, TABLES))

    def __call__(self, nombre, tipo=None, tfile=True, tparent=True, cparent=False):
        """ Convierte 'nombre' en el path completo

            Parametros:
            tipo        Tipo de dato a buscar
            tfile       Comprueba si existe el archivo y se puede leer
            tparent     Comprueba si existe la carpeta, se puede leer/escribir
            cparent     Crea la carpeta padre si no existe
        """
        ext = splitext(nombre)[1].lstrip('.')
        subpath = self.paths.get(tipo)
        if isfile(nombre) and ext.lower() == self.exts.get(tipo):
            direccion = nombre
            if subpath in nombre:
                nombre = splitext(nombre.split(subpath, 1)[1].lstrip('/'))[0]
            else:
                raise ValueError("El nombre del archivo no pertenece a la Tesina")
        elif tipo is None:
            direccion = abspath(join(self.raiz, nombre))
        elif tipo in self.paths:
            if tipo in self.exts:
                _ext = self.exts[tipo]
                _nombre = '{}.{}'.format(nombre, _ext) if _ext != ext else nombre
            else:
                _nombre = nombre
            direccion = abspath(join(self.raiz, subpath, _nombre))
        else:
            raise ValueError("Tipo no soportado ({})".format(tipo))

        if H5TESTFILE and tipo == 'h5':
            tfile = True
        elif XLSTESTFILE and tipo in ('xls', 'xlsx'):
            tfile = True

        devolver = (direccion, nombre) if tipo == 'figures' else direccion
        carpeta = dirname(direccion)

        if cparent and not isdir(carpeta):
            os.makedirs(carpeta)

        if tfile and (not isfile(direccion) or not access(direccion, R_OK)):
            raise ValueError(error.format('', tipo, direccion))
        elif tparent and (not isdir(carpeta) or not access(carpeta, R_OK | W_OK)):
            raise ValueError(error.format(' y/o escritura', tipo, direccion))

        return devolver
