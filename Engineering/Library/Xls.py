# -*- coding: utf-8 -*-
# Copyright 2012 Schmidt Cristian Hernán

import os
import re

from xlrd import open_workbook


class Conversor:
    """ Conversor de nombre a indice.
        Ejemplo:
            >>> conv = Conversor()

            >>> conv('A10')
            ('cell', {'rowx':9, 'colx':0})

            >>> conv('A10:A10')
            ('colx', {'rowx':9, 'colx':0})

            >>> conv('A10:F10') # Vector fila
            ('rowx', {'rowx':9, 'start_colx':0, 'end_colx':6})

            >>> conv('A10:A20') # Vector columna
            ('colx', {'colx':0, 'start_rowx':9, 'end_rowx':20})

            >>> conv('A10:B20') # matriz
            ValueError: Solo se aceptan vectores unidimencionales
    """

    def __init__(self):
        self.entrada = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
        self.salida = '0123456789'
        self.nombre_re = re.compile(r'^(?P<columna>[A-Z]+)(?P<fila>[0-9]+)$')

    def index(self, nombre):
        datos = self.nombre_re.match(nombre.upper())
        if datos:
            # COLUMNA
            x = 0
            for letra in datos.group('columna'):
                x = x * len(self.entrada) + self.entrada.index(letra)
            if x == 0:
                resultado = self.salida[0]
            else:
                resultado = ''
                while x > 0:
                    caracter = x % len(self.salida)
                    resultado = self.salida[caracter] + resultado
                    x = int(x // len(self.salida))
            offset = len(self.entrada) if len(datos.group('columna')) > 1 else 0
            columna = int(resultado) + offset
            # FILA
            fila = int(datos.group('fila')) - 1
            return (fila, columna)
        else:
            raise ValueError(nombre)

    def __call__(self, nombre):
        nombre = nombre.upper().replace('$', '')
        if ':' in nombre:
            nombre1, nombre2 = nombre.split(':', 1)
            fila1, columna1 = self.index(nombre1)
            fila2, columna2 = self.index(nombre2)
            if fila1 == fila2 and columna1 == columna2:
                return ('cell', {'rowx':fila1, 'colx':columna1})
            else:
                if fila1 == fila2:
                    return ('rowx', {
                        'rowx':       fila1,
                        'start_colx': columna1,
                        'end_colx':   columna2 + 1})
                elif columna1 == columna2:
                    return ('colx', {
                        'colx':       columna1,
                        'start_rowx': fila1,
                        'end_rowx':   fila2 + 1})
                else:
                    raise ValueError("Solo se aceptan vectores no matrices")
        else:
            fila, columna = self.index(nombre)
            return ('cell', {'rowx': fila, 'colx': columna})


class xls:
    """
        Envoltura para almacenar una ruta de XLS
        Usado para comprobar si un dato es una ruta XLS o no
        
        USO EXTERNO, NO USADO EN ESTE MODULO (xls.py)
    """

    def __init__(self, ruta):
        self.ruta = ruta

    def __str__(self):
        return self.ruta


class XLS:
    """
        Interfase de acceso a hoja de Excel
    """

    def __init__(self, archivo, devolver_siempre_lista=False):
        """
            devolver_siempre_lista  Si esta en False cuando se extrae un valor
                                    de un solo elemento se lo pasa directamente
        """
        self.libro = open_workbook(archivo)
        self._hoja = None
        self.index = Conversor()
        self._devolver_siempre_lista = bool(devolver_siempre_lista)

    def __bool__(self):
        return bool(self.libro)

    def hojas(self):
        return self.libro.sheet_names()

    def es_hoja(self, nombre):
        return nombre in self.hojas()

    @property
    def hoja(self):
        return self._hoja

    @hoja.setter
    def hoja(self, nombre):
        if not self.es_hoja(nombre):
            raise ValueError("Nombre de hoja inexistente")
        self._hoja = self.libro.sheet_by_name(nombre)

    def ncols(self, hoja):
        self.hoja = hoja
        return self._hoja.ncols

    def nrows(self, hoja):
        self.hoja = hoja
        return self._hoja.nrows

    def extraer(self, direcciones, clase=None, lista=None):
        """
            Extrae la información de las celdas
            Ejemplo:
                (NOTA: los "$" se eliminan)
                "Hoja 1.A1"
                "Hoja 1.A1:A20"
                "Hoja 1.A1,A10"
                "Hoja 1.A1,A10,A15:A20"
                "Hoja 1.A1;Hoja 2.A1"
            
            clase puede ser una clase de objeto o una tupla de clase,
                  para pasar el tipo tupla, pasarlo dentro de una tupla.
        """
        lista = lista if lista is not None else self._devolver_siempre_lista
        if '.' not in direcciones:
            raise ValueError("Debe haber al menos un punto para 'extraer'")

        direcciones = direcciones.replace('$', '')
        valores = []
        for hoja in direcciones.split(';'):
            nombre, celdas = hoja.rsplit('.', 1)
            self.hoja = nombre
            for rango in celdas.split(','):
                tipo, indices = self.index(rango)
                if tipo == 'rowx':
                    valores.extend(self.hoja.row_values(**indices))
                elif tipo == 'colx':
                    valores.extend(self.hoja.col_values(**indices))
                elif tipo == 'cell':
                    valores.append(self.hoja.cell_value(**indices))

        if clase is not None:
            if not isinstance(clase, tuple):
                clase = (clase,)
            for i, v in enumerate(valores):
                for cls in clase:
                    try:
                        valores[i] = cls(valores[i])
                    except:
                        pass
                    else:
                        break

        if lista or len(valores) > 1:
            return valores
        elif len(valores) == 1:
            return valores[0]
        else:
            return None
