# -*- coding: utf-8 -*-
# Copyright 2012 Schmidt Cristian Hernán

from math import ceil, floor, sin, cos, tan, modf
from numpy import pi, where, concatenate, zeros, full
from numpy import ceil as nceil, floor as nfloor
from numpy import sin as nsin, cos as ncos, tan as ntan
from numpy import modf as nmodf, abs as nabs, copysign as ncopysign

fgr = pi / 180          # factor de grados a radianes
π2 = pi / 2             # 90 grados
π = pi                  # 180 grados

ε = 7 * fgr             # Ángulo entre el lote y el norte

Xlc = 3                 # Límite construible, metros
Ylc = 3                 # Límite construible, metros

sinε = sin(ε)
cosε = cos(ε)
tanε = tan(ε)


class AinstError(ValueError):
    pass


def assert_ainst(var, amin, a, amax, b):
    if not (amin <= a <= amax):
        ainst_min = amin * b
        ainst_act =    a * b
        ainst_max = amax * b
        text = '{}={}: {} < Ainst={} < {}'
        raise AinstError(text.format(var, a, ainst_min, ainst_act, ainst_max))


class Zona:

    def __init__(self, nx, ny, nxa, nxb, xlim, ylim, ysep, x1, x2, y1, y2):
        #  __________________________
        #         |            |
        #         | getypos1   |
        #         |            |
        #     _x2_|x1          |
        #    |   /\  |         |
        #  y2|  /  \ |y1       |
        #    | /  Yp\|         /
        #    |/     /|         / ylim
        #    |\  Xp/ |         /
        #  y1| \  /  |y2       |
        #    |__\/___|         |
        #     x1 | x2          |
        #        |             |
        #        | getypos2    |
        #  ______|_____________|_____
        #
        self.nx = nx                            # número de paneles horizontalmente
        self.ny = ny                            # número de paneles verticalmente
        self.nxa = nxa                          # (lista) números de columnas por fila
        self.nxb = nxb                          # (lista) números de columnas por fila
        self.xlim = xlim                        # limite horizontal
        self.ylim = ylim                        # limite vertical
        self.ysep = ysep                        # separación entre paneles
        self.x1 = x1                            # x1 = Yp * sin(ε)
        self.x2 = x2                            # x2 = Xp * cos(ε)
        self.y1 = y1                            # y1 = Yp * cos(ε)
        self.y2 = y2                            # y2 = Xp * sin(ε)
        self.Nxa = len(nxa)                     # número de filas incompletas
        self.Nxb = len(nxb)                     # número de filas incompletas
        self.n = nx * ny + sum(nxa) + sum(nxb)  # número de paneles totales
        self.Ny = self.Nxa + ny + self.Nxb      # número total de filas
        self.NX = nx - 1
        self.NY = ny - 1
        self.NxaNY = self.Nxa + self.NY
        self.xoffset = xlim - x1 - x2 * nx
        self.yoffset = ylim - y1 - y2 * nx - self.NY * ysep

    def getrow1(self, c):
        "Devuelve la primera fila en la columna dada"
        for row, lim in enumerate(self.nxa, 0):
            if c < lim:
                return row
        return self.Nxa

    def getypos1(self, c, r):
        "Devuelve la posición relativa de la primera fila en el eje y"
        return (self.NX - c) * self.y2 + (r - self.Nxa) * self.ysep

    def getrow2(self, c):
        "Devuelve la última fila en la columna dada"
        c = self.NX - c
        for row, lim in enumerate(self.nxb, 1):
            if c < lim:
                return self.Ny - row
        return self.NxaNY

    def getypos2(self, c, r):
        "Devuelve la posición relativa de la última fila en el eje y"
        return c * self.y2 + (self.NxaNY - r) * self.ysep + self.yoffset

    @property
    def xempty(self):
        "Distancia en x sin uso (utilidad: agregarlo a Ihsep)"
        return self.xoffset

    @property
    def yempty(self):
        "Distancia en y sin uso (utilidad: agregarlo a Ihsep)"
        return min(self.getypos2(c, self.getrow2(c)) for c in range(self.nx))


class Paneles:

    def __init__(self, Xlote, Ylote, Pinst, Ainst, Hinst, Xpanel, Ypanel, Dpanel, β):
        """
             Norte
                \ε|
              ___\|__            Panel
             |       |             \
             |       |Ylote         \  →Norte
             |_______|          ___β_\_____
               Xlote

        Xlote: Ancho del lote (cuasi eje este-oeste)
        Ylote: largo del lote (cuasi eje norte-sur)
        """
        self.Xlim = Xlote - 2 * Xlc  # \ Límites
        self.Ylim = Ylote - 2 * Ylc  # / construibles

        self.Pinst = Pinst  # Posición de la instalación
        self.Ainst = Ainst  # área de captación de agua, m²
        self.Hinst = Hinst  # Altura de la instalación, m

        self.Linst = self.Pinst == "izquierda"
        self.Rinst = self.Pinst == "derecha"
        self.Tinst = self.Pinst == "arriba"
        self.Binst = self.Pinst == "abajo"

        self.Xinst = self.Linst or self.Rinst
        self.Yinst = self.Tinst or self.Binst

        self.Xpanel = Xpanel
        self.Ypanel = Ypanel
        self.Dpanel = Dpanel
        self.Apanel = Xpanel * Ypanel
        self.Ypzona = Ypanel / Dpanel
        self.apanel = self.Apanel / Dpanel

        self.β = β * fgr
        self.sinβ = sin(self.β)
        self.cosβ = cos(self.β)

        self.Xp = Xpanel
        self.Yp = Ypanel * self.cosβ
        self.Zp = Ypanel * self.sinβ

        #     _x2_x1_
        #    |   /\  |
        #  y2|  /  \ |y1
        #    | /  Yp\|      sin(ε) = x1/Yp = y2/Xp
        #    |/     /|
        #    |\  Xp/ |      cos(ε) = x2/Xp = y1/Yp
        #  y1| \  /  |y2
        #    |__\/___|
        #     x1   x2

        self.x1 = self.Yp * sinε
        self.x2 = self.Xp * cosε
        self.y1 = self.Yp * cosε
        self.y2 = self.Xp * sinε

        self.xx = self.x1 + self.x2
        self.yy = self.y1 + self.y2

        self.xstep = self.x1 / self.Dpanel
        self.ystep = self.y1 / self.Dpanel

    def distribución(self, Ihsep, Ysep):
        # No depende de α ni ψ
        xy = self.x1, self.x2, self.y1, self.y2

        def getcols(y):
            rows = []
            for i in range(1, floor(y / Ysep) + 1):
                n = floor((y - i * Ysep) / self.y2)
                if n > 0:
                    rows.append(n)
            return rows[::-1]  # de menor a mayor

        if self.Xinst:
            xlim = self.Xlim - Ihsep
            ylim = self.Ylim
            xmin = self.Ainst / ylim

            assert_ainst('x', self.xx, xmin, xlim, ylim)

            nx1 = ceil((xmin - self.x1) / self.x2)
            xlim1 = self.x1 + nx1 * self.x2  # límite exácto
            xlim2 = xlim - xlim1
            nx2 = floor((xlim2 - self.x1) / self.x2)

            if self.Rinst:
                nx1, nx2 = nx2, nx1
                xlim1, xlim2 = xlim2, xlim1
            z = []
            for nx, xlim in ((nx1, xlim1), (nx2, xlim2)):
                Δy = nx * self.y2
                ΔY = self.y1 + Δy
                ny = floor((ylim - ΔY) / Ysep + 1)
                nxa = getcols(Δy)
                nxb = getcols(ylim - self.y1 - (ny - 1) * Ysep)
                z.append(Zona(nx, ny, nxa, nxb, xlim, ylim, Ysep, *xy))
            z1, z2 = z
        else:
            xlim = self.Xlim
            ylim = self.Ylim - Ihsep
            nx = floor((xlim - self.x1) / self.x2)
            Δy = nx * self.y2
            ΔY = self.y1 + Δy
            ymin = self.Ainst / xlim

            assert_ainst('y', ΔY, ymin, ylim, xlim)

            ny1 = ceil((ymin - ΔY) / Ysep + 1)
            ylim1 = (ny1 - 1) * Ysep + ΔY  # límite exácto
            ylim2 = ylim - ylim1
            ny2 = floor((ylim2 - ΔY) / Ysep + 1)

            if self.Binst:
                ny1, ny2 = ny2, ny1
                ylim1, ylim2 = ylim2, ylim1
            nxa = getcols(Δy)
            nxb = getcols(ylim2 - self.y1 - (ny2 - 1) * Ysep)
            z1 = Zona(nx, ny1, nxa, nxa, xlim, ylim1, Ysep, *xy)
            z2 = Zona(nx, ny2, nxa, nxb, xlim, ylim2, Ysep, *xy)

        return z1, z2

    def setαψ(self, α, ψ):
        nt = ψ.size
        ψmin = ψ.min()
        ψmax = ψ.max()
        idx = []
        for l in (-π2-ε, -π2, -ε, 0, π2-ε, π2):
            if l <= ψmin:
                i = 0
            elif l >= ψmax:
                i = nt
            else:
                # ψ[i] > l             _
                # ψ tiene una forma  _/
                i = (ψ <= l).argmin()
            idx.append(i)
        εse, se, εn, nn, εsw, sw = idx
        idx = []
        for ini, end, arg in ((0, se, 'min'), (sw, nt, 'max')):
            m = α[ini:end] <= self.β
            if m.size and m.any():
                if m.all():
                    i = end if arg == 'min' else ini
                else:
                    # zse: α[i] > β
                    # zsw: α[i] >= β
                    # α tiene una forma  /\
                    i = getattr(m, 'arg'+arg)() + ini
            else:
                i = ini if arg == 'min' else end
            idx.append(i)
        zse, zsw = idx

        self.se  = se                                   #\
        self.εn  = εn                                   # \
        self.nn  = nn                                   #  \
        self.sw  = sw                                   #   \
        self.zse = zse if zse < self.se  else self.se   #    > índices
        self.zsw = zsw if zsw > self.sw  else self.sw   #   /
        self.εse = εse if εse > self.zse else self.zse  #  /
        self.εsw = εsw if εsw < self.zsw else self.zsw  # /
        self.nt = nt                                    #/

        # Precálculos
        s1 = slice(self.se, self.sw)
        tanψ = ntan(ψ[s1])
        tanα = ntan(α[s1])
        self.tanψ_tanε_xp = (tanψ + tanε) / self.Xp
        self.yp_tanψ_xp = self.Yp * tanψ / self.Xp
        self.sinβ_tanα_cosβ = self.sinβ / tanα + self.cosβ
        if self.Xinst:
            if self.Rinst:
                s2 = slice(self.zse, self.εn)
                ss = -1
            else:
                s2 = slice(self.εn, self.zsw)
                ss = +1
            sinψ = nsin(ψ[s2])
            tanψ = ntan(ψ[s2])
            tanα = ntan(α[s2])
            Hinst_tanα = (self.Hinst + self.Zp) / tanα
            self.Hinst_tanα_sinψ = (Hinst_tanα * sinψ * ss).clip(0)  # Sx
            self.T = range(s2.start, s2.stop)
            self.ψ = ψ[s2]
            self.tanψ = nabs(tanψ).tolist()
        else:
            if self.Tinst:
                s2 = (slice(self.εse, self.εsw),)
                ss = +1
            else:
                s2 = (slice(self.zse, self.εse), slice(self.εsw, self.zsw))
                ss = -1
            self.Hinst_tanα_cosψ = []  # Sy
            self.T = []
            self.ψ = []
            self.tanψ = []
            self.clim = []
            for s in s2:
                cosψ = ncos(ψ[s])
                tanψ = ntan(ψ[s])
                tanα = ntan(α[s])
                Hinst_tanα = self.Hinst / tanα
                self.Hinst_tanα_cosψ.append((Hinst_tanα * cosψ * ss).clip(0))  # Sy
                self.T.append(range(s.start, s.stop))
                self.ψ.append(ψ[s])
                self.tanψ.append(nabs(tanψ).tolist())

    def fac(self, y):
        if y < 0:
            return 0
        elif y > self.Dpanel:
            return self.Dpanel
        else:
            return y

    def yfac(self, Ypsep):
        # Distancia vertical de sombra sobre el panel
        Ypsom = self.Ypanel - Ypsep / self.sinβ_tanα_cosβ
        # Zonas iluminadas (0=todo oscuro, 3=todo iluminado)
        yfac = self.Dpanel - nceil(Ypsom / self.Ypzona)
        return yfac.clip(0, self.Dpanel).astype(int).tolist()

    def xfac(self, Ypsep):
        # Número de paneles no afectados por las sombras en los extremos
        # <0 afecta a la izquierda, >0 afecta a la derecha
        Xpsom = Ypsep * self.tanψ_tanε_xp - self.yp_tanψ_xp
        # El redondeo es +1.1 a +1 y -1.1 a -1
        return ncopysign(nfloor(nabs(Xpsom)), Xpsom).astype(int).tolist()

    def make(self, nx, ny, yfac):
        A = full((self.zse,           ny, nx),           0, dtype=int)  # ψ ≦ -90 ∧ α ≦ β
        B = full((self.nt - self.zsw, ny, nx),           0, dtype=int)  # ψ ≧ +90 ∧ α ≦ β
        C = full((self.se - self.zse, ny, nx), self.Dpanel, dtype=int)  # ψ ≦ -90 ∧ α ≧ β
        D = full((self.zsw - self.sw, ny, nx), self.Dpanel, dtype=int)  # ψ ≧ +90 ∧ α ≧ β
        m = [full((ny, nx), y, dtype=int) for y in yfac]
        return concatenate((A, C, m, D, B))

    def init(self, matrix, z, xfac):
        # extremos sin paneles por ε
        for r, x in enumerate(z.nxa, 0):
            matrix[:,+r,+x:] = -1
        for r, x in enumerate(z.nxb, 1):
            matrix[:,-r,:-x] = -1
        # xfac para -90 < ψ < +90
        # → en filas completas
        for t, x in zip(range(self.se, self.sw), xfac):
            start, stop = (None, x) if x >= 0 else (x, None)
            matrix[t, z.Nxa:-z.Nxb, slice(start,stop)] = self.Dpanel
        # → en filas incompletas
        matrix = matrix.tolist()
        for r in list(range(z.Nxa + 1)) + list(range(z.NxaNY, z.Ny)):
            for t, x in zip(range(self.se, self.sw), xfac):
                for c in range(z.nx):
                    c_prev = [c - x]
                    if t > self.εn:  # también afecta al panel anterior izquierdo
                        c_prev.append(c_prev[0] - 1)
                    p = []
                    for cc in c_prev:
                        if 0 <= cc < z.nx:
                            p.append(matrix[t][r-1][cc] > -1)
                        else:
                            p.append(False)
                    if matrix[t][r][c] > -1 and not any(p):
                        matrix[t][r][c] = self.Dpanel
        return matrix

    def sombras(self, Ihsep, Ypsep, Ysep, z1, z2):
        # yfac aplicable a todo panel que tenga paneles en frente, "zonas iluminadas"
        yfac = self.yfac(Ypsep)
        # xfac anula a yfac en los paneles de los extremos, "número de paneles sin sombra"
        xfac = self.xfac(Ypsep)

        m1 = self.make(z1.nx, z1.Ny, yfac)
        m2 = self.make(z2.nx, z2.Ny, yfac)

        m1[self.se:self.sw,0,:] = self.Dpanel  # primera fila sin sombra
        m2[self.se:self.sw,0,:] = self.Dpanel  # primera fila sin sombra

        m1 = self.init(m1, z1, xfac)
        m2 = self.init(m2, z2, xfac)

        # Agregado de las sombras de la instalación

        if self.Xinst: # ----------------------------- zonas izquierda y derecha
            if self.Rinst:  # derecha [afecta a la mañana]
                Z = z1
                M = m1

                def fn(c):
                    return Z.NX - c

                def getcols(c):
                    c = Z.nx - c
                    if c > 0:
                        c1 = c
                        c3 = c - 1
                    else:
                        c1 = 0
                        c3 = None
                    return c1, Z.nx, c3

                def crange(c):
                    return range(Z.nx - c, Z.nx)

            else:  # izquierda [afecta a la tarde]
                Z = z2
                M = m2

                def fn(c):
                    return c

                def getcols(c):
                    if c < Z.nx:
                        c2 = c3 = c
                    else:
                        c2 = Z.nx
                        c3 = None
                    return 0, c2, c3

                def crange(c):
                    return range(c)

            #Ihsep += Z.xoffset
            Sx = (self.Hinst_tanα_sinψ - Ihsep).clip(0)
            nc = Sx / self.x2
            som_dec, som_int = nmodf(nc)
            # → columnas enteras con sombra por la edificación
            ncols = som_int.clip(0, Z.nx).astype(int).tolist()
            # → en la columna del límite de la sombra
            yfac = self.Dpanel - nceil(som_dec * self.x2 / self.xstep)
            yfac = yfac.clip(0, self.Dpanel).astype(int).tolist()

            nlims = nceil(nc).clip(0, Z.nx).astype(int).tolist()

            f1 = (Z.getrow1, Z.getypos1)  # funciones de arriba
            f2 = (Z.getrow2, Z.getypos2)  # funciones de abajo

            for t, ncol, y, nlim, ψ, tanψ in zip(self.T, ncols, yfac, nlims, self.ψ, self.tanψ):

                getrow, getypos = f1 if -π2 < ψ < π2 else f2

                for r in range(Z.Ny):
                    # Aproximación del triángulo sin sombra
                    ss = []
                    for c in crange(nlim):
                        sx = Ihsep + self.x1 + self.x2 * fn(c)
                        sy = sx / tanψ
                        sy_calc = getypos(c, r) + self.yy
                        if sy_calc < sy:
                            ss.append((r, c))

                    c1, c2, c3 = getcols(ncol)

                    for c in range(c1, c2):
                        if M[t][r][c] > 0 and (r, c) not in ss:
                            M[t][r][c] = 0
                    if c3 is not None and y is not None:
                        if M[t][r][c3] > y and (r, c3) not in ss:
                            M[t][r][c3] = y

        else: # ------------------------------------------- zonas arriba y abajo
            if self.Tinst:  # arriba [afecta cuando -90 < ψ < +90]
                Z = z2
                M = m2

                getrow = Z.getrow1
                getypos = Z.getypos1

                def getrows(rl, d):
                    r = rl + d
                    if r < Z.Ny:
                        r2 = r3 = r
                    else:
                        r2 = Z.Ny
                        r3 = None
                    return rl, r2, r3

            else:  # abajo [afecta cuando (ψ < -90 ∨ ψ > +90) ∧ α > β]
                Z = z1
                M = m1

                getrow = Z.getrow2
                getypos = Z.getypos2

                def getrows(rl, d):
                    rl += 1  # 'rl' incluido en el range
                    r = rl - d
                    if r > 0:
                        r1 = r
                        r3 = r - 1
                    else:
                        r1 = 0
                        r3 = None
                    return r1, rl, r3

            #Ihsep += Z.yoffset
            for c in range(Z.nx):
                rl = getrow(c)
                ypos = getypos(c, rl)
                ΔY = Ihsep + ypos - (Ysep - self.Yp / cosε)
                for τ, Τ in enumerate(self.T):
                    Sy = (self.Hinst_tanα_cosψ[τ] - ΔY).clip(0)
                    Sx = self.Hinst_tanα_cosψ[τ] * self.tanψ[τ]

                    som_dec, som_int = nmodf(Sy / Ysep)
                    # → filas enteras con sombra por la edificación
                    nrows = som_int.clip(0, Z.Ny).astype(int).tolist()
                    # → en la fila del límite de la sombra
                    yfac = self.Dpanel - nceil(som_dec * Ysep / self.ystep)
                    yfac = yfac.clip(0, self.Dpanel).astype(int).tolist()

                    clims = Sx / self.x2
                    clims = where(self.ψ[τ] < 0, nceil(clims), nfloor(clims))
                    clims = clims.astype(int).tolist()

                    for t, nrow, y, clim, ψ, tanψ in zip(Τ, nrows, yfac, clims, self.ψ[τ], self.tanψ[τ]):
                        if ψ < 0:
                            fn = lambda c: Z.NX - c + 1
                            mod = c > Z.NX - clim
                        else:
                            fn = lambda c: c + 1
                            mod = c < clim

                        if mod:
                            # Aproximación del triángulo sin sombra
                            sx = self.x1 + self.x2 * fn(c)
                            sy = sx / tanψ
                            n = int(ceil((sy - ΔY) / Ysep))
                            if n < nrow:
                                nrow = n
                            y = None

                        r1, r2, r3 = getrows(rl, nrow)

                        for r in range(r1, r2):
                            if M[t][r][c] > 0:
                                M[t][r][c] = 0

                        if r3 is not None and y is not None:
                            if M[t][r3][c] > y:
                                M[t][r3][c] = y

        return m1, m2

    def área_efectiva(self, Ihsep, Ypsep, z1=None, z2=None):
        # Ihsep: separación horizontal o vertical de la instalación
        # Ypsep: separación entre paneles
        Ysep = Ypsep / cosε  # Separación vertical entre filas
        if not (z1 and z2):
            z1, z2 = self.distribución(Ihsep, Ysep)
        m1, m2 = self.sombras(Ihsep, Ypsep, Ysep, z1, z2)
        n = z1.n + z2.n
        a = []
        for t in range(self.nt):
            η = 0
            z = 0
            for m in (m1[t], m2[t]):
                for r in m:
                    for c in r:
                        if c > -1:
                            η += 1
                            z += c
            assert η == n
            a.append(z * self.apanel)
        # a: área efectiva por cada tiempo
        # n: número de paneles totales
        return a, n
