# -*- coding: utf-8 -*-
# Copyright 2012 Schmidt Cristian Hernán

import os
import re
import numpy

from datetime import datetime

from . import XLS, xls
from .Utils import (plantilla, saneado_latex, normalizado, listar_meta,
    escapar_meta_latex, numero_a_letra, escapar_meta_re)


def fulldatetime():
    return datetime.now().strftime("%Y%m%d%H%M%S%f")


def escapar_llaves(cadena):
    return str(cadena).replace('{', '{{').replace('}', '}}')


def inclusion(cadena, pos, inclusion):
    return cadena[:pos[0]] + inclusion + cadena[pos[1]:]


class Tabular:

    code = plantilla(r'«»«»\begin{tabular}«»{«»}«»\end{tabular}«»')

    def __init__(self, body, cols='c', extra='', align=''):
        self.body = body
        self.cols = cols
        self.extra = extra
        self.align = align

    def __str__(self):
        ini, fin = ('{', '}') if self.extra else ('', '')
        align = '[{}]'.format(self.align) if self.align else ''
        return self.code.format(ini, self.extra, align, self.cols, self.body, fin)


class Cuadro:

    conversiones = {
        float:          '.3f',
        numpy.float32:  '.3f',
        numpy.float64:  '.3f',
        int:            '.0f',
        numpy.int32:    '.0f',
        numpy.int64:    '.0f',
        str:            's',
    }
    plantillas = {
        'sep_columna':      ' & ',
        'sep_fila':         ' \\tabularnewline',
        'bigstruts': {
            'arriba':       ' \\bigstrut[t]',
            'ambos':        ' \\bigstrut', 
            'abajo':        ' \\bigstrut[b]',
        },
        'hlineas': {
            'hline':        '\\hline',
            'hhline':       plantilla('\\hhline{«»}'),
            'cline':        plantilla('\\cline{«»}'),
            'rule':         '\\toprule',
            'toprule':      '\\toprule',
            'bottomrule':   '\\bottomrule',
            'midrule':      '\\midrule',
            'cmidrule':     plantilla('\\cmidrule«1»{«0»}'),
        },
        # \multicolumn{ncols}{alignment}{contents}
        # \multirow{nrows}[bigstruts]{width}[fixup]{text}
        'multicolumn':      plantilla('\\multicolumn{{«»}}{{«»}}{{{}}}{}'),
        'multirow':         plantilla('{\\multirow{«»}«»{«»}«»{«»}}'),
        'Cuadro':           plantilla('«ini»«parametros»'
                                      '\\begin{«tipo»}«ancho»{«header»}'
                                      '\n«código»\\end{«tipo»}«fin»'),
    }
    correcciones = {
        'bigstruts': {
             None:          0,
            'arriba':       1,
            'abajo':        1,
            'ambos':        2,
        },
        'fixups': {
             None:          0,
            'hline':        1,
            'hhline':       1,
            'cline':        0,
            'toprule':      0,
            'bottomrule':   0,
            'midrule':      0,
            'cmidrule':     0,
        },
    }
    _re_config = re.compile(r'''^
        (?P<nmb>n)?                                 # 'no utilizar makebox' si la columna tiene
                                                    # fijado un tamaño con obj.columnas(tamaños)
        (?P<num>\d+)                                # columna a configurar (se contabiliza desde 1)
        (?:                                         #
           -(?P<cn>\d+)                             # unir columbas
            (?P<ca>[^:]+)?                          # alineación de la columna resultante
        )?                                          #
        (?:                                         #
           :(?P<fn>\d+)                             # unir filas
            (?::(?P<ff>[-+]?\d+(?:\.\d+)?[a-z]+))?  # fixup del dato en la celda, se aplica en forma
                                                    # automática excepto que se asigne un valor
            (?P<fa>[^,]+)?                          # ancho de la celda
        )?                                          #
        (?:                                         #
           ,(?P<f>.+)                               # Conversión del dato de la celda
        )?                                          #
        $''', re.VERBOSE | re.IGNORECASE).match
    _re_ancho_columnas_texto = re.compile(r'^\D.*').match
    _re_multicolumnas = re.compile(r'^\s*\*\s*\{\s*(\d+)\s*\}\s*\{(.+)\}\s*$').match
    
    # Los parentesis de los trimming son OBLIGATORIOS
    _hlinea = r'((?:h|hh|c)line|(?:top|c?mid|bottom)?rule)(\([^)]+\))?(?:\{?([~-]+|\d+-\d+)\}?)?'
    _re_hlinea = re.compile(_hlinea).match
    _re_primera_hlinea = re.compile(r'^\s*(\\' + _hlinea + ')').search
    _re_ultima_hlinea = re.compile(r'(\\' + _hlinea + r')\s*$').search


    def __init__(self, destino, header, tipo='tabular', ancho=None, fuente=None,
                 conversiones=None, formatos=None, faltantes='', hlinea='hline', 
                 ruletrimming='lr', none='', nsep=4, bigstruts=None, fixups=True, 
                 makebox=True, primera_linea=True, firsthline=False, lasthline=False, 
                 ignorar_none=True, convform=False, fmb=1, transfor=None):
        """
        Parámetros:

            destino         Dirección del archivo tex a generar
            header          Encabezado (configuración de columnas) de la tabla, 
                            copia de LaTeX.

            tipo            Tipo de tabla: tabular | tabularx | tabulary.
            ancho           Ancho de tabla si es tabularx o tabulary.
            fuente          Dirección del archivo XLS.
            conversión      Diccionario con las conversiones generales, según 
                            tipo, a aplicar a los datos.
            faltantes       Completar los faltantes con este valor (se lo 
                            convierte a str), excepto que sea False que 
                            desactiva la opcion y emite errores si faltan datos.
            hlinea          Línea horizontal a aplicar en forma automática:
                                'hline' aplica \hline o \hhline
                                'rule'  aplica \midrule o \cmidrule
                                <otro>  desactiva la opción
                            La opción local está por encima de esta opción global.
            none            Reemplazar en los datos los None por esto (si es 
                            que ignorar_none=False)
            nsep            Número de espacios para identar las filas, y 'nsep/2'
                             espacios para identar las hlinea
            transfor        Función de transformación de texto.

        Parámetros globales True | False:

            bigstruts       ¿Usar bigstruts? solo donde se usen líneas
                            horizontales (mejor acabado), si bigstruts = None
                            su utilización depende si se usa hlinea == 'rule'
                            (bigstrut=False) o hlinea != 'rule' (bigstrut=True)
            fixups          ¿Aplicar fixups? es una corrección fina en la
                            posición del dato en los multirow debido a las
                            líneas insertadas con hline y hhline
            makebox         ¿Usar makebox en los datos? útil para dar anchos
                            específicos a las columnas sin usar columnas de
                            párrafos, pero inutiliza las columnas decimales,
                            cada celda puede inabilitarse individualmente
            primera_linea   ¿Ejecutar set_hlinea() en la instanciación de la 
                            clase?
            firsthline      ¿Usar firsthline? primera línea horizontal que
                            permite alinear textos
            lasthline       ¿Usar lasthline? última línea horizontal que
                            permite alinear textos
            ignorar_none    ¿Ignorar los Nones en los datos?, sino 'None' se
                            reemplaza por el valor de 'none'
                                file() por defecto False
                                xls() por defecto True
            convform        ¿Aplicar el formato por sobre la conversión?
                            Si es True se aplica la decisión de formato
                            sobre el dato ya convertido
            fmb             Aplicar los makeboxs del ancho de columna a la
                            fila número 'fmb', si es None aplica a todos
        """
        self.destino = destino
        self.fuente = XLS(fuente) if fuente else None
        self.nsep = nsep
        self.sep = ' ' * self.nsep
        self.hlsep = ' ' * round(self.nsep / 2)
        if isinstance(conversiones, dict):
            self.conversiones = conversiones
        self.formatos = formatos if isinstance(formatos, dict) else {}
        self.transfor = transfor
        self.header = self.get_header(header)
        self.ncolumnas = int((len(self.header) - 1) / 2)
        self.nfilas = 0
        self.codigo = ''
        self.parametros = '' # configuraciones particulares
        self.faltantes = str(faltantes) if faltantes is not False else False
        self._makeboxs = {}
        self._ancho_columnas = {}
        self._fmb = fmb
        self._conversiones_columnas = {}
        self._celdas_ocupadas = {} # para evitar poner datos y que falle LaTeX
        self._inserciones = {
            'bigstruts': {},
            'hlineas': {},
        }
        self._reempazar = {
            'fixups': {},   # ajuste fino por la cantidad de lineas
                            # horizontales insertadas en los multirow
            'bsks': {},     # número del multirow debido a los bigstruts
                            # insertados (solo se conoce al final)
        }
        if tipo == 'tabular' or (tipo in ('tabularx', 'tabulary') and ancho):
            self._tabla = {
                'tipo': tipo,
                'ancho': ancho or '',
            }
        else:
            raise ValueError('"tipo" no implementado o falta el parámetro '
                             '"ancho" (en tabularx o tabulary)')
        self.hlinea = hlinea if hlinea in ('hline', 'rule') else False
        self.ruletrimming = ruletrimming
        if primera_linea:
            self.set_hlinea()
        self.none = none
        self.fixups = fixups
        self.makebox = makebox
        isNotRule = False if hlinea == 'rule' else True
        self.bigstruts = bigstruts if bigstruts is not None else isNotRule
        self.ultimo_bigstrut = self.bigstruts
        self.firsthline = firsthline
        self.lasthline = lasthline
        self.ignorar_none = ignorar_none
        self.convform = convform
        self.finales = []
        self.newlines = [self.plantillas['sep_fila']]


    def get_header(self, header):
        """
            Procesa el encabezado de la tabla
            Devuelve una lista la configuración en la forma:
                [intercolumna[, columna, intercolumna]+]
        """
        tokensList = (
            ('MULTICOL',        r'[*]'),
            ('INTERCOL',        r'[|@!]'),
            ('COL',             r'[>A-Za-z<]'),
            ('LLAVE_APERTURA',  r'[{]'),
            ('LLAVE_CIERRE',    r'[}]'),
            ('OPCION_APERTURA', r'[[]'),
            ('OPCION_CIERRE',   r'[\]]'),
            ('OTRO',            r'.'),
            ('FIN',             r'$'),
        )
        tokensPattern = '|'.join('(?P<{}>{})'.format(*t) for t in tokensList)
        get_token = re.compile(tokensPattern).match
        pos_meta_llaves = listar_meta(header, '{}')
        pos_meta_opciones = listar_meta(header, '[]')
        lista = []
        tipos = []
        # COLUMNA FLAGS
        # '>'     001 1
        # 'letra' 010 2
        # '<'     100 4
        getFlag = lambda c: 1 if c == '>' else (4 if c == '<' else 2)
        flag = 0
        activo = None
        opciones = 0
        llaves = 0
        ini = 0
        token = get_token(header)
        while token is not None:
            tipo = token.lastgroup
            pos = token.start()
            fin = token.end()
            buff = header[ini:pos]
            if pos == 0:
                activo = tipo
                if tipo == 'COL':
                    lista.append(None)
                    tipos.append('INTERCOL')

            if tipo == 'LLAVE_APERTURA' and pos in pos_meta_llaves:
                llaves += 1

            elif tipo == 'LLAVE_CIERRE' and pos in pos_meta_llaves:
                llaves -= 1

            elif tipo == 'OPCION_APERTURA' and pos in pos_meta_opciones:
                opciones += 1

            elif tipo == 'OPCION_CIERRE' and pos in pos_meta_opciones:
                opciones -= 1

            elif tipo != 'OTRO' and llaves == 0 and opciones == 0:
                if tipo != activo:
                    if activo == 'MULTICOL':
                        cantidad, cfg = self._re_multicolumnas(buff).groups()
                        expand = ''.join([cfg for j in range(int(cantidad))])
                        header = header[:ini] + expand + header[pos:]
                        pos_meta_llaves = listar_meta(header, '{}')
                        pos_meta_opciones = listar_meta(header, '[]')
                        activo = tipos[-1] if tipos else None
                        pos -= len(buff) + 1
                        fin = ini
                    else:
                        if buff:
                            if activo == 'COL' and tipos and tipos[-1] == 'COL':
                                lista.append(None)
                                tipos.append('INTERCOL')
                            lista.append(buff)
                            tipos.append(activo)
                        if tipo == 'FIN':
                            break
                        flag = getFlag(token.group(tipo)) if tipo == 'COL' else 0
                        activo = tipo
                        ini = pos

                elif tipo == 'COL':
                    f = getFlag(token.group(tipo))
                    if f & flag or f < flag:
                        # Si se usa '>{}' después de una 'letra' o el '<{}'
                        # pertenece a la siguiente columna por eso en el if va 
                        # "f < flag"
                        # Ejemplo: '|c<{}>{}c|' > '|', 'c<{}', None, '>{}c', '|'
                        if tipos[-1] == 'COL':
                            lista.append(None)
                            tipos.append('INTERCOL')
                        lista.append(buff)
                        tipos.append('COL')
                        ini = pos
                        flag = f
                    else:
                        flag |= f

            token = get_token(header, fin)

        if lista:
            if tipos and tipos[-1] == 'COL':
                lista.append(None)
                tipos.append('INTERCOL')
            if len(lista) >= 3 and len(lista) % 2 != 0:
                return tuple(lista)
            else:
                raise RuntimeError('ERROR INTERNO: get_header() debe '
                    'devolver una lista con una cantidad impar de elementos')
        else:
            raise ValueError('Con los datos suministrados ("{}") no es posible '
                'extraer los encabezados'.format(header))


    def set_hlinea(self, tipo=None, pos_finales=None):
        """ Agregado de lineas horizontales
            Si 'tipo' es None se aplica el fijado en init
        """
        tipo = tipo if tipo is False or tipo is not None else self.hlinea
        objeto = self._re_hlinea(str(tipo))
        if objeto:
            tipo, extra, cfg = objeto.groups()
            codigo = self.plantillas['hlineas'][tipo].format(cfg, extra or '')
            codigo = '{}{}\n'.format(self.hlsep, codigo)
            if pos_finales:
                self.finales[pos_finales] += codigo
            else:
                self.codigo += codigo
                self._inserciones['hlineas'][self.nfilas] = tipo


    def columnas(self, tamaños=None, conversiones=None):
        """ Configura las columnas a los tamaños fijados. Si es None se deja el
            tamaño natural. Agrega un \makebox[tamaño]{dato} a todos los datos
            que no formen celdas combinadas

                tamaños = {columna:'tamaño'}
                Donde:
                    columna         numero de la columna
                    tamaño          tamaño de la columna

                conversiones = {columna:'conversión'}
                Donde:
                    columna         numero de la columna
                    conversión      conversión de la columna

            NOTAS:
             1) Si el dato está en un multicolumn o multirow de una sola columna
                o fila no se considera 'celdas combinadas'

             2) Si los valores de 'tamaños' empiezan con números se los pasa 
                directamente, sino se crear un variable y se asigna la longitud
                del valor (asumiendo como texto, eliminando caracteres
                especiales de LaTeX)
        """
        if tamaños:
            if isinstance(tamaños, (list, tuple)):
                tamaños = {c: t for c, t in enumerate(tamaños, 1) if t}
            elif not isinstance(tamaños, dict):
                raise ValueError('"tamaños" debe ser list, tuple o dict')
            for col, val in tamaños.items():
                if self._re_ancho_columnas_texto(val):
                    nombre = numero_a_letra(fulldatetime())
                    par = '\\newlength{{\\{0}}}\n\\settowidth{{\\{0}}}{{{1}}}\n'
                    self.parametros += par.format(nombre, saneado_latex(val))
                    tamaños[col] = '\\{}'.format(nombre)
            self._ancho_columnas = tamaños

        if conversiones:
            if isinstance(conversiones, (list, tuple)):
                conversiones = {c: t for c, t in enumerate(conversiones, 1) if t}
            elif not isinstance(conversiones, dict):
                raise ValueError('"conversiones" debe ser list, tuple o dict')
            self._conversiones_columnas = conversiones


    def formateo(self, dato, columna, formatos, conversión=None, makebox=None,
                 transfor=None, force_makebox=False):
        """ Aplica la conversión y el formato al dato, si no se pasan se usan 
            los generales, conversión=False equivale a '{}'.format(dato)

                dato -> conversión -> transfor -> formatos -> makebox -> dato

            Las condiciones para aplicar un formato se hacen con dato
            (convform=False) o con la salida de conversión (convform=True)
        """
        d = dato
        if conversión is None:
            if self._conversiones_columnas:
                conversión = self._conversiones_columnas.get(columna)
            else:
                conversión = self.conversiones.get(type(dato))
        try:
            dato = '{:{conversión}}'.format(dato, conversión=conversión)
        except:
            dato = '{}'.format(dato)
        try:
            dato = transfor(dato)
        except TypeError:
            pass
        if formatos:
            if self.convform:
                d = dato
            for condición, formato in formatos.items():
                try:
                    if eval(condición.format(d)):
                        dato = plantilla(formato).format(dato)
                        break
                except (SyntaxError, NameError, TypeError):
                    pass
        
        makebox = makebox if makebox is not None else self.makebox
        if makebox and (self._fmb in (None, self.nfilas) or force_makebox):
            tamaño = self._ancho_columnas.get(columna)
        else:
            tamaño = None
        caja = plantilla('\makebox[«»]{{{}}}').format(tamaño) if tamaño else '{}'
        return caja.format(dato)


    def fila(self, *args, c='', hlinea=None, ruletrimming=None, hlcortes=None,
             conversión=None, formatos=None, none=None, faltantes=None,
             bigstrut=None, ignorar_none=False, al_final=False, transfor=None,
             force_makebox=True, newline=None):
        """
        *argumentos pasados por posición:
            se consideran los datos de las columnas, estos pueden contener
            direcciones del xls (fuente)

        *argumentos pasados por nombre:
            c               Configuración:
                                [n]num[-col align][:fil:fixup ancho][,conver];

                                n       Si está la 'n' no se aplica el makebox 
                                        para este dato
                                num     Número de la columna a configurar
                                col     Unir la columna 'num' hasta la columna
                                        'col'
                                align   Alineación de la columna resultante por
                                        la unión num-col, parámetro del 
                                        \multicolumn, default lo del encabezado
                                fil     Unir la celda actual con "fil - 1" filas
                                        de abajo
                                ancho   Ancho de la fila resultante por la unión
                                        num:fil, parámetro del \multirow,
                                        default '*'
                                conver  Conversión específica del dato de la celda
            hlinea:         Decide si se incluye (y que tipo) una linea abajo de
                            esta fila. Ejemplos:
                                hlinea=None  (se aplica el por defecto)
                                hlinea=False  (No se aplica)
                                hlinea='hline'
                                hlinea='----~~'  (aplica '----~~')
                               -hlinea='cline2-4'  (puede ser 'cline{2-4}')
                               -hlinea='hhline--~'  (puede ser 'hhline{--~}')
            ruletrimming:   Ajusta los bordes del hlinea si se emplea cmidrule
            hlcortes:       Lista de números que indica los cortes sobre el hlinea
                            de la fila, ejemplo: si se aplicara \cmidrule(rl){2-4}
                            con hlcortes = [3] este se transforma en 
                            \cmidrule(rl){2-3} y \cmidrule(rl){3-4}
                            NOTA 1: solo funciona si está activado 'ruletrimming'
                            NOTA 2: el '3' significa que el corte es entre el 3 
                                    y el 4
            conversión:     Conversión general para todos los datos de la fila 
                            que no contengan conversión especifica si es None se
                            consideran las conversiones general fijado en init
            faltantes:      Si es None se usa la configuración por defecto, False
                            se desactiva y cualquier otro cosa se usa para 
                            completar los faltantes
            bigstrut:       ¿Usar bigstrut en esta fila? (si corresponde en 
                            función de las lineas insertadas)
            ignorar_none:   'True' ignora los 'None'. 'False' se reemplaza los 
                            None por el valor de none o de self.none
            al_final:       'True' procesa la fila pero para ponerla al final,
                            útil para notas al pie.
            transfor:       Se aplica despues de los formatos, debe ser una
                            funsión que transforme texto en texto. Ejemplo:
                                lambda t: t.replace('.', ',')
            force_makebox:  'True' aplica el makebox si o si, fuera de un
                            multicolumn, a pesar del valor de 'fmb' del iniciador
                            de la clase. Es decir si encuentra un multicolumn
                            saltea el makebox para la siguiente fila en esa
                            columna aunque 'fmb' no lo habilite.
        """
        self.nfilas += 1  # Si 'al_final=True' se resta al final
        hlcortes = tuple(hlcortes) if hlcortes else tuple()
        ruletrimming = ruletrimming if ruletrimming is not None else self.ruletrimming
        formatos = self.formatos if formatos is None else (formatos if isinstance(formatos, dict) else {})
        transfor = self.transfor if transfor is None else transfor
        bigstrut = bigstrut if bigstrut is not None else self.bigstruts
        self.ultimo_bigstrut = bigstrut
        faltantes = faltantes if faltantes is not None else self.faltantes
        faltantes = str(faltantes) if faltantes is not False else False
        ignorar_none = ignorar_none if ignorar_none is not None else self.ignorar_none
        conversiones = {}
        makeboxs = {}
        columnas = {}
        filas = {}
        i = -1
        nmulticolumn = 0
        for col in c.split(';'):
            col = self._re_config(col)
            if col:
                num = int(col.group('num'))
                if num <= self.ncolumnas:  # se ignora lo que sobra
                    if col.group('nmb'):
                        makeboxs[num] = not bool(col.group('nmb'))
                    if col.group('cn'):
                        # Formato: intercolumna[, columna, intercolumna]+
                        pos = 2 * num - 1  # mínimo 3 y siempre impar
                        if col.group('ca') is None:
                            align = ''
                            if num > i + 1:
                                align += self.header[pos - 1] or ''
                            align += self.header[pos]
                            align += self.header[pos + 1] or ''
                            i = num
                        else:
                            align = col.group('ca') or ''
                        # para saber si faltan datos
                        nmulticolumn += int(col.group('cn')) - num
                        columnas.setdefault(num, {}).update({
                            'num': int(col.group('cn')), 
                            'align': align,
                        })
                    if col.group('fn'):
                        filas.setdefault(num, {}).update({
                            'num': int(col.group('fn')), 
                            'fixup': col.group('ff') or None,
                            'ancho': col.group('fa') or '*',
                        })
                    if col.group('f'):
                        conversiones[num] = col.group('f')

        if bigstrut:
            anterior = self._inserciones['hlineas'].get(self.nfilas - 1)
            if hlinea:
                opcion = 'ambos' if anterior else 'abajo'
            elif anterior:
                opcion = 'arriba'
            else:
                opcion = None
            if not al_final:
                self._inserciones['bigstruts'][self.nfilas] = opcion
            bigstrut = self.plantillas['bigstruts'].get(opcion, '')
        else:
            bigstrut = ''

        if newline:
            self.newlines.append(newline)
        else:
            newline = self.newlines[0]

        c = 1  # número de columna
        d = 1  # cantidad de datos
        hhline = ''
        codigo = ''

        if faltantes is not False:
            if ignorar_none:
                nargs = len([i for i in args if i is not None])
            else:
                nargs = len(args)
            nocupado = sum([v[1] for c, v in self._celdas_ocupadas.get(self.nfilas, {}).items()])
            nfaltantes = self.ncolumnas - (nargs + nmulticolumn + nocupado)
            args += tuple([faltantes for i in range(nfaltantes)])

        for dato in args:
            if dato is None and ignorar_none:
                d += 1
                continue # siguiente dato

            if isinstance(dato, xls):
                if self.fuente:
                    dato = ' '.join(self.fuente.extraer(str(dato)))
                else:
                    raise ValueError('No se especificó ninguna fuente XLS')

            while True:
                ocupado = self._celdas_ocupadas.get(self.nfilas, {}).get(c)
                if ocupado:
                    codigo += ocupado[0]
                    c = ocupado[1] + 1
                else:
                    break

            if dato is None:
                dato = none if none is not None else self.none

            mbs = makeboxs.get(c)
            fmbs = False
            if force_makebox:
                if c in columnas:
                    for i in range(c, columnas[c]['num'] + 1):
                        self._makeboxs[i] = True
                    mbs = False
                else:
                    fmbs = self._makeboxs.pop(c) if c in self._makeboxs else False

            conv = conversiones.get(c) or conversión
            dato = self.formateo(dato, c, formatos, conv, mbs, transfor, fmbs)

            if c in columnas or c in filas:
                if c in columnas:
                    nc = columnas[c]['num']
                    tamaño = nc - c + 1
                    align = escapar_llaves(columnas[c]['align'])
                    column = self.plantillas['multicolumn'].format(tamaño, align)
                else:
                    nc = c
                    tamaño = 1
                    column = '{}{}' # dato, sep

                if nc < self.ncolumnas:
                    sep = self.plantillas['sep_columna']
                else:
                    sep = bigstrut + newline

                if c in filas:
                    nf = filas[c]['num']
                    remfilas = tuple(range(self.nfilas, self.nfilas + nf))
                    bsk = '«bsk:{}:{}:{}»'.format(c, nf, fulldatetime())
                    self._reempazar['bsks'][bsk] = remfilas
                    ancho = filas[c]['ancho']
                    fixup = filas[c]['fixup']
                    if fixup is not None:
                        fixup = '[{}]'.format(fixup) if fixup else ''
                    else:
                        fixup = '«fixup:{}:{}:{}»'.format(c, nf, fulldatetime())
                        self._reempazar['fixups'][fixup] = remfilas
                    multirow = self.plantillas['multirow'].format(
                        nf, bsk, ancho, fixup, dato)
                    for f in remfilas:
                        self._celdas_ocupadas.setdefault(f, {}).update(
                            {c: (column.format('', sep), nc)})
                    hhline += ('~' if nf > 1 else '-') * tamaño
                    codigo += column.format(multirow, sep)
                else:
                    hhline += '-' * tamaño
                    codigo += column.format(dato, sep)

                if nc < self.ncolumnas:
                    c = nc + 1
                    d += 1
                else:
                    c = nc
                    break
            else:
                hhline += '-'
                if c < self.ncolumnas:
                    codigo += dato + self.plantillas['sep_columna']
                    c += 1
                    d += 1
                else:
                    codigo += dato + bigstrut + newline
                    break

        if len(args) < d:
            codigo += bigstrut + newline

        codigo = '{}{}\n'.format(self.sep, codigo)
        if al_final:
            self.nfilas -= 1
            pos_finales = len(self.finales)
            self.finales.append(codigo)
        else:
            pos_finales = None
            self.codigo += codigo

        if hlinea and (hlinea.count('-') or hlinea.count('~')):
            hhline = hlinea + ('-' * (self.ncolumnas - len(hlinea)))
            hlinea = 'rule'

        if hlinea is None:
            if self.hlinea == 'hline':
                if '~' in hhline:
                    hlinea = ['hhline{}'.format(hhline)]
                else:
                    hlinea = ['hline']
            else:
                hlinea = []
        elif hlinea == 'rule':
            ng = hhline.count('-')
            if ng == len(hhline) and not (ruletrimming and hlcortes):
                hlinea = ['midrule']
            elif ng > 0:
                indices = []
                for i, c in enumerate(hhline, 1):
                    if c == '-':
                        indices.append(i)
                lista = []
                ini = fin = indices[0]
                for v in indices:
                    if v > fin + 1:
                        lista.append((ini, fin))
                        ini = fin = v
                    else:
                        fin = v
                else:
                    lista.append((ini, fin))
                lista_cortada = []
                if hlcortes:
                    for vi, vf in lista:
                        for c in sorted(hlcortes):
                            if vi <= c < vf:
                                lista_cortada.append((vi, c))
                                vi = c + 1
                        lista_cortada.append((vi, vf))
                else:
                    lista_cortada = lista
                trim = '(' + ruletrimming + ')' if ruletrimming else ''
                hlinea = []
                for (i, f) in lista_cortada:
                    hlinea.append('cmidrule{}{}-{}'.format(trim, i, f))
            else:
                hlinea = []

        for hl in hlinea:
            self.set_hlinea(hl, pos_finales)


    def __call__(self, *args, **kwargs):
        self.fila(*args, **kwargs)


    def xls(self, ruta, **kwargs):
        """ Agregado de una fila a partir de un vector en la ruta del xls
            Los parámetros son los mismos que los de fila()
            Por defecto ignorar_none=True a diferencia de fila()
        """
        if self.fuente:
            valores = self.fuente.extraer(ruta)
            kwargs['ignorar_none'] = kwargs.get('ignorar_none', True)
            self.fila(*valores, **kwargs)
        else:
            raise ValueError('No se especificó ninguna fuente XLS')


    def filas(self, filas, xls=False):
        """ Envoltura de método "fila" para múltiples filas """
        if isinstance(filas, (list, tuple, zip)):
            for fila in filas:
                if xls:
                    self.xls(*fila)
                else:
                    self.fila(*fila)
        else:
            raise ValueError('"filas" debe ser list o tuple')


    def __setitem__(self, var, val):
        """ Seteo de parámetros """
        if var in ('arraystretch', 'multirowsetup'):
            self.parametros += '\\renewcommand*{{\\{}}}{{{}}}\n'.format(var, val)

        elif var in ('extrarowheight', 'tabcolsep', 'arrayrulewidth'):
            self.parametros += '\\setlength{{\\{}}}{{{}}}\n'.format(var, val)

        elif var == 'sisetup':
            self.parametros += '\\{}{{{}}}\n'.format(var, val)

        elif var == '_latex_':
            self.parametros += str(val).strip() + '\n'

        else:
            raise KeyError('"{}" no implementado'.format(var))


    def tex(self):
        """ Genera el código LaTeX (PGF/TikZ) y lo guarda en el archivo destino
            si hay al menos una serie cargada.
        """
        # Elimina la hline final (por si queda repetido)
        ultima_hlinea = self._re_ultima_hlinea(self.codigo)
        if ultima_hlinea:
            a, b = ultima_hlinea.span()
            if b == len(self.codigo):
                self.codigo = self.codigo[:a].rstrip(" ")

        if self.hlinea == 'rule':
            if self.ultimo_bigstrut:
                regex = r'({})?\s*({})\s*$'.format(
                    join_no_meta(plantillas['bigstruts'].values()),
                    join_no_meta(self.newlines),
                )
                ultimo = re.search(regex, self.codigo)
                if ultimo:
                    bigstrut = ultimo.group(1)
                    if bigstrut:
                        pos = ultimo.span(1)
                        if '[t]' in bigstrut:
                            opcion = 'ambos'
                            bigstrut = self.plantillas['bigstruts'][opcion]
                        elif '[b]' in bigstrut:
                            opcion = 'abajo'
                        else:
                            opcion = 'ambos'
                    else:
                        pos = (ultimo.start(), ultimo.start())
                        opcion = 'abajo'
                        bigstrut = self.plantillas['bigstruts'][opcion]
                    self.codigo = inclusion(self.codigo, pos, bigstrut)
                    self._inserciones['bigstruts'][self.nfilas] = opcion

            self.codigo += self.hlsep
            self.codigo += self.plantillas['hlineas']['bottomrule']
            self.codigo += '\n'
            self._inserciones['hlineas'][self.nfilas] = 'bottomrule'

        for bsk, remfilas in self._reempazar['bsks'].items():
            val = 0
            for fila in remfilas:
                index = self._inserciones['bigstruts'].get(fila)
                val += self.correcciones['bigstruts'][index]
            rem = '[{}]'.format(val) if val > 0 and self.bigstruts else ''
            self.codigo = self.codigo.replace(bsk, rem)

        for fixup, remfilas in self._reempazar['fixups'].items():
            val = 0
            for fila in remfilas[:-1]:
                index = self._inserciones['hlineas'].get(fila)
                val += self.correcciones['fixups'][index]
            val = round(val / 2, 4)
            rem = '[-{}\\arrayrulewidth]'.format(val) if val > 0 and self.fixups else ''
            self.codigo = self.codigo.replace(fixup, rem)

        if self.firsthline:
            primera = self._re_primera_hlinea(self.codigo)
            if primera:
                pos = primera.span(1)
                self.codigo = inclusion(self.codigo, pos, '\\firsthline')
            else:
                self.codigo = self.sep + '\\firsthline\n' + self.codigo

        if self.lasthline:
            ultima = self._re_ultima_hlinea(self.codigo)
            if ultima:
                pos = ultima.span(1)
                self.codigo = inclusion(self.codigo, pos, '\\lasthline')
            else:
                self.codigo = self.codigo + self.sep + '\\lasthline\n'

        for linea in self.finales:
            self.codigo = self.codigo + self.sep + linea.strip() + '\n'

        if self.parametros:
            ini = '{\n'
            fin = '\n}'
        else:
            ini = ''
            fin = ''

        date = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
        codigo = self.plantillas['Cuadro'].format(**{
            'ini': '% {}\n{}'.format(date, ini),
            'parametros': self.parametros,
            'tipo': self._tabla['tipo'],
            'ancho': self._tabla['ancho'],
            'header': ''.join([i for i in self.header if i]),
            'código': self.codigo,
            'fin': fin,
        })
        with open(self.destino, encoding='utf-8', mode='w') as fichero:
            fichero.write(codigo)


BASE = r"""% «FECHA»
\tikzsetnextfilename{«ARCHIVO»}
\begin{tikzpicture}«OPCIONES»
    \pgfplotsset{%
        «PARAMETROS»
    }
    «EJES»
\end{tikzpicture}
"""
EJE = r"""% Eje
    \begin{«TIPO»}[%
            «PARAMETROS»
        ]
        «OBJETOSI»
        «SERIES»
        «OBJETOSF»
    \end{«TIPO»}"""
SERIE = r"""% Serie
        «GRUPO»
        \addplot[%
            «PARAMETROS»
        ] plot coordinates {%
            «DATOS»
        };
        \label{«ETIQUETA»}
        «LEYENDAS»"""
NEXTGROUPPLOT = r"\nextgroupplot[«»]"
ADDLEGENDENTRY = r"\addlegendentry{«»}"
ADDLEGENDIMAGE = r"\addlegendimage{/pgfplots/refstyle={«»}}"
OBJETO = r"«»"


class Figura:
    """
        Interfase para la creación de gráficos en LaTeX a través de PGF/TikZ
        
        Figura
           |-- Parámetros
           \-- Ejes
                |-- Parámetros
                |-- Objetos 1
                |-- Series
                |     |-- Grupo
                |     |-- Parámetros
                |     |-- Datos
                |     |-- Etiquetas
                |     \-- Leyendas
                \-- Objetos 2
    """
    # Plantillas
    p_base = plantilla(BASE)
    p_eje = plantilla(EJE)
    p_serie = plantilla(SERIE)
    p_gplot = plantilla(NEXTGROUPPLOT)
    p_lentry = plantilla(ADDLEGENDENTRY)
    p_limage = plantilla(ADDLEGENDIMAGE)
    p_objeto = plantilla(OBJETO)

    serializar_default = (
        ['mark', 'smooth', 'xbar', 'ybar'],  # ini
        ['xtick align', 'ytick align'],      # fin
    )

    tikz_opciones = ['trim axis left', 'trim axis right', '>=latex']

    def __init__(self, destino, tikzpath, fuente=None, parametros=None,
                 usar_default=True, usar_default_orden=True, conv=None):
        """ Parámetros:

            destino             Dirección del archivo tex a generar
            tikzpath            Subpath del archivo tex sin extension, relativo
                                a la carpeta de tikz definido en el archivo
                                tex principal, para \\tikzsetnextfilename{}
            fuente              Dirección del archivo XLS.
            parametros          diccionario (opcional), puede contener posiciones
                                en el archivo *.xls
            usar_defaul         habitila/deshabilita el uso de los parámetros
                                por defecto
            usar_default_orden  habitila/deshabilita el orden por defecto para
                                los parámetros
            conv                Contiene las conversiones a aplicar a cada dato de
                                cada serie (es el formato a aplicar a cada numero)
        """
        self.destino = destino
        self.tikzpath = tikzpath
        self.fuente = XLS(fuente) if fuente else None
        self._default = {}  # variable para set_default()
        self.usar_default = usar_default
        self.usar_default_orden = usar_default_orden
        self.parametros = self.default(dict(parametros or {}), 'gráfico')
        self.ejes = []
        self.nejes = 0
        self.nseries = 0
        self.nsep = 4
        self.conv = conv


    def eje(self, tipo, parametros=None, orden=None):
        """ Parámetros:
            tipo            'axis' o 'groupplot'
            parametros      diccionario (opcional), puede contener
                            posiciones en el archivo *.xls
            orden           ordenación por defecto de los parámetros True | false
        """
        if tipo not in ('axis', 'groupplot'):
            raise ValueError("Tipo no implementado")
        if self.ejes:
            if tipo == 'groupplot':
                raise ValueError("Solo se admite 1 eje de tipo 'groupplot'")
            elif tipo != self.ejes[0]['TIPO']:
                raise ValueError("No pueden mezclarse tipos de ejes")
        self.nejes += 1
        parametros = self.default(dict(parametros or {}), 'eje')
        self.ejes.append({
            'series':       [],
            'objetosi':     [],
            'objetosf':     [],
            'TIPO':         tipo,
            'PARAMETROS':   self.serializar(parametros, ' ' * 12, orden),
            'SERIES':       '% <-- series --> %', # Luego se actualiza
            'OBJETOSI':     '% <-- objetos al inicio --> %', # Luego se actualiza
            'OBJETOSF':     '% <-- objetos al final --> %', # Luego se actualiza
        })


    def serie(self, x, y, leyenda=None, etiqueta=None, parametros=None,
              orden=None, eje=None, grupo=None, conv=None):
        """ Parámetros:
            x, y            posiciones en el archivo *.xls o iterables
            leyenda         texto o posiciones en el archivo *.xls (opcional)
            etiqueta        texto o posiciones en el archivo *.xls (opcional)
            parametros      diccionario (opcional), puede contener posiciones en 
                            el archivo *.xls
            orden           ordenación por defecto de los parámetros True | false
            eje             número del eje a agregar la serie, si se omite se lo 
                            agrega al útimo eje
            grupo           diccionario (opcional), puede contener posiciones en
                            el archivo *.xls
            conv            Contiene las conversiones a aplicar a cada dato (es 
                            el formato a aplicar a cada numero)
        """
        if self.nejes > 0:
            self.nseries += 1
            xy = []
            for nombre, val in (('X', x), ('Y', y)):
                if isinstance(val, str):
                    if self.fuente:
                        try:
                            #xy.append([float(i) for i in self.fuente.extraer(val)])
                            xy.append(self.fuente.extraer(val))
                        except ValueError:
                            raise ValueError(("Error extrayendo valores para {} "
                                "del *.xls").format(nombre))
                    else:
                        raise ValueError(("No se especificó ninguna fuente "
                            "para extraer los valores de {}").format(nombre))
                elif hasattr(val, '__iter__'):
                    xy.append(val)
                else:
                    raise ValueError(("{} debe ser texto (posiciones de un *.xls) "
                        "o un iterable").format(nombre))
            conv = conv or self.conv
            if conv:
                if not isinstance(conv, (list, tuple)) or len(conv) < 2:
                    conv = (conv, conv)
                xconv = conv[0]
                yconv = conv[1]
            else:
                xconv = ''
                yconv = ''
            rows = []
            for xi, yj in zip(*xy):
                try:
                    xx = '{:{conv}}'.format(xi, conv=xconv)
                except:
                    xx = xi
                try:
                    yy = '{:{conv}}'.format(yj, conv=yconv)
                except:
                    yy = yj
                rows.append('({}, {})'.format(xx, yy))

            if leyenda not in (False, None):
                leyenda = str(self.fuente_extraer(leyenda))

            if etiqueta or leyenda:
                serie_etiqueta = self.fuente_extraer(etiqueta) or leyenda
            else:
                serie_etiqueta = str(self.nseries)
            etiqueta = normalizado(saneado_latex('pgfplots:{}:{}'.format(
                self.tikzpath, serie_etiqueta), seguro='_'))

            grupo = self.default(dict(grupo or {}), 'grupo')
            parametros = self.default(dict(parametros or {}), 'serie')
            sep = ' ' * 12
            self.ejes[self.nejes - 1 if eje is None else eje]['series'].append({
                'ETIQUETA':     etiqueta,
                'leyenda':      leyenda,
                'DATOS':        ('\n' + sep).join(rows),
                'PARAMETROS':   self.serializar(parametros, sep, orden),
                'grupo':        self.serializar(grupo, ' ' * 8),
                'GRUPO':        '% <-- grupo --> %', # Luego se actualiza
                'LEYENDAS':     '% <-- leyendas --> %', # Luego se actualiza
            })
        else:
            raise ValueError("Debe agregar un eje para poder agregar series")


    def objeto(self, obj, eje=None, inicio=False):
        if self.nejes > 0:
            eje = self.nejes - 1 if eje is None else eje
            key = 'objetosi' if inicio else 'objetosf'
            self.ejes[eje][key].append(obj)
        else:
            raise ValueError("Debe agregar un eje para poder agregar objetos")


    def serializar(self, parametros, sep='', orden=None):
        """ serializar un diccionario devolviendo 'clave1=valor1,clave2=valor2'
            'orden' es una tupla de tuplas con los ordenes de inicio y final 
            obligatorio de los parámetros
        """
        ini = []
        fin = []
        if isinstance(orden, (list, tuple)) and len(orden) > 1:
            for index, lista in ((0, ini), (1, fin)):
                val = orden[index]
                if val:
                    if isinstance(val, (list, tuple)):
                        tmp = list(val)
                    else:
                        tmp = [val]
                    if self.usar_default_orden:
                        tmp += self.serializar_default[index]
                    for i in tmp:
                        if i in parametros:
                            lista.append((i, parametros.pop(i)))
        lista = []
        for clave, val in ini + list(sorted(parametros.items())) + fin:
            if isinstance(val, dict):
                sep2 = sep + ' ' * self.nsep if sep else ''
                sub = self.serializar(val, sep2, orden)
                lista.append('{}={{\n{}{}\n{}}}'.format(clave, sep2, sub, sep))
            elif val is None:
                lista.append(clave)
            elif val is True or val is False:
                lista.append('{}={}'.format(clave, str(val).lower()))
            else:
                lista.append('{}={}'.format(clave, val))
        return (',\n' + sep).join(lista)


    def actualizar(self, dictA, dictB):
        """ Actualiza recursivamente dictA con dictB, parecido a realizar 
            dictA.update(dictB), buscando los datos en el archivo fuente si
            es posible.
        """
        for clave, val in dictB.items():
            if isinstance(val, dict) and isinstance(dictA.get(clave), dict):
                dictA[clave] = self.actualizar(dictA[clave], val)
            else:
                dictA[clave] = self.fuente_extraer(val)
        return dictA


    def fuente_extraer(self, objeto):
        """
            Busca el "objeto" dentro del archivo *.xls y devuelve una cadena
            de texto con la concatenación de todos los valores encontrados, 
            si falla, devuelve el objeto sin cambios.
        """
        if self.fuente:
            try:
                buscar = str(objeto)
                if buscar.startswith('{') and buscar.endswith('}'):
                    buscar = str(objeto).lstrip('{').rstrip('}')
                    llaves = True
                else:
                    llaves = False
                vals = self.fuente.extraer(buscar)
                objeto = escapar_meta_latex(' '.join([str(i) for i in vals]))
                if llaves:
                    objeto = '{{{}}}'.format(objeto)
            except ValueError:
                pass
        return objeto


    def set_default(self, seccion, parametros):
        """ Permite agregar parámetros por default a todos los ejes y/o series
            que se agreguen. Este método se le suma a los parámetros por default 
            de la figura (si están habilitados). Primero estan los valores por 
            defecto, luego los seteados con set_default y por encima de todos 
            los pasados particularmente en el agregado del eje o serie.
        """
        if isinstance(parametros, dict):
            self._default[seccion] = parametros
        else:
            raise TypeError('parámetros debe ser un diccionario')


    def add_set_default(self, seccion, parametros):
        """ Permite agregar parámetros por default a los ya fijados por 
            "set_default"
        """
        if isinstance(parametros, dict):
            self._default.setdefault(seccion, {}).update(parametros)
        else:
            raise TypeError('parámetros debe ser un diccionario')


    def default(self, parametros, seccion):
        """ Aplica los valores por defecto a los parámetros
        """
        valores = {}
        if self.usar_default:
            if seccion == 'gráfico':
                valores.update({
                    'scale only axis': True,
                    'width': '\\figurewidth',
                    'height': '\\figureheight',
                    'legend style': {
                        'font': '\\tiny',
                        'rounded corners': '2pt',
                        'legend columns': '1',
                       #'draw': 'black',
                       #'fill': 'white',
                       #'draw opacity': 0.4,
                       #'fill opacity': 0.7,
                       #'text opacity': 1.0,
                        'at': '{(0.97,0.96)}',
                        'anchor': 'north east',
                       #'at': '{(0.50,0.96)}',
                       #'anchor': 'north',
                    },
                })
            elif seccion == 'eje':
                valores.update({
                    'xmode': 'normal',
                    'ymode': 'normal',
                    'grid': 'none',
                    'enlargelimits': 'true',
                    'scaled ticks': 'true',
                    'axis on top': None,
                })
            elif seccion == 'serie':
                valores.update({
                    'mark': 'none',
                   #'smooth': None,
                   #'only marks': None,
                })
           #elif seccion == 'grupo':
           #    valores.update({
           #        'ymax': 10,
           #    })
        valores.update(self._default.get(seccion, {}))
        valores = self.actualizar(valores, parametros)
        return valores


    def tex(self, usar_leyendas=None):
        """ Genera el código LaTeX (PGF/TikZ) y lo guarda en el archivo destino
            si hay al menos una serie cargada.
        """
        if not (self.ejes and self.ejes[0]['series']):
            raise ValueError("No hay datos cargados para graficar")

        groupplot = self.ejes[0]['TIPO'] == 'groupplot'
        pos_leyendas = 0 if groupplot else -1
        leyendas_excluidas = False
        leyendas = []
        
        leyendaError = ValueError('Intenta agregar una leyenda despues de '
            'ignorar una o mas, no permitido')

        for i, serie in enumerate(self.ejes[0]['series']):
            isL = serie['leyenda'] is not False
            L = serie['leyenda'] or '~'
            if groupplot:
                serie['GRUPO'] = self.p_gplot.format(serie['grupo'])
            if leyendas_excluidas and isL:
                raise leyendaError
            isE = groupplot and i != 0
            
            leyendas.append('{}{}'.format(
                self.p_limage.format(serie['ETIQUETA']) if isE else '',
                self.p_lentry.format(L) if isL else '',
            ) or '% relax')
            leyendas_excluidas = True if serie['leyenda'] is False else False

        if self.nejes > 1:
            for eje in self.ejes[1:]:
                for serie in eje['series']:
                    isL = serie['leyenda'] is not False
                    L = serie['leyenda'] or '~'
                    if leyendas_excluidas and isL:
                        raise leyendaError
                    leyendas.append('{}{}'.format(
                        self.p_limage.format(serie['ETIQUETA']),
                        self.p_lentry.format(L) if isL else '',
                    ))
                    leyendas_excluidas = True if serie['leyenda'] is False else False

        if usar_leyendas is True or (usar_leyendas is None and len(leyendas) > 1):
            sep = '\n' + ' ' * 8
            self.ejes[0]['series'][pos_leyendas]['LEYENDAS'] = sep.join(leyendas)

        ejes = []
        for eje in self.ejes:
            series = []
            for serie in eje['series']:
                series.append(self.p_serie.format(**serie))
            if series:
                eje['SERIES'] = '\n{}'.format(' ' * 8).join(series)

            for key in ('objetosi', 'objetosf'):
                objetos = []
                for objeto in eje[key]:
                    objetos.append(self.p_objeto.format(objeto))
                if objetos:
                    eje[key.upper()] = '\n{}'.format(' ' * 8).join(objetos)

            ejes.append(self.p_eje.format(**eje))

        if self.tikz_opciones:
            tikz_opciones = '[{}]'.format(', '.join(self.tikz_opciones))
        else:
            tikz_opciones = ''

        codigo = self.p_base.format(**{
            'FECHA': datetime.now().strftime("%d/%m/%Y %H:%M:%S"),
            'ARCHIVO': self.tikzpath,
            'OPCIONES': tikz_opciones,
            'PARAMETROS': self.serializar(self.parametros, ' ' * 8),
            'EJES': '\n{}'.format(' ' * self.nsep).join(ejes),
        })
        with open(self.destino, encoding='utf-8', mode='w') as fichero:
            fichero.write(codigo)
