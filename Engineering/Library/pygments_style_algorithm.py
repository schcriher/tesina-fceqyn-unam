# -*- coding: utf-8 -*-
"""
    pygments.styles.algorithm
    ~~~~~~~~~~~~~~~~~~~~~~~~~

    Simple print-friendly style for displaying algorithms.

    https://github.com/noah/pygments-style-algorithm

    :copyright: Copyright 2012 Noah K. Tilton
    :license: BSD, same as Pygments.  See LICENSE file
    :acknowledgements: Thanks to Hugo Maia Vieira for the setuptools structure.
"""

from pygments.style import Style
from pygments.token import Keyword, Name, Comment, String, Error, \
     Number, Operator, Generic, Whitespace


class AlgorithmStyle(Style):
    """
    Simple print-friendly style for displaying algorithms.
    """

    background_color = "#f8f8f8"
    default_style = ""

    styles = {
        Whitespace:                "#000",
        Comment:                   "noitalic #555",
        Comment.Preproc:           "noitalic #000",

        Keyword:                   "bold #000",
        Keyword.Pseudo:            "nobold",
        Keyword.Type:              "nobold #000",

        Operator:                  "#000",
        Operator.Word:             "bold #000",

        Name.Builtin:              "bold #000",
        Name.Function:             "bold #000",
        Name.Class:                "bold #000",
        Name.Namespace:            "bold #000",
        Name.Exception:            "bold #000",
        Name.Variable:             "#2e3436",
        Name.Constant:             "#2e3436",
        Name.Label:                "#555753",
        Name.Entity:               "bold #999999",
        Name.Attribute:            "#555753",
        Name.Tag:                  "bold #2e3436",
        Name.Decorator:            "#888a85",

        String:                    "#000",
        String.Doc:                "#333",
        String.Interpol:           "bold #555753",
        String.Escape:             "bold #555753",
        String.Regex:              "#888a85",
        String.Symbol:             "#888a85",
        String.Other:              "#555753",
        Number:                    "#000",

        Generic.Heading:           "bold #2e3436",
        Generic.Subheading:        "bold #555753",
        Generic.Deleted:           "#2e3436",
        Generic.Inserted:          "#555753",
        Generic.Error:             "#2e3436",
        Generic.Emph:              "italic",
        Generic.Strong:            "bold",
        Generic.Prompt:            "bold #000080",
        Generic.Output:            "#888",
        Generic.Traceback:         "#555753",

        Error:                     "border:#FF0000"
    }
