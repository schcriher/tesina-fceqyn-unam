# -*- coding: utf-8 -*-
# Copyright 2012 Schmidt Cristian Hernán

import re
import os
import sys
import math
import numpy

from time import time
from multiprocessing import Queue
from threading import Thread
from scipy import stats
from datetime import datetime
from itertools import chain
from collections import deque
from unicodedata import normalize, category

META_CHAR_RE = r'$()*+.?[\]^{|}'
META_CHAR_LATEX = r'#$%&\^_{}~'


def fround(num, fun, dec):
    f = 10 ** dec
    return fun(num * f) / f

def fceil(num, dec):
    return fround(num, math.ceil, dec)

def ffloor(num, dec):
    return fround(num, math.floor, dec)


def año_bisiesto(año):
    return (año % 4 == 0 and not año % 100 == 0) or año % 400 == 0


def instanciar(Clase):
    return Clase()


def unit2multiple(value, digit=1):
    "Conversor de tamaños de archivos"
    multiple = 1000
    if value < multiple:
        prefix = 'B'
    else:
        prefixes = ('KB', 'MB', 'GB')
        for prefix in prefixes:
            value /= multiple
            if value < multiple:
                break
    return '{0:.{digit}f}{1}'.format(value, prefix, digit=digit)


class Cache:

    def __init__(self, func):
        self.func = func
        self.data = {}
        self.call = 0
        self.calc = 0
        self.none = object()
        self.active = True

    def activate_cache(self, active):
        self.active = bool(active)

    def __call__(self, *args):
        self.call += 1
        if self.active:
            if args in self.data:
                return self.data[args]
            else:
                data = self.func(*args)
                self.data[args] = data
                self.calc += 1
                return data
        else:
            return self.func(*args)

    @property
    def name(self):
        return self.func.__name__

    @property
    def ratio(self):
        return self.calc / self.call

    @property
    def info(self):
        return '{}: call={} calc={} ratio={:0.3f}'.format(
            self.name, self.call, self.calc, self.ratio)


class Progress:

    def __init__(self, total, filename=None, step=1, rem=True):
        self.cola = Queue(maxsize=total)
        self.offset = 0
        self.start = datetime.now()
        self.dtime = ''
        self.total = total
        self.count = 1
        self.step = step if step else 0
        self.next = 0
        self.msg = self.getmsg(rem).format
        if filename:
            dst = os.path.expanduser('~/.local/logs/thesis')
            if not os.path.isdir(dst):
                os.makedirs(dst)
            name = os.path.basename(filename)
            import logging
            logging.basicConfig(
                level = logging.DEBUG,
                format = '%(asctime)s  %(message)s',
                datefmt = '%Y/%m/%d %H:%M:%S',
                filename = '{}/{}.log'.format(dst, name),
                filemode = 'w',
            )
            self.log = logging.getLogger(name)
            self.log.info(os.path.abspath(filename))
        else:
            self.log = None
        self.thread = Thread(target=self.run)
        self.thread.start()

    def getmsg(self, rem):
        t = [i / self.total for i in range(9)]
        n = len(t)
        d = 0
        while True:
            dd = 5 if d > 0 else 4
            fmt = '{{0:{}.{}%}}'.format(dd+d, d)
            if len(set(fmt.format(i) for i in t)) == n:
                break
            d += 1
        return fmt + ('  [{1}]' if rem else '') + '  {2}  '

    def __call__(self, message):
        self.cola.put(message)

    def run(self):
        while True:
            message = self.cola.get()
            end = self.count == self.total
            dt = datetime.now() - self.start
            t = time()
            if end:
                self.dtime = str(dt)
            if self.count == 1:
                frac = 0
                rem = '-:--:--'
            else:
                frac = (self.count - 1) / self.total
                rem = str(dt / frac - dt).split('.')[0]
            msg = self.msg(frac, rem, message)
            if t >= self.next or end:
                self.next = time() + self.step
                sys.stdout.write('\b \b' * self.offset)
                self.offset = len(msg)
                sys.stdout.write(msg)
                sys.stdout.flush()
            if self.log:
                self.log.info(msg)
                if end:
                    self.log.info(self.dtime)
            self.count += 1
            if end:
                break

    def __del__(self):
        sys.stdout.write('\b \b' * self.offset + self.dtime + '\n')
        sys.stdout.flush()
        try:
            self.thread.join()
        except RuntimeError:
            pass


@instanciar
class plantilla:
    filtros = (
        # Patron Reemplazo
        ('(\{)',    '{{'),
        ('(\})',    '}}'),
        ('(«)',      '{'),
        ('(»)',      '}'),
    )
    patron = re.compile('|'.join([f[0] for f in filtros])).sub
    
    def __call__(self, codigo):
        return self.patron(self.reemplazar, codigo)
    
    def reemplazar(self, objeto):
        return self.filtros[objeto.lastindex - 1][1]


@instanciar
class escapar_meta_latex:
    filtros = {
        '#':  '\\#',
        '$':  '\\$',
        '%':  '\\%',
        '&':  '\\&',
        '\\': '\\textbackslash',
        '^':  '\\textasciicircum',
        '_':  '\\_',
        '{':  '\\{',
        '}':  '\\}',
        '~':  '\\textasciitilde',
    }
    patron = re.compile('([{}])'.format(''.join(filtros).replace('\\', '\\\\'))).sub
    
    def __call__(self, codigo):
        return self.patron(self.reemplazar, codigo)
    
    def reemplazar(self, objeto):
        return self.filtros[objeto.group(0)]


def recta(x, y):
    """
        Ajusta X e Y a Y=f(X), devuelve una función ejecutable y un dict con:
        
        slope           Pendiente
        intercept       Ordenada al origen (intercección)
        r_value         Coeficiente de correlación
        r_squared       Resultado de 'r_value ** 2'
        p_value         Ho) slope == 0  Ha) slope != 0
        std_err         Error estandar de la estimación
    """
    slope, intercept, r_value, p_value, std_err = stats.linregress(x, y)
    ajustes = {
        'slope':          slope,
        'intercept':      intercept,
        'r_value':        r_value,
        'r_squared':      r_value ** 2,
        'p_value':        p_value,
        'std_err':        std_err,
    }
    def f(x):
        return numpy.array(x) * slope + intercept
    return f, ajustes


def printpns(p_value, r_squared, lim=0.06):
    if p_value > lim:
        c = '    \033[41m\033[1;37m {} \033[0m\n'
        m = c.format("PENDIENTE NO SIGNIFICATIVA: p-valor={:0.4f}, R²={:.2%}")
        sys.stderr.write(m.format(p_value, r_squared))
        sys.stderr.flush()


def elimina_caracteres_diacriticos(cadena):
    """ Elimina los caracteres Mark y Nonspacing (category 'Mn') de la cadena."""
    return ''.join([c for c in normalize('NFKD', cadena) if category(c) != 'Mn'])


def elimina_caracteres_latex(cadena, seguro=None, eliminar=None):
    seguro = seguro or ''
    eliminar = META_CHAR_LATEX + (eliminar or '')
    return ''.join([c for c in cadena if c in seguro or c not in eliminar])


def saneado_latex(cadena, seguro=None, eliminar='!=°'):
    """ Senea la cadena: elimina los caracteres diacríticos y los comandos 
        especiales de LaTeX, ademas de convertir en str la 'cadena'
    """
    if not isinstance(cadena, str):
        cadena = str(cadena)
    cadena = elimina_caracteres_diacriticos(cadena)
    cadena = elimina_caracteres_latex(cadena, seguro, eliminar)
    return cadena


def listar_meta(cadena, meta, escapador='\\', dic=False):
    """ Devuelve una lista con las posiciones de los metacaracteres en la 'cadena'
        que NO estén escapados con el 'escapador'.
        
        dic=True devuelve un diccionario {meta:[posiciones]}, sino [posiciones]
    """
    escapador = escapador.replace('\\', '\\\\')
    meta = meta.replace('\\', '\\\\').replace(']', '\\]')
    patron = re.compile('({}*)([{}])'.format(escapador, meta))
    devolver = {} if dic else []
    for c in patron.finditer(cadena):
        if len(c.group(1)) % 2 == 0:
            if dic:
                devolver.setdefault(c.group(2), []).append(c.start())
            else:
                devolver.append(c.start())
    return devolver


def objeto_re_meta(meta, flags=0):
    """ Genera el objeto re para buscar los metacaracteres"""
    code = '([{}])'.format(meta.replace('\\', '\\\\').replace(']', '\\]'))
    return re.compile(code, flags=flags)


def escapar_meta_re(texto):
    """ Función para escapar metacaracteres re"""
    re_meta = objeto_re_meta(META_CHAR_RE)
    return re_meta.sub(r'\\\1', texto)


def join_no_meta(lista):
    return '|'.join(escapar_meta_re(i) for i in lista)


_eliminable = re.compile('[()]')
_guion_bajo = re.compile('[\s]+')

def normalizado(cadena):
    cadena = _eliminable.sub('', cadena)
    cadena = _guion_bajo.sub('_', cadena)
    return cadena.lower()


# Días por mes reales
días_mes_r = {
    1:  31,
    2:  28,
    3:  31,
    4:  30,
    5:  31,
    6:  30,
    7:  31,
    8:  31,
    9:  30,
    10: 31,
    11: 30,
    12: 31,
}

días_mes_racum0 = {}
días_mes_racum = {}
tmp = 0
for mes, días in días_mes_r.items():
    días_mes_racum0[mes] = tmp
    tmp += días
    días_mes_racum[mes] = tmp

# Días por mes promedios
días_mes_p = 365 / 12
días_mes_pacum0 = {}
días_mes_pacum = {}
tmp = 0
for i in range(1, 13):
    días_mes_pacum0[i] = round(tmp, 2)
    tmp += días_mes_p
    días_mes_pacum[i] = round(tmp, 2)


# Nombres y números de los meses
meses_num_nom = {
    1:  'enero',
    2:  'febrero',
    3:  'marzo',
    4:  'abril',
    5:  'mayo',
    6:  'junio',
    7:  'julio',
    8:  'agosto',
    9:  'septiembre',
    10: 'octubre',
    11: 'noviembre',
    12: 'diciembre',
}
meses_nom_num = {v: c for c, v in meses_num_nom.items()}


def getday(data):
    """
        getday('13 de octubre') -> 286, (13, 10), '13 de octubre'
        getday((13, 10))        -> 286, (13, 10), '13 de octubre'
        getday(286)             -> 286, (13, 10), '13 de octubre'
    """
    if isinstance(data, str):
        parts = data.lower().split()
        for mes, nombre in meses_num_nom.items():
            if nombre in parts:
                break
        else:
            raise ValueError('overflow data ({})'.format(data))
        digits = [int(i) for i in parts if i.isdigit()]
        days = [i for i in digits if 1 <= i <= días_mes_r[mes]]
        if days:
            día = days[0]
        else:
            raise ValueError('overflow day ({})'.format(data))

    elif hasattr(data, '__iter__'):
        día, mes = data

    else:
        data = int(data)
        for mes, n in días_mes_racum.items():
            if n >= data:
                break
        else:
            raise ValueError('overflow data ({})'.format(data))
        día = data - días_mes_racum0[mes]

    n = días_mes_racum0[mes] + día
    return n, (día, mes), '{:2.0f} de {}'.format(día, meses_num_nom[mes])


class Codificador:
    def __init__(self, entrada, salida):
        self.entrada = str(entrada)
        self.salida = str(salida)

    def __repr__(self):
        return "<Codificador: base{} ({})>".format(len(self.salida), self.salida)

    def encode(self, s):
        return self.convertir(s, self.entrada, self.salida)

    def decode(self, s):
        return self.convertir(s, self.salida, self.entrada)

    def convertir(self, s, entrada, salida):
        # Proceso: entrada_baseA => x (entero) => salida_baseB
        s = str(s)
        x = 0
        for caracter in s:
            x = x * len(entrada) + entrada.index(caracter)
        if x == 0:
            resultado = salida[0]
        else:
            resultado = ''
            while x > 0:
                caracter = x % len(salida)
                resultado = salida[caracter] + resultado
                x = int(x // len(salida))
        return resultado

    def __call__(self, s):
        return self.encode(s)

numero_a_letra = Codificador('0123456789', 'OABCDEFGHI')


def sturges(size):
    """Regla de Sturges: número de clases para un histograma"""
    return math.ceil(1 + math.log(size, 2))
