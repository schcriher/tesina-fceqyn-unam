#-*- coding: utf-8 -*-

from math import ceil, floor, inf
from pandas import HDFStore, DataFrame
from os.path import splitext, basename
from itertools import cycle

from Library.Panel import Ylc, cosε
from Library.Utils import Progress

exec(open('Modelo.py').read())
energía_día.activate_cache(False)       # Se modifica 'pot' en cada prueba

dbd = HDFStore('Data.h5', mode='r')
rad = dbd.get('nasa/R')['1984':'2014']  # Datos reales (31 años completos)
hsp = dbd.get('hottel')                 # horas solares pico (kWh/(m²·día))
dbd.close()

# Planta
Mhfull = 24920                          # Producción al 100%, gH2/día
Dstock = 6                              # Stock de optimización, días (serán 13)

NR = len(rad.resample('A'))
dias = [d + 365 * (i // Ndl) for i, d in enumerate(NL * NR)]
ND = len(dias)

S = Sol(ωi, ωf, ωs, β)  # el área efectiva no es simétrica respecto de ψ
α = S.α()               # rad
ψ = S.ψ()               # rad
I = S.I(ffv)            # W/m² (potencia disponible)
ω = {d: S.ω[nl] for d, nl in zip(dias, cycle(NL))}

Id = 5
lote = 'mp{}'.format(Id)
*_, Xlote, Ylote, Pinst, Ihsep, Ypsep = Lotes[Id]
Alote = Xlote * Ylote                       # m²
Ainst = 0.137 * Alote                       # m² (ylim=15.88)
Ysep = Ypsep / cosε
P = Paneles(Xlote, Ylote, Pinst, Ainst, Hinst, Xpanel, Ypanel, Dpanel, β)
z1, z2 = P.distribución(Ihsep, Ysep)

Z1_Ylim = z1.ylim
Z2_Ylim = z2.ylim

def get_ylote(z2_ylim):
    ylote = Ylc + Z1_Ylim + Ihsep + z2_ylim + Ylc
    assert ylote <= Ylote
    return ylote

hcil = int(Mhfull / mhcil)
Mhs = [int(mhcil * i) for i in range(int(floor(hcil / 2)), hcil + 1)]
Ves = (30, 60)

progress = Progress(len(Mhs) * len(Ves), __file__)

data = {
    'NP': [],  # Nivel de producción, %
    'Mh': [],  # Producción de H2, g/día
    'Mo': [],  # Producción de O2, g/día
    'Ve': [],  # Capacidad del electrolizador, Nm³H2/h
    'n1': [],  # Número de paneles de la zona 1
    'n2': [],  # Número de paneles de la zona 2
    'nt': [],  # Número total de paneles
    'yl': [],  # Longitud 'ylim' de la zona 2
    'ch': [],  # Número de cilindros de hidrógeno por día
    'co': [],  # Número de cilindros de oxígeno por día
}
CACHE = {}
for Mh in Mhs:
    for Ve in Ves:
        progress("Mh={}, Ve={}".format(Mh, Ve))

        def fobj(z2_ylim):
            ylote = get_ylote(z2_ylim)
            P = Paneles(Xlote, ylote, Pinst, Ainst, Hinst, Xpanel, Ypanel, Dpanel, β)
            z1, z2 = P.distribución(Ihsep, Ysep)

            global pot  # Potencia, W
            if z2.n in CACHE:
                pot = CACHE[z2.n]
            else:
                pot1 = {}
                for d in range(365):
                    P.setαψ(α[d], ψ[d])
                    a, n = P.área_efectiva(Ihsep, Ypsep, z1, z2)
                    pot1[d] = I[d] * a * fin

                pot2 = {}
                for d, nl in zip(dias, cycle(NL)):
                    pot2[d] = pot1[nl] * rad[d] / hsp[nl]

                pot = {lote: pot2}
                CACHE[z2.n] = pot

            EE, ES, ER, EV, EC = energía_serie(lote, Ve, Dstock, Mh, dias)
            return Mh - sum(EE) * feh / ND  # Objetivo - Alcanzado

        z2_ylim = find1(fobj, 0.01, 0, Z2_Ylim, 0.001)

        if fobj(z2_ylim) < 10:
            ylote = get_ylote(z2_ylim)
            P = Paneles(Xlote, ylote, Pinst, Ainst, Hinst, Xpanel, Ypanel, Dpanel, β)
            z1, z2 = P.distribución(Ihsep, Ysep)
            Mo = Mh * feo
            data['NP'].append(Mh / Mhfull * 100)
            data['Mh'].append(Mh)
            data['Mo'].append(Mo)
            data['Ve'].append(Ve)
            data['n1'].append(z1.n)
            data['n2'].append(z2.n)
            data['nt'].append(z1.n + z2.n)
            data['yl'].append(z2_ylim)
            data['ch'].append(int(Mh / mhcil))
            data['co'].append(Mo / mocil)

# Guardado de los datos
filename = '{[0]}.h5'.format(splitext(basename(__file__)))
db = HDFStore(filename, mode='w')
db.put('raw', DataFrame(data))
db.close()
