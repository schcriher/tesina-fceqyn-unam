#-*- coding: utf-8 -*-

#     http://webbook.nist.gov/cgi/cbook.cgi?ID=C1333740
#     http://webbook.nist.gov/cgi/cbook.cgi?ID=C7782447
# [1] http://portalweb.cammesa.com/default.aspx
# [2] http://www.emsadigital.com.ar/servicios/cuadrotarifario.asp
#     http://www.emsadigital.com.ar/servicios/cuadrotarifario_240.asp
# [3] Datos de Oeste Gases (Moroziuk Ruben)
#     Y http://www.norriscylinder.com/dot-specifications.php

from Library.Sol import tpi, fdr, frd, fgr, frg, frh, fhr, fds, fsd, frs, fsr
from Library.Panel import ε, π, π2, Xlc, Ylc

β = 30                                  # Ángulo de inclinación del panel

ωc = 45/60                              # Mediodía laboral 12:45 (0 a las 12:00)
ωr = 3.5                                # ±3.5 h respecto del mediodía laboral
ωi = ωc - ωr                            # hora de inicio laboral
ωf = ωc + ωr                            # hora de final laboral
ωs = 300/3600                           # step, cada 300 segundos (5 minutos)

N = tuple(range(365))                   # Días del año completo
NL = tuple(n for n in N if (n%6!=0 and n%7!=0 and n%10!=0)) # Días del año laboral
Ndt = len(N)                            # número de día totales por año, 365
Ndl = len(NL)                           # número de día laborales por año, 240

# Panel de referencia
Ppanel = 0.320                          # kW \
Xpanel = 1.960 + 0.02                   # m   > ffv = 0.16 kW/m² (16%)
Ypanel = 0.992 + 0.02                   # m  /
Apanel = Xpanel * Ypanel                # m²
Dpanel = 3                              # Número de diodos de bypass (zonas)
ffv = Ppanel / Apanel                   # eficiencia fotovoltaica
fin = 0.95                              # eficiencia inversor, adimensional

# Densidades                              http://webbook.nist.gov/chemistry/fluid
dh =     89.885                         # g/m³ \
do =   1429.08                          # g/m³  > CNPT (1atm, 0°C)
da = 999843.1                           # g/m³ /
dh2 =    14.24                          # g/L  \ 200bar
do2 =   266.31                          # g/L  / 30°C

# Masas molares
mh = 2.01588                            # g/mol
mo = 31.9988                            # g/mol
ma = mh + mo / 2                        # g/mol
mho = 0.5 * mo / mh                     # gO2/gH2, estequiometría - hidrólisis

# Compresión adiabática de 10 a 200 bar   Datos a 25°C y 10 bar
vh = 1.23691                            # m³/kg, volumen específico
cph = 28.8899                           # J/(mol·K), calor específico a P=cte
cvh = 20.5363                           # J/(mol·K), calor específico a T=cte
vo =  0.0769827                         # m³/kg, volumen específico
cpo = 29.8857                           # J/(mol·K), calor específico a P=cte
cvo = 21.1403                           # J/(mol·K), calor específico a T=cte
cah = cph / cvh                         # coeficiente adiabático
cao = cpo / cvo                         # coeficiente adiabático
fcah = (cah - 1) / cah
fcao = (cao - 1) / cao
# Consumo en kWh/Nm³H2
fconv = 10 / 36000 * 1.05               # 10 bar→Pa; J→kWh; kg→g; +5% motor
fp = 200 / 10
ech = fconv * vh * (fp ** fcah - 1) * dh / fcah
eco = fconv * vo * (fp ** fcao - 1) * dh / (fcao * mho)

# Factores unitarios
fee = 5.2 + ech + eco                   # 5.2 kWh/Nm³H2 + energía por compresión
feh = dh * 1000 / (fee * 3600)          # gH2/MJ
feo = mho                               # gO2/gH2
fea = 1.8 * da / (dh * 1000)            # gH2O/gH2, 1.5~2 LH2O/Nm³H2
ffc = 5 * 60 / (65 * dh)                # MJ/gH2, 65 L/min para 5 kW

# Cilindro 50L @ 200bar, [3]
dcil = 0.244                            # m, diámetro
vcil = 50                               # L, volumen
mhcil = dh2 * vcil                      # gH2/cilindro
mocil = do2 * vcil                      # gO2/cilindro

# Costos (Septiembre del 2017)
Ch = 1446 * 1.21 / 1000                 # $/gH2
Co = 84 * 1.21 / 1000                   # $/gO2
Cv = 240 / 3600                         # $/MJ, 240 $/MWh [1]
Cc = 0.735 * 1.21 * 1000 / 3600         # $/MJ, 0.735 $/kWh [2]
Cd = 17.3                               # $/US$
Cf = 1.8 * Cd * ffv * 1000              # $/m², 1.8 US$/W
Ce = 800e3 * Cd / 120                   # $/Nm³H2/h, Equipo 120 Nm³H2/h 800e3 US$
Ct = 100                                # $/m², PIP: entre 20 y 100
Ccil = 5125                             # $/cilindro, abagas.com

# Lotes del PIP
Hinst = 6                               # metros de altura de la edificación
Lotes = (
    #Id, Manzana y Parcela, Xlote,  Ylote, Pinst,       Ihsep, Ypsep
    (0, 'M 1, 18 a 22',     40.00,  31.66, 'izquierda', 4.3,   1.62),  # 18, 19, 22  Ylote=31.67
    (1, 'D 26 a 30',        63.59,  40.00, 'izquierda', 4.3,   1.69),
    (2, 'L 1 a 7',          38.00,  84.55, 'arriba',    9.9,   1.58),  # 1           Xlote=39.00
    (3, 'M 2 a 6',          43.00,  95.00, 'arriba',   10.4,   1.58),
    (4, 'M 13 a 17',        43.00,  95.00, 'abajo',     4.9,   1.58),
    (5, 'D 31',             63.59, 104.55, 'arriba',   10.9,   1.59),
)

# Factores de converción
fcvp = dh * 1e6 / (feh * 3600)      # Nm³→g;MJ→J;g→MJ;h→s [Nm³·h → W]
fcae = frh * 3600 / 1e6             # rad→h;h→s;J→MJ      [W·rad → MJ]
