#-*- coding: utf-8 -*-

# Cambiando la distribución horaria semanal de 8 hora de lunes a viernes a 6:40
# de lunes a sábado, sumando 40 horas semanales acorde a la ley 11544

from math import floor, ceil
from numpy import inf
from pandas import HDFStore, DataFrame
from os.path import splitext, basename
from itertools import product

from Library.Utils import Progress, ffloor

exec(open('Modelo.py').read())

Id = 5
*_, Xlote, Ylote, Pinst, Ihsep, Ypsep = Lotes[5]
lote = 'mp{}'.format(Id)

N = tuple(range(365))                   # Días del año completo
NL = tuple(n for n in N if (n%7!=0 and n%10!=0)) # Días del año laboral (lunes a sábado)
Ndt = len(N)                            # número de día totales por año, 365
Ndl = len(NL)                           # número de día laborales por año, 281

ωc = 45/60                              # Mediodía laboral 12:45 (0 a las 12:00)
ωr = 2+50/60                            # ±2:50 h respecto del mediodía laboral
ωi = ωc - ωr                            # hora de inicio laboral
ωf = ωc + ωr                            # hora de final laboral
ωs = 300/3600                           # step, cada 300 segundos (5 minutos)

S = Sol(ωi, ωf, ωs, β)  # el área efectiva no es simétrica respecto de ψ
α = S.α()               # rad
ψ = S.ψ()               # rad
I = S.I(ffv)            # W/m² (potencia disponible)
ω = S.ω

Ve = 60                                 # Capacidad del electrolizador, Nm³H2/h
Dstock = 13                             # Stock de hidrógeno, días
Mhactual = 24920                        # Producción, gH2/día
MHMAX = 78750                           # g/día, 78.75 kg/día = 18900 kgH2/año
Npanel = 1551                           # Número de paneles
Af = Npanel * Apanel                    # Área fotovoltaica, m²
L = Xlote * Ylote                       # Área del lote, m²

# Reemplazo de Potencias.py
Ainst = 0.15 * Xlote * Ylote
P = Paneles(Xlote, Ylote, Pinst, Ainst, Hinst, Xpanel, Ypanel, Dpanel, β)
POT = {}
for d in range(365):
    P.setαψ(α[d], ψ[d])
    a, n = P.área_efectiva(Ihsep, Ypsep)
    POT[d] = I[d] * a * fin                         # Potencia, W
pot = {lote: POT}

def fobj(Mh):
    ee = sum(energía_serie(lote, Ve, Dstock, Mh, dias=NL)[0])
    return round(Mh - ee * feh / Ndl, 2)

Mh = floor(find2(fobj, 0.01, 100, MHMAX, 0.01))

print('Mh={} ({:.1%}) (número de días laborales: {})'.format(Mh, Mh / Mhactual - 1, len(NL)))

