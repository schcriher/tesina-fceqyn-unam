#-*- coding: utf-8 -*-

from numpy import pi, sin, cos, tan

from Library.Sol import Sol
from Library.Panel import ε, cosε, π2

exec(open('Variables.py').read())
S = Sol(ωi, ωf, ωs, β)

L1 = []
L2 = []
T1 = []
T2 = []
R1 = []
R2 = []
for α, ψ in zip(S.α(), S.ψ()):
    ψε = ψ + ε
    l = ψε > 0
    t = (ψε > -π2) & (ψε < π2)
    r = ψε < 0
    L1.extend((34.8 * tan(α[l]) / sin(ψε[l]) + Hinst).tolist())
    L2.extend((34.8 * tan(α[l]) / sin(ψε[l])).tolist())
    T1.extend((38.0 * tan(α[t]) / cos(ψε[t]) + Hinst).tolist())
    T2.extend(((38.0 + 26.78)* tan(α[t]) / cos(ψε[t])).tolist())
    R1.extend((16.0 * tan(α[r]) / sin(-ψε[r]) + Hinst).tolist())
    R2.extend((16.0 * tan(α[r]) / sin(-ψε[r])).tolist())

print("""Alturas máximas sin sombra en los paneles (en metros)
 (L) Izquierda:  z1={:4.1f}  z2={:4.1f}
 (T) Arriba:     z1={:4.1f}  z2={:4.1f}
 (R) Derecha:    z1={:4.1f}  z2={:4.1f}
""".format(min(L1), min(L2), min(T1), min(T2), min(R1), min(R2)))
