#!/usr/bin/env python3
#-*- coding: utf-8 -*-

# http://www.mail-archive.com/sundial@uni-koeln.de/msg01050.html
# http://www.mail-archive.com/sundial@uni-koeln.de/msg01051.html
# http://gearju.com/225768904560/Data/Engineering/Solar/Solar%20Engineering%20of%20Thermal%20Processes,%204th%20Edition%20-%20GearTeam.pdf
# https://eva.fing.edu.uy/pluginfile.php/54561/mod_resource/content/1/Curso-FES-Radiacion-solar-v2.3.pdf

from math import pi, sin, cos, tan, acos, exp
from scipy.integrate import quad

tpi = 2 * pi            # 2·π
fdr = tpi / 365         # factor de días a radianes
fgr = tpi / 360         # factor de grados a radianes
fhr = tpi / 24          # factor de horas a radianes
frh = 24 / tpi          # factor de radianes a horas

Gsc = 1367              # W/m², extraterrestre
φ = -27.367 * fgr       # rad, latitud geográfica

f = 2 * frh / 1000      # simetría; rad → hora; W → kW

def irradiacion(n, β, a0, a1, k):
    # n: Número de día del año (0 < n < 364)
    # β: Ángulo de inclinación de la superficie (0=piso; 90=pared)
    # ω: Desplazamiento angular del sol sobre el plano de su trayectoria,
    #    ω < 0 antes del mediodía solar, radianes

    # Γ: ángulo diario, radianes
    Γ = n * fdr
    sin1Γ = sin(    Γ); cos1Γ = cos(    Γ)
    sin2Γ = sin(2 * Γ); cos2Γ = cos(2 * Γ)
    sin3Γ = sin(3 * Γ); cos3Γ = cos(3 * Γ)

    # Spencer (1971) error máximo: 0.0006 rad
    # δ: declinación solar, radianes
    δ = (0.006918
        - 0.399912 * cos1Γ + 0.070257 * sin1Γ
        - 0.006758 * cos2Γ + 0.000907 * sin2Γ
        - 0.002697 * cos3Γ + 0.001480 * sin3Γ
    )

    # Spencer (1971) error máximo: 0.0001
    # ρ: factor de corrección de la distancia Tierra-Sol
    ρ = (1.000110
        + 0.034221 * cos1Γ + 0.001280 * sin1Γ
        + 0.000719 * cos2Γ + 0.000077 * sin2Γ
    )

    cosθ_a = sin(δ) * sin(φ + β)
    cosθ_b = cos(δ) * cos(φ + β)

    def irradiancia(ω):
        # Duffie and Beckman (1980)
        # θ: ángulo zenital
        # Para el Hemisferio Sur (φ<0) y mirando al norte (γ=180):
        cosθ = cosθ_a + cosθ_b * cos(ω)

        # Hottel (1976) transmitancia atmosférica para la radiación directa
        τb = a0 + a1 * exp(-k / cosθ)

        # Liu y Jordan (1960) transmitancia atmosférica para la radiación difusa
        τd = 0.2710 - 0.2939 * τb

        Go = Gsc * ρ * cosθ     # Irradiancia directa sobre el panel
        Gcb = τb * Go           # Irradiancia, componente directa
        Gcd = τd * Go           # Irradiancia, componente difusa
        G = Gcb + Gcd           # Irradiancia global, W/m²
        return G                # W/m²

    # ωs: ángulo horario de puesta del sol (sunset), radianes
    ωs = acos(-tan(φ + β) * tan(δ))

    return quad(irradiancia, 0, ωs)[0] * f  # kW·h/m²
