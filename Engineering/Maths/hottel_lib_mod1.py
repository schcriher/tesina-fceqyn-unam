#!/usr/bin/env python3
#-*- coding: utf-8 -*-

from sympy import *
init_printing()

# ---------------------------------------------------------------------------- #

δ, φ, β, γ, ω = symbols('δ, φ, β, γ, ω', real=True)

sinδ = sin(δ); cosδ = cos(δ)
sinφ = sin(φ); cosφ = cos(φ)
sinβ = sin(β); cosβ = cos(β)
sinγ = sin(γ); cosγ = cos(γ)
sinω = sin(ω); cosω = cos(ω)

cosθ_orig = (
    + sinδ * sinφ * cosβ
    - sinδ * cosφ * sinβ * cosγ
    + cosδ * cosφ * cosβ        * cosω
    + cosδ * sinφ * sinβ * cosγ * cosω
    + cosδ        * sinβ * sinγ * sinω
)

cosθ = (
    (sinδ * sinφ + cosδ * cosφ * cosω) * cosβ +
    ((cosδ * sinφ * cosω - sinδ * cosφ) * cosγ + cosδ * sinω * sinγ) * sinβ
)

assert cosθ_orig.expand() == cosθ.expand()

for name, code in (('cosθ_orig', cosθ_orig), ('cosθ', cosθ)):
    print('count_ops({}): [{}] {}'.format(name, code.count_ops(), code.count_ops(visual=True)))

# ---------------------------------------------------------------------------- #

sinδ, sinφ, sinβ, sinγ, sinω = symbols('sinδ, sinφ, sinβ, sinγ, sinω', real=True)
cosδ, cosφ, cosβ, cosγ, cosω = symbols('cosδ, cosφ, cosβ, cosγ, cosω', real=True)

cosθ_orig = (
    + sinδ * sinφ * cosβ
    - sinδ * cosφ * sinβ * cosγ
    + cosδ * cosφ * cosβ        * cosω
    + cosδ * sinφ * sinβ * cosγ * cosω
    + cosδ        * sinβ * sinγ * sinω
)

cosθ = (
    (sinδ * sinφ + cosδ * cosφ * cosω) * cosβ +
    ((cosδ * sinφ * cosω - sinδ * cosφ) * cosγ + cosδ * sinω * sinγ) * sinβ
)

assert cosθ_orig.expand() == cosθ.expand()

for name, code in (('cosθ_orig', cosθ_orig), ('cosθ', cosθ)):
    print('count_ops({}): [{}] {}'.format(name, code.count_ops(), code.count_ops(visual=True)))
