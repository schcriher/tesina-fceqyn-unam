#!/usr/bin/env python3
#-*- coding: utf-8 -*-

import math
fgr = math.pi / 180
err = 1e-15
VARS = {
    'Ypanel': 1,
    'Ypsep': 1.5,
    'Ypsom': None,  # incógnita
    'α': 39 * fgr,
    'β': 27 * fgr,
    'ψ': 10 * fgr,
}
LOCALS = locals()



from sympy import *
init_printing()

for k in VARS:
    LOCALS[k] = Symbol(k, real=True)

Yp = Ypanel * cos(β)
Zp = Ypanel * sin(β)

tan_α2 = tan(α) / cos(ψ)

h = Ypsom * sin(β)
d1 = h / tan_α2
d2 = h / tan(β)
D = Zp / tan_α2

eq = d1 + d2 + Ypsep - D - Yp
roots = solve(eq, Ypsom)

Ypsom_s = simplify(roots[0])
Ypsom_sv = Ypsom_s.subs({LOCALS[k]:v for k, v in VARS.items()})



Ypsom_m = Ypanel - Ypsep / (cos(β) + sin(β) * cos(ψ) / tan(α))
Ypsom_mv = Ypsom_m.subs({LOCALS[k]:v for k, v in VARS.items()})



from math import *
for k, v in VARS.items():
    LOCALS[k] = v

Ypsom_mc = Ypanel - Ypsep / (cos(β) + sin(β) * cos(ψ) / tan(α))



a = abs(Ypsom_sv - Ypsom_mv) < err
b = abs(Ypsom_sv - Ypsom_mc) < err
c = abs(Ypsom_mv - Ypsom_mc) < err
print('''
Resultados (|err| < {}: {} {} {})

 Ypsom_sv = {}
 Ypsom_mv = {}
 Ypsom_mc = {}

Ypsom: 


{}


{}
'''.format(err, a, b, c, Ypsom_sv, Ypsom_mv, Ypsom_mc, pretty(Ypsom_s), pretty(Ypsom_m)))
