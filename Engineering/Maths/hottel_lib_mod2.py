#!/usr/bin/env python3
#-*- coding: utf-8 -*-

from sympy import *
init_printing()

# ---------------------------------------------------------------------------- #

δ, φ, β, γ, ω = symbols('δ, φ, β, γ, ω', real=True)

γ = pi

sinδ = sin(δ); cosδ = cos(δ)
sinφ = sin(φ); cosφ = cos(φ)
sinβ = sin(β); cosβ = cos(β)
sinγ = sin(γ); cosγ = cos(γ)
sinω = sin(ω); cosω = cos(ω)

cosθ_orig = (
    + sinδ * sinφ * cosβ
    - sinδ * cosφ * sinβ * cosγ
    + cosδ * cosφ * cosβ        * cosω
    + cosδ * sinφ * sinβ * cosγ * cosω
    + cosδ        * sinβ * sinγ * sinω
)

cosθ = sin(δ) * sin(φ + β) + cos(δ) * cos(φ + β) * cos(ω)

assert trigsimp(cosθ_orig, method='fu') == cosθ

for name, code in (('cosθ_orig', cosθ_orig), ('cosθ     ', cosθ)):
    print('count_ops({}): [{}] {}'.format(name, code.count_ops(), code.count_ops(visual=True)))

