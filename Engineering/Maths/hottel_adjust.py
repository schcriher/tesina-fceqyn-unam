#!/usr/bin/env python3
#-*- coding: utf-8 -*-

from numpy import linspace
from pandas import HDFStore
from scipy.optimize import curve_fit
from scipy.integrate import simps

exec(open('hottel_lib.py').read())

db = HDFStore('../Data.h5', mode='r')
R = db.get('nasa/Rdm')
db.close()

N = tuple(range(365))                   # días del año
β = 0                                   # horizontal (HSP - NASA)

# A = 0.100  # km, altitud
# r0 = 0.95  # \
# r1 = 0.98  #  > Clíma tropical
# rk = 1.02  # /
# a0 = r0 * (0.4237 - 0.00821 * (6.0 - A) ** 2) # 0.131014405 (0.1379099)
# a1 = r1 * (0.5055 + 0.00595 * (6.5 - A) ** 2) # 0.734227760 (0.7492120)
# k  = rk * (0.2711 + 0.01858 * (2.5 - A) ** 2) # 0.385683216 (0.3781208)

infos = []
names = ['a0', 'a1', 'k']
values = [0.3, 0.3, 0.3]
digits = [4, 4, 4]

for i in range(3):

    def func(N, *args):
        a0, a1, k = values[:i] + list(args)
        return [irradiacion(n, β, a0, a1, k) for n in N]

    popt = curve_fit(func, N, R, values[i:], ftol=1e-3)[0]
    values[i] = round(popt[0], digits[i])

    for j, v in zip(range(i+1, 3), popt[1:]):
        values[j] = v

    info = '\n'.join('{:2} = {}'.format(n, v) for n, v in zip(names[i:], popt))
    infos.append(info)
    print('\n' * bool(i) + info)

with open('hottel_adjust.info', encoding='utf-8', mode='w') as f:
    f.write('\n\n'.join(infos))
    f.write('\n\na0 = {}\na1 = {}\nk  = {}\n'.format(*values))
