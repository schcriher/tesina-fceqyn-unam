#-*- coding: utf-8 -*-
# http://globalsolaratlas.info/

from numpy import mean
from scipy.integrate import simps

from Library.Sol import Sol
from Library.Panel import Paneles

exec(open('Variables.py').read())

ωi = -8                     # \ Máxima cantidad de horas por día 14.05
ωf = +8                     # / Se asume ± 8 del mediodía solar

S = Sol(ωi, ωf, ωs, β)
ω = S.ω
α = S.α()                   # rad
ψ = S.ψ()                   # rad
I = S.I(ffv)                # W/m² (potencia disponible)

_, mp, Xlote, Ylote, Pinst, Ihsep, Ypsep = Lotes[5]
Ainst = 0.118 * Xlote * Ylote
P = Paneles(Xlote, Ylote, Pinst, Ainst, Hinst, Xpanel, Ypanel, Dpanel, β)
E = []
for d in range(365):
    P.setαψ(α[d], ψ[d])
    a, n = P.área_efectiva(Ihsep, Ypsep)
    E.append(simps(I[d] * a * fin, ω[d]) * fcae)    # Energía total, MJ

Npanel = 1551               # Cantidad de paneles
Pf = Npanel * Ppanel        # Potencia instalada, kWp
f_mj_kwh = 1000 / 3600      # factor de conversión, MJ → kWh
print("kWh/kWp de la planta: {}".format(mean(E) * f_mj_kwh / Pf))
