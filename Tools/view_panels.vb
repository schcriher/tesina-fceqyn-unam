Option Explicit

REM https://wiki.openoffice.org/wiki/Documentation/OOoAuthors_User_Manual/Getting_Started/Creating_a_macro
REM https://wiki.openoffice.org/wiki/Documentation/BASIC_Guide/Cells_and_Ranges
REM http://www.openoffice.org/api/docs/common/ref/com/sun/star/sheet/CellFlags.html

Dim view As Object
Dim sheet As Object
Dim document As Object
Dim dispatcher As Object
Dim clear As Integer

Sub Borrar()
    view = thisComponent.CurrentController
    sheet = view.ActiveSheet
    with com.sun.star.sheet.CellFlags
        clear = .VALUE + .STRING + .DATETIME + .FORMULA + .ANNOTATION
    end with
    sheet.getCellRangeByName("B2:AZ40").clearContents(clear)
    view.select(sheet.getCellRangeByName("A1"))
End Sub

Sub Importar()
    view = thisComponent.CurrentController
    sheet = view.ActiveSheet
    document = view.Frame
    dispatcher = createUnoService("com.sun.star.frame.DispatchHelper")
    view.select(sheet.getCellRangeByName("B2"))
    dispatcher.executeDispatch(document, ".uno:Paste", "", 0, Array())
    view.select(sheet.getCellRangeByName("A1"))
End Sub

Sub Borrar_e_Importar()
    Borrar()
    Importar()
End Sub
