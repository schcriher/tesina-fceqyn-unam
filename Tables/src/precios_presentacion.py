filtros = (     # todo con IVA
    (3, 5, 2),  # Precio del Hidrógeno, $/kg
    (4, 5, 2),  # Precio del Hidrógeno, $/cil
    (5, 5, 2),  # Precio del Oxígeno, $/kg
    (6, 5, 2),  # Precio del Oxígeno, $/cil
)
header = {idx: 'S[table-format = {}.{}]'.format(i, d) for idx, i, d in filtros}
c = Cuadro(
    destino = BASE,
    header = ''.join([header.get(i, 'c') for i in range(1, 7)]),
    hlinea = 'rule',
)
c['sisetup'] = 'per-mode = symbol'
c['sisetup'] = 'table-number-alignment = center'
c.columnas(conversiones={idx: '.{}f'.format(d) for idx, i, d in filtros})

hoja = "Programa de producción"
ini = 25

pureza = Economicidad.extraer("{}.A{}:A{}".format(hoja, ini, ini + 1))
ventas = Economicidad.extraer("{}.B{}:B{}".format(hoja, ini, ini + 1))
ph = Economicidad.extraer("{}.C{}:C{}".format(hoja, ini, ini + 1))
po = Economicidad.extraer("{}.D{}:D{}".format(hoja, ini, ini + 1))

p = GT(r"Pureza")
v = GT(r"Producción")
h = GT(r"Precio \ch{H2} con IVA")
o = GT(r"Precio \ch{O2} con IVA")

kg = "{\si{\peso\per\kg}}"
cil = "{\si{\peso\per\cilindro}}"

c(p, v, h, o, c='1:2;2:2;3-4;5-6', hlinea='rule', hlcortes=[4])
c(kg, cil, kg, cil, hlinea='rule')

for pureza, ventas, ph, po in zip(pureza, ventas, ph, po):
    pureza = r'\num{{{:.1f}}}'.format(float(pureza))
    ventas = r'\SI{{{:.0f}}}{{\percent}}'.format(float(ventas) * 100)
    ph *= 1.21
    po *= 1.21
    phc = ph * (mhcil / 1000)
    poc = po * (mocil / 1000)
    c(pureza, ventas, ph, phc, po, poc)

c.tex()
