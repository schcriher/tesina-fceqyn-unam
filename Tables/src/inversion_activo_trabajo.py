filtros = (
    (3, 7, 0),  # 1er año
    (4, 7, 0),  # 3er año
    (5, 7, 0),  # 5to año
    (6, 7, 0),  # total
)
header = {idx: 'S[table-format = {}.{}]'.format(i, d) for idx, i, d in filtros}
header.update({1: 'l'})
c = Cuadro(
    destino = BASE,
    header = ''.join([header.get(i, 'c') for i in range(1, 7)]),
    hlinea = 'rule',
)
c['sisetup'] = 'per-mode = symbol'
c['sisetup'] = 'table-number-alignment = center'
c.columnas(conversiones={idx: '.{}f'.format(d) for idx, i, d in filtros})

hoja = "Inversiones"
ini = 30
fin = 39

rubro = Economicidad.extraer("{}.A{}:A{}".format(hoja, ini, fin))
días = Economicidad.extraer("{}.B{}:B{}".format(hoja, ini, fin))
año1 = Economicidad.extraer("{}.C{}:C{}".format(hoja, ini, fin))
año3 = Economicidad.extraer("{}.D{}:D{}".format(hoja, ini, fin))
año5 = Economicidad.extraer("{}.E{}:E{}".format(hoja, ini, fin))
total = Economicidad.extraer("{}.F{}:F{}".format(hoja, ini, fin))

r = r"{Rubro}"
d = r"{Días}"
a = r"{A realizar en el}"
t = r"{Total}"

a1 = r"{1\textsuperscript{er} año}"
a3 = r"{3\textsuperscript{er} año}"
a5 = r"{5\textsuperscript{to} año}"

c(r, d, a, t, c='1:2;2:2;3-5c;6:2', hlinea='rule')
c(a1, a3, a5, hlinea='rule')

hl = (3, 4, 6, 8)
dt = enumerate(zip(rubro, días, año1, año3, año5, total))
for idx, (rubro, días, año1, año3, año5, total) in dt:
    if rubro.startswith('.') or rubro.startswith(' '):
        rubro = r'~~~{}'.format(rubro.strip('.').strip().strip('.').strip())
    if días:
        días = int(días)
    hlinea = 'rule' if idx in hl else None
    c(rubro, días, año1, año3, año5, total, hlinea=hlinea)

c.tex()
