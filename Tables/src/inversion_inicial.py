hoja = "Inversion inicial"
data = (
    ('equipos_nacionales',  2,  7, 'Cantidad', 6, 4, 7),
    ('equipos_importados', 12, 12, 'Cantidad', 7, 1, 7),
    ('rodados',            17, 18, 'Cantidad', 6, 1, 5),
    ('muebles',            23, 27, 'Cantidad', 4, 2, 5),
    ('terreno',            32, 32, 'Superficie (\si{\m\squared})', 3, 4, 6),
    ('construcciones',     34, 35, 'Superficie (\si{\m\squared})', 5, 3, 7),
)
for tabla, ini, fin, unidad, a, b, c in data:
    filtros = (
        (2, a, 2),  # Precio/costo
        (3, b, 0),  # Cantidad
        (4, c, 0),  # Total
    )
    header = {idx: 'S[table-format = {}.{}]'.format(i, d) for idx, i, d in filtros}
    c = Cuadro(
        destino = '{}_{}'.format(BASE, tabla),
        header = ''.join([header.get(i, 'l') for i in range(1, 5)]),
        hlinea = 'rule',
    )
    c['sisetup'] = 'per-mode = symbol'
    c['sisetup'] = 'table-number-alignment = center'
    c.columnas(conversiones={idx: '.{}f'.format(d) for idx, i, d in filtros})

    item = Economicidad.extraer("{}.A{}:A{}".format(hoja, ini, fin), lista=True)
    precio = Economicidad.extraer("{}.B{}:B{}".format(hoja, ini, fin), lista=True)
    cantidad = Economicidad.extraer("{}.C{}:C{}".format(hoja, ini, fin), lista=True)
    total = Economicidad.extraer("{}.D{}:D{}".format(hoja, ini, fin), lista=True)

    n = len(item)
    i = 0

    ti = GT(r"Item")
    tp = GT(r"Precio \si{\peso}")
    tc = GT(unidad)
    tt = GT(r"Total \si{\peso}")

    c(ti, tp, tc, tt, hlinea='rule')
    for item, precio, cantidad, total in zip(item, precio, cantidad, total):
        i += 1
        hlinea = None if i < n else 'rule'
        c(item, precio, cantidad, total, hlinea=hlinea)

    if tabla != 'terreno':
        c('Total', Economicidad.extraer("{}.D{}".format(hoja, fin + 1)), c='1-3')

    c.tex()
