hoja = "Fuente y uso de fondos"
ini = 2
fin = 19
sep = ''
for name, a, b, f in (('a', 0, 6, 8), ('b', 6, 11, 7)):
    filtros = (
        ( 2, 8, 0),  # Año 0/6
        ( 3, 8, 0),  # Año 1/7
        ( 4, 8, 0),  # Año 2/8
        ( 5, 8, 0),  # Año 3/9
        ( 6, 8, 0),  # Año 4/10
        ( 7, 8, 0),  # Año 5
    )
    header = {idx: 'S[table-format = {}.{}]'.format(i, d) for idx, i, d in filtros}
    c = Cuadro(
        destino = '{}_{}'.format(BASE, name),
        header = sep + sep.join([header.get(i, 'l') for i in range(1, f)]) + sep,
        hlinea = 'rule',
    )
    c['_latex_'] = '\\footnotesize'
    c['sisetup'] = 'per-mode = symbol'
    c['sisetup'] = 'table-number-alignment = center'
    c.columnas(conversiones={idx: '.{}f'.format(d) for idx, i, d in filtros})

    item = Economicidad.extraer("{}.A{}:A{}".format(hoja, ini, fin))
    cols = 'BCDEFGHIJKL'
    data = [Economicidad.extraer("{0}.{3}{1}:{3}{2}".format(hoja, ini, fin, l)) for l in cols[a:b]]

    años = [r"{{Año {}}}".format(i) for i in range(a, b)]
    c(r"{Ejercicio}", *años, hlinea='rule')

    hl = (4, 5, 12, 14, 16)
    dt = enumerate(zip(item, *data))
    for idx, (item, *row) in dt:
        hlinea = 'rule' if idx in hl else None
        if item.startswith('.') or item.startswith(' '):
            item = r'~~~{}'.format(item.strip('.').strip().strip('.').strip())
        c(item, *row, hlinea=hlinea)

    c.tex()
