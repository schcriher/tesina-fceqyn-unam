filtros = (
    (2, 3, 0),  # Nivel de Producción
    (3, 8, 0),  # Costos Fijos
    (4, 8, 0),  # Costos Variables
    (5, 8, 0),  # Costos Totales
    (6, 8, 0),  # Ingresos
)
header = {idx: 'S[table-format = {}.{}]'.format(i, d) for idx, i, d in filtros}
c = Cuadro(
    destino = BASE,
    header = ''.join([header.get(i, 'c') for i in range(1, 8)]),
    hlinea = 'rule',
)
c['_latex_'] = '\\footnotesize'
c['sisetup'] = 'per-mode = symbol'
c['sisetup'] = 'table-number-alignment = center'
c.columnas(conversiones={idx: '.{}f'.format(d) for idx, i, d in filtros})

a = GT(r"Período\\(\si{\year})")
p = GT(r"Nivel de\\Producción\\(\si{\percent})")
f = GT(r"Costos\\Fijos\\(\si{\peso})")
v = GT(r"Costos\\Variables\\(\si{\peso})")
t = GT(r"Costos\\Totales\\(\si{\peso})")
i = GT(r"Ingresos\\(\si{\peso})")
e = GT(r"Punto de\\equilibrio\\(\si{\percent})")

c(a, p, f, v, t, i, e, hlinea='rule')

hoja = "Punto de equilibrio"
data = (
    ( 6, 1),
    (20, 2),
    (34, 3),
    (48, 4),
    (62, 5),
    (76, 6),
)
for row, año in data:
    nivel = Economicidad.extraer("{}.A{}:A{}".format(hoja, row, row + 1))
    fijo = Economicidad.extraer("{}.B{}:B{}".format(hoja, row, row + 1))
    variable = Economicidad.extraer("{}.C{}:C{}".format(hoja, row, row + 1))
    total = Economicidad.extraer("{}.D{}:D{}".format(hoja, row, row + 1))
    ingresos = Economicidad.extraer("{}.E{}:E{}".format(hoja, row, row + 1))
    equilibrio = Economicidad.extraer("{}.B{}".format(hoja, row + 2))
    equilibrio = r'\SI{{{:.1f}}}{{\percent}}'.format(float(equilibrio) * 100)
    for n, (p, f, v, t, i) in enumerate(zip(nivel, fijo, variable, total, ingresos)):
        if n == 0:
            año = año if año < 6 else '6 al 10'
            c(año, p, f, v, t, i, equilibrio, c='1:2;7:2')
        else:
            c(p, f, v, t, i, hlinea='rule')

c.tex()
