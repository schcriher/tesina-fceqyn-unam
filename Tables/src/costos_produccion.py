filtros = (
    ( 2, 8, 0),  # Fijos     \
    ( 3, 8, 0),  # Variables  > Año 1/3/5
    ( 4, 8, 0),  # Total     /
    ( 5, 8, 0),  # Fijos     \
    ( 6, 8, 0),  # Variables  > Año 2/4/6-10
    ( 7, 8, 0),  # Total     /
)
header = {idx: 'S[table-format = {}.{}]'.format(i, d) for idx, i, d in filtros}
sep = '@{ }'

hoja = "Costos de producción"
ini = 3
fin = 19

for name, o in (('a', 0), ('b', 2), ('c', 4)):
    c = Cuadro(
        destino = '{}_{}'.format(BASE, name),
        header = sep + sep.join([header.get(i, 'l') for i in range(1, 8)]) + sep,
        hlinea = 'rule',
    )
    c['_latex_'] = '\\footnotesize'
    c['sisetup'] = 'per-mode = symbol'
    c['sisetup'] = 'table-number-alignment = center'
    c.columnas(conversiones={idx: '.{}f'.format(d) for idx, i, d in filtros})

    rubro = Economicidad.extraer("{}.A{}:A{}".format(hoja, ini, fin))

    a1f = Economicidad.extraer("{0}.{3}{1}:{3}{2}".format(hoja, ini, fin, chr(ord('B')+o*3)))
    a1v = Economicidad.extraer("{0}.{3}{1}:{3}{2}".format(hoja, ini, fin, chr(ord('C')+o*3)))
    a1t = Economicidad.extraer("{0}.{3}{1}:{3}{2}".format(hoja, ini, fin, chr(ord('D')+o*3)))
    a2f = Economicidad.extraer("{0}.{3}{1}:{3}{2}".format(hoja, ini, fin, chr(ord('E')+o*3)))
    a2v = Economicidad.extraer("{0}.{3}{1}:{3}{2}".format(hoja, ini, fin, chr(ord('F')+o*3)))
    a2t = Economicidad.extraer("{0}.{3}{1}:{3}{2}".format(hoja, ini, fin, chr(ord('G')+o*3)))

    r = r"{Rubro}"
    a1 = r"{{Año {}}}".format(1 + o)
    a2 = r"{{Año {}}}".format(2 + o).replace('6', '6 al 10')
    f = r"{Fijos}"
    v = r"{Variables}"
    t = r"{Total}"

    c(r, a1, a2, c='1:2;2-4c;5-7c', hlinea='rule', hlcortes=[4])
    c(f, v, t, f, v, t, hlinea='rule')

    hl = (9, 12, 15)
    dt = enumerate(zip(rubro, a1f, a1v, a1t, a2f, a2v, a2t))
    for idx, row in dt:
        hlinea = 'rule' if idx in hl else None
        c(*row, hlinea=hlinea)

    c.tex()
