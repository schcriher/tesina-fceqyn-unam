filtros = (
    (2, 8, 0),  # Créditos
    (3, 8, 0),  # Aportes propios
    (4, 6, 0),  # Aportes generados
    (5, 8, 0),  # Total
)
header = {idx: 'S[table-format = {}.{}]'.format(i, d) for idx, i, d in filtros}
sep = ''
c = Cuadro(
    destino = BASE,
    header = sep + sep.join([header.get(i, 'l') for i in range(1, 6)]) + sep,
    hlinea = 'rule',
)
c['_latex_'] = '\\footnotesize'
c['sisetup'] = 'per-mode = symbol'
c['sisetup'] = 'table-number-alignment = center'
c.columnas(conversiones={idx: '.{}f'.format(d) for idx, i, d in filtros})

hoja = "Financiamiento"
ini = 2
fin = 15

rubro = Economicidad.extraer("{}.A{}:A{}".format(hoja, ini, fin))
credito = Economicidad.extraer("{}.B{}:B{}".format(hoja, ini, fin))
propios = Economicidad.extraer("{}.C{}:C{}".format(hoja, ini, fin))
autogen = Economicidad.extraer("{}.D{}:D{}".format(hoja, ini, fin))
total = Economicidad.extraer("{}.E{}:E{}".format(hoja, ini, fin))

ru = GT(r"Rubro")
cr = GT(r"Crédito")
ap = GT(r"Aportes\\propios")
aa = GT(r"Aportes\\generados")
to = GT(r"Total")

c(ru, cr, ap, aa, to, hlinea='rule')

hl = (12,)
dt = enumerate(zip(rubro, credito, propios, autogen, total))
for idx, (rubro, credito, propios, autogen, total) in dt:
    hlinea = 'rule' if idx in hl else None
    c(rubro, credito, propios, autogen, total, hlinea=hlinea)

f = r'{{\scriptsize\SI{{{:.1f}}}{{\percent}}}}'.format
r = [f(Economicidad.extraer("{}.{}{}".format(hoja, l, fin + 1)) * 100) for l in 'BCDE']
c('', *r, al_final=True)

c.tex()
