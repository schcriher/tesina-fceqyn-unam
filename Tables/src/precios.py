filtros = (
    (3, 5, 2),  # Precio del Hidrógeno, sin IVA
    (4, 5, 2),  # Precio del Hidrógeno, con IVA
    (5, 4, 2),  # Precio del Oxígeno, sin IVA
    (6, 4, 2),  # Precio del Oxígeno, con IVA
)
header = {idx: 'S[table-format = {}.{}]'.format(i, d) for idx, i, d in filtros}
c = Cuadro(
    destino = BASE,
    header = ''.join([header.get(i, 'c') for i in range(1, 7)]),
    hlinea = 'rule',
)
c['sisetup'] = 'per-mode = symbol'
c['sisetup'] = 'table-number-alignment = center'
c.columnas(conversiones={idx: '.{}f'.format(d) for idx, i, d in filtros})

hoja = "Programa de producción"
ini = 25

pureza = Economicidad.extraer("{}.A{}:A{}".format(hoja, ini, ini + 1))
ventas = Economicidad.extraer("{}.B{}:B{}".format(hoja, ini, ini + 1))
ph = Economicidad.extraer("{}.C{}:C{}".format(hoja, ini, ini + 1))
po = Economicidad.extraer("{}.D{}:D{}".format(hoja, ini, ini + 1))

p = GT(r"Pureza")
v = GT(r"Producción")
h = GT(r"Precio \ch{H2}\\(\si{\peso\per\kg})")
o = GT(r"Precio \ch{O2}\\(\si{\peso\per\kg})")

sin = "{Sin IVA}"
con = "{Con IVA}"

c(p, v, h, o, c='1:2;2:2;3-4;5-6', hlinea='rule', hlcortes=[4])
c(sin, con, sin, con, hlinea='rule')

for pureza, ventas, ph, po in zip(pureza, ventas, ph, po):
    pureza = r'\num{{{:.1f}}}'.format(float(pureza))
    ventas = r'\SI{{{:.0f}}}{{\percent}}'.format(float(ventas) * 100)
    c(pureza, ventas, ph, ph*1.21, po, po*1.21)

c.tex()
