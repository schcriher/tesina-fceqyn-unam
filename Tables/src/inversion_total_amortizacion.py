filtros = (
    (3, 8, 0),  # A Realizar
    (4, 8, 0),  # Inversión Total
    (5, 2, 0),  # Vida Útil
    (6, 7, 0),  # Año 1 al 3
    (7, 7, 0),  # Año 4 al 5
    (8, 7, 0),  # Año 6 al 10
)
header = {idx: 'S[table-format = {}.{}]'.format(i, d) for idx, i, d in filtros}
sep1 = '@{ }'
sep2 = '@{~~}'
c = Cuadro(
    destino = BASE,
    header = sep1 + 'l@{\hspace{-3ex}}c' + sep1 + sep2.join([header.get(i, 'c') for i in range(3, 9)]) + sep1,
    hlinea = 'rule',
)
c['_latex_'] = '\\footnotesize'
c['sisetup'] = 'per-mode = symbol'
c['sisetup'] = 'table-number-alignment = center'
filtros += ((2, 1, 0),)  # Realizadas
c.columnas(conversiones={idx: '.{}f'.format(d) for idx, i, d in filtros})

hoja = "Inversiones"
ini = 3
fin = 26

rubro = Economicidad.extraer("{}.A{}:A{}".format(hoja, ini, fin))
realizadas = Economicidad.extraer("{}.B{}:B{}".format(hoja, ini, fin))
a_realizar = Economicidad.extraer("{}.C{}:C{}".format(hoja, ini, fin))
total = Economicidad.extraer("{}.D{}:D{}".format(hoja, ini, fin))
vida = Economicidad.extraer("{}.E{}:E{}".format(hoja, ini, fin))
periodo1 = Economicidad.extraer("{}.G{}:G{}".format(hoja, ini, fin)) # Año 1 al 3
periodo2 = Economicidad.extraer("{}.H{}:H{}".format(hoja, ini, fin)) # Año 4 al 5
periodo3 = Economicidad.extraer("{}.I{}:I{}".format(hoja, ini, fin)) # Año 6 al 10

ru = GT(r"Rubro")
re = GT(r"Realizadas")
ar = GT(r"A Realizar")
it = GT(r"Inversión\\Total")
vu = GT(r"Vida\\Útil")
aa = GT(r"Amortización Anual")

p1 = r"{1 al 3}"
p2 = r"{4 al 5}"
p3 = r"{6 al 10}"

c(ru, re, ar, it, vu, aa, c='1:2;2:2;3:2;4:2;5:2;6-8c', hlinea='rule')
c(p1, p2, p3, hlinea='rule')

hl = (9, 10, 15, 17, 22)
dt = enumerate(zip(rubro, realizadas, a_realizar, total, vida, periodo1, periodo2, periodo3))
for idx, (rubro, realizadas, a_realizar, total, vida, periodo1, periodo2, periodo3) in dt:
    rubro = rubro.replace("%", "\%")
    rubro = rubro.replace("~", "\\textasciitilde")
    hlinea = 'rule' if idx in hl else None
    c(rubro, realizadas, a_realizar, total, vida, periodo1, periodo2, periodo3, hlinea=hlinea)

c.tex()
