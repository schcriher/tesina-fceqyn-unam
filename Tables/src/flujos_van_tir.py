filtros = (
    ( 1, 2, 0),  # Año
    ( 2, 8, 0),  # Inversión en Activo Fijo
    ( 3, 7, 0),  # Inversión en Activo de Trabajo
    ( 4, 8, 0),  # Impuesto a las Ganancias
    ( 5, 8, 0),  # Total Egresos
    ( 6, 8, 0),  # Utilidad antes del Impuesto a las Ganancias
    ( 7, 7, 0),  # Amortizaciones
    ( 8, 7, 0),  # Interés Financieros
    ( 9, 8, 0),  # Total Ingresos
    (10, 9, 0),  # Diferencia
)
header = {idx: 'S[table-format = {}.{}]'.format(i, d) for idx, i, d in filtros}
sep = '@{~~}'
c = Cuadro(
    destino = BASE,
    header = sep + sep.join([header.get(i, 'c') for i in range(1, 11)]) + sep,
    hlinea = 'rule',
)
c['_latex_'] = '\\footnotesize'
c['sisetup'] = 'per-mode = symbol'
c['sisetup'] = 'table-number-alignment = center'
c.columnas(
    tamaños={idx:'12ex' for idx in range(2, 11)},
    conversiones={idx: '.{}f'.format(d) for idx, i, d in filtros},
)

hoja = "VAN y TIR"
ini = 6
fin = 16

año = Economicidad.extraer("{}.A{}:A{}".format(hoja, ini, fin))
iaf = Economicidad.extraer("{}.B{}:B{}".format(hoja, ini, fin))
iat = Economicidad.extraer("{}.C{}:C{}".format(hoja, ini, fin))
ig = Economicidad.extraer("{}.D{}:D{}".format(hoja, ini, fin))
te = Economicidad.extraer("{}.E{}:E{}".format(hoja, ini, fin))
uai = Economicidad.extraer("{}.F{}:F{}".format(hoja, ini, fin))
amo = Economicidad.extraer("{}.G{}:G{}".format(hoja, ini, fin))
ifs = Economicidad.extraer("{}.H{}:H{}".format(hoja, ini, fin))
ti = Economicidad.extraer("{}.I{}:I{}".format(hoja, ini, fin))
diff = Economicidad.extraer("{}.J{}:J{}".format(hoja, ini, fin))

trema = Economicidad.extraer("{}.C18".format(hoja)) * 100
vs = Economicidad.extraer("{}.C19".format(hoja))
van = Economicidad.extraer("{}.J18".format(hoja))
tir = Economicidad.extraer("{}.J20".format(hoja)) * 100

titles = (
    GT(r'Año'),
    GT(r'Inversión en\\Activo Fijo'),
    r'\scalebox{{0.95}}[1]{{{}}}'.format(GT(r'Inversión en\\Activo de\\Trabajo')),
    GT(r'Impuesto\\a las\\Ganancias'),
    GT(r'Total\\Egresos'),
    r'\scalebox{{0.90}}[1]{{{}}}'.format(GT(r'Utilidad antes\\del Impuesto a\\las Ganancias')),
    r'\scalebox{{0.88}}[1]{{{}}}'.format(GT(r'Amortización')),
    GT(r'Interés\\Financieros'),
    GT(r'Total\\Ingresos'),
    GT(r'Diferencia'),
)
c(*titles, hlinea='rule')
n = len(año) - 1
dt = enumerate(zip(año, iaf, iat, ig, te, uai, amo, ifs, ti, diff))
for idx, row in dt:
    hlinea = 'rule' if idx == n else None
    c(*row, hlinea=hlinea)

percent = '{{\SI{{{:.1f}}}{{\percent}}}}'.format
c('', '{TREMA:}', percent(trema), '', '', '', '', '', '{VAN:}', van)
c('', '{VS:}', vs, '', '', '', '', '', '{TIR:}', percent(tir))

c.tex()
