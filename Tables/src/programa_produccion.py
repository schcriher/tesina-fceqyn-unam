hoja = "Programa de producción"
filtros = (
    (5, 5, 0),  # Producción Anual (kg)
    (6, 4, 0),  # Producción Anual (cilindros)
    (7, 8, 0),  # Ingreso Anual ($)
)
header = {idx: 'S[table-format = {}.{}]'.format(i, d) for idx, i, d in filtros}
c = Cuadro(
    destino = BASE,
    header = '@{~~}'.join([header.get(i, 'c') for i in range(1, 8)]),
    hlinea = 'rule',
)
c['_latex_'] = '\\footnotesize'
c['sisetup'] = 'table-number-alignment = center'
c.columnas(conversiones={idx: '.{}f'.format(d) for idx, i, d in filtros})

periodo = GT(r"Período\\Anual")
uso = GT(r"Uso de la\\Capacidad\\Instalada")
producto = GT(r"Producto")
pur = GT(r"Pureza")
pak = GT(r"Producción\\Anual\\(\si{\kg})")
pac = GT(r"Producción\\Anual\\(\si{\cilindro})")
iap = GT(r"Ingreso\\Anual\\(\si{\peso})")

c(periodo, uso, producto, pur, pak, pac, iap, hlinea='rule')

hlinea = {
    0: None,
    1: '~~----',
    2: None,
    3: 'rule',
}
for index in (2, 6, 10):
    periodo = Economicidad.extraer("{}.A{}".format(hoja, index))
    uso = Economicidad.extraer("{}.B{}".format(hoja, index))
    uso = r'\SI{{{:.0f}}}{{\percent}}'.format(float(uso) * 100)
    producto = Economicidad.extraer("{}.C{},C{}".format(hoja, index, index + 2))
    pur = Economicidad.extraer("{}.D{}:D{}".format(hoja, index, index + 3))
    pak = Economicidad.extraer("{}.E{}:E{}".format(hoja, index, index + 3))
    pac = Economicidad.extraer("{}.F{}:F{}".format(hoja, index, index + 3))
    iap = Economicidad.extraer("{}.G{}:G{}".format(hoja, index, index + 3))
    for i in range(4):
        _pur = pur[i]
        _pak = pak[i]
        _pac = pac[i]
        _iap = iap[i]
        if i == 0:
            row = periodo, uso, producto[0], _pur, _pak, _pac, _iap
            cfg = '1:4:-2.3pt;2:4:-2.3pt;3:2:0.6pt'
        elif i == 2:
            row = producto[1], _pur, _pak, _pac, _iap
            cfg = '3:2:0.6pt'
        else:
            row = _pur, _pak, _pac, _iap
            cfg = ''
        c(*row, c=cfg, hlinea=hlinea[i])

c.tex()
