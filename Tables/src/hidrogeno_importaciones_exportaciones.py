filtros = (
    (4, 0),  # años {un "multirow" dentro de un "S[table-format]" no es posible}
    (4, 0),  # impm
    (5, 0),  # impv
    (4, 0),  # expm
    (5, 0),  # expv
)
c = Cuadro(
    destino = BASE,
    header = ''.join(['c'] + ['S[table-format = {}.{}]'.format(i, d) for i, d in filtros[1:]]),
    hlinea = 'rule',
)
c['sisetup'] = 'table-number-alignment = center'
c.columnas(conversiones = ['.{}f'.format(d) for i, d in filtros])

rows = (
    Mercado.extraer("Resumen.A4:A18", int),
    Mercado.extraer("Resumen.B4:B18"),
    Mercado.extraer("Resumen.C4:C18"),
    Mercado.extraer("Resumen.E4:E18"),
    Mercado.extraer("Resumen.F4:F18"),
)
masa = r'{Masa, \si{\kilogram}}'
valor = r'{Valor, \si{\dolar}}'

c('{Año}', '{Importaciones}', '{Exportaciones}', c='1:2;2-3;4-5', hlinea='rule', hlcortes=[3])
c(masa, valor, masa, valor, hlinea='rule')

for row in zip(*rows):
    c(*row)

c.tex()
