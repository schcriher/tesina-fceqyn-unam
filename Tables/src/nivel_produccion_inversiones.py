db = dbi.get('raw').astype(float)
db.Mh /= 1000   # g → kg (diario)
db.Mo = db.Mh * feo
db = db[db.NP >= 60]
filtros = (
    (1, 2, 2),  # NP
    (2, 2, 0),  # ch
    (3, 2, 1),  # co
    (4, 2, 0),  # Ve
    (5, 4, 0),  # nt
)
header = {idx: 'S[table-format = {}.{}]'.format(i, d) for idx, i, d in filtros}
c = Cuadro(
    destino = BASE,
    header = ''.join([header.get(i, 'c') for i in range(1, 6)]),
    hlinea = 'rule',
)
c['sisetup'] = 'per-mode = symbol'
c['sisetup'] = 'table-number-alignment = center'
c.columnas(conversiones={idx: '.{}f'.format(d) for idx, i, d in filtros})

r1a = GT(r"Producción")
r1b = GT(r"Electrolizador\\\si{\NmcHh}")
r1c = GT(r"Número de\\Paneles")

r2a = GT(r'\si{\percent}')
r2b = GT(r'\si{\cil\Hidrogeno\per\day}')
r2c = GT(r'\si{\cil\Oxigeno\per\day}')

c(r1a, r1b, r1c, c='1-3c;4:2;5:2', hlinea='rule')
c(r2a, r2b, r2c, hlinea='rule')

for row in zip(db.NP, db.ch, db.co, db.Ve, db.nt):
    c(*row)

c.tex()
