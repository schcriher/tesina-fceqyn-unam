hoja = "Datos de orígen"
data = (
    ('materia_prima',               5,  7, 1, 6, 1, 'unidades', 'Materia prima'),
    ('insumos_produccion',         14, 18, 4, 5, 7, 'unidades', 'Insumos de produccion'),
    ('programa_produccion_ventas', 25, 29, 5, 5, 8, '\si{\kg}', 'Producto'),
)
units = (
    ('Kg',  r'\si{\kg}'),
    ('kWh', r'\si{\kWh}'),
    ('Km',  r'\si{\kilo\meter}'),
    ('m³',  r'\si{\meter\cubed}'),
)
for name, ini, fin, c, u, t, unit, title in data:
    filtros = (
        (2, c, 2),  # costos
        (3, u, 0),  # unidades  \  Año 1-2
        (4, t, 0),  # totales   /
        (5, u, 0),  # unidades  \  Año 3-4
        (6, t, 0),  # totales   /
        (7, u, 0),  # unidades  \  Año 5-10
        (8, t, 0),  # totales   /
    )
    header = {idx: 'S[table-format = {}.{}]'.format(i, d) for idx, i, d in filtros}
    sep = ''
    c = Cuadro(
        destino = '{}_{}'.format(BASE, name),
        header = sep + sep.join([header.get(i, 'l') for i in range(1, 9)]) + sep,
        hlinea = 'rule',
        fmb = 2,
    )
    #c['_latex_'] = '\\footnotesize'
    c['sisetup'] = 'per-mode = symbol'
    c['sisetup'] = 'table-number-alignment = center'
    c.columnas(
        tamaños={idx:'10ex' for idx in range(3, 9)},
        conversiones={idx: '.{}f'.format(d) for idx, i, d in filtros},
    )
    it = Economicidad.extraer("{}.A{}:A{}".format(hoja, ini, fin))
    co = Economicidad.extraer("{}.B{}:B{}".format(hoja, ini, fin))
    u1 = Economicidad.extraer("{}.C{}:C{}".format(hoja, ini, fin))
    t1 = Economicidad.extraer("{}.D{}:D{}".format(hoja, ini, fin))
    u2 = Economicidad.extraer("{}.E{}:E{}".format(hoja, ini, fin))
    t2 = Economicidad.extraer("{}.F{}:F{}".format(hoja, ini, fin))
    u3 = Economicidad.extraer("{}.G{}:G{}".format(hoja, ini, fin))
    t3 = Economicidad.extraer("{}.H{}:H{}".format(hoja, ini, fin))

    pp = name == 'programa_produccion_ventas'
    pa = GT(r"Período del año 1 al 2")
    pb = GT(r"Período del año 3 al 4")
    pc = GT(r"Período del año 5 al 10")
    ta = GT(r"Precio\\con IVA") if pp else GT(r"Costo")
    tb = GT(r"Producción\\{}".format(unit)) if pp else GT(r"Consumo\\{}".format(unit))
    tc = GT(r"Ingreso\\Total") if pp else GT(r"Costo\\Total")

    c(title, ta, pa, pb, pc, c='1:2:-8pt;2:2:-8pt;3-4c;5-6c;7-8c', hlinea='rule', hlcortes=[4, 6])
    c(tb, tc, tb, tc, tb, tc, hlinea='rule')

    cut = len(it) - 2
    dt = enumerate(zip(it, co, u1, t1, u2, t2, u3, t3))
    for idx, (it, co, u1, t1, u2, t2, u3, t3) in dt:
        hlinea = 'rule' if idx == cut else None
        for text, code in units:
            it = it.replace(text, code)
        c(it, co, u1, t1, u2, t2, u3, t3, hlinea=hlinea)

    c.tex()
