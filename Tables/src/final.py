Mh = dbf.get_storer('ede').attrs.Mh / 1000
db = dbf.get('raw').astype(float)
db = db[db.D < 11]
db.Mh /= 1000  # g → kg (diario)
varinfo = (# variable, integer, decimal, explicación
    ('Mh', 2, 3, r'Producción diaria de hidrógeno, \si{\kg\Hidrogeno\per\day}'),
    ('NA', 2, 0, r'Número de años autosuficientes de un total 31'),
    ('D',  1, 0, r'Número de días de stock, \si{\day}'),
    ('I',  8, 0, r'Inversión necesaria, \si{\peso}'),
    ('IM', 7, 0, r'Inversión relativa, \si{\peso}/(\si{\kg\Hidrogeno\per\day})'),
    ('A1', 2, 2, r'Amortización vendiendo la energía sobrante, \si{\month}'),
    ('A2', 2, 2, r'Amortización desechando la energía sobrante, \si{\month}'),
)
n = len(varinfo)
var = [v for v, i, d, e in varinfo]
encabezado = ''.join('S[table-format = {}.{}]'.format(i, d) for v, i, d, e in varinfo)
c = Cuadro(
    destino = BASE,
    header = encabezado,
    hlinea = 'rule',
)
c['sisetup'] = 'table-number-alignment = center'
c.columnas(conversiones = ['{}.{}f'.format(i+d+1, d) for v, i, d, e in varinfo])
c(*['{{{}}}'.format(i) for i in var], hlinea='rule')  # títulos

for row in db.iterrows():
    r = row[1][var]
    if r.Mh == Mh:
        elem0 = r'\rowcolor{{rowgray}}{:.{d}f}'.format(r[0], d=varinfo[0][2])
        nums = [elem0] + list(r[1:])
    else:
        nums = list(r)
    c(*nums)

cfg = {
    'c': '1-{}l'.format(n),
    'al_final': True,
    'newline': r'\\[-0.5ex]',
}
c(r'{\scriptsize Referencia:}', **cfg)
for v, i, d, e in varinfo:
    c(r'{{\scriptsize {}: {}}}'.format(v, e), **cfg)
c.tex()
