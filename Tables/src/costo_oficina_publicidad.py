filtros = (
    (2, 7, 0),  # Costo
)
header = {idx: 'S[table-format = {}.{}]'.format(i, d) for idx, i, d in filtros}
sep = ''
c = Cuadro(
    destino = BASE,
    header = sep + sep.join([header.get(i, 'l') for i in range(1, 3)]) + sep,
    hlinea = 'rule',
)
c['sisetup'] = 'per-mode = symbol'
c['sisetup'] = 'table-number-alignment = center'
c.columnas(conversiones={idx: '.{}f'.format(d) for idx, i, d in filtros})

hoja = "Presup costos admin y ventas"
ini = 12
fin = 16

item = Economicidad.extraer("{}.A{}:A{}".format(hoja, ini, fin))
costo = Economicidad.extraer("{}.B{}:B{}".format(hoja, ini, fin))

c(GT(r"Rubro"), GT(r"Costo"), hlinea='rule')

cut = len(item) - 2
dt = enumerate(zip(item, costo))
for idx, row in dt:
    hlinea = 'rule' if idx == cut else None
    c(*row, hlinea=hlinea)

c.tex()
