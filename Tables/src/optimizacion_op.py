db = dbo.get('op').astype(float)
db.Mh /= 1000  # g → kg
varinfo = (# variable, integer, decimal, explicación
    ('Id', 1, 0, r'Identificador del lote, archivo \texttt{Variables.py}, variable \texttt{Lotes}'),
    ('Mh', 2, 3, r'Producción diaria de hidrógeno, \si{\kg\Hidrogeno\per\day}'),
    ('Ve', 2, 0, r'Capacidad del electrolizador, \si{\NmcHh}'),
    ('N',  4, 0, r'Número de paneles fotovoltaicos'),
    ('D',  1, 0, r'Número de días de stock, \si{\day}'),
    ('IM', 7, 0, r'Inversión relativa, \si{\peso}/(\si{\kg\Hidrogeno\per\day})'),
    ('I',  8, 0, r'Inversión necesaria, \si{\peso}'),
    ('A',  2, 0, r'Amortización, \si{\month}'),
)
n = len(varinfo)
var = [v for v, i, d, e in varinfo]
c = Cuadro(
    destino = BASE,
    header = ''.join('S[table-format = {}.{}]'.format(i, d) for v, i, d, e in varinfo),
    hlinea = 'rule',
)
c['sisetup'] = 'table-number-alignment = center'
c.columnas(conversiones = ['{}.{}f'.format(i+d+1, d) for v, i, d, e in varinfo])
c(*['{{{}}}'.format(i) for i in var], hlinea='rule')  # títulos

for row in db.iterrows():
    c(*row[1][var])

cfg = {
    'c': '1-{}l'.format(n),
    'al_final': True,
    'newline': r'\\[-0.5ex]',
}
c(r'{\scriptsize Referencia:}', **cfg)
for v, i, d, e in varinfo:
    c(r'{{\scriptsize {}: {}}}'.format(v, e), **cfg)
c.tex()
