db = dbd.get('inta/P')  # Precipitación, mm
varinfo = (
    (4, 0, 'Año'),
    (4, 1, 'Precipitación, \si{\milli\meter}'),
)
c = Cuadro(
    destino = BASE,
    header = ''.join('S[table-format = {}.{}]'.format(i, d) for i, d, n in varinfo),
    hlinea = 'rule',
)
c['sisetup'] = 'table-number-alignment = center'
c.columnas(conversiones = ['{}.{}f'.format(i+d+(1 if d else 0), d) for i, d, n in varinfo])
c(*['{{{}}}'.format(n) for i, d, n in varinfo], hlinea='rule')  # títulos
N = 0
P = 0
for y in range(2005, 2011):
    p = db[str(y)].sum()
    c(y, p)
    N += 1
    P += p
c(r'\scriptsize Precipitación promedio \SI{{{:.1f}}}{{\milli\meter}}'.format(P/N), c='1-{}l'.format(len(varinfo)), al_final=True)
c.tex()
