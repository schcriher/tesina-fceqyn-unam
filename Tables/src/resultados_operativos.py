filtros = (
    (2, 8, 0),  # 1
    (3, 8, 0),  # 2
    (4, 8, 0),  # 3
    (5, 8, 0),  # 4
    (6, 8, 0),  # 5
    (7, 8, 0),  # 6-10
)
header = {idx: 'S[table-format = {}.{}]'.format(i, d) for idx, i, d in filtros}
sep = '@{ }'
c = Cuadro(
    destino = BASE,
    header = sep + sep.join([header.get(i, 'l') for i in range(1, 8)]) + sep,
    hlinea = 'rule',
)
c['_latex_'] = '\\footnotesize'
c['sisetup'] = 'per-mode = symbol'
c['sisetup'] = 'table-number-alignment = center'
c.columnas(conversiones={idx: '.{}f'.format(d) for idx, i, d in filtros})

hoja = "Cuadro de resultados"
ini = 2
fin = 12

it = Economicidad.extraer("{}.A{}:A{}".format(hoja, ini, fin))
a1 = Economicidad.extraer("{}.B{}:B{}".format(hoja, ini, fin))
a2 = Economicidad.extraer("{}.C{}:C{}".format(hoja, ini, fin))
a3 = Economicidad.extraer("{}.D{}:D{}".format(hoja, ini, fin))
a4 = Economicidad.extraer("{}.E{}:E{}".format(hoja, ini, fin))
a5 = Economicidad.extraer("{}.F{}:F{}".format(hoja, ini, fin))
a6 = Economicidad.extraer("{}.G{}:G{}".format(hoja, ini, fin))

c('{Ejercicio}', '{Año 1}', '{Año 2}', '{Año 3}', '{Año 4}', '{Año 5}', '{Año 6 al 10}', hlinea='rule')

hl = (2, 3, 6, 7, 9)
dt = enumerate(zip(it, a1, a2, a3, a4, a5, a6))
for idx, (it, a1, a2, a3, a4, a5, a6) in dt:
    hlinea = 'rule' if idx in hl else None
    if it.startswith('.') or it.startswith(' '):
        it = r'~~~{}'.format(it.strip('.').strip().strip('.').strip())
    c(it, a1, a2, a3, a4, a5, a6, hlinea=hlinea)

c.tex()
