filtros = (
    (4, 0),  # años {un "multirow" dentro de un "S[table-format]" no es posible}
    (7, 0),  # impm
    (6, 0),  # impv
    (7, 0),  # expm
    (6, 0),  # expv
)
c = Cuadro(
    destino = BASE,
    header = ''.join(['c'] + ['S[table-format = {}.{}]'.format(i, d) for i, d in filtros[1:]]),
    hlinea = 'rule',
)
c['sisetup'] = 'table-number-alignment = center'
c.columnas(conversiones = ['.{}f'.format(d) for i, d in filtros])

rows = (
    Mercado.extraer("Resumen.A20:A34", int),
    Mercado.extraer("Resumen.B20:B34"),
    Mercado.extraer("Resumen.C20:C34"),
    Mercado.extraer("Resumen.E20:E34"),
    Mercado.extraer("Resumen.F20:F34"),
)
masa = r'{Masa, \si{\kilogram}}'
valor = r'{Valor, \si{\dolar}}'

c('{Año}', '{Importaciones}', '{Exportaciones}', c='1:2;2-3;4-5', hlinea='rule', hlcortes=[3])
c(masa, valor, masa, valor, hlinea='rule')

for row in zip(*rows):
    c(*row)

c.tex()
