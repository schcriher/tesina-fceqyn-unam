filtros = (
    (1, 1, 0),  # año
    (2, 2, 0),  # semestre
    (3, 8, 0),  # deuda
    (4, 7, 0),  # Pago semestral: pago de capital
    (5, 7, 0),  # Pago semestral: interés
    (6, 7, 0),  # Pago semestral: cuota
    (7, 7, 0),  # Insidencia anual: pago de capital
    (8, 7, 0),  # Insidencia anual: interés
    (9, 7, 0),  # Insidencia anual: cuota
)
header = {idx: 'S[table-format = {}.{}]'.format(i, d) for idx, i, d in filtros}
sep = ''
c = Cuadro(
    destino = BASE,
    header = sep + sep.join([header.get(i, 'c') for i in range(1, 10)]) + sep,
    hlinea = 'rule',
)
c['_latex_'] = '\\footnotesize'
c['sisetup'] = 'per-mode = symbol'
c['sisetup'] = 'table-number-alignment = center'
c.columnas(conversiones={idx: '.{}f'.format(d) for idx, i, d in filtros})

hoja = "Financiamiento"
ini = 22
fin = 32

año = Economicidad.extraer("{}.G{}:G{}".format(hoja, ini, fin))
semestre = Economicidad.extraer("{}.H{}:H{}".format(hoja, ini, fin))
deuda = Economicidad.extraer("{}.I{}:I{}".format(hoja, ini, fin))
psp = Economicidad.extraer("{}.J{}:J{}".format(hoja, ini, fin)) # Pago semestral: pago de capital
psi = Economicidad.extraer("{}.K{}:K{}".format(hoja, ini, fin)) # Pago semestral: interés
psc = Economicidad.extraer("{}.L{}:L{}".format(hoja, ini, fin)) # Pago semestral: cuota
iap = Economicidad.extraer("{}.M{}:M{}".format(hoja, ini, fin)) # Insidencia anual: pago de capital
iai = Economicidad.extraer("{}.N{}:N{}".format(hoja, ini, fin)) # Insidencia anual: interés
iac = Economicidad.extraer("{}.O{}:O{}".format(hoja, ini, fin)) # Insidencia anual: cuota

añ = GT(r"Año")
se = GT(r"Semestre")
de = GT(r"Deuda")
ps = GT(r"Pago Semestral")
ia = GT(r"Insidencia Anual")

p1 = r"{Capital}"
p2 = r"{Interés}"
p3 = r"{Cuota}"

c(añ, se, de, ps, ia, c='1:2;2:2;3:2;4-6c;7-9c', hlinea='rule', hlcortes=[6])
c(p1, p2, p3, p1, p2, p3, hlinea='rule')

dt = enumerate(zip(año, semestre, deuda, psp, psi, psc, iap, iai, iac))
for idx, (año, semestre, deuda, psp, psi, psc, iap, iai, iac) in dt:
    if idx == 0:
        row = año, '', deuda, '', psi, '', '', iai, ''
        cfg = {'hlinea': 'rule'}
    elif idx % 2:
        row = año, semestre, deuda, psp, psi, psc, iap, iai, iac
        cfg = {'c': '1:2;7:2;8:2;9:2'}
    else:
        row = semestre, deuda, psp, psi, psc
        cfg = {'hlinea': 'rule'}
    c(*row, **cfg)

c.tex()
