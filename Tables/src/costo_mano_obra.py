filtros = (
    (2, 1, 0),  # Plazas/turno
    (3, 5, 0),  # Sueldo mensual
    (4, 6, 0),  # Sueldo anual
    (5, 7, 0),  # Sueldo total
)
header = {idx: 'S[table-format = {}.{}]'.format(i, d) for idx, i, d in filtros}
sep = ''
data = (
    ('directa',         2,  6, (1, 3), 'Mano de obra'),
    ('indirecta',      10, 16, (3, 5), 'Mano de obra'),
    ('administrativa',  2,  9, (4, 6), 'Presup costos admin y ventas'),
)
for name, ini, fin, hl, hoja in data:
    c = Cuadro(
        destino = '{}_{}'.format(BASE, name),
        header = sep + sep.join([header.get(i, 'l') for i in range(1, 6)]) + sep,
        hlinea = 'rule',
    )
    c['sisetup'] = 'per-mode = symbol'
    c['sisetup'] = 'table-number-alignment = center'
    c.columnas(
        tamaños={idx:'8ex' for idx in range(2, 6)},
        conversiones={idx: '.{}f'.format(d) for idx, i, d in filtros},
    )
    plaza = Economicidad.extraer("{}.A{}:A{}".format(hoja, ini, fin))
    turno = Economicidad.extraer("{}.B{}:B{}".format(hoja, ini, fin))
    sueldom = Economicidad.extraer("{}.C{}:C{}".format(hoja, ini, fin))
    sueldoa = Economicidad.extraer("{}.D{}:D{}".format(hoja, ini, fin))
    sueldot = Economicidad.extraer("{}.E{}:E{}".format(hoja, ini, fin))

    pl = GT(r"Plaza")
    np = GT(r"Plazas")
    sm = GT(r"Sueldo\\Mensual")
    sa = GT(r"Sueldo\\Anual")
    st = GT(r"Sueldo\\Total")

    c(pl, np, sm, sa, st, hlinea='rule')

    dt = enumerate(zip(plaza, turno, sueldom, sueldoa, sueldot))
    for idx, (plaza, turno, sueldom, sueldoa, sueldot) in dt:
        plaza = plaza.replace("%", "\%")
        hlinea = 'rule' if idx in hl else None
        c(plaza, turno, sueldom, sueldoa, sueldot, hlinea=hlinea)

    c.tex()
