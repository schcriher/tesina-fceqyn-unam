c = Cuadro(
    destino = BASE,
    header = 'ccc',
    hlinea = 'rule',
)

c.columnas(['mRepublica Checam'] + ['mmPlaneadasmm'] * 2)

c('País', 'Estaciones de servicio de \ch{H2}', c='1:2;2-3')
c('Activas', 'Planeadas', hlinea='rule')

df_t = dbd.get('pnl/estaciones/totales')
df_e = dbd.get('pnl/estaciones/electroliticas')
sets = df_t.index, df_e.active, df_e.planned, df_t.active, df_t.planned
data = '{}{:.0f} de {:.0f}{}'

def diff(e, t):
    d = len(str(e)) - len(str(t))
    if d:
        ph = '\phantom{{{}}}'.format('0' * abs(d))
        return ('', ph) if d > 0 else (ph, '')
    else:
        return ('', '')

datos = []
for pais, ea, ep, ta, tp in zip(*sets):
    if pais in paises_en_es:
        pais = paises_en_es.pop(pais)
    else:
        sys.stderr.write('  \033[41m\033[1;37m* No hay traducción para: {} \033[0m\n'.format(pais))

    d1_a, d1_b = diff(ea, ta)
    d2_a, d2_b = diff(ep, tp)
    d1 = data.format(d1_a, ea, ta, d1_b) if ta else '0'
    d2 = data.format(d2_a, ep, tp, d2_b) if tp else '0'

    datos.append((pais, d1, d2))

for pais, d1, d2 in sorted(datos):
    c(pais, d1, d2)

c(r'\scriptsize Nomenclatura: estaciones con procesos electrolíticos y totales', c='1-3', al_final=True)

c.tex()
