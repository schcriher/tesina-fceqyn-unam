#!/usr/bin/env python3
#-*- coding: utf-8 -*-

import datetime
INICIO = datetime.datetime.now()

import re
import sys

from os import getcwd, makedirs, chdir, remove, rename, listdir
from os.path import splitext, abspath, join, isdir, isfile, basename, dirname, getmtime
from tempfile import mkstemp
from argparse import ArgumentParser
from subprocess import PIPE, Popen

NAME = basename(__file__)
ROOT = dirname(abspath(__file__))
THSS = 'Tesis'
WORK = 'Build'
TABLES = 'Tables'
FIGURES = 'Figures'
CHAPTERS = 'Chapters'
APPENDICES = join(CHAPTERS, 'Appendices')

BACKUP = 'Backups'
THESIS = '{}.tex'.format(THSS)

RUNLOG = join(ROOT, WORK, 'RUN.log')

TEXLOG = join(WORK, '{}.log'.format(THSS))  # pdflatex
BIBLOG = join(WORK, '{}.blg'.format(THSS))  # bibliography
ACRLOG = join(WORK, '{}.alg'.format(THSS))  # acronyms
GLOLOG = join(WORK, '{}.glg'.format(THSS))  # glossary
CMDOUT = join(WORK, '{}.out'.format(THSS))  # «general»

LOG = "{{}} > '{}'".format(CMDOUT).format

PRST = 'Presentacion'
PRST_TEXLOG = join(WORK, '{}.log'.format(PRST))
PRST_CMDOUT = join(WORK, '{}.out'.format(PRST))
PRST_LOG = "{{}} > '{}'".format(CMDOUT).format

ENG = 'Engineering'
sys.path.insert(0, ENG)


class ToSystemException(Exception):
    pass

class ExecException(Exception):
    pass


GUION = '\033[35m-\033[0m'
FLECHA = '\033[35m→\033[0m'


def print_action(text):
    print(" \033[33m{}\033[0m".format(text))

def print_action_error(text):
    print(" \033[41m\033[1;37m ERROR {} \033[0m".format(text))

def print_data(text):
    print("  {}".format(text))

def print_info(text):
    print("  \033[32m{}\033[0m".format(text))

def print_error(text):
    print("  \033[31m{}\033[0m".format(text))

def print_advertencia(text):
    print("  \033[36m{}\033[0m".format(text))


COLOR_BASH_RE = re.compile('\033\[([01]|(1;)?[34][0-7])m')

def safe_error(err):
    if "Unescaped left brace in regex is deprecated" in err:
        return ""
    return err

def tosystem(cmd):
    save(RUNLOG, "\n\n{}\n{}".format(getcwd(), cmd.replace("\n", " ")), mode='a')
    if '"' in cmd:
        run = "bash -c '{}' ".format(cmd.replace("'", "\\'"))
    else:
        run = 'bash -c "{}" '.format(cmd.replace('"', '\\"'))
    stdout, stderr = Popen(run, stderr=PIPE, shell=True).communicate()
    err = safe_error(stderr.decode().strip())
    if err:
        cwd = 'cd {}'.format(getcwd().replace('/home/cristian/', '~/'))
        m = "\n\033[1;36m{}\n{}\n\033[41m\033[1;37m ERROR: {} \033[0m\n"
        print(m.format(cwd, COLOR_BASH_RE.sub('', cmd).replace('\n', '\\n'), err))
        raise ToSystemException()


def clear(tex=False, pyc=False):
    if tex:
        pdf = '{}.pdf'.format(THSS)
        if isfile(pdf):
            tosystem("echo '  ./{0}'; rm -f '{0}'".format(pdf))
        tosystem("find './{}' -type f -printf '  %p\n' -delete".format(WORK))
        for d, t in ((TABLES, 'tex'), (FIGURES, 'tex'), (FIGURES, 'pdf')):
            tosystem("find './{}/{}' -type f -printf '  %p\n' -delete".format(d, t))
    delete = "find . -type f -name '{}' -printf '  %p\n' -delete".format
    if pyc:
        tosystem(delete('*.pyc'))
    tosystem(delete('*~'))


def save(filename, content, mode='w'):
    assert mode in 'wa', 'mode must be "w" or "a", not {}'.format(mode)
    with open(filename, encoding='utf-8', mode=mode) as f:
        f.write(content)


def load(filename):
    assert isfile(filename), 'No existe {}'.format(filename)
    with open(filename, encoding='utf-8', mode='r') as f:
        content = f.read()
    return content


def set_build_dir():
    makedirs(join(WORK, APPENDICES), exist_ok=True)
    makedirs(join(WORK, FIGURES, 'pdf'), exist_ok=True)


def texlister(typs=None):
    set_build_dir()
    tosystem("Tools/texlister '{}' '{}'".format(THESIS, WORK))
    if typs:
        lst = []
        for typ in typs:
            f = join(WORK, '{}.lst{}'.format(THSS, typ))
            if isfile(f):
                lst.extend(load(f).strip('\n').split('\n'))
        return lst


def basic_clear():
    print_action('Borrando archivos temporales')
    clear()


def gettime(file):
    return round(getmtime(file) * 1e3)


def close_without_except(db):
    try:
        db.close()
    except:
        pass


# ---------------------------------------------------------------------------- #


def gen_grep(typ, prst=False):
    files = [CMDOUT]
    exclude = [
        'Font Warning. Font shape .LGR/lmtt/bx/n. undefined',
        'Font Warning. Some font shapes were not available. defaults substituted',
        'file:line:error style messages enabled',
        'tex/generic/pgfplots/pgfplots\.errorbars\.code\.tex',
        'infwarerr ..../../.. v... Providing info/warning/error messages',
        'glossaries Warning: No language module detected for .(latin|.*greek).',
        'Package biblatex Warning: Language .latin. not supported',
        'scrlayer-scrpage Info: deactivating warning for font element',
        'Package glossaries Warning: No .printglossary or .printglossaries found.',
        'chemmacros.module.errorchec',
        'errorcheck',
        ' 0 warnings',
    ]
    if typ == "pdflatex":
        files.append(PRST_TEXLOG if prst else TEXLOG)
    elif typ == "biber":
        files.append(BIBLOG)
    elif typ == "makeglossaries":
        files.append(ACRLOG)
        files.append(GLOLOG)
    else:
        files = [typ]
        latex_warning = (
            'Citation',
            'Reference',
            'undefined references',
            'Empty bibliography',
            'Float too large for page',
        )
        exclude.extend([
            'LaTeX Warning.+({})'.format('|'.join(latex_warning)),
            'longtable Warning: Table widths have changed. Rerun LaTeX',
            'biblatex Warning: Please .re.run Biber on the file',
            'longtable Warning: Column widths have changed',
        ])
    filelist = ' '.join("'{}'".format(f) for f in files)
    inc = "'(Fatal error occurred|Undefined control sequence|warning|error)'"
    exc = "'({})'".format('|'.join(exclude))
    grep = "grep {} --ignore-case --extended-regexp --regexp={} {}"
    cases = (
        ('--invert-match --line-number', exc,           filelist),
        ('',                             inc, "| sed 's/^/   /'"),
        ('--color',                      inc,                 ''),
    )
    return " | ".join(grep.format(*i) for i in cases)


# ---------------------------------------------------------------------------- #


def busca_errores_tipeo(texs):
    patterns = (
        # OS
        re.compile(r'Figuras/', re.IGNORECASE).finditer,
        re.compile(r'Cuadros/', re.IGNORECASE).finditer,
        # TeX
        re.compile(r'figuras?\s*?\\ref\{',   re.IGNORECASE).finditer,
        re.compile(r'cuadros?\s*?\\ref\{',   re.IGNORECASE).finditer,
        re.compile(r'[^\s~]\\cite\{',        re.IGNORECASE).finditer,
        re.compile(r'tablas?[~\s]*?\\ref\{', re.IGNORECASE).finditer,
        # tikz
        re.compile(r"""at\s*=\s*\{\s*\(\s*(0.04|0.96),\s*[0-9.]+\s*\)\s*\}""", re.IGNORECASE).finditer,
        re.compile(r"""at\s*=\s*\{\s*\(\s*[0-9.]+,\s*(0.03|0.97)\s*\)\s*\}""", re.IGNORECASE).finditer,
    )
    for tex in texs:
        with open(tex, encoding='utf-8', mode='r') as fid:
            fisrt = True
            for i, line in enumerate(fid, 1):
                errs = []
                for pattern in patterns:
                    for e in pattern(line):
                        errs.append(e.span())
                if errs:
                    if fisrt:
                        print_info(tex)
                        fisrt = False
                    ini = 0
                    mjs = ' {:4.0f}\033[0m  '.format(i)
                    for i, f in sorted(errs):
                        mjs += '{}\033[41m\033[1;37m{}\033[0m'.format(line[ini:i], line[i:f])
                        ini = f
                    mjs += line[ini:].rstrip('\n')
                    print_info(mjs)


# ---------------------------------------------------------------------------- #


def py2tex():
    from pygments import highlight
    from pygments.lexers import Python3Lexer
    from pygments.formatters import LatexFormatter

    from Library.pygments_style_algorithm import AlgorithmStyle

    from Library.Utils import elimina_caracteres_diacriticos

    header = '\chapter{{Archivo: \\texttt{{{}}}}}\label{{py:{}}}\n{}'
    style = join('Settings', 'Python.tex')
    path = join('Chapters', 'Appendices')
    files = (
        "Engineering/Library/Sol.py",
        "Engineering/Library/Panel.py",
        "Engineering/Variables.py",
       #"Engineering/Ángulos.py",
       #"Engineering/Separaciones.py",
        "Engineering/Potencias.py",
        "Engineering/Modelo.py",
        "Engineering/Optimización.py",
        "Engineering/Inversiones.py",
        "Engineering/Sombras.py",
        "Engineering/Final.py",
        "Engineering/Evolución.py",
        "Engineering/GSA.py",
        "Engineering/LAS.py",
       #"Engineering/Video.py",
    )

    lexer = Python3Lexer()
    fancyvrb = ('frame=single,rulecolor=\\color{gray},framesep=6pt,'
        'numbersep=3pt,fontsize=\scriptsize')
    #formatter = LatexFormatter(style='vs', linenos=True, verboptions=fancyvrb)
    formatter = LatexFormatter(style=AlgorithmStyle, linenos=True, verboptions=fancyvrb)

    for py in files:
        name = basename(py)
        base = elimina_caracteres_diacriticos(splitext(name)[0])
        tex = join(path, 'py{}.tex'.format(base))

        assert isfile(py), "No existe el archivo '{}'".format(py)

        code = load(py)
        code = highlight(code, lexer, formatter)
        save(tex, header.format(name, base.lower(), code))

        o = max(len(f) for f in files)
        print_data('{:{o}} {} {}'.format(py, FLECHA, tex, o=o))

    save(style, formatter.get_style_defs())


# ---------------------------------------------------------------------------- #


def compuestos(texs):
    from functools import partial

    compound = re.compile(r'''([A-Z](?:\^\\T+(?:$|\s+|\{\})|[\w^{}#()'"+=-])*)''').findall
    comments = partial(re.compile(r'^(.*?)%.*$', re.MULTILINE).sub, r'\1')
    letters = partial(re.compile('[^a-z]', re.IGNORECASE).sub, '')

    def command(cmd, code):
        tokens = (
            ('COMMAND', r'\\{}\{{'.format(cmd)),
            ('OPEN',    r'\{'),
            ('CLOSE',   r'\}'),
            ('NEWLINE',  '\n'),  # ¿por qué debe estar si o si?
            ('OTHER',   r'.' ),
        )
        pattern = '|'.join('(?P<{}>{})'.format(*t) for t in tokens)
        get_token = re.compile(pattern).match
        start = None
        key = 0
        obj = get_token(code)
        while obj is not None:
            typ = obj.lastgroup
            end = obj.end()
            if typ == 'COMMAND':
                start = end
                key += 1
            elif typ == 'OPEN' and start is not None:
                key += 1
            elif typ == 'CLOSE' and start is not None:
                key -= 1
                if key == 0:
                    c = code[start:end-1]
                    yield c
                    start = None
            obj = get_token(code, end)

    database = 'Chemical.tex'
    need = '$NEED NAME$'
    try:
        code = load(database)
        chapter = list(command('addchap\*', code))[0]
        formulas = command('ch', code)
        names = command('textnormal', code)
        db = {}
        for f, n in zip(formulas, names):
            if n != need:
                db[f.strip().strip('{}')] = n
        IGNORAR = set(command('IGNORECOMPOUND', code))
    except:
        db = {}
        IGNORAR = set()
        chapter = 'Nomenclatura química'

    SOBRAN = set(db)
    AGREGAR = set()
    FALTAN = set()
    for tex in texs:
        if tex != database:
            with open(tex, encoding='utf-8', mode='r') as f:
                for line in f:
                    for e in command('ch', comments(line)):
                        for c in compound(e):
                            c = c.strip().strip('{}')
                            if c not in IGNORAR:
                                AGREGAR.add(c)
                            if c in SOBRAN:
                                SOBRAN.remove(c)

    ch = '\\textbf{{\\ch{{{}}}}}'
    tn = '\\textnormal{{{}}}'
    d1 = max(len(c) for c in list(AGREGAR) + list(SOBRAN)) + len(ch) - ch.count('{') - 1
    d2 = max(len(n) for n in db.values()) + len(tn) - tn.count('{') - 1
    row = '{{:{}}} & {{:{}}} \\strut\\\\\n'.format(d1, d2).format
    chap = '\\addchap*{{{0}}}\\addcontentsline{{toc}}{{chapter}}{{{0}}}\n\n'.format
    ignore = '\\IGNORECOMPOUND{{{}}}\n'.format

    with open(database, encoding='utf-8', mode='w') as f:
        f.write(chap(chapter))

        if AGREGAR:
            f.write('\\begin{longtabu}[l]{@{}cl@{}}\n')
            for c in sorted(AGREGAR, key=letters):
                if c not in db:
                    db[c] = need
                    FALTAN.add(c)
                f.write('  ' + row(ch.format(c), tn.format(db[c])))
            f.write('\\end{longtabu}\n\n')

        if SOBRAN:
            for c in sorted(SOBRAN, key=letters):
                if c not in db:
                    db[c] = need
                    FALTAN.add(c)
                f.write('% ' + row(ch.format(c), tn.format(db[c])))
            f.write('\n')

        if IGNORAR:
            for c in sorted(IGNORAR, key=letters):
                f.write('% ' + ignore(c))
            f.write('\n')

    a, b = ('\033[41m\033[1;37m ', ' \033[0m') if FALTAN else ('', '')
    var = len(AGREGAR), len(SOBRAN), len(IGNORAR), a, len(FALTAN), b
    print_data('Agregado={}, sobran={}, ignorados={}, {}faltan={}{}'.format(*var))


# ---------------------------------------------------------------------------- #


def script(G, files):
    for src, base in files:
        code = load(src)
        date = datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S")
        gvar = G.copy()
        gvar.update({
            'BASE': base,
            'DATE': date,
        })
        if FIGURES in src:
            ftex, fname = G['loc'](base, FIGURES.lower(), tfile=False, cparent=True)
            header = '% {}\n\\tikzsetnextfilename{{{}}}'.format(date, fname)
            gvar.update({
                'FTEX': ftex,
                'HEADER': header,
            })
        print_data('{}'.format(src))
        try:
            exec(code, gvar)
        except:
            import traceback
            n = '\n'
            s = ' ' * 3
            tb = traceback.format_exc().strip().split(n)[3:]
            e = '{0}Traceback:\n{0}{1}'.format(s, (n + s).join(tb))
            print('\033[41m\033[1;37m{}\033[0m\n'.format(e))
            tosystem("touch -m '{}'".format(src))
            raise ExecException()


def emptyfolder(path):
    for name in listdir(path):
        remove(join(path, name))


def filefinder(path, base, ext):
    files = []
    times = []
    for name in sorted(listdir(path)):
        if name.startswith(base) and name.endswith(ext):
            f = join(path, name)
            files.append(f)
            times.append(gettime(f))
    return files, sorted(times)[0] if times else 0


def scripts_makes(tabs, figs, nobuild=False, full=False):
    import sys
    import math
    import numpy
    import scipy
    import pandas
    import datetime
    import itertools
    import functools

    fdbd = join(ENG, 'Data.h5')
    fdba = join(ENG, 'Ángulos.h5')
    fdbs = join(ENG, 'Separaciones.h5')
    fdbp = join(ENG, 'Potencias.h5')
    fdbo = join(ENG, 'Optimización.h5')
    fdbf = join(ENG, 'Final.h5')
    fdbe = join(ENG, 'Evolución.h5')
    fdbi = join(ENG, 'Inversiones.h5')

    dbd = pandas.HDFStore(fdbd, mode='r') if isfile(fdbd) else None
    dba = pandas.HDFStore(fdba, mode='r') if isfile(fdba) else None
    dbs = pandas.HDFStore(fdbs, mode='r') if isfile(fdbs) else None
    dbp = pandas.HDFStore(fdbp, mode='r') if isfile(fdbp) else None
    dbo = pandas.HDFStore(fdbo, mode='r') if isfile(fdbo) else None
    dbf = pandas.HDFStore(fdbf, mode='r') if isfile(fdbf) else None
    dbe = pandas.HDFStore(fdbe, mode='r') if isfile(fdbe) else None
    dbi = pandas.HDFStore(fdbi, mode='r') if isfile(fdbi) else None

    from Library import loc, XLS, xls, Tabular, Cuadro, Figura, histograma, paises_en_es
    from Library.Sol import Sol
    from Library.Utils import recta, printpns
    from Library.Panel import Paneles

    cwd = getcwd()
    chdir(join(cwd, "Engineering"))
    exec(open('Modelo.py').read())  # incluye 'Variables.py'
    chdir(cwd)

    Mercado = XLS('Mercado')
    Economicidad = XLS('Economicidad')
    SAVE = save
    GT = lambda code: '{{{}}}'.format(Tabular(code, cols='@{}c@{}'))

    def tick(var, n=1):
        sep = ',' * int(n) + ' '
        return '{{{}}}'.format(sep.join(map(str, var)))
    #tick = lambda i: '{{{}}}'.format(', '.join(map(str, i)))
    tick_dayofyear_12 = tick('{:.2f}'.format(i * 365 / 12) for i in range(13))
    ticklabels_month = tick(str(i) for i in range(1, 13))

    globalvars = locals()
    try:
        if tabs[0]:
            script(globalvars, tabs[0])

            srcbases = set(n[:-3] for n in listdir(tabs[1]) if n.endswith('.py'))
            texbases = set(n[:-4] for n in listdir(tabs[2]) if n.endswith('.tex'))

            # tex que no tiene su py es obsoleto
            for name in texbases:
                if not any(n in name for n in srcbases):
                    remove(join(tabs[2], name + '.tex'))

        if figs[0]:
            script(globalvars, figs[0])

            srcbases = set(n[:-3] for n in listdir(figs[1]) if n.endswith('.py'))
            texbases = set(n[:-4] for n in listdir(figs[2]) if n.endswith('.tex'))
            pdfbases = set(n[:-4] for n in listdir(figs[3]) if n.endswith('.pdf'))

            # tex que no tiene su py es obsoleto
            for name in texbases.copy():
                if not any(n in name for n in srcbases):
                    remove(join(figs[2], name + '.tex'))
                    texbases.remove(name)

            # pdf que no tiene su tex es obsoleto
            for name in pdfbases.difference(texbases):
                remove(join(figs[3], name + '.pdf'))
                pdfbases.remove(name)

            bases = pyfiles(texlister('b'), ret='base', dic=True).get(FIGURES, [])
            names = sorted(n for n in texbases.difference(pdfbases) if n in bases)
            if names and not nobuild:
                print_action('Creando figuras (método {})'.format('lento' if full else 'rápido'))
                make = (
                    "pdflatex "
                    "-halt-on-error "
                    "-interaction=batchmode "
                    "-output-directory '{0}' "
                    "-jobname '{1}/pdf/{2}' "
                    "'\\def\\tikzexternalrealjob{{{3}}}\\input{{{3}}}'"
                )
                if full:
                    work = THSS
                else:
                    main = (
                        "\\newif\\ifmain\n"
                        "\\maintrue\n"
                        "\\input{{Settings/Preamble}}\n"
                        "\\begin{{document}}\n"
                        "\\input{{{}/tex/{}}}\n"
                        "\\end{{document}}\n"
                    )
                    work = '{}_work'.format(THSS)
                    wtex = '{}.tex'.format(work)
                n = len(names)
                d = len(str(n))
                for i, name in enumerate(names, 1):
                    pdf = '{}/pdf/{}.pdf'.format(FIGURES, name)
                    log = '{}/{}/pdf/{}.log'.format(WORK, FIGURES, name)
                    print_data('{:{d}}/{} {} {}'.format(i, n, GUION, pdf, d=d))
                    try:
                        if not full:
                            save(wtex, main.format(FIGURES, name))
                        tosystem(LOG(make.format(WORK, FIGURES, name, work)))
                        tosystem("mv '{0}/{1}' '{1}'".format(WORK, pdf))
                        tosystem(gen_grep(log))
                    except ToSystemException:
                        if isfile(pdf):
                            remove(pdf)
                        sys.exit(1)
                if not full:
                    remove(wtex)

    except (SystemExit, ExecException):
        # Ya se imprimio el error en pantalla
        sys.exit(1)

    except:
        import traceback
        print(traceback.format_exc())
        sys.exit(1)

    finally:
        close_without_except(dbd)
        close_without_except(dba)
        close_without_except(dbs)
        close_without_except(dbp)
        close_without_except(dbo)
        close_without_except(dbf)
        close_without_except(dbe)
        close_without_except(dbi)


def scripts_files(typ, bases, force):
    dirsrc = join(typ, 'src')
    dirtex = join(typ, 'tex')
    dirpdf = join(typ, 'pdf')

    makedirs(dirsrc, exist_ok=True)
    makedirs(dirtex, exist_ok=True)
    if typ == FIGURES:
        makedirs(dirpdf, exist_ok=True)

    if force:
        emptyfolder(dirtex)
        if typ == FIGURES:
            emptyfolder(dirpdf)
            emptyfolder(join(WORK, dirpdf))

    lst = []
    for name in sorted(listdir(dirsrc)):
        base = name[:-3]
        if name.endswith('.py') and base in bases:
            src = join(dirsrc, name)
            t = gettime(src)
            texs, tt = filefinder(dirtex, base, '.tex')
            make = t > tt
            if typ == FIGURES:
                pdfs, tp = filefinder(dirpdf, base, '.pdf')
                make = make or t > tp
            else:
                pdfs = []
            if make:
                for f in texs + pdfs:
                    remove(f)
                lst.append((src, base))

    return lst, dirsrc, dirtex, dirpdf


def scripts(force=False):
    bases = pyfiles(texlister('bd'), ret='pybase', dic=True)
    tabs = scripts_files(TABLES, bases.get(TABLES, []), force)
    figs = scripts_files(FIGURES, bases.get(FIGURES, []), force)
    return (tabs, figs) if tabs[0] or figs[0] else False


# ---------------------------------------------------------------------------- #


def pyfiles(files, ret, dic):  # ret='orig|py|pybase|base', dic=True|False
    dat = {}
    for f in files:
        if f.count('/') == 2:
            typ, d, name = f.split('/')
            base, e = splitext(name)
            pybase = base
            while True:
                py = '{}/src/{}.py'.format(typ, pybase)
                if isfile(py):
                    dat.setdefault(dic and typ, set()).add(
                        f if ret == 'orig' else
                        py if ret == 'py' else
                        pybase if ret == 'pybase' else
                        base
                    )
                    break
                pybase = '_'.join(pybase.split('_')[:-1])
                if not pybase:
                    break
    return dat if dic else dat.get(dic, set())


def inexistentes():
    lst = set(texlister('d'))
    lst1 = pyfiles(lst, ret='orig', dic=False)
    lst2 = lst.difference(lst1)
    return lst1, lst2


def print_inexistentes(lst1, lst2):
    for f in sorted(lst1):
        print_info(f)
    for f in sorted(lst2):
        print_error(f)


def figuras_inexistentes():
    texs = set(n[:-4] for n in listdir(join(FIGURES, 'tex')) if n.endswith('.tex'))
    pdfs = set(n[:-4] for n in listdir(join(FIGURES, 'pdf')) if n.endswith('.pdf'))
    diff = [join(FIGURES, 'err', base) for base in texs.symmetric_difference(pdfs)]
    pys = pyfiles(diff, ret='py', dic=False)
    for py in pys:
        tosystem("touch -m '{}'".format(py))
    return pys


# ---------------------------------------------------------------------------- #


def main():
    parser = ArgumentParser(add_help=False)
    option = (
        ('-h', 0, 'Muestra esta ayuda',                                         'store_true', False),
        ('-b', 0, 'Crea una copia de seguridad',                                'store_true', False),
        ('-f', 0, 'Busca el texto suministrado en *.(py|tex|bib|sty)',          'store',         ''),
        ('-F', 0, 'Busca el texto suministrado en todos los archivos',          'store',         ''),
        ('-t', 0, 'Busca los TODO, FIXME y XXX en *.(py|tex|bib|sty)',          'store_true', False),
        ('-T', 0, 'Busca los TODO, FIXME y XXX en todos los archivos',          'store_true', False),
        ('-E', 0, 'Busca errores típicos de tipeo',                             'store_true', False),
        ('-e', 0, 'Busca archivos inexistentes',                                'store_true', False),
        ('-D', 0, 'Busca archivos incluidos mas de 1 vez',                      'store_true', False),
        ('-r', 0, 'Resetea la carpeta de Testing',                              'store_true', False),
        ('-w', 0, 'Muestra acciones pendientes (en -e y -s)',                   'store_true', False),
        ('-v', 0, 'Compilar presentación',                                      'store_true', False),
        ('-ø', 0, 'Compilar cubierta simple',                                   'store_true', False),
        #
        ('-l', 1, 'Limpiar: t=LaTeX, p=Python',                                 'store',         ''),
        ('-L', 1, 'Equivale a \033[1m-l lp\033[0m',                             'store_true', False),
        ('-p', 1, 'Convertir archivos python a LaTeX',                          'store_true', False),
        ('-q', 1, 'Buscar compuestos químicos',                                 'store_true', False),
        ('-s', 1, 'Procesar los cuadros y figuras',                             'store_true', False),
        ('-S', 1, 'Forzar el procesado de los cuadros y figuras',               'store_true', False),
        ('-ß', 1, '* Procesar las figuras sin compilarlas',                     'store_true', False),
        ('-ł', 1, '* Compila las figuras con la tesis completa (con -s o -S)',  'store_true', False),
        ('-c', 1, 'Compila 1 ciclo (pdflatex, biber, makeglossaries)',          'store_true', False),
        ('-C', 1, 'Compila 3 ciclos (pdflatex, biber, makeglossaries)',         'store_true', False),
        ('-j', 1, '* Compila 1 ciclo (biber, makeglossaries, pdflatex)',        'store_true', False),
        ('-z', 1, 'Ejecuta \033[1mpdflatex\033[0m',                             'store_true', False),
        ('-Z', 1, 'Ejecuta \033[1mpdflatex\033[0m, sin \033[1m-s\033[0m',       'store_true', False),
        ('-m', 1, 'Mostrar pdf',                                                'store_true', False),
        ('-x', 1, 'Equivale a \033[1m-sqpcm\033[0m',                            'store_true', False),
        ('-X', 1, 'Equivale a \033[1m-LSpqCm\033[0m',                           'store_true', False),
    )
    for flag, group, help, action, default in option:
        deco = '\033[1;35m□\033[0m ' if group else '\033[1;32m●\033[0m '
        parser.add_argument(flag, help=deco+help, action=action, default=default)
    args = parser.parse_args()

    if args.h:
        parser.print_help()

    elif args.b:
        basic_clear()
        print_action("Creando copia de seguridad")
        chdir(dirname(ROOT))
        makedirs(BACKUP, exist_ok=True)
        exc = ' '.join('--exclude="{}"'.format(e) for e in (WORK, "__pycache__", ".ipython", ".git"))
        cmd = (
            'FILE="{0}/{1}.$(date +%Y%m%d.%H%M).tar.gz" && '
            'tar {2} -czf "$FILE" "{1}" && '
            'echo "  $(du --si "$FILE")"'
        ).format(BACKUP, basename(ROOT), exc)
        tosystem(cmd)
        tosystem('sync')
        chdir(ROOT)

    elif args.f or args.F or args.t or args.T or args.D:
        grep = (
            'grep '
            '--color '
            '--recursive '                      # -r -R
            '--line-number '                    # -n
            '--initial-tab '                    # -T
            '--extended-regexp '                # -E
            '--binary-files=without-match '     # -I
            '--exclude={{Main.run,py*.tex,{},*~,{}}} '
            '--exclude-dir={{Informes,Pruebas,tex,pdf,.ipython,{}}} '
            '{}"{}"'
        )
        if args.D:
            print_action('Buscando input o include duplicados')
            texins = [f[1:] for f in texlister('a')]
            for t in set(texins):
                if texins.count(t) > 1:
                    print_error(t)
                    cmd = grep.format(NAME, '{}.dep'.format(THSS), WORK, '', t)
                    tosystem(cmd)
        else:
            print_action('Buscando')
            xml = 'Xml.py,' if args.t else ''
            include = '' if args.F or args.T else '--include=*.{py,tex,bib,sty} '
            pattern = args.f or args.F or r'\\b(TODO|FIXME|XXX)\\b'
            cmd = grep.format(NAME, xml, '', include, pattern)
            tosystem(cmd.replace(",}", "}"))

    elif args.E:
        print_action('Buscando errores de tipeo')
        busca_errores_tipeo(texlister('b'))

    elif args.e:
        print_action('Buscando inexistentes')
        print_inexistentes(*inexistentes())

    elif args.r:
        print_action('Reseteando de Testing')
        tosystem('../Testing/make -i')

    elif args.w:
        set_build_dir()
        tabsfigs = scripts()
        if tabsfigs:
            print_action('Script a ejecutar')
            for f in tabsfigs[0][0] + tabsfigs[1][0]:
                print_info(f[0])

        lst1, lst2 = inexistentes()
        if lst1 or lst2:
            print_action('Archvios inexistentes')
            print_inexistentes(lst1, lst2)

    elif args.v:
        print_action('Compilando presentación')
        cmd = PRST_LOG(("pdflatex "
            "-halt-on-error "
            "-file-line-error "
            "-interaction batchmode "
            "-output-directory '{}' "
            "'{}'").format(WORK, PRST))
        tosystem(cmd)
        tosystem(cmd)
        tosystem(gen_grep("pdflatex", prst=True))

        cmd = "rm -f '{2}.{3}' && {0} -f '{1}/{2}.{3}' '{2}.{3}'".format
        tosystem(cmd('mv', WORK, PRST, 'pdf'))
        tosystem(cmd('cp', WORK, PRST, 'dep'))

    elif args.ø:
        print_action('Compilando cubierta')
        work = 'Cubierta'
        tex = LOG(("pdflatex "
            "-halt-on-error "
            "-file-line-error "
            "-interaction batchmode "
            "-output-directory '{}' "
            "'{}'").format(WORK, work))
        tosystem(tex)
        tosystem(gen_grep("pdflatex"))
        cmd = "rm -f '{2}.{3}' && {0} -f '{1}/{2}.{3}' '{2}.{3}'".format
        tosystem(cmd('mv', WORK, work, 'pdf'))

    else:
        if args.ł and not (args.s or args.S):
            print_advertencia('El parámetro ł no se usará, debe ir con -s o -S')

        if args.x:
            args.p = args.q = args.s = args.c = args.m = True
        elif args.X:
            args.L = args.p = args.S = args.q = args.C = args.m = True

        if args.l or args.L:
            print_action('Borrando archivos')
            l = 'tp' if args.L else args.l
            clear('t' in l, 'p' in l)
        elif args.Z or args.x:
            basic_clear()

        if args.p:
            print_action('Convirtiendo archivos Python a LaTeX')
            set_build_dir()
            py2tex()

        if args.q:
            print_action('Buscando compuestos químicos')
            compuestos(texlister('b'))

        if args.s or args.S or args.ß or args.c or args.C or args.j or args.z:
            set_build_dir()
            def run_scripts():
                tabsfigs = scripts(args.S)
                if tabsfigs or args.s or args.S or args.ß:
                    print_action('Ejecutando scripts')
                    if tabsfigs:
                        scripts_makes(*tabsfigs, nobuild=args.ß, full=args.ł)
            run_scripts()
            if not args.ß and figuras_inexistentes():
                print_advertencia('Hay imágenes sin generar (touch and re-run scripts)')
                run_scripts()
                faltan = figuras_inexistentes()
                if faltan:
                    print_action_error('Los siguientes scripts no generan todas las imágenes necesarias')
                    for f in faltan:
                        print_error(f)
                    return

        if args.c or args.C or args.j or args.z or args.Z:
            print_action('Compilando tesina')
            if args.ß:
                print_action_error("Ejecutar 'ß' con 'cCzZ' puede generar errores")
                return
            if not args.Z:
                lst = inexistentes()
                if lst[0] or lst[1]:
                    print_action_error('archivos problemáticos')
                    print_inexistentes(*lst)
                    return

            tex = LOG(("pdflatex "
                "-halt-on-error "
                "-file-line-error "
                "-interaction batchmode "
                "-output-directory '{}' "
                "'{}'").format(WORK, THSS))

            bib = LOG(("biber "
                "--output_directory '{}' "
                "'{}'").format(WORK, THSS))

            glo = LOG(("makeglossaries "
                "-d '{}' '{}'").format(WORK, THSS))

            if args.z or args.Z:
                cmds = [tex]
                nt = len(cmds)
                grep = [True]
            elif args.j:
                cmds = [bib, glo, tex]
                nt = len(cmds)
                grep = [True] * nt
            else:
                n1 = 3 if args.C else 1
                n2 = 2 if n1 > 1 else 1
                cmds = [tex, bib, glo] * n1 + [tex] * n2
                nt = len(cmds)
                grep = [False] * nt
                offset = 0 if n1 == 1 else 1
                grep[-3-offset] = True
                grep[-2-offset] = True
                grep[-1] = True

            d = len(str(nt))
            for i, cmd in enumerate(cmds):
                name = cmd.split(' ', 1)[0]
                print_data('{:{d}}/{} {} {}'.format(i+1, nt, GUION, name, d=d))
                tosystem(cmd)
                if grep[i]:
                    tosystem(gen_grep(name))

            cmd = "rm -f '{2}.{3}' && {0} -f '{1}/{2}.{3}' '{2}.{3}'".format
            tosystem(cmd('mv', WORK, THSS, 'pdf'))
            tosystem(cmd('cp', WORK, THSS, 'dep'))

        if args.m:
            pdf = '{}.pdf'.format(THSS)
            print_action('Abriendo {}'.format(pdf))
            if isfile(pdf):
                tosystem("xdg-open '{}' 2>/dev/null&".format(pdf))
            else:
                print_error('No existe el fichero: {}'.format(pdf))

        if len(sys.argv) == 1:  # sin argumentos
            basic_clear()


if __name__ == '__main__':
    chdir(ROOT)
    exit_code = 0
    try:
        save(RUNLOG, str(INICIO))
        main()
    except (SystemExit, ToSystemException):
        exit_code = 1
    finally:
        print_action(datetime.datetime.now() - INICIO)
        sys.exit(exit_code)
